-- Magento DB backup
--
-- Host: localhost    Database: ma_sahara
-- ------------------------------------------------------
-- Server version: 10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_passwords`
--

DROP TABLE IF EXISTS `admin_passwords`;
CREATE TABLE `admin_passwords` (
  `password_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Password Id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User Id',
  `password_hash` varchar(100) DEFAULT NULL COMMENT 'Password Hash',
  `expires` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Expires',
  `last_updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Last Updated',
  PRIMARY KEY (`password_id`),
  KEY `ADMIN_PASSWORDS_USER_ID` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin Passwords';

--
-- Table structure for table `admin_system_messages`
--

DROP TABLE IF EXISTS `admin_system_messages`;
CREATE TABLE `admin_system_messages` (
  `identity` varchar(100) NOT NULL COMMENT 'Message id',
  `severity` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Problem type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`identity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin System Messages';

--
-- Dumping data for table `admin_system_messages`
--

LOCK TABLES `admin_system_messages` WRITE;
/*!40000 ALTER TABLE `admin_system_messages` DISABLE KEYS */;
INSERT INTO `admin_system_messages` VALUES ('da332d712f3215b9b94bfa268c398323',2,'2016-11-05 03:06:21');
/*!40000 ALTER TABLE `admin_system_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user`
--

DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `firstname` varchar(32) DEFAULT NULL COMMENT 'User First Name',
  `lastname` varchar(32) DEFAULT NULL COMMENT 'User Last Name',
  `email` varchar(128) DEFAULT NULL COMMENT 'User Email',
  `username` varchar(40) DEFAULT NULL COMMENT 'User Login',
  `password` varchar(255) NOT NULL COMMENT 'User Password',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'User Created Time',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'User Modified Time',
  `logdate` timestamp NULL DEFAULT NULL COMMENT 'User Last Login Time',
  `lognum` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'User Login Number',
  `reload_acl_flag` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Reload ACL',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'User Is Active',
  `extra` text COMMENT 'User Extra Data',
  `rp_token` text COMMENT 'Reset Password Link Token',
  `rp_token_created_at` timestamp NULL DEFAULT NULL COMMENT 'Reset Password Link Token Creation Date',
  `interface_locale` varchar(16) NOT NULL DEFAULT 'en_US' COMMENT 'Backend interface locale',
  `failures_num` smallint(6) DEFAULT '0' COMMENT 'Failure Number',
  `first_failure` timestamp NULL DEFAULT NULL COMMENT 'First Failure',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Expiration Lock Dates',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ADMIN_USER_USERNAME` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Admin User Table';

--
-- Dumping data for table `admin_user`
--

LOCK TABLES `admin_user` WRITE;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
INSERT INTO `admin_user` VALUES ('1','admin','admin','admin@gmail.com','admin','e14b129944ba2ef792610024beb93891b588e997906b51cd9b5d06b07629b75a:6yQUDJKTItZlyuZJMwwT0DA3IC8det8Q:1','2016-10-06 02:19:37','2016-11-29 01:21:03','2016-11-29 01:21:03',78,0,1,'a:1:{s:11:\"configState\";a:58:{s:16:\"currency_options\";s:1:\"1\";s:21:\"currency_yahoofinance\";s:1:\"0\";s:16:\"currency_fixerio\";s:1:\"0\";s:20:\"currency_webservicex\";s:1:\"0\";s:15:\"currency_import\";s:1:\"0\";s:7:\"web_url\";s:1:\"1\";s:7:\"web_seo\";s:1:\"1\";s:12:\"web_unsecure\";s:1:\"0\";s:10:\"web_secure\";s:1:\"0\";s:11:\"web_default\";s:1:\"1\";s:10:\"web_cookie\";s:1:\"0\";s:24:\"web_browser_capabilities\";s:1:\"0\";s:13:\"contact_email\";s:1:\"1\";s:15:\"contact_contact\";s:1:\"1\";s:15:\"system_currency\";s:1:\"0\";s:22:\"system_full_page_cache\";s:1:\"0\";s:11:\"system_smtp\";s:1:\"0\";s:11:\"system_cron\";s:1:\"0\";s:15:\"general_country\";s:1:\"1\";s:14:\"general_region\";s:1:\"1\";s:14:\"general_locale\";s:1:\"1\";s:25:\"general_store_information\";s:1:\"1\";s:25:\"general_single_store_mode\";s:1:\"1\";s:26:\"plazathemes_design_general\";s:1:\"0\";s:25:\"plazathemes_design_header\";s:1:\"0\";s:25:\"plazathemes_design_footer\";s:1:\"0\";s:21:\"producttab_new_status\";s:1:\"1\";s:22:\"categorytab_new_status\";s:1:\"1\";s:19:\"brandslider_general\";s:1:\"1\";s:17:\"quickview_general\";s:1:\"1\";s:13:\"catalog_price\";s:1:\"1\";s:21:\"catalog_product_video\";s:1:\"1\";s:20:\"catalog_fields_masks\";s:1:\"0\";s:14:\"catalog_review\";s:1:\"0\";s:16:\"catalog_frontend\";s:1:\"1\";s:20:\"catalog_productalert\";s:1:\"1\";s:19:\"catalog_placeholder\";s:1:\"1\";s:25:\"catalog_productalert_cron\";s:1:\"1\";s:25:\"catalog_recently_products\";s:1:\"1\";s:26:\"catalog_layered_navigation\";s:1:\"1\";s:14:\"catalog_search\";s:1:\"1\";s:11:\"catalog_seo\";s:1:\"1\";s:18:\"catalog_navigation\";s:1:\"1\";s:20:\"catalog_downloadable\";s:1:\"1\";s:22:\"catalog_custom_options\";s:1:\"0\";s:28:\"saleproductslider_new_status\";s:1:\"1\";s:13:\"system_backup\";s:1:\"0\";s:34:\"system_media_storage_configuration\";s:1:\"0\";s:24:\"system_adminnotification\";s:1:\"0\";s:22:\"pricecountdown_general\";s:1:\"0\";s:31:\"pricecountdown_countdown_slider\";s:1:\"1\";s:30:\"pricecountdown_in_product_list\";s:1:\"0\";s:30:\"pricecountdown_in_product_view\";s:1:\"0\";s:27:\"newproductslider_new_status\";s:1:\"1\";s:11:\"web_session\";s:1:\"0\";s:27:\"recentproductslider_general\";s:1:\"1\";s:14:\"mfblog_general\";s:1:\"1\";s:27:\"newsletterpopup_popup_group\";s:1:\"1\";}}',NULL,NULL,'en_US',0,NULL,NULL);
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user_session`
--

DROP TABLE IF EXISTS `admin_user_session`;
CREATE TABLE `admin_user_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `session_id` varchar(128) NOT NULL COMMENT 'Session id value',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT 'Admin User ID',
  `status` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Current Session status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `ip` varchar(15) NOT NULL COMMENT 'Remote user IP',
  PRIMARY KEY (`id`),
  KEY `ADMIN_USER_SESSION_SESSION_ID` (`session_id`),
  KEY `ADMIN_USER_SESSION_USER_ID` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COMMENT='Admin User sessions table';

--
-- Dumping data for table `admin_user_session`
--

LOCK TABLES `admin_user_session` WRITE;
/*!40000 ALTER TABLE `admin_user_session` DISABLE KEYS */;
INSERT INTO `admin_user_session` VALUES ('1','jvrcutds08622btgp7ts9l8je0','1',1,'2016-10-06 02:58:26','2016-10-06 04:17:41','::1'),('2','rjh5fipu9li13i4a1hklqvlrb7','1',1,'2016-10-06 06:25:32','2016-10-06 06:29:53','::1'),('3','mofl7ek7qvsfee152hc4boj1j6','1',1,'2016-10-06 07:23:29','2016-10-06 09:17:57','::1'),('4','e08ufl6hbr858q7tdpe5pkbmt7','1',1,'2016-10-06 09:34:52','2016-10-06 10:12:55','::1'),('5','ur3g71jgefvbdhigh2opchhb82','1',1,'2016-10-07 03:00:50','2016-10-07 03:12:47','::1'),('6','nh15nr22pnfadm7eup9om8nrq0','1',1,'2016-10-07 07:09:21','2016-10-07 07:26:57','::1'),('7','fs63fscor3mjqu3m391agsito0','1',1,'2016-10-07 08:07:50','2016-10-07 08:55:41','::1'),('8','jvu6ftus8s3k52i25gcf2pfdh5','1',1,'2016-10-07 09:21:32','2016-10-07 09:48:58','::1'),('9','3kbmhc7k2r94n2e38hneq4be95','1',1,'2016-10-08 01:45:44','2016-10-08 02:35:04','::1'),('10','96vigppdoa4c7bpogrgcs0tgd7','1',1,'2016-10-08 04:07:14','2016-10-08 05:01:15','::1'),('11','g620f865cuud63sl8p5hhovlb5','1',1,'2016-10-10 01:35:46','2016-10-10 02:34:13','::1'),('12','bihtu86m7qrerg12q27ie6u4u3','1',1,'2016-10-10 03:22:40','2016-10-10 04:07:36','::1'),('13','5etjlhiteodtnu43hjadar1p47','1',1,'2016-10-10 06:23:42','2016-10-10 06:34:20','::1'),('14','f8oh94vuulgnejgmhduv8gnue0','1',1,'2016-10-10 07:44:46','2016-10-10 08:03:25','::1'),('15','8qd5tf2q46hm6g1tuj4nv6bl31','1',1,'2016-10-10 09:04:54','2016-10-10 09:37:40','::1'),('16','uhlgsrub09v1ti7ruocvvf23j1','1',1,'2016-10-10 10:24:25','2016-10-10 10:30:50','::1'),('17','5vpepct1nt1dafr1jrch6koil4','1',1,'2016-10-11 01:21:24','2016-10-11 01:56:43','::1'),('18','57a4t6j0iosmug3cdmk5j923g2','1',1,'2016-10-11 03:11:41','2016-10-11 03:28:11','::1'),('19','cuu5ifj4m46ruueppl9qtjkae7','1',1,'2016-10-11 03:46:37','2016-10-11 04:05:01','::1'),('20','h2dau13cc3ord2tbujdcmv60k0','1',1,'2016-10-11 06:59:11','2016-10-11 07:25:12','::1'),('21','kkm2tdm9ig8l34q6u31eauc405','1',1,'2016-10-11 07:58:34','2016-10-11 08:41:02','::1'),('22','993u12uegkdbtt1p8c8m7ct6l7','1',1,'2016-10-11 09:24:40','2016-10-11 10:34:38','::1'),('23','sjrnk1cebasgr3kehkqj6egro4','1',1,'2016-10-12 01:30:25','2016-10-12 03:47:14','::1'),('24','di4ga5tmmogp3hd53ppaa08435','1',1,'2016-10-12 04:11:46','2016-10-12 04:11:52','::1'),('25','6r6vis331a6q6iv3lpfg31chj5','1',1,'2016-10-12 04:35:28','2016-10-12 04:41:31','::1'),('26','db0fm6c29c08956vihjbjbgp64','1',1,'2016-10-12 06:43:17','2016-10-12 08:06:29','::1'),('27','i60do59gt01lf5a5oag5fmsl02','1',1,'2016-10-12 08:26:20','2016-10-12 09:57:20','::1'),('28','uq4uorj0gkvlrvti00ofbev054','1',1,'2016-10-13 09:09:35','2016-10-13 09:18:44','::1'),('29','fa8e9r72ubrumq1n32ur231cn7','1',1,'2016-10-14 09:41:55','2016-10-14 09:59:09','::1'),('30','pscp0gtqr7lum44aan0nt92lc6','1',1,'2016-10-15 03:56:01','2016-10-15 04:20:35','::1'),('31','1q2cug8j42m82f92lsc7s9fn14','1',1,'2016-10-17 01:32:42','2016-10-17 01:42:48','::1'),('32','0fce5s5hrg8nt7mcs837n46fb7','1',1,'2016-10-17 02:19:29','2016-10-17 03:18:05','::1'),('33','jgotc0t6o457rjp7ismgv88di5','1',1,'2016-10-17 06:35:58','2016-10-17 06:59:51','::1'),('34','6r20jbl8n0l9r0s7amen6757t2','1',1,'2016-10-17 07:35:29','2016-10-17 08:17:52','::1'),('35','tf3fs977a9hs2s09982c4nvu03','1',1,'2016-10-17 09:04:49','2016-10-17 09:39:05','::1'),('36','hc5sm00dtsadl9k775dt00nf36','1',2,'2016-10-18 01:27:53','2016-10-18 01:28:27','::1'),('37','4dt4sd4j7l2skutlba71fceec2','1',1,'2016-10-18 01:28:27','2016-10-18 02:15:01','::1'),('38','pr5d2102q45thkbsgql069i3g6','1',1,'2016-10-18 09:33:01','2016-10-18 09:37:20','::1'),('39','ue4v0akrrok3kgnedkri51q8u4','1',1,'2016-10-19 07:12:39','2016-10-19 07:36:00','192.168.1.93'),('40','hndqge2757iadjd0pp1a2jjtc0','1',1,'2016-11-05 02:45:52','2016-11-05 03:10:06','192.168.1.99'),('41','iloqghffgmqvvv4uj2rs55bpu3','1',2,'2016-11-05 03:44:58','2016-11-05 04:06:31','192.168.1.99'),('42','dhrk1amc6guehh5ugv6gp6feo3','1',1,'2016-11-05 04:06:31','2016-11-05 04:08:52','192.168.1.99'),('43','m3mvv1s82imj0l80ksscr80ot5','1',1,'2016-11-08 06:49:40','2016-11-08 06:55:25','192.168.1.99'),('44','noi0v2tqh70fndn4ie35f33fd3','1',1,'2016-11-12 04:28:44','2016-11-12 04:39:48','192.168.1.99'),('45','hq881rm5dj7ke94qkpvhscf3v5','1',1,'2016-11-15 08:28:47','2016-11-15 08:47:51','192.168.1.99'),('46','sv2vnuf1v1qlhksko7fg24voe2','1',1,'2016-11-16 01:40:55','2016-11-16 01:45:44','192.168.1.99'),('47','51q4k70d5altnvkud3jtcepul3','1',1,'2016-11-16 02:06:19','2016-11-16 02:13:09','192.168.1.99'),('48','d70piouon8f91meotlf03b9786','1',1,'2016-11-16 06:45:00','2016-11-16 06:46:45','192.168.1.99'),('49','keohhufqrbbaepk7jelvp73ac1','1',2,'2016-11-17 07:31:27','2016-11-17 07:36:25','192.168.1.2'),('50','cga1ql33ph4bciklvsq1ec0uo4','1',1,'2016-11-17 07:36:25','2016-11-17 07:39:21','192.168.1.99'),('51','456b3r9cd3h011srbi8pt58kc0','1',1,'2016-11-17 10:25:31','2016-11-17 10:33:14','192.168.1.99'),('52','djeca3bgfv8ibch29fcs8ba8i3','1',1,'2016-11-23 01:12:48','2016-11-23 01:15:37','192.168.1.99'),('53','1dmjsbfb8pbk75q3cpjv473c20','1',1,'2016-11-23 08:30:36','2016-11-23 08:52:35','192.168.1.99'),('54','kvio6t3mh574jmhnmebsjki1t7','1',2,'2016-11-23 09:54:13','2016-11-23 09:54:17','192.168.1.99'),('55','j19jc5nho8g4r841mcmcp5lqj7','1',1,'2016-11-23 09:54:17','2016-11-23 10:27:16','192.168.1.99'),('56','bt738tv78ur6mnandbqaukc6j5','1',1,'2016-11-23 15:03:39','2016-11-23 15:11:44','::1'),('57','v401doonctpl8vbsl33ob2upk2','1',1,'2016-11-23 15:33:41','2016-11-23 17:13:30','::1'),('58','qfb8rqs7dp4qs1uib5p1vfgjv1','1',1,'2016-11-24 02:22:50','2016-11-24 02:42:51','::1'),('59','n94qjqumia3ujoao6thkfp4jg5','1',1,'2016-11-24 03:39:21','2016-11-24 04:09:41','::1'),('60','sfv3o5lubmfkqf9s4nmm3k0901','1',1,'2016-11-24 04:25:45','2016-11-24 04:27:20','::1'),('61','srftqi7qibia0q8i9gaam3srr6','1',1,'2016-11-24 06:18:00','2016-11-24 06:20:44','::1'),('62','f56vurlgsq2kuo2s01a1o21092','1',1,'2016-11-24 08:11:05','2016-11-24 08:18:32','::1'),('63','10qgeo8vf2fjkbfjnohlofhmd5','1',1,'2016-11-24 09:19:05','2016-11-24 09:27:08','192.168.1.99'),('64','3njt9nuu3a250ti05s4q3irjg1','1',1,'2016-11-24 09:50:07','2016-11-24 10:11:21','192.168.1.99'),('65','flfh0ik503j9ksgn5sva434v94','1',1,'2016-11-25 01:42:32','2016-11-25 02:57:22','192.168.1.99'),('66','8i15pvur7qh8fa10iuahe4g6e6','1',1,'2016-11-25 03:34:31','2016-11-25 03:42:17','192.168.1.99'),('67','1acve23cgpjjo4qajqkvftnt67','1',1,'2016-11-25 04:03:35','2016-11-25 04:05:54','192.168.1.99'),('68','5p53a5iejr29oe0dn7etivlbb5','1',1,'2016-11-25 09:10:10','2016-11-25 09:12:35','192.168.1.99'),('69','btpklsqeq97t6t272i70f1hnh4','1',1,'2016-11-26 04:11:39','2016-11-26 04:33:14','192.168.1.99'),('70','bpgq8ontsuc0d2vf33jo4iffj1','1',1,'2016-11-26 04:50:56','2016-11-26 05:06:35','192.168.1.99'),('71','a41veggo7iqfeud67f9hook5u3','1',1,'2016-11-27 14:27:47','2016-11-27 15:46:42','::1'),('72','tl68fgb9h80h0hof5ljspql216','1',1,'2016-11-28 01:48:25','2016-11-28 01:52:17','192.168.1.99'),('73','p9c6pv4vnh4gkss0tuoshvu354','1',1,'2016-11-28 02:36:45','2016-11-28 02:39:21','192.168.1.2'),('74','h6odflv4e7sbut772fjk6ldjp4','1',1,'2016-11-28 03:53:36','2016-11-28 04:14:43','192.168.1.99'),('75','t2aqs0ob0qbq087mebclo5f632','1',1,'2016-11-28 05:23:14','2016-11-28 05:29:57','192.168.1.99'),('76','k0ff7cm51cfb71gv2otm6fiph5','1',1,'2016-11-28 06:01:59','2016-11-28 06:46:11','192.168.1.99'),('77','60cr677r0vkf5j72iotd648vs7','1',1,'2016-11-28 09:19:37','2016-11-28 09:26:11','192.168.1.99'),('78','q22lte60fr6cvn20089t683pt2','1',1,'2016-11-29 01:21:05','2016-11-29 01:22:29','192.168.1.99');
/*!40000 ALTER TABLE `admin_user_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adminnotification_inbox`
--

DROP TABLE IF EXISTS `adminnotification_inbox`;
CREATE TABLE `adminnotification_inbox` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Notification id',
  `severity` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Problem type',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `description` text COMMENT 'Description',
  `url` varchar(255) DEFAULT NULL COMMENT 'Url',
  `is_read` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if notification read',
  `is_remove` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if notification might be removed',
  PRIMARY KEY (`notification_id`),
  KEY `ADMINNOTIFICATION_INBOX_SEVERITY` (`severity`),
  KEY `ADMINNOTIFICATION_INBOX_IS_READ` (`is_read`),
  KEY `ADMINNOTIFICATION_INBOX_IS_REMOVE` (`is_remove`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Adminnotification Inbox';

--
-- Dumping data for table `adminnotification_inbox`
--

LOCK TABLES `adminnotification_inbox` WRITE;
/*!40000 ALTER TABLE `adminnotification_inbox` DISABLE KEYS */;
INSERT INTO `adminnotification_inbox` VALUES ('1',1,'2016-10-12 20:27:35','Community Edition 2.0.10 and 2.1.2 Resolve Critical Security Issues – 10/12/2016','Magento Community Edition 2.0.10 and 2.1.2 resolve multiple security issues, including critical vulnerabilities with certain payment methods and Zend Framework libraries. The releases also make several functional improvements and API enhancements. Learn more about the security issues in the Magento Security Center at https://magento.com/security/patches/magento-2010-and-212-security-update and functional updates at http://devdocs.magento.com/guides/v2.1/release-notes/bk-release-notes.html.','https://magento.com/security/patches/magento-2010-and-212-security-update',0,0),('2',1,'2016-10-25 16:53:36','Dirty COW Linux OS Vulnerability – 10/25/2016','Dirty COW (CVE-2016-5195) is a serious vulnerability affecting most Linux Operating Systems that can be exploited to gain root access to the server. More information on how to immediately update your Linux OS is available in the Magento Security Center at https://magento.com/security/vulnerabilities/new-linux-operating-system-vulnerability. ','https://magento.com/security/vulnerabilities/new-linux-operating-system-vulnerability',0,0);
/*!40000 ALTER TABLE `adminnotification_inbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorization_role`
--

DROP TABLE IF EXISTS `authorization_role`;
CREATE TABLE `authorization_role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Role ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Role ID',
  `tree_level` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role Tree Level',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role Sort Order',
  `role_type` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Role Type',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID',
  `user_type` varchar(16) DEFAULT NULL COMMENT 'User Type',
  `role_name` varchar(50) DEFAULT NULL COMMENT 'Role Name',
  PRIMARY KEY (`role_id`),
  KEY `AUTHORIZATION_ROLE_PARENT_ID_SORT_ORDER` (`parent_id`,`sort_order`),
  KEY `AUTHORIZATION_ROLE_TREE_LEVEL` (`tree_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Admin Role Table';

--
-- Dumping data for table `authorization_role`
--

LOCK TABLES `authorization_role` WRITE;
/*!40000 ALTER TABLE `authorization_role` DISABLE KEYS */;
INSERT INTO `authorization_role` VALUES ('1','0',1,1,'G','0','2','Administrators'),('2','1',2,0,'U','1','2','admin');
/*!40000 ALTER TABLE `authorization_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorization_rule`
--

DROP TABLE IF EXISTS `authorization_rule`;
CREATE TABLE `authorization_rule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule ID',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Role ID',
  `resource_id` varchar(255) DEFAULT NULL COMMENT 'Resource ID',
  `privileges` varchar(20) DEFAULT NULL COMMENT 'Privileges',
  `permission` varchar(10) DEFAULT NULL COMMENT 'Permission',
  PRIMARY KEY (`rule_id`),
  KEY `AUTHORIZATION_RULE_RESOURCE_ID_ROLE_ID` (`resource_id`,`role_id`),
  KEY `AUTHORIZATION_RULE_ROLE_ID_RESOURCE_ID` (`role_id`,`resource_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Admin Rule Table';

--
-- Dumping data for table `authorization_rule`
--

LOCK TABLES `authorization_rule` WRITE;
/*!40000 ALTER TABLE `authorization_rule` DISABLE KEYS */;
INSERT INTO `authorization_rule` VALUES ('1','1','Magento_Backend::all',NULL,'allow');
/*!40000 ALTER TABLE `authorization_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
CREATE TABLE `cache` (
  `id` varchar(200) NOT NULL COMMENT 'Cache Id',
  `data` mediumblob COMMENT 'Cache Data',
  `create_time` int(11) DEFAULT NULL COMMENT 'Cache Creation Time',
  `update_time` int(11) DEFAULT NULL COMMENT 'Time of Cache Updating',
  `expire_time` int(11) DEFAULT NULL COMMENT 'Cache Expiration Time',
  PRIMARY KEY (`id`),
  KEY `CACHE_EXPIRE_TIME` (`expire_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Caches';

--
-- Table structure for table `cache_tag`
--

DROP TABLE IF EXISTS `cache_tag`;
CREATE TABLE `cache_tag` (
  `tag` varchar(100) NOT NULL COMMENT 'Tag',
  `cache_id` varchar(200) NOT NULL COMMENT 'Cache Id',
  PRIMARY KEY (`tag`,`cache_id`),
  KEY `CACHE_TAG_CACHE_ID` (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag Caches';

--
-- Table structure for table `captcha_log`
--

DROP TABLE IF EXISTS `captcha_log`;
CREATE TABLE `captcha_log` (
  `type` varchar(32) NOT NULL COMMENT 'Type',
  `value` varchar(32) NOT NULL COMMENT 'Value',
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Count',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Update Time',
  PRIMARY KEY (`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Count Login Attempts';

--
-- Table structure for table `catalog_category_entity`
--

DROP TABLE IF EXISTS `catalog_category_entity`;
CREATE TABLE `catalog_category_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attriute Set ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Category ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `path` varchar(255) NOT NULL COMMENT 'Tree Path',
  `position` int(11) NOT NULL COMMENT 'Position',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT 'Tree Level',
  `children_count` int(11) NOT NULL COMMENT 'Child Count',
  PRIMARY KEY (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_LEVEL` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Table';

--
-- Dumping data for table `catalog_category_entity`
--

LOCK TABLES `catalog_category_entity` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity` DISABLE KEYS */;
INSERT INTO `catalog_category_entity` VALUES ('1',0,'0','2016-10-06 02:16:37','2016-10-07 08:55:38','1','0','0','29'),('2',3,'1','2016-10-06 02:16:44','2016-10-07 08:55:38','1/2','1','1','28'),('3',3,'2','2016-10-06 07:31:51','2016-10-06 07:31:52','1/2/3','1','2','0'),('4',3,'2','2016-10-06 07:34:56','2016-10-06 07:34:56','1/2/4','2','2','0'),('5',3,'2','2016-10-06 07:37:47','2016-10-07 08:55:38','1/2/5','3','2','21'),('6',3,'2','2016-10-06 07:40:52','2016-10-06 07:40:54','1/2/6','4','2','0'),('7',3,'2','2016-10-06 07:56:27','2016-10-06 07:56:27','1/2/7','5','2','0'),('8',3,'2','2016-10-06 07:59:56','2016-10-06 07:59:57','1/2/8','6','2','0'),('9',3,'2','2016-10-06 08:09:09','2016-10-06 08:09:10','1/2/9','7','2','0'),('10',3,'5','2016-10-07 08:26:45','2016-10-07 08:41:46','1/2/5/10','1','3','6'),('11',3,'5','2016-10-07 08:29:47','2016-10-07 08:48:21','1/2/5/11','2','3','6'),('12',3,'5','2016-10-07 08:32:45','2016-10-07 08:55:38','1/2/5/12','3','3','6'),('13',3,'10','2016-10-07 08:34:09','2016-10-07 08:34:10','1/2/5/10/13','1','4','0'),('14',3,'10','2016-10-07 08:37:18','2016-10-07 08:37:19','1/2/5/10/14','2','4','0'),('15',3,'10','2016-10-07 08:38:32','2016-10-07 08:38:33','1/2/5/10/15','3','4','0'),('16',3,'10','2016-10-07 08:39:31','2016-10-07 08:39:31','1/2/5/10/16','4','4','0'),('17',3,'10','2016-10-07 08:40:43','2016-10-07 08:40:44','1/2/5/10/17','5','4','0'),('18',3,'10','2016-10-07 08:41:46','2016-10-07 08:41:47','1/2/5/10/18','6','4','0'),('19',3,'11','2016-10-07 08:42:30','2016-10-07 08:42:30','1/2/5/11/19','1','4','0'),('20',3,'11','2016-10-07 08:43:33','2016-10-07 08:43:34','1/2/5/11/20','2','4','0'),('21',3,'11','2016-10-07 08:45:08','2016-10-07 08:45:09','1/2/5/11/21','3','4','0'),('22',3,'11','2016-10-07 08:46:04','2016-10-07 08:46:04','1/2/5/11/22','4','4','0'),('23',3,'11','2016-10-07 08:47:01','2016-10-07 08:47:01','1/2/5/11/23','5','4','0'),('24',3,'11','2016-10-07 08:48:21','2016-10-07 08:48:21','1/2/5/11/24','6','4','0'),('25',3,'12','2016-10-07 08:49:15','2016-10-07 08:49:16','1/2/5/12/25','1','4','0'),('26',3,'12','2016-10-07 08:50:11','2016-10-07 08:50:11','1/2/5/12/26','2','4','0'),('27',3,'12','2016-10-07 08:51:01','2016-10-07 08:51:01','1/2/5/12/27','3','4','0'),('28',3,'12','2016-10-07 08:51:51','2016-10-07 08:51:52','1/2/5/12/28','4','4','0'),('29',3,'12','2016-10-07 08:54:31','2016-10-07 08:54:32','1/2/5/12/29','5','4','0'),('30',3,'12','2016-10-07 08:55:38','2016-10-07 08:55:38','1/2/5/12/30','6','4','0');
/*!40000 ALTER TABLE `catalog_category_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_entity_datetime`
--

DROP TABLE IF EXISTS `catalog_category_entity_datetime`;
CREATE TABLE `catalog_category_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Datetime Attribute Backend Table';

--
-- Table structure for table `catalog_category_entity_decimal`
--

DROP TABLE IF EXISTS `catalog_category_entity_decimal`;
CREATE TABLE `catalog_category_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` decimal(12,4) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Decimal Attribute Backend Table';

--
-- Table structure for table `catalog_category_entity_int`
--

DROP TABLE IF EXISTS `catalog_category_entity_int`;
CREATE TABLE `catalog_category_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` int(11) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Integer Attribute Backend Table';

--
-- Dumping data for table `catalog_category_entity_int`
--

LOCK TABLES `catalog_category_entity_int` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity_int` DISABLE KEYS */;
INSERT INTO `catalog_category_entity_int` VALUES ('1',69,0,'1','1'),('2',46,0,'2','1'),('3',69,0,'2','1'),('4',46,0,'3','1'),('5',54,0,'3','1'),('6',69,0,'3','1'),('7',70,0,'3','0'),('8',71,0,'3','0'),('9',136,0,'3','1'),('10',139,0,'3','1'),('11',140,0,'3','1'),('12',46,0,'4','1'),('13',54,0,'4','1'),('14',69,0,'4','1'),('15',70,0,'4','0'),('16',71,0,'4','0'),('17',136,0,'4','1'),('18',139,0,'4','1'),('19',140,0,'4','1'),('20',46,0,'5','1'),('21',54,0,'5','1'),('22',69,0,'5','1'),('23',70,0,'5','0'),('24',71,0,'5','0'),('25',136,0,'5','1'),('26',139,0,'5','1'),('27',140,0,'5','1'),('28',46,0,'6','1'),('29',54,0,'6','1'),('30',69,0,'6','1'),('31',70,0,'6','0'),('32',71,0,'6','0'),('33',136,0,'6','1'),('34',139,0,'6','1'),('35',140,0,'6','1'),('36',46,0,'7','1'),('37',54,0,'7','1'),('38',69,0,'7','1'),('39',70,0,'7','0'),('40',71,0,'7','0'),('41',136,0,'7','1'),('42',139,0,'7','1'),('43',140,0,'7','1'),('44',46,0,'8','1'),('45',54,0,'8','1'),('46',69,0,'8','1'),('47',70,0,'8','0'),('48',71,0,'8','0'),('49',136,0,'8','1'),('50',139,0,'8','1'),('51',140,0,'8','1'),('52',46,0,'9','1'),('53',54,0,'9','1'),('54',69,0,'9','1'),('55',70,0,'9','0'),('56',71,0,'9','0'),('57',136,0,'9','1'),('58',139,0,'9','1'),('59',140,0,'9','1'),('60',46,0,'10','1'),('61',54,0,'10','1'),('62',69,0,'10','1'),('63',70,0,'10','0'),('64',71,0,'10','0'),('65',136,0,'10','1'),('66',139,0,'10','1'),('67',140,0,'10','1'),('68',46,0,'11','1'),('69',54,0,'11','1'),('70',69,0,'11','1'),('71',70,0,'11','0'),('72',71,0,'11','0'),('73',136,0,'11','1'),('74',139,0,'11','1'),('75',140,0,'11','1'),('76',46,0,'12','1'),('77',54,0,'12','1'),('78',69,0,'12','1'),('79',70,0,'12','0'),('80',71,0,'12','0'),('81',136,0,'12','1'),('82',139,0,'12','1'),('83',140,0,'12','1'),('84',46,0,'13','1'),('85',54,0,'13','1'),('86',69,0,'13','1'),('87',70,0,'13','0'),('88',71,0,'13','0'),('89',136,0,'13','1'),('90',139,0,'13','1'),('91',140,0,'13','1'),('92',46,0,'14','1'),('93',54,0,'14','1'),('94',69,0,'14','1'),('95',70,0,'14','0'),('96',71,0,'14','0'),('97',136,0,'14','1'),('98',139,0,'14','1'),('99',140,0,'14','1'),('100',46,0,'15','1'),('101',54,0,'15','1'),('102',69,0,'15','1'),('103',70,0,'15','0'),('104',71,0,'15','0'),('105',136,0,'15','1'),('106',139,0,'15','1'),('107',140,0,'15','1'),('108',46,0,'16','1'),('109',54,0,'16','1'),('110',69,0,'16','1'),('111',70,0,'16','0'),('112',71,0,'16','0'),('113',136,0,'16','1'),('114',139,0,'16','1'),('115',140,0,'16','1'),('116',46,0,'17','1'),('117',54,0,'17','1'),('118',69,0,'17','1'),('119',70,0,'17','0'),('120',71,0,'17','0'),('121',136,0,'17','1'),('122',139,0,'17','1'),('123',140,0,'17','1'),('124',46,0,'18','1'),('125',54,0,'18','1'),('126',69,0,'18','1'),('127',70,0,'18','0'),('128',71,0,'18','0'),('129',136,0,'18','1'),('130',139,0,'18','1'),('131',140,0,'18','1'),('132',46,0,'19','1'),('133',54,0,'19','1'),('134',69,0,'19','1'),('135',70,0,'19','0'),('136',71,0,'19','0'),('137',136,0,'19','1'),('138',139,0,'19','1'),('139',140,0,'19','1'),('140',46,0,'20','1'),('141',54,0,'20','1'),('142',69,0,'20','1'),('143',70,0,'20','0'),('144',71,0,'20','0'),('145',136,0,'20','1'),('146',139,0,'20','1'),('147',140,0,'20','1'),('148',46,0,'21','1'),('149',54,0,'21','1'),('150',69,0,'21','1'),('151',70,0,'21','0'),('152',71,0,'21','0'),('153',136,0,'21','1'),('154',139,0,'21','1'),('155',140,0,'21','1'),('156',46,0,'22','1'),('157',54,0,'22','1'),('158',69,0,'22','1'),('159',70,0,'22','0'),('160',71,0,'22','0'),('161',136,0,'22','1'),('162',139,0,'22','1'),('163',140,0,'22','1'),('164',46,0,'23','1'),('165',54,0,'23','1'),('166',69,0,'23','1'),('167',70,0,'23','0'),('168',71,0,'23','0'),('169',136,0,'23','1'),('170',139,0,'23','1'),('171',140,0,'23','1'),('172',46,0,'24','1'),('173',54,0,'24','1'),('174',69,0,'24','1'),('175',70,0,'24','0'),('176',71,0,'24','0'),('177',136,0,'24','1'),('178',139,0,'24','1'),('179',140,0,'24','1'),('180',46,0,'25','1'),('181',54,0,'25','1'),('182',69,0,'25','1'),('183',70,0,'25','0'),('184',71,0,'25','0'),('185',136,0,'25','1'),('186',139,0,'25','1'),('187',140,0,'25','1'),('188',46,0,'26','1'),('189',54,0,'26','1'),('190',69,0,'26','1'),('191',70,0,'26','0'),('192',71,0,'26','0'),('193',136,0,'26','1'),('194',139,0,'26','1'),('195',140,0,'26','1'),('196',46,0,'27','1'),('197',54,0,'27','1'),('198',69,0,'27','1'),('199',70,0,'27','0'),('200',71,0,'27','0'),('201',136,0,'27','1'),('202',139,0,'27','1'),('203',140,0,'27','1'),('204',46,0,'28','1'),('205',54,0,'28','1'),('206',69,0,'28','1'),('207',70,0,'28','0'),('208',71,0,'28','0'),('209',136,0,'28','1'),('210',139,0,'28','1'),('211',140,0,'28','1'),('212',46,0,'29','1'),('213',54,0,'29','1'),('214',69,0,'29','1'),('215',70,0,'29','0'),('216',71,0,'29','0'),('217',136,0,'29','1'),('218',139,0,'29','1'),('219',140,0,'29','1'),('220',46,0,'30','1'),('221',54,0,'30','1'),('222',69,0,'30','1'),('223',70,0,'30','0'),('224',71,0,'30','0'),('225',136,0,'30','1'),('226',139,0,'30','1'),('227',140,0,'30','1'),('228',54,0,'2','1'),('229',70,0,'2','0'),('230',71,0,'2','0'),('231',136,0,'2','1'),('232',139,0,'2','1'),('233',140,0,'2','1');
/*!40000 ALTER TABLE `catalog_category_entity_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_entity_text`
--

DROP TABLE IF EXISTS `catalog_category_entity_text`;
CREATE TABLE `catalog_category_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Text Attribute Backend Table';

--
-- Table structure for table `catalog_category_entity_varchar`
--

DROP TABLE IF EXISTS `catalog_category_entity_varchar`;
CREATE TABLE `catalog_category_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Varchar Attribute Backend Table';

--
-- Dumping data for table `catalog_category_entity_varchar`
--

LOCK TABLES `catalog_category_entity_varchar` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity_varchar` DISABLE KEYS */;
INSERT INTO `catalog_category_entity_varchar` VALUES ('1',45,0,'1','Root Catalog'),('2',45,0,'2','Default Category'),('3',52,0,'2','PRODUCTS'),('4',45,0,'3','Men'),('5',52,0,'3','PRODUCTS'),('6',117,0,'3','men'),('7',118,0,'3','men'),('8',45,0,'4','Women'),('9',52,0,'4','PRODUCTS'),('10',117,0,'4','women'),('11',118,0,'4','women'),('12',45,0,'5','Fashion'),('13',52,0,'5','PRODUCTS'),('14',117,0,'5','fashion'),('15',118,0,'5','fashion'),('16',45,0,'6','Gift'),('17',52,0,'6','PRODUCTS'),('18',117,0,'6','gift'),('19',118,0,'6','gift'),('20',45,0,'7','Kids'),('21',52,0,'7','PRODUCTS'),('22',117,0,'7','kids'),('23',118,0,'7','kids'),('24',45,0,'8','Jewelry'),('25',52,0,'8','PRODUCTS'),('26',117,0,'8','jewelry'),('27',118,0,'8','jewelry'),('28',45,0,'9','Custom Menu'),('29',52,0,'9','PRODUCTS'),('30',117,0,'9','contact-us'),('31',118,0,'9','contact-us'),('32',45,0,'10','WOMEN'),('33',52,0,'10','PRODUCTS'),('34',117,0,'10','women'),('35',118,0,'10','fashion/women'),('36',45,0,'11','MEN'),('37',52,0,'11','PRODUCTS'),('38',117,0,'11','men'),('39',118,0,'11','fashion/men'),('40',45,0,'12','FASHION'),('41',52,0,'12','PRODUCTS'),('42',117,0,'12','fashion'),('43',118,0,'12','fashion/fashion'),('44',45,0,'13','Accessories'),('45',52,0,'13','PRODUCTS'),('46',117,0,'13','accessories'),('47',118,0,'13','fashion/women/accessories'),('48',45,0,'14','Bags & Purses'),('49',52,0,'14','PRODUCTS'),('50',117,0,'14','bags-purses'),('51',118,0,'14','fashion/women/bags-purses'),('52',45,0,'15','Beauty'),('53',52,0,'15','PRODUCTS'),('54',117,0,'15','beauty'),('55',118,0,'15','fashion/women/beauty'),('56',45,0,'16','Blazers'),('57',52,0,'16','PRODUCTS'),('58',117,0,'16','blazers'),('59',118,0,'16','fashion/women/blazers'),('60',45,0,'17','Coats & Jackets'),('61',52,0,'17','PRODUCTS'),('62',117,0,'17','coats-jackets'),('63',118,0,'17','fashion/women/coats-jackets'),('64',45,0,'18','Curve & Plus Size'),('65',52,0,'18','PRODUCTS'),('66',117,0,'18','curve-plus-size'),('67',118,0,'18','fashion/women/curve-plus-size'),('68',45,0,'19','Coats & Jackets'),('69',52,0,'19','PRODUCTS'),('70',117,0,'19','coats-jackets'),('71',118,0,'19','fashion/men/coats-jackets'),('72',45,0,'20','Curve & Plus Size'),('73',52,0,'20','PRODUCTS'),('74',117,0,'20','curve-plus-size'),('75',118,0,'20','fashion/men/curve-plus-size'),('76',45,0,'21','Denim'),('77',52,0,'21','PRODUCTS'),('78',117,0,'21','denim'),('79',118,0,'21','fashion/men/denim'),('80',45,0,'22','Designer'),('81',52,0,'22','PRODUCTS'),('82',117,0,'22','designer'),('83',118,0,'22','fashion/men/designer'),('84',45,0,'23','Dresses '),('85',52,0,'23','PRODUCTS'),('86',117,0,'23','dresses'),('87',118,0,'23','fashion/men/dresses'),('88',45,0,'24','Accessories'),('89',52,0,'24','PRODUCTS'),('90',117,0,'24','accessories'),('91',118,0,'24','fashion/men/accessories'),('92',45,0,'25','Bags & Purses'),('93',52,0,'25','PRODUCTS'),('94',117,0,'25','bags-purses'),('95',118,0,'25','fashion/fashion/bags-purses'),('96',45,0,'26','Beauty'),('97',52,0,'26','PRODUCTS'),('98',117,0,'26','beauty'),('99',118,0,'26','fashion/fashion/beauty'),('100',45,0,'27','Blazers'),('101',52,0,'27','PRODUCTS'),('102',117,0,'27','blazers'),('103',118,0,'27','fashion/fashion/blazers'),('104',45,0,'28','Coats & Jackets'),('105',52,0,'28','PRODUCTS'),('106',117,0,'28','coats-jackets'),('107',118,0,'28','fashion/fashion/coats-jackets'),('108',45,0,'29','Curve & Plus Size'),('109',52,0,'29','PRODUCTS'),('110',117,0,'29','curve-plus-size'),('111',118,0,'29','fashion/fashion/curve-plus-size'),('112',45,0,'30','Denim'),('113',52,0,'30','PRODUCTS'),('114',117,0,'30','denim'),('115',118,0,'30','fashion/fashion/denim'),('116',48,0,'2','women_4_1.jpg'),('117',117,0,'2','default-category'),('119',48,0,'3','women_4_1.jpg'),('121',48,0,'4','women_4_1.jpg'),('144',48,0,'5','women_4_1.jpg'),('146',48,0,'6','women_4_1.jpg');
/*!40000 ALTER TABLE `catalog_category_entity_varchar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_product`
--

DROP TABLE IF EXISTS `catalog_category_product`;
CREATE TABLE `catalog_category_product` (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`entity_id`,`category_id`,`product_id`),
  UNIQUE KEY `CATALOG_CATEGORY_PRODUCT_CATEGORY_ID_PRODUCT_ID` (`category_id`,`product_id`),
  KEY `CATALOG_CATEGORY_PRODUCT_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Category Linkage Table';

--
-- Dumping data for table `catalog_category_product`
--

LOCK TABLES `catalog_category_product` WRITE;
/*!40000 ALTER TABLE `catalog_category_product` DISABLE KEYS */;
INSERT INTO `catalog_category_product` VALUES ('7','2','2','1'),('8','3','2','1'),('9','4','2','1'),('10','5','2','1'),('11','6','2','1'),('12','7','2','1'),('13','8','2','1'),('14','2','3','1'),('15','3','3','1'),('16','4','3','1'),('17','5','3','1'),('18','6','3','1'),('19','7','3','1'),('20','8','3','1'),('21','9','3','1'),('22','2','4','1'),('23','3','4','1'),('24','4','4','1'),('25','5','4','1'),('26','6','4','1'),('27','7','4','1'),('28','8','4','1'),('29','9','4','1'),('30','2','5','1'),('31','3','5','1'),('32','4','5','1'),('33','5','5','1'),('34','6','5','1'),('35','7','5','1'),('36','8','5','1'),('37','9','5','1'),('45','2','9','1'),('46','4','9','1'),('47','5','9','1'),('48','6','9','1'),('49','7','9','1'),('50','8','9','1'),('51','2','10','1'),('52','4','10','1'),('53','5','10','1'),('54','6','10','1'),('55','8','10','1'),('56','2','11','1'),('57','4','11','1'),('58','5','11','1'),('65','2','14','1'),('66','4','14','1'),('67','5','14','1'),('68','2','15','1'),('69','3','15','1'),('70','4','15','1'),('71','5','15','1'),('72','6','15','1'),('73','7','15','1'),('74','8','15','1'),('75','2','16','1'),('76','3','16','1'),('77','4','16','1'),('78','5','16','1'),('79','6','16','1'),('80','7','16','1'),('81','8','16','1'),('82','2','17','1'),('83','3','17','1'),('84','4','17','1'),('85','5','17','1'),('86','6','17','1'),('87','7','17','1'),('88','8','17','1'),('89','2','18','1'),('90','3','18','1'),('91','4','18','1'),('92','5','18','1'),('93','6','18','1'),('94','7','18','1'),('95','8','18','1'),('96','2','19','1'),('98','4','19','1'),('99','5','19','1'),('100','6','19','1'),('101','7','19','1'),('102','8','19','1'),('103','3','14','1'),('104','6','14','1'),('105','7','14','1'),('106','8','14','1');
/*!40000 ALTER TABLE `catalog_category_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_product_cl`
--

DROP TABLE IF EXISTS `catalog_category_product_cl`;
CREATE TABLE `catalog_category_product_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=393 DEFAULT CHARSET=utf8 COMMENT='catalog_category_product_cl';

--
-- Dumping data for table `catalog_category_product_cl`
--

LOCK TABLES `catalog_category_product_cl` WRITE;
/*!40000 ALTER TABLE `catalog_category_product_cl` DISABLE KEYS */;
INSERT INTO `catalog_category_product_cl` VALUES ('1','1'),('2','2'),('3','3'),('4','3'),('5','3'),('6','3'),('7','3'),('8','3'),('9','3'),('10','3'),('11','3'),('12','3'),('13','1'),('14','2'),('15','4'),('16','4'),('17','4'),('18','4'),('19','4'),('20','4'),('21','4'),('22','4'),('23','4'),('24','4'),('25','1'),('26','2'),('27','5'),('28','5'),('29','5'),('30','5'),('31','5'),('32','5'),('33','5'),('34','5'),('35','5'),('36','5'),('37','1'),('38','2'),('39','6'),('40','6'),('41','6'),('42','6'),('43','6'),('44','6'),('45','6'),('46','6'),('47','6'),('48','6'),('49','1'),('50','2'),('51','7'),('52','7'),('53','7'),('54','7'),('55','7'),('56','7'),('57','7'),('58','7'),('59','7'),('60','7'),('61','1'),('62','2'),('63','8'),('64','8'),('65','8'),('66','8'),('67','8'),('68','8'),('69','8'),('70','8'),('71','8'),('72','8'),('73','1'),('74','2'),('75','9'),('76','9'),('77','9'),('78','9'),('79','9'),('80','9'),('81','9'),('82','9'),('83','9'),('84','9'),('85','1'),('86','2'),('87','5'),('88','10'),('89','10'),('90','10'),('91','10'),('92','10'),('93','10'),('94','10'),('95','10'),('96','10'),('97','10'),('98','1'),('99','2'),('100','5'),('101','11'),('102','11'),('103','11'),('104','11'),('105','11'),('106','11'),('107','11'),('108','11'),('109','11'),('110','11'),('111','1'),('112','2'),('113','5'),('114','12'),('115','12'),('116','12'),('117','12'),('118','12'),('119','12'),('120','12'),('121','12'),('122','12'),('123','12'),('124','1'),('125','2'),('126','5'),('127','10'),('128','13'),('129','13'),('130','13'),('131','13'),('132','13'),('133','13'),('134','13'),('135','13'),('136','13'),('137','13'),('138','1'),('139','2'),('140','5'),('141','10'),('142','14'),('143','14'),('144','14'),('145','14'),('146','14'),('147','14'),('148','14'),('149','14'),('150','14'),('151','14'),('152','1'),('153','2'),('154','5'),('155','10'),('156','15'),('157','15'),('158','15'),('159','15'),('160','15'),('161','15'),('162','15'),('163','15'),('164','15'),('165','15'),('166','1'),('167','2'),('168','5'),('169','10'),('170','16'),('171','16'),('172','16'),('173','16'),('174','16'),('175','16'),('176','16'),('177','16'),('178','16'),('179','16'),('180','1'),('181','2'),('182','5'),('183','10'),('184','17'),('185','17'),('186','17'),('187','17'),('188','17'),('189','17'),('190','17'),('191','17'),('192','17'),('193','17'),('194','1'),('195','2'),('196','5'),('197','10'),('198','18'),('199','18'),('200','18'),('201','18'),('202','18'),('203','18'),('204','18'),('205','18'),('206','18'),('207','18'),('208','1'),('209','2'),('210','5'),('211','11'),('212','19'),('213','19'),('214','19'),('215','19'),('216','19'),('217','19'),('218','19'),('219','19'),('220','19'),('221','19'),('222','1'),('223','2'),('224','5'),('225','11'),('226','20'),('227','20'),('228','20'),('229','20'),('230','20'),('231','20'),('232','20'),('233','20'),('234','20'),('235','20'),('236','1'),('237','2'),('238','5'),('239','11'),('240','21'),('241','21'),('242','21'),('243','21'),('244','21'),('245','21'),('246','21'),('247','21'),('248','21'),('249','21'),('250','1'),('251','2'),('252','5'),('253','11'),('254','22'),('255','22'),('256','22'),('257','22'),('258','22'),('259','22'),('260','22'),('261','22'),('262','22'),('263','22'),('264','1'),('265','2'),('266','5'),('267','11'),('268','23'),('269','23'),('270','23'),('271','23'),('272','23'),('273','23'),('274','23'),('275','23'),('276','23'),('277','23'),('278','1'),('279','2'),('280','5'),('281','11'),('282','24'),('283','24'),('284','24'),('285','24'),('286','24'),('287','24'),('288','24'),('289','24'),('290','24'),('291','24'),('292','1'),('293','2'),('294','5'),('295','12'),('296','25'),('297','25'),('298','25'),('299','25'),('300','25'),('301','25'),('302','25'),('303','25'),('304','25'),('305','25'),('306','1'),('307','2'),('308','5'),('309','12'),('310','26'),('311','26'),('312','26'),('313','26'),('314','26'),('315','26'),('316','26'),('317','26'),('318','26'),('319','26'),('320','1'),('321','2'),('322','5'),('323','12'),('324','27'),('325','27'),('326','27'),('327','27'),('328','27'),('329','27'),('330','27'),('331','27'),('332','27'),('333','27'),('334','1'),('335','2'),('336','5'),('337','12'),('338','28'),('339','28'),('340','28'),('341','28'),('342','28'),('343','28'),('344','28'),('345','28'),('346','28'),('347','28'),('348','1'),('349','2'),('350','5'),('351','12'),('352','29'),('353','29'),('354','29'),('355','29'),('356','29'),('357','29'),('358','29'),('359','29'),('360','29'),('361','29'),('362','1'),('363','2'),('364','5'),('365','12'),('366','30'),('367','30'),('368','30'),('369','30'),('370','30'),('371','30'),('372','30'),('373','30'),('374','30'),('375','30'),('376','2'),('377','2'),('378','2'),('379','2'),('380','2'),('381','2'),('382','2'),('383','3'),('384','4'),('385','5'),('386','6'),('387','3'),('388','4'),('389','5'),('390','6'),('391','7'),('392','9');
/*!40000 ALTER TABLE `catalog_category_product_cl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_product_index`
--

DROP TABLE IF EXISTS `catalog_category_product_index`;
CREATE TABLE `catalog_category_product_index` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) DEFAULT NULL COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  PRIMARY KEY (`category_id`,`product_id`,`store_id`),
  KEY `CAT_CTGR_PRD_IDX_PRD_ID_STORE_ID_CTGR_ID_VISIBILITY` (`product_id`,`store_id`,`category_id`,`visibility`),
  KEY `CAT_CTGR_PRD_IDX_STORE_ID_CTGR_ID_VISIBILITY_IS_PARENT_POSITION` (`store_id`,`category_id`,`visibility`,`is_parent`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Index';

--
-- Dumping data for table `catalog_category_product_index`
--

LOCK TABLES `catalog_category_product_index` WRITE;
/*!40000 ALTER TABLE `catalog_category_product_index` DISABLE KEYS */;
INSERT INTO `catalog_category_product_index` VALUES ('2','2','10001',0,1,4),('2','3','10001',0,1,4),('2','4','10001',0,1,4),('2','5','10001',0,1,4),('2','9','10001',0,1,4),('2','10','10001',0,1,4),('2','11','10001',0,1,4),('2','14','10001',0,1,4),('2','15','10001',0,1,4),('2','16','10001',0,1,4),('2','17','10001',0,1,4),('2','18','10001',0,1,4),('2','19','10001',0,1,4),('3','2','1',1,1,4),('3','3','1',1,1,4),('3','4','1',1,1,4),('3','5','1',1,1,4),('3','14','1',1,1,4),('3','15','1',1,1,4),('3','16','1',1,1,4),('3','17','1',1,1,4),('3','18','1',1,1,4),('4','2','1',1,1,4),('4','3','1',1,1,4),('4','4','1',1,1,4),('4','5','1',1,1,4),('4','9','1',1,1,4),('4','10','1',1,1,4),('4','11','1',1,1,4),('4','14','1',1,1,4),('4','15','1',1,1,4),('4','16','1',1,1,4),('4','17','1',1,1,4),('4','18','1',1,1,4),('4','19','1',1,1,4),('5','2','1',1,1,4),('5','3','1',1,1,4),('5','4','1',1,1,4),('5','5','1',1,1,4),('5','9','1',1,1,4),('5','10','1',1,1,4),('5','11','1',1,1,4),('5','14','1',1,1,4),('5','15','1',1,1,4),('5','16','1',1,1,4),('5','17','1',1,1,4),('5','18','1',1,1,4),('5','19','1',1,1,4),('6','2','1',1,1,4),('6','3','1',1,1,4),('6','4','1',1,1,4),('6','5','1',1,1,4),('6','9','1',1,1,4),('6','10','1',1,1,4),('6','14','1',1,1,4),('6','15','1',1,1,4),('6','16','1',1,1,4),('6','17','1',1,1,4),('6','18','1',1,1,4),('6','19','1',1,1,4),('7','2','1',1,1,4),('7','3','1',1,1,4),('7','4','1',1,1,4),('7','5','1',1,1,4),('7','9','1',1,1,4),('7','14','1',1,1,4),('7','15','1',1,1,4),('7','16','1',1,1,4),('7','17','1',1,1,4),('7','18','1',1,1,4),('7','19','1',1,1,4),('8','2','1',1,1,4),('8','3','1',1,1,4),('8','4','1',1,1,4),('8','5','1',1,1,4),('8','9','1',1,1,4),('8','10','1',1,1,4),('8','14','1',1,1,4),('8','15','1',1,1,4),('8','16','1',1,1,4),('8','17','1',1,1,4),('8','18','1',1,1,4),('8','19','1',1,1,4),('9','3','1',1,1,4),('9','4','1',1,1,4),('9','5','1',1,1,4),('2','2','10001',0,2,4),('2','3','10001',0,2,4),('2','4','10001',0,2,4),('2','5','10001',0,2,4),('2','9','10001',0,2,4),('2','10','10001',0,2,4),('2','11','10001',0,2,4),('2','14','10001',0,2,4),('2','15','10001',0,2,4),('2','16','10001',0,2,4),('2','17','10001',0,2,4),('2','18','10001',0,2,4),('2','19','10001',0,2,4),('3','2','1',1,2,4),('3','3','1',1,2,4),('3','4','1',1,2,4),('3','5','1',1,2,4),('3','14','1',1,2,4),('3','15','1',1,2,4),('3','16','1',1,2,4),('3','17','1',1,2,4),('3','18','1',1,2,4),('4','2','1',1,2,4),('4','3','1',1,2,4),('4','4','1',1,2,4),('4','5','1',1,2,4),('4','9','1',1,2,4),('4','10','1',1,2,4),('4','11','1',1,2,4),('4','14','1',1,2,4),('4','15','1',1,2,4),('4','16','1',1,2,4),('4','17','1',1,2,4),('4','18','1',1,2,4),('4','19','1',1,2,4),('5','2','1',1,2,4),('5','3','1',1,2,4),('5','4','1',1,2,4),('5','5','1',1,2,4),('5','9','1',1,2,4),('5','10','1',1,2,4),('5','11','1',1,2,4),('5','14','1',1,2,4),('5','15','1',1,2,4),('5','16','1',1,2,4),('5','17','1',1,2,4),('5','18','1',1,2,4),('5','19','1',1,2,4),('6','2','1',1,2,4),('6','3','1',1,2,4),('6','4','1',1,2,4),('6','5','1',1,2,4),('6','9','1',1,2,4),('6','10','1',1,2,4),('6','14','1',1,2,4),('6','15','1',1,2,4),('6','16','1',1,2,4),('6','17','1',1,2,4),('6','18','1',1,2,4),('6','19','1',1,2,4),('7','2','1',1,2,4),('7','3','1',1,2,4),('7','4','1',1,2,4),('7','5','1',1,2,4),('7','9','1',1,2,4),('7','14','1',1,2,4),('7','15','1',1,2,4),('7','16','1',1,2,4),('7','17','1',1,2,4),('7','18','1',1,2,4),('7','19','1',1,2,4),('8','2','1',1,2,4),('8','3','1',1,2,4),('8','4','1',1,2,4),('8','5','1',1,2,4),('8','9','1',1,2,4),('8','10','1',1,2,4),('8','14','1',1,2,4),('8','15','1',1,2,4),('8','16','1',1,2,4),('8','17','1',1,2,4),('8','18','1',1,2,4),('8','19','1',1,2,4),('9','3','1',1,2,4),('9','4','1',1,2,4),('9','5','1',1,2,4),('2','2','10001',0,3,4),('2','3','10001',0,3,4),('2','4','10001',0,3,4),('2','5','10001',0,3,4),('2','9','10001',0,3,4),('2','10','10001',0,3,4),('2','11','10001',0,3,4),('2','14','10001',0,3,4),('2','15','10001',0,3,4),('2','16','10001',0,3,4),('2','17','10001',0,3,4),('2','18','10001',0,3,4),('2','19','10001',0,3,4),('3','2','1',1,3,4),('3','3','1',1,3,4),('3','4','1',1,3,4),('3','5','1',1,3,4),('3','14','1',1,3,4),('3','15','1',1,3,4),('3','16','1',1,3,4),('3','17','1',1,3,4),('3','18','1',1,3,4),('4','2','1',1,3,4),('4','3','1',1,3,4),('4','4','1',1,3,4),('4','5','1',1,3,4),('4','9','1',1,3,4),('4','10','1',1,3,4),('4','11','1',1,3,4),('4','14','1',1,3,4),('4','15','1',1,3,4),('4','16','1',1,3,4),('4','17','1',1,3,4),('4','18','1',1,3,4),('4','19','1',1,3,4),('5','2','1',1,3,4),('5','3','1',1,3,4),('5','4','1',1,3,4),('5','5','1',1,3,4),('5','9','1',1,3,4),('5','10','1',1,3,4),('5','11','1',1,3,4),('5','14','1',1,3,4),('5','15','1',1,3,4),('5','16','1',1,3,4),('5','17','1',1,3,4),('5','18','1',1,3,4),('5','19','1',1,3,4),('6','2','1',1,3,4),('6','3','1',1,3,4),('6','4','1',1,3,4),('6','5','1',1,3,4),('6','9','1',1,3,4),('6','10','1',1,3,4),('6','14','1',1,3,4),('6','15','1',1,3,4),('6','16','1',1,3,4),('6','17','1',1,3,4),('6','18','1',1,3,4),('6','19','1',1,3,4),('7','2','1',1,3,4),('7','3','1',1,3,4),('7','4','1',1,3,4),('7','5','1',1,3,4),('7','9','1',1,3,4),('7','14','1',1,3,4),('7','15','1',1,3,4),('7','16','1',1,3,4),('7','17','1',1,3,4),('7','18','1',1,3,4),('7','19','1',1,3,4),('8','2','1',1,3,4),('8','3','1',1,3,4),('8','4','1',1,3,4),('8','5','1',1,3,4),('8','9','1',1,3,4),('8','10','1',1,3,4),('8','14','1',1,3,4),('8','15','1',1,3,4),('8','16','1',1,3,4),('8','17','1',1,3,4),('8','18','1',1,3,4),('8','19','1',1,3,4),('9','3','1',1,3,4),('9','4','1',1,3,4),('9','5','1',1,3,4),('2','2','10001',0,4,4),('2','3','10001',0,4,4),('2','4','10001',0,4,4),('2','5','10001',0,4,4),('2','9','10001',0,4,4),('2','10','10001',0,4,4),('2','11','10001',0,4,4),('2','14','10001',0,4,4),('2','15','10001',0,4,4),('2','16','10001',0,4,4),('2','17','10001',0,4,4),('2','18','10001',0,4,4),('2','19','10001',0,4,4),('3','2','1',1,4,4),('3','3','1',1,4,4),('3','4','1',1,4,4),('3','5','1',1,4,4),('3','14','1',1,4,4),('3','15','1',1,4,4),('3','16','1',1,4,4),('3','17','1',1,4,4),('3','18','1',1,4,4),('4','2','1',1,4,4),('4','3','1',1,4,4),('4','4','1',1,4,4),('4','5','1',1,4,4),('4','9','1',1,4,4),('4','10','1',1,4,4),('4','11','1',1,4,4),('4','14','1',1,4,4),('4','15','1',1,4,4),('4','16','1',1,4,4),('4','17','1',1,4,4),('4','18','1',1,4,4),('4','19','1',1,4,4),('5','2','1',1,4,4),('5','3','1',1,4,4),('5','4','1',1,4,4),('5','5','1',1,4,4),('5','9','1',1,4,4),('5','10','1',1,4,4),('5','11','1',1,4,4),('5','14','1',1,4,4),('5','15','1',1,4,4),('5','16','1',1,4,4),('5','17','1',1,4,4),('5','18','1',1,4,4),('5','19','1',1,4,4),('6','2','1',1,4,4),('6','3','1',1,4,4),('6','4','1',1,4,4),('6','5','1',1,4,4),('6','9','1',1,4,4),('6','10','1',1,4,4),('6','14','1',1,4,4),('6','15','1',1,4,4),('6','16','1',1,4,4),('6','17','1',1,4,4),('6','18','1',1,4,4),('6','19','1',1,4,4),('7','2','1',1,4,4),('7','3','1',1,4,4),('7','4','1',1,4,4),('7','5','1',1,4,4),('7','9','1',1,4,4),('7','14','1',1,4,4),('7','15','1',1,4,4),('7','16','1',1,4,4),('7','17','1',1,4,4),('7','18','1',1,4,4),('7','19','1',1,4,4),('8','2','1',1,4,4),('8','3','1',1,4,4),('8','4','1',1,4,4),('8','5','1',1,4,4),('8','9','1',1,4,4),('8','10','1',1,4,4),('8','14','1',1,4,4),('8','15','1',1,4,4),('8','16','1',1,4,4),('8','17','1',1,4,4),('8','18','1',1,4,4),('8','19','1',1,4,4),('9','3','1',1,4,4),('9','4','1',1,4,4),('9','5','1',1,4,4);
/*!40000 ALTER TABLE `catalog_category_product_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_product_index_tmp`
--

DROP TABLE IF EXISTS `catalog_category_product_index_tmp`;
CREATE TABLE `catalog_category_product_index_tmp` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  KEY `CAT_CTGR_PRD_IDX_TMP_PRD_ID_CTGR_ID_STORE_ID` (`product_id`,`category_id`,`store_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Indexer Temp Table';

--
-- Table structure for table `catalog_compare_item`
--

DROP TABLE IF EXISTS `catalog_compare_item`;
CREATE TABLE `catalog_compare_item` (
  `catalog_compare_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Compare Item ID',
  `visitor_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Visitor ID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store ID',
  PRIMARY KEY (`catalog_compare_item_id`),
  KEY `CATALOG_COMPARE_ITEM_PRODUCT_ID` (`product_id`),
  KEY `CATALOG_COMPARE_ITEM_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  KEY `CATALOG_COMPARE_ITEM_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `CATALOG_COMPARE_ITEM_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Compare Table';

--
-- Table structure for table `catalog_eav_attribute`
--

DROP TABLE IF EXISTS `catalog_eav_attribute`;
CREATE TABLE `catalog_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `frontend_input_renderer` varchar(255) DEFAULT NULL COMMENT 'Frontend Input Renderer',
  `is_global` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Global',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `is_searchable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable',
  `is_filterable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable',
  `is_comparable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Comparable',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `is_html_allowed_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is HTML Allowed On Front',
  `is_used_for_price_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Price Rules',
  `is_filterable_in_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable In Search',
  `used_in_product_listing` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used In Product Listing',
  `used_for_sort_by` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Sorting',
  `apply_to` varchar(255) DEFAULT NULL COMMENT 'Apply To',
  `is_visible_in_advanced_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible In Advanced Search',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_wysiwyg_enabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is WYSIWYG Enabled',
  `is_used_for_promo_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Promo Rules',
  `is_required_in_admin_store` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Required In Admin Store',
  `is_used_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used in Grid',
  `is_visible_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible in Grid',
  `is_filterable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable in Grid',
  `search_weight` float NOT NULL DEFAULT '1' COMMENT 'Search Weight',
  `additional_data` text COMMENT 'Additional swatch attributes data',
  PRIMARY KEY (`attribute_id`),
  KEY `CATALOG_EAV_ATTRIBUTE_USED_FOR_SORT_BY` (`used_for_sort_by`),
  KEY `CATALOG_EAV_ATTRIBUTE_USED_IN_PRODUCT_LISTING` (`used_in_product_listing`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog EAV Attribute Table';

--
-- Dumping data for table `catalog_eav_attribute`
--

LOCK TABLES `catalog_eav_attribute` WRITE;
/*!40000 ALTER TABLE `catalog_eav_attribute` DISABLE KEYS */;
INSERT INTO `catalog_eav_attribute` VALUES (45,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(46,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(47,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,'0',1,0,0,0,0,0,'1',NULL),(48,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(49,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(50,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(51,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(52,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(53,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(54,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(55,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(56,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(57,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(58,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(59,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(60,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(61,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(62,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(63,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(64,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(65,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(66,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(67,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Sortby\\Available',0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(68,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Sortby\\DefaultSortby',0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(69,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(70,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(71,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(72,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Pricestep',0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(73,NULL,0,1,1,0,0,0,0,0,0,1,1,NULL,1,'0',0,0,0,0,0,0,'5',NULL),(74,NULL,1,1,1,0,1,0,0,0,0,0,0,NULL,1,'0',0,0,0,0,0,0,'6',NULL),(75,NULL,0,1,1,0,1,0,1,0,0,0,0,NULL,1,'0',1,0,0,0,0,0,'1',NULL),(76,NULL,0,1,1,0,1,0,1,0,0,1,0,NULL,1,'0',1,0,0,1,0,0,'1',NULL),(77,NULL,2,1,1,1,0,0,0,0,0,1,1,'simple,virtual,bundle,downloadable,configurable',1,'0',0,0,0,0,0,0,'1',NULL),(78,NULL,2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(79,NULL,2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,1,0,0,'1',NULL),(80,NULL,2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,1,0,0,'1',NULL),(81,NULL,2,1,0,0,0,0,0,0,0,0,0,'simple,virtual,downloadable',0,'0',0,0,0,1,0,1,'1',NULL),(82,'Magento\\Catalog\\Block\\Adminhtml\\Product\\Helper\\Form\\Weight',1,1,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(83,NULL,1,1,1,1,1,0,0,0,0,0,0,NULL,1,'0',0,0,0,1,0,1,'1',NULL),(84,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,1,'1',NULL),(85,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,1,'1',NULL),(86,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,1,'1',NULL),(87,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(88,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(89,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(90,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(91,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(92,NULL,2,1,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,0,0,0,'1',NULL),(93,NULL,0,1,1,1,1,0,0,0,0,0,0,NULL,1,'0',0,0,0,1,0,1,'1',NULL),(94,NULL,2,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(95,NULL,2,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(96,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(97,'Magento\\Framework\\Data\\Form\\Element\\Hidden',2,1,1,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,1,0,0,0,'1',NULL),(98,NULL,0,0,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,0,0,0,'1',NULL),(99,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,1,0,0,0,'1',NULL),(100,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,1,'1',NULL),(101,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(102,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(103,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(104,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(105,'Magento\\Catalog\\Block\\Adminhtml\\Product\\Helper\\Form\\Category',1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(106,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(107,NULL,1,0,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(108,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(109,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(110,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(111,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(112,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(113,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(114,NULL,2,1,0,0,0,0,0,0,0,0,0,'simple,bundle,grouped,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(115,'Magento\\CatalogInventory\\Block\\Adminhtml\\Form\\Field\\Stock',1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(116,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(117,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(118,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(119,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,1,0,1,'1',NULL),(120,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(121,'Magento\\Msrp\\Block\\Adminhtml\\Product\\Helper\\Form\\Type',2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,downloadable,bundle,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(122,'Magento\\Msrp\\Block\\Adminhtml\\Product\\Helper\\Form\\Type\\Price',2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,downloadable,bundle,configurable',0,'0',0,0,0,0,0,0,'1',NULL),(123,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,'0',0,0,0,0,0,0,'1',NULL),(124,NULL,1,1,0,0,0,0,0,0,0,0,0,'bundle',0,'0',0,0,0,0,0,0,'1',NULL),(125,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,'0',0,0,0,0,0,0,'1',NULL),(126,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,'0',0,0,0,0,0,0,'1',NULL),(127,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,'0',0,0,0,0,0,0,'1',NULL),(128,NULL,1,0,0,0,0,0,0,0,0,1,0,'downloadable',0,'0',0,0,0,0,0,0,'1',NULL),(129,NULL,0,0,0,0,0,0,0,0,0,0,0,'downloadable',0,'0',0,0,0,0,0,0,'1',NULL),(130,NULL,0,0,0,0,0,0,0,0,0,0,0,'downloadable',0,'0',0,0,0,0,0,0,'1',NULL),(131,NULL,1,0,0,0,0,0,0,0,0,1,0,'downloadable',0,'0',0,0,0,0,0,0,'1',NULL),(132,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(133,NULL,2,1,1,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(134,'Magento\\GiftMessage\\Block\\Adminhtml\\Product\\Helper\\Form\\Config',1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(135,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(136,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(137,NULL,0,1,0,0,0,0,0,0,0,1,0,'simple,virtual,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(138,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(139,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(140,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(141,NULL,1,1,0,0,0,0,0,0,0,0,0,'simple,configurable,bundle,virtual,downloadable',0,'0',0,0,0,0,0,1,'1',NULL);
/*!40000 ALTER TABLE `catalog_eav_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_attribute_cl`
--

DROP TABLE IF EXISTS `catalog_product_attribute_cl`;
CREATE TABLE `catalog_product_attribute_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=463 DEFAULT CHARSET=utf8 COMMENT='catalog_product_attribute_cl';

--
-- Dumping data for table `catalog_product_attribute_cl`
--

LOCK TABLES `catalog_product_attribute_cl` WRITE;
/*!40000 ALTER TABLE `catalog_product_attribute_cl` DISABLE KEYS */;
INSERT INTO `catalog_product_attribute_cl` VALUES ('1','1'),('2','1'),('3','1'),('4','1'),('5','1'),('6','1'),('7','1'),('8','1'),('9','1'),('10','1'),('11','1'),('12','1'),('13','1'),('14','1'),('15','1'),('16','1'),('17','1'),('18','1'),('19','1'),('20','1'),('21','1'),('22','2'),('23','2'),('24','2'),('25','2'),('26','2'),('27','2'),('28','2'),('29','2'),('30','2'),('31','2'),('32','2'),('33','2'),('34','2'),('35','2'),('36','2'),('37','2'),('38','2'),('39','2'),('40','2'),('41','2'),('42','2'),('43','2'),('44','2'),('45','2'),('46','2'),('47','2'),('48','2'),('49','2'),('50','2'),('51','2'),('52','2'),('53','3'),('54','3'),('55','3'),('56','3'),('57','3'),('58','3'),('59','3'),('60','3'),('61','3'),('62','3'),('63','3'),('64','3'),('65','3'),('66','3'),('67','3'),('68','3'),('69','3'),('70','3'),('71','3'),('72','3'),('73','3'),('74','3'),('75','3'),('76','4'),('77','4'),('78','4'),('79','4'),('80','4'),('81','4'),('82','4'),('83','4'),('84','4'),('85','4'),('86','4'),('87','4'),('88','4'),('89','4'),('90','4'),('91','4'),('92','4'),('93','4'),('94','4'),('95','4'),('96','4'),('97','4'),('98','5'),('99','5'),('100','5'),('101','5'),('102','5'),('103','5'),('104','5'),('105','5'),('106','5'),('107','5'),('108','5'),('109','5'),('110','5'),('111','5'),('112','5'),('113','5'),('114','5'),('115','5'),('116','5'),('117','5'),('118','5'),('174','9'),('175','9'),('176','9'),('177','9'),('178','9'),('179','9'),('180','9'),('181','9'),('182','9'),('183','9'),('184','9'),('185','9'),('186','9'),('187','9'),('188','9'),('189','9'),('190','9'),('191','9'),('192','9'),('193','9'),('194','9'),('195','10'),('196','10'),('197','10'),('198','10'),('199','10'),('200','10'),('201','10'),('202','10'),('203','10'),('204','10'),('205','10'),('206','10'),('207','10'),('208','10'),('209','10'),('210','10'),('211','10'),('212','10'),('213','10'),('214','10'),('215','10'),('216','11'),('217','11'),('218','11'),('219','11'),('220','11'),('221','11'),('222','11'),('223','11'),('224','11'),('225','11'),('226','11'),('227','11'),('228','11'),('229','11'),('230','11'),('231','11'),('232','11'),('233','11'),('234','11'),('235','11'),('236','11'),('237','12'),('238','12'),('239','12'),('240','12'),('241','12'),('242','12'),('243','12'),('244','12'),('245','12'),('246','12'),('247','12'),('248','12'),('249','12'),('250','12'),('251','12'),('252','12'),('253','12'),('254','12'),('255','12'),('256','12'),('257','12'),('258','13'),('259','13'),('260','13'),('261','13'),('262','13'),('263','13'),('264','13'),('265','13'),('266','13'),('267','13'),('268','13'),('269','13'),('270','13'),('271','13'),('272','13'),('273','13'),('274','13'),('275','13'),('276','13'),('277','13'),('278','13'),('279','14'),('280','14'),('281','14'),('282','14'),('283','14'),('284','14'),('285','14'),('286','14'),('287','14'),('288','14'),('289','14'),('290','14'),('291','14'),('292','14'),('293','14'),('294','14'),('295','14'),('296','14'),('297','14'),('298','14'),('299','14'),('300','15'),('301','15'),('302','15'),('303','15'),('304','15'),('305','15'),('306','15'),('307','15'),('308','15'),('309','15'),('310','15'),('311','15'),('312','15'),('313','15'),('314','15'),('315','15'),('316','15'),('317','15'),('318','15'),('319','15'),('320','15'),('321','15'),('322','15'),('323','16'),('324','16'),('325','16'),('326','16'),('327','16'),('328','16'),('329','16'),('330','16'),('331','16'),('332','16'),('333','16'),('334','16'),('335','16'),('336','16'),('337','16'),('338','16'),('339','16'),('340','16'),('341','16'),('342','16'),('343','16'),('344','16'),('345','17'),('346','17'),('347','17'),('348','17'),('349','17'),('350','17'),('351','17'),('352','17'),('353','17'),('354','17'),('355','17'),('356','17'),('357','17'),('358','17'),('359','17'),('360','17'),('361','17'),('362','17'),('363','17'),('364','17'),('365','17'),('366','17'),('367','18'),('368','18'),('369','18'),('370','18'),('371','18'),('372','18'),('373','18'),('374','18'),('375','18'),('376','18'),('377','18'),('378','18'),('379','18'),('380','18'),('381','18'),('382','18'),('383','18'),('384','18'),('385','18'),('386','18'),('387','18'),('388','18'),('389','19'),('390','19'),('391','19'),('392','19'),('393','19'),('394','19'),('395','19'),('396','19'),('397','19'),('398','19'),('399','19'),('400','19'),('401','19'),('402','19'),('403','19'),('404','19'),('405','19'),('406','19'),('407','19'),('408','19'),('409','19'),('410','19'),('411','3'),('412','3'),('413','2'),('414','2'),('415','4'),('416','5'),('417','9'),('418','10'),('419','11'),('420','14'),('421','15'),('422','15'),('423','14'),('424','14'),('425','14'),('426','14'),('427','14'),('428','14'),('429','14'),('430','14'),('431','2'),('432','2'),('433','2'),('434','3'),('435','3'),('436','14'),('437','14'),('438','2'),('439','2'),('440','2'),('441','2'),('442','2'),('443','10'),('444','10'),('445','4'),('446','4'),('447','18'),('448','18'),('449','5'),('450','5'),('451','9'),('452','9'),('453','11'),('454','11'),('455','15'),('456','15'),('457','17'),('458','17'),('459','16'),('460','16'),('461','19'),('462','19');
/*!40000 ALTER TABLE `catalog_product_attribute_cl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_bundle_option`
--

DROP TABLE IF EXISTS `catalog_product_bundle_option`;
CREATE TABLE `catalog_product_bundle_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `required` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Required',
  `position` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  `type` varchar(255) DEFAULT NULL COMMENT 'Type',
  PRIMARY KEY (`option_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_OPTION_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option';

--
-- Table structure for table `catalog_product_bundle_option_value`
--

DROP TABLE IF EXISTS `catalog_product_bundle_option_value`;
CREATE TABLE `catalog_product_bundle_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_BUNDLE_OPTION_VALUE_OPTION_ID_STORE_ID` (`option_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option Value';

--
-- Table structure for table `catalog_product_bundle_price_index`
--

DROP TABLE IF EXISTS `catalog_product_bundle_price_index`;
CREATE TABLE `catalog_product_bundle_price_index` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `min_price` decimal(12,4) NOT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) NOT NULL COMMENT 'Max Price',
  PRIMARY KEY (`entity_id`,`website_id`,`customer_group_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_CUSTOMER_GROUP_ID` (`customer_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Price Index';

--
-- Table structure for table `catalog_product_bundle_selection`
--

DROP TABLE IF EXISTS `catalog_product_bundle_selection`;
CREATE TABLE `catalog_product_bundle_selection` (
  `selection_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Selection Id',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
  `parent_product_id` int(10) unsigned NOT NULL COMMENT 'Parent Product Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `position` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_default` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Default',
  `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Price Type',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Selection Price Value',
  `selection_qty` decimal(12,4) DEFAULT NULL COMMENT 'Selection Qty',
  `selection_can_change_qty` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Selection Can Change Qty',
  PRIMARY KEY (`selection_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_OPTION_ID` (`option_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection';

--
-- Table structure for table `catalog_product_bundle_selection_price`
--

DROP TABLE IF EXISTS `catalog_product_bundle_selection_price`;
CREATE TABLE `catalog_product_bundle_selection_price` (
  `selection_id` int(10) unsigned NOT NULL COMMENT 'Selection Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Price Type',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Selection Price Value',
  PRIMARY KEY (`selection_id`,`website_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_PRICE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection Price';

--
-- Table structure for table `catalog_product_bundle_stock_index`
--

DROP TABLE IF EXISTS `catalog_product_bundle_stock_index`;
CREATE TABLE `catalog_product_bundle_stock_index` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `stock_status` smallint(6) DEFAULT '0' COMMENT 'Stock Status',
  PRIMARY KEY (`entity_id`,`website_id`,`stock_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Stock Index';

--
-- Table structure for table `catalog_product_category_cl`
--

DROP TABLE IF EXISTS `catalog_product_category_cl`;
CREATE TABLE `catalog_product_category_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8 COMMENT='catalog_product_category_cl';

--
-- Dumping data for table `catalog_product_category_cl`
--

LOCK TABLES `catalog_product_category_cl` WRITE;
/*!40000 ALTER TABLE `catalog_product_category_cl` DISABLE KEYS */;
INSERT INTO `catalog_product_category_cl` VALUES ('1','1'),('2','1'),('3','1'),('4','1'),('5','1'),('6','1'),('7','1'),('8','1'),('9','1'),('10','1'),('11','1'),('12','1'),('13','2'),('14','2'),('15','2'),('16','2'),('17','2'),('18','2'),('19','2'),('20','2'),('21','2'),('22','2'),('23','2'),('24','2'),('25','2'),('26','3'),('27','3'),('28','3'),('29','3'),('30','3'),('31','3'),('32','3'),('33','3'),('34','3'),('35','3'),('36','3'),('37','3'),('38','3'),('39','3'),('40','4'),('41','4'),('42','4'),('43','4'),('44','4'),('45','4'),('46','4'),('47','4'),('48','4'),('49','4'),('50','4'),('51','4'),('52','4'),('53','4'),('54','5'),('55','5'),('56','5'),('57','5'),('58','5'),('59','5'),('60','5'),('61','5'),('62','5'),('63','5'),('64','5'),('65','5'),('66','5'),('67','5'),('91','9'),('92','9'),('93','9'),('94','9'),('95','9'),('96','9'),('97','9'),('98','9'),('99','9'),('100','9'),('101','9'),('102','9'),('103','10'),('104','10'),('105','10'),('106','10'),('107','10'),('108','10'),('109','10'),('110','10'),('111','10'),('112','10'),('113','10'),('114','11'),('115','11'),('116','11'),('117','11'),('118','11'),('119','11'),('120','11'),('121','11'),('122','11'),('123','12'),('124','12'),('125','12'),('126','12'),('127','12'),('128','12'),('129','12'),('130','12'),('131','12'),('132','13'),('133','13'),('134','13'),('135','13'),('136','13'),('137','13'),('138','13'),('139','13'),('140','13'),('141','14'),('142','14'),('143','14'),('144','14'),('145','14'),('146','14'),('147','14'),('148','14'),('149','14'),('150','15'),('151','15'),('152','15'),('153','15'),('154','15'),('155','15'),('156','15'),('157','15'),('158','15'),('159','15'),('160','15'),('161','15'),('162','15'),('163','16'),('164','16'),('165','16'),('166','16'),('167','16'),('168','16'),('169','16'),('170','16'),('171','16'),('172','16'),('173','16'),('174','16'),('175','16'),('176','17'),('177','17'),('178','17'),('179','17'),('180','17'),('181','17'),('182','17'),('183','17'),('184','17'),('185','17'),('186','17'),('187','17'),('188','17'),('189','18'),('190','18'),('191','18'),('192','18'),('193','18'),('194','18'),('195','18'),('196','18'),('197','18'),('198','18'),('199','18'),('200','18'),('201','18'),('202','19'),('203','19'),('204','19'),('205','19'),('206','19'),('207','19'),('208','19'),('209','19'),('210','19'),('211','19'),('212','19'),('213','19'),('214','19'),('215','19'),('216','14'),('217','14'),('218','14'),('219','14'),('220','2'),('221','2'),('222','3'),('223','3'),('224','14'),('225','14'),('226','2'),('227','2'),('228','2'),('229','2'),('230','2'),('231','10'),('232','10'),('233','4'),('234','4'),('235','18'),('236','18'),('237','5'),('238','5'),('239','9'),('240','9'),('241','11'),('242','11'),('243','15'),('244','15'),('245','17'),('246','17'),('247','16'),('248','16'),('249','19'),('250','19');
/*!40000 ALTER TABLE `catalog_product_category_cl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity`
--

DROP TABLE IF EXISTS `catalog_product_entity`;
CREATE TABLE `catalog_product_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set ID',
  `type_id` varchar(32) NOT NULL DEFAULT 'simple' COMMENT 'Type ID',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `has_options` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Has Options',
  `required_options` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Required Options',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  PRIMARY KEY (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_ATTRIBUTE_SET_ID` (`attribute_set_id`),
  KEY `CATALOG_PRODUCT_ENTITY_SKU` (`sku`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Table';

--
-- Dumping data for table `catalog_product_entity`
--

LOCK TABLES `catalog_product_entity` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity` DISABLE KEYS */;
INSERT INTO `catalog_product_entity` VALUES ('2',4,'simple','Accumsan Elit',0,0,'2016-10-08 04:37:53','2016-10-08 04:37:53'),('3',4,'simple','Nunc Facilisis',0,0,'2016-10-08 04:52:47','2016-10-08 04:52:47'),('4',4,'simple','Etiam Gravida',0,0,'2016-10-10 01:55:32','2016-10-10 01:55:32'),('5',4,'simple','Donec Non Est',0,0,'2016-10-10 02:05:46','2016-10-10 02:05:46'),('9',4,'simple','Habitant',0,0,'2016-10-10 02:16:00','2016-10-10 02:16:00'),('10',4,'simple','Tempus',0,0,'2016-10-10 02:17:34','2016-10-10 02:17:34'),('11',4,'simple','Aliquam',0,0,'2016-10-10 02:20:35','2016-10-10 02:20:35'),('14',4,'simple','squa',0,0,'2016-10-10 02:33:17','2016-10-10 02:33:17'),('15',4,'simple','Levis',0,0,'2016-10-11 09:53:28','2016-10-11 09:53:28'),('16',4,'simple','puma',0,0,'2016-10-11 09:58:58','2016-10-11 09:58:58'),('17',4,'simple','adidas',0,0,'2016-10-11 10:02:49','2016-10-11 10:02:49'),('18',4,'simple','nike',0,0,'2016-10-11 10:05:04','2016-10-11 10:05:04'),('19',4,'simple','violet',0,0,'2016-10-11 10:09:04','2016-10-11 10:09:04');
/*!40000 ALTER TABLE `catalog_product_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_datetime`
--

DROP TABLE IF EXISTS `catalog_product_entity_datetime`;
CREATE TABLE `catalog_product_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DATETIME_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Datetime Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_datetime`
--

LOCK TABLES `catalog_product_entity_datetime` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_datetime` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_datetime` VALUES ('2',79,0,'2','2016-10-08 00:00:00'),('3',80,0,'2','2018-10-08 00:00:00'),('4',94,0,'2','2016-10-08 00:00:00'),('5',101,0,'2','2016-10-08 00:00:00'),('6',79,0,'3','2016-10-25 00:00:00'),('7',80,0,'3','2017-10-31 00:00:00'),('8',94,0,'3','2016-10-08 00:00:00'),('9',101,0,'3','2016-10-08 00:00:00'),('10',79,0,'4','2016-10-10 00:00:00'),('11',94,0,'4','2016-10-10 00:00:00'),('12',101,0,'4','2016-10-10 00:00:00'),('13',79,0,'15','2016-10-11 00:00:00'),('14',80,0,'15','2017-10-11 00:00:00'),('15',94,0,'15','2016-10-11 00:00:00'),('16',95,0,'15','2015-10-11 00:00:00'),('17',101,0,'15','2016-10-11 00:00:00'),('18',79,0,'16','2016-10-11 00:00:00'),('19',80,0,'16','2017-10-11 00:00:00'),('20',94,0,'16','2016-10-11 00:00:00'),('21',101,0,'16','2016-10-11 00:00:00'),('22',79,0,'17','2016-10-11 00:00:00'),('23',94,0,'17','2016-10-11 00:00:00'),('24',101,0,'17','2016-10-11 00:00:00'),('25',79,0,'18','2016-10-11 00:00:00'),('26',80,0,'18','2018-10-11 00:00:00'),('27',94,0,'18','2016-10-12 00:00:00'),('28',101,0,'18','2016-10-11 00:00:00'),('29',79,0,'19','2016-10-11 00:00:00'),('30',80,0,'19','2017-10-11 00:00:00'),('31',94,0,'19','2016-10-11 00:00:00'),('32',101,0,'19','2016-10-11 00:00:00'),('33',80,0,'4','2017-10-12 00:00:00'),('34',79,0,'5','2016-10-12 00:00:00'),('35',80,0,'5','2017-10-12 00:00:00'),('36',94,0,'5','2016-10-12 00:00:00'),('37',101,0,'5','2016-10-12 00:00:00'),('38',79,0,'9','2016-10-12 00:00:00'),('39',80,0,'9','2018-10-12 00:00:00'),('40',94,0,'9','2016-10-12 00:00:00'),('41',101,0,'9','2016-10-12 00:00:00'),('42',79,0,'10','2016-10-12 00:00:00'),('43',80,0,'10','2017-10-12 00:00:00'),('44',94,0,'10','2016-10-12 00:00:00'),('45',101,0,'10','2016-10-12 00:00:00'),('46',79,0,'11','2016-10-12 00:00:00'),('47',80,0,'11','2018-10-12 00:00:00'),('48',94,0,'11','2016-10-12 00:00:00'),('49',101,0,'11','2016-10-12 00:00:00'),('50',79,0,'14','2016-10-12 00:00:00'),('51',80,0,'14','2018-10-12 00:00:00'),('52',94,0,'14','2016-10-12 00:00:00'),('53',101,0,'14','2016-10-12 00:00:00');
/*!40000 ALTER TABLE `catalog_product_entity_datetime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_decimal`
--

DROP TABLE IF EXISTS `catalog_product_entity_decimal`;
CREATE TABLE `catalog_product_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` decimal(12,4) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Decimal Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_decimal`
--

LOCK TABLES `catalog_product_entity_decimal` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_decimal` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_decimal` VALUES ('2',77,0,'2','190.0000'),('3',78,0,'2','150.0000'),('5',77,0,'3','170.0000'),('6',78,0,'3','150.0000'),('8',77,0,'4','65.0000'),('9',78,0,'4','60.0000'),('10',77,0,'5','70.0000'),('14',77,0,'9','70.0000'),('15',77,0,'10','80.0000'),('16',77,0,'11','70.0000'),('19',77,0,'14','70.0000'),('20',77,0,'15','80.0000'),('21',78,0,'15','60.0000'),('23',77,0,'16','110.0000'),('24',78,0,'16','80.0000'),('25',77,0,'17','150.0000'),('26',78,0,'17','100.0000'),('27',77,0,'18','205.0000'),('28',78,0,'18','150.0000'),('29',77,0,'19','50.0000'),('30',78,0,'19','30.0000'),('31',78,0,'5','50.0000'),('32',78,0,'9','50.0000'),('33',78,0,'10','70.0000'),('34',78,0,'11','60.0000'),('35',78,0,'14','50.0000');
/*!40000 ALTER TABLE `catalog_product_entity_decimal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_gallery`
--

DROP TABLE IF EXISTS `catalog_product_entity_gallery`;
CREATE TABLE `catalog_product_entity_gallery` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Gallery Attribute Backend Table';

--
-- Table structure for table `catalog_product_entity_int`
--

DROP TABLE IF EXISTS `catalog_product_entity_int`;
CREATE TABLE `catalog_product_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` int(11) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_INT_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Integer Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_int`
--

LOCK TABLES `catalog_product_entity_int` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_int` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_int` VALUES ('6',97,0,'2','1'),('7',99,0,'2','4'),('8',115,0,'2','1'),('9',133,0,'2','2'),('10',137,0,'2','0'),('11',97,0,'3','1'),('12',99,0,'3','4'),('13',115,0,'3','1'),('14',133,0,'3','2'),('15',137,0,'3','0'),('16',97,0,'4','1'),('17',99,0,'4','4'),('18',115,0,'4','1'),('19',133,0,'4','2'),('20',137,0,'4','0'),('21',97,0,'5','1'),('22',99,0,'5','4'),('23',115,0,'5','1'),('24',133,0,'5','2'),('25',137,0,'5','0'),('41',97,0,'9','1'),('42',99,0,'9','4'),('43',115,0,'9','1'),('44',133,0,'9','2'),('45',137,0,'9','0'),('46',97,0,'10','1'),('47',99,0,'10','4'),('48',115,0,'10','1'),('49',133,0,'10','2'),('50',137,0,'10','0'),('51',97,0,'11','1'),('52',99,0,'11','4'),('53',115,0,'11','1'),('54',133,0,'11','2'),('55',137,0,'11','0'),('66',97,0,'14','1'),('67',99,0,'14','4'),('68',115,0,'14','1'),('69',133,0,'14','2'),('70',137,0,'14','0'),('71',97,0,'15','1'),('72',99,0,'15','4'),('73',115,0,'15','1'),('74',133,0,'15','2'),('75',137,0,'15','0'),('76',97,0,'16','1'),('77',99,0,'16','4'),('78',115,0,'16','1'),('79',133,0,'16','2'),('80',137,0,'16','0'),('81',97,0,'17','1'),('82',99,0,'17','4'),('83',115,0,'17','1'),('84',133,0,'17','2'),('85',137,0,'17','0'),('86',97,0,'18','1'),('87',99,0,'18','4'),('88',115,0,'18','1'),('89',133,0,'18','2'),('90',137,0,'18','0'),('91',97,0,'19','1'),('92',99,0,'19','4'),('93',115,0,'19','1'),('94',133,0,'19','2'),('95',137,0,'19','0'),('96',83,0,'2','11'),('97',93,0,'2','5'),('98',83,0,'3','11'),('99',93,0,'3','4'),('100',83,0,'14','9'),('101',93,0,'14','4'),('102',83,0,'10','9'),('103',93,0,'10','4'),('104',83,0,'4','9'),('105',93,0,'4','4'),('106',83,0,'18','9'),('107',93,0,'18','4'),('108',83,0,'5','9'),('109',93,0,'5','4'),('110',83,0,'9','9'),('111',93,0,'9','4'),('112',83,0,'11','9'),('113',93,0,'11','4'),('114',83,0,'15','9'),('115',93,0,'15','4'),('116',83,0,'17','9'),('117',93,0,'17','4'),('118',83,0,'16','9'),('119',93,0,'16','4'),('120',83,0,'19','9'),('121',93,0,'19','4');
/*!40000 ALTER TABLE `catalog_product_entity_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery`;
CREATE TABLE `catalog_product_entity_media_gallery` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  `media_type` varchar(32) NOT NULL DEFAULT 'image' COMMENT 'Media entry type',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Visibility status',
  PRIMARY KEY (`value_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Media Gallery Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_media_gallery`
--

LOCK TABLES `catalog_product_entity_media_gallery` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_media_gallery` VALUES ('1',90,'/2/4/24.jpg','image',0),('3',90,'/2/_/2_1.jpg','image',0),('4',90,'/2/_/2_2.jpg','image',0),('5',90,'/3/_/3_1.jpg','image',0),('6',90,'/5/_/5_1__2.jpg','image',0),('7',90,'/i/m/img02.jpg','image',0),('9',90,'/6/_/6.jpg','image',0),('10',90,'/i/m/img01.jpg','image',0),('11',90,'/i/m/img03_1.jpg','image',0),('12',90,'/1/4/14.jpg','image',0),('13',90,'/1/6/16.jpg','image',0),('14',90,'/1/8/18.jpg','image',0),('15',90,'/2/0/20.jpg','image',0),('16',90,'/2/2/22.jpg','image',0),('19',90,'/6/_/6_1.jpg','image',0),('20',90,'/i/m/img01_1.jpg','image',0),('21',90,'/6/_/6_2.jpg','image',0),('22',90,'/i/m/img01_2.jpg','image',0),('23',90,'/2/_/2_1_1.jpg','image',0),('24',90,'/2/_/2_2_1.jpg','image',0),('25',90,'/3/_/3_1_1.jpg','image',0),('26',90,'/i/m/img01_3.jpg','image',0),('27',90,'/i/m/img02_1.jpg','image',0),('28',90,'/i/m/img04.jpg','image',0),('29',90,'/5/_/5_1__2_2.jpg','image',0),('30',90,'/6/_/6_3.jpg','image',0),('31',90,'/i/m/img01_4.jpg','image',0),('32',90,'/i/m/img03_1_1.jpg','image',0),('33',90,'/i/m/img04_1.jpg','image',0),('34',90,'/3/_/3_1_2.jpg','image',0),('35',90,'/4/_/4_1_1.jpg','image',0),('36',90,'/i/m/img01_5.jpg','image',0),('37',90,'/i/m/img02_2.jpg','image',0),('38',90,'/6/_/6_4.jpg','image',0),('39',90,'/i/m/img01_6.jpg','image',0),('40',90,'/i/m/img04_2.jpg','image',0),('41',90,'/3/_/3_1_3.jpg','image',0),('42',90,'/4/_/4_1_2.jpg','image',0),('43',90,'/i/m/img01_7.jpg','image',0),('44',90,'/i/m/img02_3.jpg','image',0),('45',90,'/5/_/5_1__2_3.jpg','image',0),('46',90,'/6/_/6_5.jpg','image',0),('47',90,'/i/m/img01_8.jpg','image',0),('48',90,'/i/m/img02_4.jpg','image',0),('49',90,'/3/_/3_1_4.jpg','image',0),('50',90,'/4/_/4_1_3.jpg','image',0),('51',90,'/i/m/img01_9.jpg','image',0),('52',90,'/i/m/img02_5.jpg','image',0),('53',90,'/i/m/img01_10.jpg','image',0),('54',90,'/i/m/img02_6.jpg','image',0),('55',90,'/6/_/6_6.jpg','image',0),('56',90,'/i/m/img01_11.jpg','image',0),('57',90,'/i/m/img04_3.jpg','image',0),('58',90,'/5/_/5_1__2_4.jpg','image',0),('59',90,'/6/_/6_7.jpg','image',0),('60',90,'/i/m/img01_12.jpg','image',0),('61',90,'/i/m/img02_7.jpg','image',0),('62',90,'/3/_/3_1_5.jpg','image',0),('63',90,'/4/_/4_1_4.jpg','image',0),('64',90,'/i/m/img01_13.jpg','image',0),('65',90,'/i/m/img02_8.jpg','image',0),('66',90,'/6/_/6_8.jpg','image',0),('67',90,'/i/m/img01_14.jpg','image',0),('68',90,'/i/m/img04_4.jpg','image',0),('69',90,'/3/_/3_1_6.jpg','image',0),('70',90,'/4/_/4_1_5.jpg','image',0),('71',90,'/i/m/img01_15.jpg','image',0),('72',90,'/i/m/img02_9.jpg','image',0);
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery_value`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value`;
CREATE TABLE `catalog_product_entity_media_gallery_value` (
  `value_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Value ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  `position` int(10) unsigned DEFAULT NULL COMMENT 'Position',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Disabled',
  `record_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Record Id',
  PRIMARY KEY (`record_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_VALUE_ID` (`value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Media Gallery Attribute Value Table';

--
-- Dumping data for table `catalog_product_entity_media_gallery_value`
--

LOCK TABLES `catalog_product_entity_media_gallery_value` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_media_gallery_value` VALUES ('3',0,'2',NULL,'2',0,'63'),('4',0,'2',NULL,'3',0,'64'),('26',0,'2',NULL,'3',0,'65'),('27',0,'2',NULL,'4',0,'66'),('28',0,'2',NULL,'5',0,'67'),('5',0,'3',NULL,'1',0,'81'),('29',0,'3',NULL,'2',0,'82'),('30',0,'3',NULL,'3',0,'83'),('31',0,'3',NULL,'4',0,'84'),('32',0,'3',NULL,'5',0,'85'),('33',0,'3',NULL,'6',0,'86'),('6',0,'4',NULL,'1',0,'87'),('34',0,'4',NULL,'2',0,'88'),('35',0,'4',NULL,'3',0,'89'),('36',0,'4',NULL,'4',0,'90'),('37',0,'4',NULL,'5',0,'91'),('7',0,'5',NULL,'1',0,'92'),('38',0,'5',NULL,'2',0,'93'),('39',0,'5',NULL,'3',0,'94'),('40',0,'5',NULL,'4',0,'95'),('9',0,'9',NULL,'1',0,'96'),('41',0,'9',NULL,'2',0,'97'),('42',0,'9',NULL,'3',0,'98'),('43',0,'9',NULL,'4',0,'99'),('44',0,'9',NULL,'5',0,'100'),('10',0,'10',NULL,'1',0,'101'),('45',0,'10',NULL,'2',0,'102'),('46',0,'10',NULL,'3',0,'103'),('47',0,'10',NULL,'4',0,'104'),('48',0,'10',NULL,'5',0,'105'),('11',0,'11',NULL,'1',0,'106'),('49',0,'11',NULL,'2',0,'107'),('50',0,'11',NULL,'3',0,'108'),('51',0,'11',NULL,'4',0,'109'),('52',0,'11',NULL,'5',0,'110'),('24',0,'14',NULL,'3',0,'111'),('25',0,'14',NULL,'4',0,'112'),('53',0,'14',NULL,'3',0,'113'),('54',0,'14',NULL,'4',0,'114'),('19',0,'15',NULL,'1',0,'115'),('55',0,'15',NULL,'2',0,'116'),('56',0,'15',NULL,'3',0,'117'),('57',0,'15',NULL,'4',0,'118'),('20',0,'16',NULL,'1',0,'119'),('58',0,'16',NULL,'2',0,'120'),('59',0,'16',NULL,'3',0,'121'),('60',0,'16',NULL,'4',0,'122'),('61',0,'16',NULL,'5',0,'123'),('21',0,'17',NULL,'1',0,'124'),('62',0,'17',NULL,'2',0,'125'),('63',0,'17',NULL,'3',0,'126'),('64',0,'17',NULL,'4',0,'127'),('65',0,'17',NULL,'5',0,'128'),('22',0,'18',NULL,'1',0,'129'),('66',0,'18',NULL,'2',0,'130'),('67',0,'18',NULL,'3',0,'131'),('68',0,'18',NULL,'4',0,'132'),('23',0,'19',NULL,'1',0,'133'),('69',0,'19',NULL,'2',0,'134'),('70',0,'19',NULL,'3',0,'135'),('71',0,'19',NULL,'4',0,'136'),('72',0,'19',NULL,'5',0,'137');
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery_value_to_entity`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value_to_entity`;
CREATE TABLE `catalog_product_entity_media_gallery_value_to_entity` (
  `value_id` int(10) unsigned NOT NULL COMMENT 'Value media Entry ID',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Product entity ID',
  UNIQUE KEY `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_VAL_ID_ENTT_ID` (`value_id`,`entity_id`),
  KEY `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link Media value to Product entity table';

--
-- Dumping data for table `catalog_product_entity_media_gallery_value_to_entity`
--

LOCK TABLES `catalog_product_entity_media_gallery_value_to_entity` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value_to_entity` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_media_gallery_value_to_entity` VALUES ('3','2'),('4','2'),('5','3'),('6','4'),('7','5'),('9','9'),('10','10'),('11','11'),('19','15'),('20','16'),('21','17'),('22','18'),('23','19'),('24','14'),('25','14'),('26','2'),('27','2'),('28','2'),('29','3'),('30','3'),('31','3'),('32','3'),('33','3'),('34','4'),('35','4'),('36','4'),('37','4'),('38','5'),('39','5'),('40','5'),('41','9'),('42','9'),('43','9'),('44','9'),('45','10'),('46','10'),('47','10'),('48','10'),('49','11'),('50','11'),('51','11'),('52','11'),('53','14'),('54','14'),('55','15'),('56','15'),('57','15'),('58','16'),('59','16'),('60','16'),('61','16'),('62','17'),('63','17'),('64','17'),('65','17'),('66','18'),('67','18'),('68','18'),('69','19'),('70','19'),('71','19'),('72','19');
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value_to_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery_value_video`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value_video`;
CREATE TABLE `catalog_product_entity_media_gallery_value_video` (
  `value_id` int(10) unsigned NOT NULL COMMENT 'Media Entity ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `provider` varchar(32) DEFAULT NULL COMMENT 'Video provider ID',
  `url` text COMMENT 'Video URL',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `description` text COMMENT 'Page Meta Description',
  `metadata` text COMMENT 'Video meta data',
  UNIQUE KEY `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_VAL_ID_STORE_ID` (`value_id`,`store_id`),
  KEY `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_STORE_ID_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Video Table';

--
-- Table structure for table `catalog_product_entity_text`
--

DROP TABLE IF EXISTS `catalog_product_entity_text`;
CREATE TABLE `catalog_product_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TEXT_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Text Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_text`
--

LOCK TABLES `catalog_product_entity_text` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_text` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_text` VALUES ('2',85,0,'2','Accumsan Elit'),('3',85,0,'3','Nunc Facilisis'),('4',75,0,'4','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque libero. Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget veliNulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('5',85,0,'4','Etiam Gravida'),('6',85,0,'5','Donec Non Est'),('10',85,0,'9','Habitant'),('11',85,0,'10','Tempus'),('12',85,0,'11','Aliquam'),('15',85,0,'14','squa'),('16',75,0,'15','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue'),('17',76,0,'15','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('18',85,0,'15','Levis'),('19',85,0,'16','puma'),('20',75,0,'16','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue'),('21',76,0,'16','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('22',75,0,'17','<p>Text &amp; call online - free! ... Unlimited talk, text &amp; 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue</p>'),('23',76,0,'17','<p>Text &amp; call online - free! ... Unlimited talk, text &amp; 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk</p>'),('24',85,0,'17','adidas'),('25',85,0,'18','nike'),('26',75,0,'19','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('27',76,0,'19','<p>Text &amp; call online - free! ... Unlimited talk, text &amp; 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk</p>'),('28',85,0,'19','violet'),('29',75,0,'3','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('30',76,0,'3','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('31',75,0,'2','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('32',76,0,'2','<p>Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.</p>'),('33',75,0,'5','<p>Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.</p>'),('34',76,0,'5','<p>Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.</p>'),('35',75,0,'9','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('36',76,0,'9','<p>Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.</p>'),('37',75,0,'10','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('38',75,0,'11','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('39',76,0,'11','<p>Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.</p>'),('40',75,0,'14','<p>Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue</p>'),('41',75,0,'18','<p>Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.</p>'),('42',76,0,'10','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('43',76,0,'4','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. '),('44',76,0,'14','<p>Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue</p>'),('45',76,0,'18','<p>Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.</p>');
/*!40000 ALTER TABLE `catalog_product_entity_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_tier_price`
--

DROP TABLE IF EXISTS `catalog_product_entity_tier_price`;
CREATE TABLE `catalog_product_entity_tier_price` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `all_groups` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Applicable To All Customer Groups',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group ID',
  `qty` decimal(12,4) NOT NULL DEFAULT '1.0000' COMMENT 'QTY',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_E8AB433B9ACB00343ABB312AD2FAB087` (`entity_id`,`all_groups`,`customer_group_id`,`qty`,`website_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TIER_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TIER_PRICE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Tier Price Attribute Backend Table';

--
-- Table structure for table `catalog_product_entity_varchar`
--

DROP TABLE IF EXISTS `catalog_product_entity_varchar`;
CREATE TABLE `catalog_product_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Varchar Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_varchar`
--

LOCK TABLES `catalog_product_entity_varchar` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_varchar` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_varchar` VALUES ('16',73,0,'2','Accumsan Elit'),('17',84,0,'2','Accumsan Elit'),('18',86,0,'2','Accumsan Elit '),('19',87,0,'2','/2/_/2_1.jpg'),('20',88,0,'2','/2/_/2_1.jpg'),('21',89,0,'2','/2/_/2_1.jpg'),('22',106,0,'2','container2'),('23',119,0,'2','accumsan-elit'),('24',132,0,'2','/2/_/2_1.jpg'),('25',134,0,'2','0'),('26',141,0,'2','1'),('35',73,0,'3','Nunc Facilisis'),('36',84,0,'3','Nunc Facilisis'),('37',86,0,'3','Nunc Facilisis '),('38',87,0,'3','/3/_/3_1.jpg'),('39',88,0,'3','/3/_/3_1.jpg'),('40',89,0,'3','/3/_/3_1.jpg'),('41',106,0,'3','container2'),('42',119,0,'3','nunc-facilisis'),('43',132,0,'3','/3/_/3_1.jpg'),('44',134,0,'3','2'),('45',141,0,'3','1'),('46',73,0,'4','Etiam Gravida'),('47',84,0,'4','Etiam Gravida'),('48',86,0,'4','Etiam Gravida Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, '),('49',106,0,'4','container2'),('50',119,0,'4','etiam-gravida'),('51',134,0,'4','2'),('52',141,0,'4','1'),('53',87,0,'4','/5/_/5_1__2.jpg'),('54',88,0,'4','/5/_/5_1__2.jpg'),('55',89,0,'4','/5/_/5_1__2.jpg'),('56',132,0,'4','/5/_/5_1__2.jpg'),('61',73,0,'5','Donec Non Est'),('62',84,0,'5','Donec Non Est'),('63',86,0,'5','Donec Non Est '),('64',87,0,'5','/i/m/img02.jpg'),('65',88,0,'5','/i/m/img02.jpg'),('66',89,0,'5','/i/m/img02.jpg'),('67',106,0,'5','container2'),('68',119,0,'5','donec-non-est'),('69',132,0,'5','/i/m/img02.jpg'),('70',134,0,'5','2'),('71',141,0,'5','1'),('113',73,0,'9','Habitant'),('114',84,0,'9','Habitant'),('115',86,0,'9','Habitant '),('116',87,0,'9','/6/_/6.jpg'),('117',88,0,'9','/6/_/6.jpg'),('118',89,0,'9','/6/_/6.jpg'),('119',106,0,'9','container2'),('120',119,0,'9','habitant'),('121',132,0,'9','/6/_/6.jpg'),('122',134,0,'9','2'),('123',141,0,'9','1'),('128',73,0,'10','Tempus'),('129',84,0,'10','Tempus'),('130',86,0,'10','Tempus '),('131',87,0,'10','/i/m/img01.jpg'),('132',88,0,'10','/i/m/img01.jpg'),('133',89,0,'10','/i/m/img01.jpg'),('134',106,0,'10','container2'),('135',119,0,'10','tempus'),('136',132,0,'10','/i/m/img01.jpg'),('137',134,0,'10','2'),('138',141,0,'10','1'),('143',73,0,'11','Aliquam'),('144',84,0,'11','Aliquam'),('145',86,0,'11','Aliquam '),('146',87,0,'11','/i/m/img03_1.jpg'),('147',88,0,'11','/i/m/img03_1.jpg'),('148',89,0,'11','/i/m/img03_1.jpg'),('149',106,0,'11','container2'),('150',119,0,'11','aliquam'),('151',132,0,'11','/i/m/img03_1.jpg'),('152',134,0,'11','2'),('153',141,0,'11','1'),('188',73,0,'14','squa'),('189',84,0,'14','squa'),('190',86,0,'14','squa '),('191',87,0,'14','/2/_/2_2_1.jpg'),('192',88,0,'14','/2/_/2_2_1.jpg'),('193',89,0,'14','/2/_/2_2_1.jpg'),('194',106,0,'14','container2'),('195',119,0,'14','squa'),('196',132,0,'14','/2/_/2_2_1.jpg'),('197',134,0,'14','2'),('198',141,0,'14','1'),('199',73,0,'15','Levis'),('200',84,0,'15','Levis'),('201',86,0,'15','Levis Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('202',87,0,'15','/6/_/6_1.jpg'),('203',88,0,'15','/6/_/6_1.jpg'),('204',89,0,'15','/6/_/6_1.jpg'),('205',106,0,'15','container2'),('206',119,0,'15','levis'),('207',132,0,'15','/6/_/6_1.jpg'),('208',134,0,'15','2'),('209',141,0,'15','1'),('214',73,0,'16','puma'),('215',84,0,'16','puma'),('216',86,0,'16','puma '),('217',87,0,'16','/i/m/img01_1.jpg'),('218',88,0,'16','/i/m/img01_1.jpg'),('219',89,0,'16','/i/m/img01_1.jpg'),('220',106,0,'16','container2'),('221',119,0,'16','puma'),('222',132,0,'16','/i/m/img01_1.jpg'),('223',134,0,'16','2'),('224',141,0,'16','1'),('233',73,0,'17','adidas'),('234',84,0,'17','adidas'),('235',86,0,'17','adidas Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('236',87,0,'17','/6/_/6_2.jpg'),('237',88,0,'17','/6/_/6_2.jpg'),('238',89,0,'17','/6/_/6_2.jpg'),('239',106,0,'17','container2'),('240',119,0,'17','adidas'),('241',132,0,'17','/6/_/6_2.jpg'),('242',134,0,'17','2'),('243',141,0,'17','1'),('248',73,0,'18','nike'),('249',84,0,'18','nike'),('250',86,0,'18','nike '),('251',87,0,'18','/i/m/img01_2.jpg'),('252',88,0,'18','/i/m/img01_2.jpg'),('253',89,0,'18','/i/m/img01_2.jpg'),('254',106,0,'18','container2'),('255',119,0,'18','nike'),('256',132,0,'18','/i/m/img01_2.jpg'),('257',134,0,'18','2'),('258',141,0,'18','1'),('263',73,0,'19','violet'),('264',84,0,'19','violet'),('265',86,0,'19','violet Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('266',87,0,'19','/2/_/2_1_1.jpg'),('267',88,0,'19','/2/_/2_1_1.jpg'),('268',89,0,'19','/2/_/2_1_1.jpg'),('269',106,0,'19','container2'),('270',119,0,'19','violet'),('271',132,0,'19','/2/_/2_1_1.jpg'),('272',134,0,'19','2'),('273',141,0,'19','1');
/*!40000 ALTER TABLE `catalog_product_entity_varchar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav`
--

DROP TABLE IF EXISTS `catalog_product_index_eav`;
CREATE TABLE `catalog_product_index_eav` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Index Table';

--
-- Dumping data for table `catalog_product_index_eav`
--

LOCK TABLES `catalog_product_index_eav` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_eav` DISABLE KEYS */;
INSERT INTO `catalog_product_index_eav` VALUES ('2',83,1,'11'),('2',83,2,'11'),('2',83,3,'11'),('2',83,4,'11'),('2',93,1,'5'),('2',93,2,'5'),('2',93,3,'5'),('2',93,4,'5'),('3',83,1,'11'),('3',83,2,'11'),('3',83,3,'11'),('3',83,4,'11'),('3',93,1,'4'),('3',93,2,'4'),('3',93,3,'4'),('3',93,4,'4'),('4',83,1,'9'),('4',83,2,'9'),('4',83,3,'9'),('4',83,4,'9'),('4',93,1,'4'),('4',93,2,'4'),('4',93,3,'4'),('4',93,4,'4'),('10',83,1,'9'),('10',83,2,'9'),('10',83,3,'9'),('10',83,4,'9'),('10',93,1,'4'),('10',93,2,'4'),('10',93,3,'4'),('10',93,4,'4'),('14',83,1,'9'),('14',83,2,'9'),('14',83,3,'9'),('14',83,4,'9'),('14',93,1,'4'),('14',93,2,'4'),('14',93,3,'4'),('14',93,4,'4');
/*!40000 ALTER TABLE `catalog_product_index_eav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav_decimal`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal`;
CREATE TABLE `catalog_product_index_eav_decimal` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Index Table';

--
-- Table structure for table `catalog_product_index_eav_decimal_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal_idx`;
CREATE TABLE `catalog_product_index_eav_decimal_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Indexer Index Table';

--
-- Table structure for table `catalog_product_index_eav_decimal_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal_tmp`;
CREATE TABLE `catalog_product_index_eav_decimal_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Indexer Temp Table';

--
-- Table structure for table `catalog_product_index_eav_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_idx`;
CREATE TABLE `catalog_product_index_eav_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Indexer Index Table';

--
-- Dumping data for table `catalog_product_index_eav_idx`
--

LOCK TABLES `catalog_product_index_eav_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_eav_idx` DISABLE KEYS */;
INSERT INTO `catalog_product_index_eav_idx` VALUES ('2',83,1,'11'),('2',83,2,'11'),('2',83,3,'11'),('2',83,4,'11'),('2',93,1,'5'),('2',93,2,'5'),('2',93,3,'5'),('2',93,4,'5'),('3',83,1,'11'),('3',83,2,'11'),('3',83,3,'11'),('3',83,4,'11'),('3',93,1,'4'),('3',93,2,'4'),('3',93,3,'4'),('3',93,4,'4'),('4',83,1,'9'),('4',83,2,'9'),('4',83,3,'9'),('4',83,4,'9'),('4',93,1,'4'),('4',93,2,'4'),('4',93,3,'4'),('4',93,4,'4'),('10',83,1,'9'),('10',83,2,'9'),('10',83,3,'9'),('10',83,4,'9'),('10',93,1,'4'),('10',93,2,'4'),('10',93,3,'4'),('10',93,4,'4'),('14',83,1,'9'),('14',83,2,'9'),('14',83,3,'9'),('14',83,4,'9'),('14',93,1,'4'),('14',93,2,'4'),('14',93,3,'4'),('14',93,4,'4');
/*!40000 ALTER TABLE `catalog_product_index_eav_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_tmp`;
CREATE TABLE `catalog_product_index_eav_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Indexer Temp Table';

--
-- Table structure for table `catalog_product_index_price`
--

DROP TABLE IF EXISTS `catalog_product_index_price`;
CREATE TABLE `catalog_product_index_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_MIN_PRICE` (`min_price`),
  KEY `CAT_PRD_IDX_PRICE_WS_ID_CSTR_GROUP_ID_MIN_PRICE` (`website_id`,`customer_group_id`,`min_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Index Table';

--
-- Dumping data for table `catalog_product_index_price`
--

LOCK TABLES `catalog_product_index_price` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price` DISABLE KEYS */;
INSERT INTO `catalog_product_index_price` VALUES ('2',0,1,2,'190.0000','150.0000','150.0000','150.0000',NULL),('2',1,1,2,'190.0000','150.0000','150.0000','150.0000',NULL),('2',2,1,2,'190.0000','150.0000','150.0000','150.0000',NULL),('2',3,1,2,'190.0000','150.0000','150.0000','150.0000',NULL),('3',0,1,2,'170.0000','170.0000','170.0000','170.0000',NULL),('3',1,1,2,'170.0000','170.0000','170.0000','170.0000',NULL),('3',2,1,2,'170.0000','170.0000','170.0000','170.0000',NULL),('3',3,1,2,'170.0000','170.0000','170.0000','170.0000',NULL),('4',0,1,2,'65.0000','60.0000','60.0000','60.0000',NULL),('4',1,1,2,'65.0000','60.0000','60.0000','60.0000',NULL),('4',2,1,2,'65.0000','60.0000','60.0000','60.0000',NULL),('4',3,1,2,'65.0000','60.0000','60.0000','60.0000',NULL),('5',0,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('5',1,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('5',2,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('5',3,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('9',0,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('9',1,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('9',2,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('9',3,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('10',0,1,2,'80.0000','70.0000','70.0000','70.0000',NULL),('10',1,1,2,'80.0000','70.0000','70.0000','70.0000',NULL),('10',2,1,2,'80.0000','70.0000','70.0000','70.0000',NULL),('10',3,1,2,'80.0000','70.0000','70.0000','70.0000',NULL),('11',0,1,2,'70.0000','60.0000','60.0000','60.0000',NULL),('11',1,1,2,'70.0000','60.0000','60.0000','60.0000',NULL),('11',2,1,2,'70.0000','60.0000','60.0000','60.0000',NULL),('11',3,1,2,'70.0000','60.0000','60.0000','60.0000',NULL),('14',0,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('14',1,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('14',2,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('14',3,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('15',0,1,2,'80.0000','60.0000','60.0000','60.0000',NULL),('15',1,1,2,'80.0000','60.0000','60.0000','60.0000',NULL),('15',2,1,2,'80.0000','60.0000','60.0000','60.0000',NULL),('15',3,1,2,'80.0000','60.0000','60.0000','60.0000',NULL),('16',0,1,2,'110.0000','80.0000','80.0000','80.0000',NULL),('16',1,1,2,'110.0000','80.0000','80.0000','80.0000',NULL),('16',2,1,2,'110.0000','80.0000','80.0000','80.0000',NULL),('16',3,1,2,'110.0000','80.0000','80.0000','80.0000',NULL),('17',0,1,2,'150.0000','100.0000','100.0000','100.0000',NULL),('17',1,1,2,'150.0000','100.0000','100.0000','100.0000',NULL),('17',2,1,2,'150.0000','100.0000','100.0000','100.0000',NULL),('17',3,1,2,'150.0000','100.0000','100.0000','100.0000',NULL),('18',0,1,2,'205.0000','150.0000','150.0000','150.0000',NULL),('18',1,1,2,'205.0000','150.0000','150.0000','150.0000',NULL),('18',2,1,2,'205.0000','150.0000','150.0000','150.0000',NULL),('18',3,1,2,'205.0000','150.0000','150.0000','150.0000',NULL),('19',0,1,2,'50.0000','30.0000','30.0000','30.0000',NULL),('19',1,1,2,'50.0000','30.0000','30.0000','30.0000',NULL),('19',2,1,2,'50.0000','30.0000','30.0000','30.0000',NULL),('19',3,1,2,'50.0000','30.0000','30.0000','30.0000',NULL);
/*!40000 ALTER TABLE `catalog_product_index_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_bundle_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_idx`;
CREATE TABLE `catalog_product_index_price_bundle_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class Id',
  `price_type` smallint(5) unsigned NOT NULL COMMENT 'Price Type',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'Special Price',
  `tier_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tier Percent',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Orig Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Idx';

--
-- Table structure for table `catalog_product_index_price_bundle_opt_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_opt_idx`;
CREATE TABLE `catalog_product_index_price_bundle_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `alt_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `alt_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Opt Idx';

--
-- Table structure for table `catalog_product_index_price_bundle_opt_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_opt_tmp`;
CREATE TABLE `catalog_product_index_price_bundle_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `alt_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `alt_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Opt Tmp';

--
-- Table structure for table `catalog_product_index_price_bundle_sel_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_sel_idx`;
CREATE TABLE `catalog_product_index_price_bundle_sel_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Id',
  `group_type` smallint(5) unsigned DEFAULT '0' COMMENT 'Group Type',
  `is_required` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Required',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Sel Idx';

--
-- Table structure for table `catalog_product_index_price_bundle_sel_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_sel_tmp`;
CREATE TABLE `catalog_product_index_price_bundle_sel_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Id',
  `group_type` smallint(5) unsigned DEFAULT '0' COMMENT 'Group Type',
  `is_required` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Required',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Sel Tmp';

--
-- Table structure for table `catalog_product_index_price_bundle_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_tmp`;
CREATE TABLE `catalog_product_index_price_bundle_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class Id',
  `price_type` smallint(5) unsigned NOT NULL COMMENT 'Price Type',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'Special Price',
  `tier_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tier Percent',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Orig Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Tmp';

--
-- Table structure for table `catalog_product_index_price_cfg_opt_agr_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_agr_idx`;
CREATE TABLE `catalog_product_index_price_cfg_opt_agr_idx` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Aggregate Index Table';

--
-- Table structure for table `catalog_product_index_price_cfg_opt_agr_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_agr_tmp`;
CREATE TABLE `catalog_product_index_price_cfg_opt_agr_tmp` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Aggregate Temp Table';

--
-- Table structure for table `catalog_product_index_price_cfg_opt_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_idx`;
CREATE TABLE `catalog_product_index_price_cfg_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Index Table';

--
-- Table structure for table `catalog_product_index_price_cfg_opt_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_tmp`;
CREATE TABLE `catalog_product_index_price_cfg_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Temp Table';

--
-- Table structure for table `catalog_product_index_price_downlod_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_downlod_idx`;
CREATE TABLE `catalog_product_index_price_downlod_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Minimum price',
  `max_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Maximum price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Indexer Table for price of downloadable products';

--
-- Table structure for table `catalog_product_index_price_downlod_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_downlod_tmp`;
CREATE TABLE `catalog_product_index_price_downlod_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Minimum price',
  `max_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Maximum price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Temporary Indexer Table for price of downloadable products';

--
-- Table structure for table `catalog_product_index_price_final_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_final_idx`;
CREATE TABLE `catalog_product_index_price_final_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Final Index Table';

--
-- Table structure for table `catalog_product_index_price_final_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_final_tmp`;
CREATE TABLE `catalog_product_index_price_final_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Final Temp Table';

--
-- Table structure for table `catalog_product_index_price_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_idx`;
CREATE TABLE `catalog_product_index_price_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_MIN_PRICE` (`min_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Index Table';

--
-- Dumping data for table `catalog_product_index_price_idx`
--

LOCK TABLES `catalog_product_index_price_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_idx` DISABLE KEYS */;
INSERT INTO `catalog_product_index_price_idx` VALUES ('2',0,1,2,'190.0000','150.0000','150.0000','150.0000',NULL),('2',1,1,2,'190.0000','150.0000','150.0000','150.0000',NULL),('2',2,1,2,'190.0000','150.0000','150.0000','150.0000',NULL),('2',3,1,2,'190.0000','150.0000','150.0000','150.0000',NULL),('3',0,1,2,'170.0000','170.0000','170.0000','170.0000',NULL),('3',1,1,2,'170.0000','170.0000','170.0000','170.0000',NULL),('3',2,1,2,'170.0000','170.0000','170.0000','170.0000',NULL),('3',3,1,2,'170.0000','170.0000','170.0000','170.0000',NULL),('4',0,1,2,'65.0000','60.0000','60.0000','60.0000',NULL),('4',1,1,2,'65.0000','60.0000','60.0000','60.0000',NULL),('4',2,1,2,'65.0000','60.0000','60.0000','60.0000',NULL),('4',3,1,2,'65.0000','60.0000','60.0000','60.0000',NULL),('5',0,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('5',1,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('5',2,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('5',3,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('9',0,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('9',1,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('9',2,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('9',3,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('10',0,1,2,'80.0000','70.0000','70.0000','70.0000',NULL),('10',1,1,2,'80.0000','70.0000','70.0000','70.0000',NULL),('10',2,1,2,'80.0000','70.0000','70.0000','70.0000',NULL),('10',3,1,2,'80.0000','70.0000','70.0000','70.0000',NULL),('11',0,1,2,'70.0000','60.0000','60.0000','60.0000',NULL),('11',1,1,2,'70.0000','60.0000','60.0000','60.0000',NULL),('11',2,1,2,'70.0000','60.0000','60.0000','60.0000',NULL),('11',3,1,2,'70.0000','60.0000','60.0000','60.0000',NULL),('14',0,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('14',1,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('14',2,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('14',3,1,2,'70.0000','50.0000','50.0000','50.0000',NULL),('15',0,1,2,'80.0000','60.0000','60.0000','60.0000',NULL),('15',1,1,2,'80.0000','60.0000','60.0000','60.0000',NULL),('15',2,1,2,'80.0000','60.0000','60.0000','60.0000',NULL),('15',3,1,2,'80.0000','60.0000','60.0000','60.0000',NULL),('16',0,1,2,'110.0000','80.0000','80.0000','80.0000',NULL),('16',1,1,2,'110.0000','80.0000','80.0000','80.0000',NULL),('16',2,1,2,'110.0000','80.0000','80.0000','80.0000',NULL),('16',3,1,2,'110.0000','80.0000','80.0000','80.0000',NULL),('17',0,1,2,'150.0000','100.0000','100.0000','100.0000',NULL),('17',1,1,2,'150.0000','100.0000','100.0000','100.0000',NULL),('17',2,1,2,'150.0000','100.0000','100.0000','100.0000',NULL),('17',3,1,2,'150.0000','100.0000','100.0000','100.0000',NULL),('18',0,1,2,'205.0000','150.0000','150.0000','150.0000',NULL),('18',1,1,2,'205.0000','150.0000','150.0000','150.0000',NULL),('18',2,1,2,'205.0000','150.0000','150.0000','150.0000',NULL),('18',3,1,2,'205.0000','150.0000','150.0000','150.0000',NULL),('19',0,1,2,'50.0000','30.0000','30.0000','30.0000',NULL),('19',1,1,2,'50.0000','30.0000','30.0000','30.0000',NULL),('19',2,1,2,'50.0000','30.0000','30.0000','30.0000',NULL),('19',3,1,2,'50.0000','30.0000','30.0000','30.0000',NULL);
/*!40000 ALTER TABLE `catalog_product_index_price_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_opt_agr_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_agr_idx`;
CREATE TABLE `catalog_product_index_price_opt_agr_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Aggregate Index Table';

--
-- Table structure for table `catalog_product_index_price_opt_agr_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_agr_tmp`;
CREATE TABLE `catalog_product_index_price_opt_agr_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Aggregate Temp Table';

--
-- Table structure for table `catalog_product_index_price_opt_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_idx`;
CREATE TABLE `catalog_product_index_price_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Index Table';

--
-- Table structure for table `catalog_product_index_price_opt_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_tmp`;
CREATE TABLE `catalog_product_index_price_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Temp Table';

--
-- Table structure for table `catalog_product_index_price_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_tmp`;
CREATE TABLE `catalog_product_index_price_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_MIN_PRICE` (`min_price`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Temp Table';

--
-- Table structure for table `catalog_product_index_tier_price`
--

DROP TABLE IF EXISTS `catalog_product_index_tier_price`;
CREATE TABLE `catalog_product_index_tier_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_TIER_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_TIER_PRICE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Tier Price Index Table';

--
-- Table structure for table `catalog_product_index_website`
--

DROP TABLE IF EXISTS `catalog_product_index_website`;
CREATE TABLE `catalog_product_index_website` (
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `website_date` date DEFAULT NULL COMMENT 'Website Date',
  `rate` float DEFAULT '1' COMMENT 'Rate',
  PRIMARY KEY (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_WEBSITE_WEBSITE_DATE` (`website_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Website Index Table';

--
-- Dumping data for table `catalog_product_index_website`
--

LOCK TABLES `catalog_product_index_website` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_website` DISABLE KEYS */;
INSERT INTO `catalog_product_index_website` VALUES (1,'2016-10-17','1');
/*!40000 ALTER TABLE `catalog_product_index_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link`
--

DROP TABLE IF EXISTS `catalog_product_link`;
CREATE TABLE `catalog_product_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `linked_product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Linked Product ID',
  `link_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Link Type ID',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `CATALOG_PRODUCT_LINK_LINK_TYPE_ID_PRODUCT_ID_LINKED_PRODUCT_ID` (`link_type_id`,`product_id`,`linked_product_id`),
  KEY `CATALOG_PRODUCT_LINK_PRODUCT_ID` (`product_id`),
  KEY `CATALOG_PRODUCT_LINK_LINKED_PRODUCT_ID` (`linked_product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Product Linkage Table';

--
-- Dumping data for table `catalog_product_link`
--

LOCK TABLES `catalog_product_link` WRITE;
/*!40000 ALTER TABLE `catalog_product_link` DISABLE KEYS */;
INSERT INTO `catalog_product_link` VALUES ('25','2','3',1),('26','2','4',1),('27','2','5',1),('28','2','9',1),('29','2','10',1),('30','2','11',1),('31','2','14',1),('32','2','15',1),('33','2','16',1),('34','2','17',1),('35','2','18',1),('36','2','19',1),('49','3','2',1),('50','3','4',1),('51','3','5',1),('53','3','9',1),('52','3','10',1),('54','3','11',1),('67','4','2',1),('68','4','3',1),('69','4','5',1),('70','4','9',1),('71','4','10',1),('79','5','9',1),('80','5','10',1),('81','5','11',1),('82','5','14',1),('83','5','15',1),('92','9','2',1),('93','9','3',1),('94','9','4',1),('95','9','5',1),('96','9','10',1),('97','9','11',1),('106','10','5',1),('107','10','9',1),('108','10','11',1),('109','10','14',1),('110','10','15',1),('111','10','16',1),('124','11','4',1),('125','11','5',1),('126','11','9',1),('127','11','10',1),('128','11','14',1),('137','14','4',1),('138','14','5',1),('139','14','9',1),('140','14','10',1),('141','14','11',1),('148','15','5',1),('147','15','9',1),('149','15','10',1),('150','15','11',1),('158','16','4',1),('159','16','5',1),('160','16','9',1),('162','16','10',1),('161','16','11',1),('171','17','4',1),('172','17','5',1),('173','17','9',1),('174','17','10',1),('183','18','5',1),('184','18','9',1),('185','18','10',1),('186','18','11',1),('195','19','4',1),('196','19','5',1),('197','19','9',1),('198','19','10',1),('37','2','3',4),('38','2','4',4),('39','2','5',4),('40','2','9',4),('41','2','10',4),('42','2','11',4),('43','2','14',4),('44','2','15',4),('45','2','16',4),('46','2','17',4),('47','2','18',4),('48','2','19',4),('55','3','2',4),('56','3','4',4),('57','3','5',4),('58','3','9',4),('59','3','10',4),('60','3','11',4),('61','3','14',4),('62','3','15',4),('63','3','16',4),('64','3','17',4),('65','3','18',4),('66','3','19',4),('72','4','2',4),('73','4','3',4),('74','4','5',4),('75','4','9',4),('76','4','10',4),('77','4','11',4),('78','4','14',4),('84','5','3',4),('85','5','4',4),('86','5','9',4),('87','5','10',4),('88','5','11',4),('89','5','14',4),('90','5','15',4),('91','5','16',4),('98','9','2',4),('99','9','3',4),('100','9','4',4),('101','9','5',4),('102','9','10',4),('103','9','11',4),('104','9','14',4),('105','9','15',4),('112','10','2',4),('113','10','3',4),('114','10','4',4),('115','10','5',4),('116','10','9',4),('117','10','11',4),('118','10','14',4),('119','10','15',4),('120','10','16',4),('121','10','17',4),('122','10','18',4),('123','10','19',4),('130','11','2',4),('131','11','3',4),('129','11','4',4),('132','11','5',4),('133','11','9',4),('134','11','10',4),('135','11','14',4),('136','11','15',4),('144','14','4',4),('142','14','5',4),('143','14','9',4),('145','14','10',4),('146','14','11',4),('151','15','4',4),('152','15','5',4),('153','15','9',4),('154','15','10',4),('155','15','11',4),('156','15','14',4),('157','15','16',4),('163','16','4',4),('164','16','5',4),('165','16','9',4),('166','16','10',4),('169','16','11',4),('167','16','14',4),('168','16','15',4),('170','16','17',4),('175','17','4',4),('176','17','5',4),('178','17','9',4),('177','17','10',4),('179','17','11',4),('180','17','14',4),('181','17','15',4),('182','17','16',4),('187','18','4',4),('188','18','5',4),('189','18','9',4),('190','18','10',4),('191','18','11',4),('192','18','14',4),('193','18','15',4),('194','18','16',4),('200','19','4',4),('199','19','5',4),('201','19','9',4),('202','19','10',4),('203','19','11',4),('204','19','15',4),('205','19','16',4);
/*!40000 ALTER TABLE `catalog_product_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link_attribute`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute`;
CREATE TABLE `catalog_product_link_attribute` (
  `product_link_attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product Link Attribute ID',
  `link_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Link Type ID',
  `product_link_attribute_code` varchar(32) DEFAULT NULL COMMENT 'Product Link Attribute Code',
  `data_type` varchar(32) DEFAULT NULL COMMENT 'Data Type',
  PRIMARY KEY (`product_link_attribute_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_LINK_TYPE_ID` (`link_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Attribute Table';

--
-- Dumping data for table `catalog_product_link_attribute`
--

LOCK TABLES `catalog_product_link_attribute` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_attribute` DISABLE KEYS */;
INSERT INTO `catalog_product_link_attribute` VALUES (1,1,'position','int'),(2,4,'position','int'),(3,5,'position','int'),(4,3,'position','int'),(5,3,'qty','decimal');
/*!40000 ALTER TABLE `catalog_product_link_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link_attribute_decimal`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_decimal`;
CREATE TABLE `catalog_product_link_attribute_decimal` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_DEC_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_DECIMAL_LINK_ID` (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Decimal Attribute Table';

--
-- Table structure for table `catalog_product_link_attribute_int`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_int`;
CREATE TABLE `catalog_product_link_attribute_int` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_INT_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_INT_LINK_ID` (`link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Integer Attribute Table';

--
-- Dumping data for table `catalog_product_link_attribute_int`
--

LOCK TABLES `catalog_product_link_attribute_int` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_attribute_int` DISABLE KEYS */;
INSERT INTO `catalog_product_link_attribute_int` VALUES ('25',1,'25','1'),('26',1,'26','2'),('27',1,'27','3'),('28',1,'28','4'),('29',1,'29','5'),('30',1,'30','6'),('31',1,'31','7'),('32',1,'32','8'),('33',1,'33','9'),('34',1,'34','10'),('35',1,'35','11'),('36',1,'36','12'),('37',2,'37','12'),('38',2,'38','11'),('39',2,'39','10'),('40',2,'40','9'),('41',2,'41','8'),('42',2,'42','7'),('43',2,'43','6'),('44',2,'44','5'),('45',2,'45','4'),('46',2,'46','3'),('47',2,'47','2'),('48',2,'48','1'),('49',1,'49','1'),('50',1,'50','2'),('51',1,'51','3'),('52',1,'52','4'),('53',1,'53','5'),('54',1,'54','6'),('55',2,'55','1'),('56',2,'56','2'),('57',2,'57','3'),('58',2,'58','4'),('59',2,'59','5'),('60',2,'60','6'),('61',2,'61','7'),('62',2,'62','8'),('63',2,'63','9'),('64',2,'64','10'),('65',2,'65','11'),('66',2,'66','12'),('67',1,'67','1'),('68',1,'68','2'),('69',1,'69','3'),('70',1,'70','4'),('71',1,'71','5'),('72',2,'72','1'),('73',2,'73','2'),('74',2,'74','3'),('75',2,'75','4'),('76',2,'76','5'),('77',2,'77','6'),('78',2,'78','7'),('79',1,'79','1'),('80',1,'80','2'),('81',1,'81','3'),('82',1,'82','4'),('83',1,'83','5'),('84',2,'84','1'),('85',2,'85','2'),('86',2,'86','3'),('87',2,'87','4'),('88',2,'88','5'),('89',2,'89','6'),('90',2,'90','7'),('91',2,'91','8'),('92',1,'92','1'),('93',1,'93','2'),('94',1,'94','3'),('95',1,'95','4'),('96',1,'96','5'),('97',1,'97','6'),('98',2,'98','1'),('99',2,'99','2'),('100',2,'100','3'),('101',2,'101','4'),('102',2,'102','5'),('103',2,'103','6'),('104',2,'104','7'),('105',2,'105','8'),('106',1,'106','1'),('107',1,'107','2'),('108',1,'108','3'),('109',1,'109','4'),('110',1,'110','5'),('111',1,'111','6'),('112',2,'112','1'),('113',2,'113','2'),('114',2,'114','3'),('115',2,'115','4'),('116',2,'116','5'),('117',2,'117','6'),('118',2,'118','7'),('119',2,'119','8'),('120',2,'120','9'),('121',2,'121','10'),('122',2,'122','11'),('123',2,'123','12'),('124',1,'124','1'),('125',1,'125','2'),('126',1,'126','3'),('127',1,'127','4'),('128',1,'128','5'),('129',2,'129','1'),('130',2,'130','2'),('131',2,'131','3'),('132',2,'132','4'),('133',2,'133','5'),('134',2,'134','6'),('135',2,'135','7'),('136',2,'136','8'),('137',1,'137','1'),('138',1,'138','2'),('139',1,'139','3'),('140',1,'140','4'),('141',1,'141','5'),('142',2,'142','1'),('143',2,'143','2'),('144',2,'144','3'),('145',2,'145','4'),('146',2,'146','5'),('147',1,'147','1'),('148',1,'148','2'),('149',1,'149','3'),('150',1,'150','4'),('151',2,'151','1'),('152',2,'152','2'),('153',2,'153','3'),('154',2,'154','4'),('155',2,'155','5'),('156',2,'156','6'),('157',2,'157','7'),('158',1,'158','1'),('159',1,'159','2'),('160',1,'160','3'),('161',1,'161','4'),('162',1,'162','5'),('163',2,'163','1'),('164',2,'164','2'),('165',2,'165','3'),('166',2,'166','4'),('167',2,'167','5'),('168',2,'168','6'),('169',2,'169','7'),('170',2,'170','8'),('171',1,'171','1'),('172',1,'172','2'),('173',1,'173','3'),('174',1,'174','4'),('175',2,'175','1'),('176',2,'176','2'),('177',2,'177','3'),('178',2,'178','4'),('179',2,'179','5'),('180',2,'180','6'),('181',2,'181','7'),('182',2,'182','8'),('183',1,'183','1'),('184',1,'184','2'),('185',1,'185','3'),('186',1,'186','4'),('187',2,'187','1'),('188',2,'188','2'),('189',2,'189','3'),('190',2,'190','4'),('191',2,'191','5'),('192',2,'192','6'),('193',2,'193','7'),('194',2,'194','8'),('195',1,'195','1'),('196',1,'196','2'),('197',1,'197','3'),('198',1,'198','4'),('199',2,'199','1'),('200',2,'200','2'),('201',2,'201','3'),('202',2,'202','4'),('203',2,'203','5'),('204',2,'204','6'),('205',2,'205','7');
/*!40000 ALTER TABLE `catalog_product_link_attribute_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link_attribute_varchar`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_varchar`;
CREATE TABLE `catalog_product_link_attribute_varchar` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_VCHR_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_VARCHAR_LINK_ID` (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Varchar Attribute Table';

--
-- Table structure for table `catalog_product_link_type`
--

DROP TABLE IF EXISTS `catalog_product_link_type`;
CREATE TABLE `catalog_product_link_type` (
  `link_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Type ID',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  PRIMARY KEY (`link_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Type Table';

--
-- Dumping data for table `catalog_product_link_type`
--

LOCK TABLES `catalog_product_link_type` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_type` DISABLE KEYS */;
INSERT INTO `catalog_product_link_type` VALUES (1,'relation'),(3,'super'),(4,'up_sell'),(5,'cross_sell');
/*!40000 ALTER TABLE `catalog_product_link_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_option`
--

DROP TABLE IF EXISTS `catalog_product_option`;
CREATE TABLE `catalog_product_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `type` varchar(50) DEFAULT NULL COMMENT 'Type',
  `is_require` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Required',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `max_characters` int(10) unsigned DEFAULT NULL COMMENT 'Max Characters',
  `file_extension` varchar(50) DEFAULT NULL COMMENT 'File Extension',
  `image_size_x` smallint(5) unsigned DEFAULT NULL COMMENT 'Image Size X',
  `image_size_y` smallint(5) unsigned DEFAULT NULL COMMENT 'Image Size Y',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_id`),
  KEY `CATALOG_PRODUCT_OPTION_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Table';

--
-- Table structure for table `catalog_product_option_price`
--

DROP TABLE IF EXISTS `catalog_product_option_price`;
CREATE TABLE `catalog_product_option_price` (
  `option_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Price ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `price_type` varchar(7) NOT NULL DEFAULT 'fixed' COMMENT 'Price Type',
  PRIMARY KEY (`option_price_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_PRICE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_PRICE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Price Table';

--
-- Table structure for table `catalog_product_option_title`
--

DROP TABLE IF EXISTS `catalog_product_option_title`;
CREATE TABLE `catalog_product_option_title` (
  `option_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Title ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`option_title_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TITLE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Title Table';

--
-- Table structure for table `catalog_product_option_type_price`
--

DROP TABLE IF EXISTS `catalog_product_option_type_price`;
CREATE TABLE `catalog_product_option_type_price` (
  `option_type_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type Price ID',
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Type ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `price_type` varchar(7) NOT NULL DEFAULT 'fixed' COMMENT 'Price Type',
  PRIMARY KEY (`option_type_price_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TYPE_PRICE_OPTION_TYPE_ID_STORE_ID` (`option_type_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Price Table';

--
-- Table structure for table `catalog_product_option_type_title`
--

DROP TABLE IF EXISTS `catalog_product_option_type_title`;
CREATE TABLE `catalog_product_option_type_title` (
  `option_type_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type Title ID',
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Type ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`option_type_title_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TYPE_TITLE_OPTION_TYPE_ID_STORE_ID` (`option_type_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Title Table';

--
-- Table structure for table `catalog_product_option_type_value`
--

DROP TABLE IF EXISTS `catalog_product_option_type_value`;
CREATE TABLE `catalog_product_option_type_value` (
  `option_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_type_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_VALUE_OPTION_ID` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Value Table';

--
-- Table structure for table `catalog_product_price_cl`
--

DROP TABLE IF EXISTS `catalog_product_price_cl`;
CREATE TABLE `catalog_product_price_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=336 DEFAULT CHARSET=utf8 COMMENT='catalog_product_price_cl';

--
-- Dumping data for table `catalog_product_price_cl`
--

LOCK TABLES `catalog_product_price_cl` WRITE;
/*!40000 ALTER TABLE `catalog_product_price_cl` DISABLE KEYS */;
INSERT INTO `catalog_product_price_cl` VALUES ('1','1'),('2','1'),('3','1'),('4','1'),('5','1'),('6','1'),('7','1'),('8','1'),('9','2'),('10','2'),('11','2'),('12','2'),('13','2'),('14','2'),('15','2'),('16','2'),('17','2'),('18','2'),('19','2'),('20','2'),('21','2'),('22','2'),('23','2'),('24','2'),('25','2'),('26','3'),('27','3'),('28','3'),('29','3'),('30','3'),('31','3'),('32','3'),('33','3'),('34','3'),('35','3'),('36','3'),('37','3'),('38','3'),('39','4'),('40','4'),('41','4'),('42','4'),('43','4'),('44','4'),('45','4'),('46','4'),('47','4'),('48','4'),('49','4'),('50','4'),('51','4'),('52','4'),('53','4'),('54','5'),('55','5'),('56','5'),('57','5'),('58','5'),('59','5'),('60','5'),('82','9'),('83','9'),('84','9'),('85','9'),('86','9'),('87','9'),('88','9'),('89','10'),('90','10'),('91','10'),('92','10'),('93','10'),('94','10'),('95','10'),('96','11'),('97','11'),('98','11'),('99','11'),('100','11'),('101','11'),('102','11'),('103','12'),('104','12'),('105','12'),('106','12'),('107','12'),('108','12'),('109','12'),('110','13'),('111','13'),('112','13'),('113','13'),('114','13'),('115','13'),('116','13'),('117','14'),('118','14'),('119','14'),('120','14'),('121','14'),('122','14'),('123','14'),('124','1'),('125','12'),('126','13'),('127','15'),('128','15'),('129','15'),('130','15'),('131','15'),('132','15'),('133','15'),('134','15'),('135','15'),('136','15'),('137','15'),('138','15'),('139','15'),('140','15'),('141','16'),('142','16'),('143','16'),('144','16'),('145','16'),('146','16'),('147','16'),('148','16'),('149','16'),('150','16'),('151','16'),('152','16'),('153','16'),('154','16'),('155','16'),('156','16'),('157','17'),('158','17'),('159','17'),('160','17'),('161','17'),('162','17'),('163','17'),('164','17'),('165','17'),('166','17'),('167','17'),('168','18'),('169','18'),('170','18'),('171','18'),('172','18'),('173','18'),('174','18'),('175','18'),('176','18'),('177','18'),('178','18'),('179','18'),('180','19'),('181','19'),('182','19'),('183','19'),('184','19'),('185','19'),('186','19'),('187','19'),('188','19'),('189','19'),('190','19'),('191','19'),('192','3'),('193','3'),('194','3'),('195','3'),('196','3'),('197','2'),('198','2'),('199','2'),('200','4'),('201','4'),('202','4'),('203','5'),('204','5'),('205','5'),('206','5'),('207','5'),('208','5'),('209','9'),('210','9'),('211','9'),('212','9'),('213','9'),('214','9'),('215','10'),('216','10'),('217','10'),('218','10'),('219','10'),('220','10'),('221','11'),('222','11'),('223','11'),('224','11'),('225','11'),('226','11'),('227','14'),('228','14'),('229','14'),('230','14'),('231','14'),('232','14'),('233','15'),('234','15'),('235','15'),('236','15'),('237','15'),('238','15'),('239','16'),('240','18'),('241','18'),('242','18'),('243','18'),('244','14'),('245','14'),('246','14'),('247','14'),('248','2'),('249','2'),('250','2'),('251','3'),('252','3'),('253','3'),('254','14'),('255','14'),('256','14'),('257','2'),('258','2'),('259','2'),('260','2'),('261','2'),('262','2'),('263','2'),('264','2'),('265','2'),('266','10'),('267','10'),('268','10'),('269','10'),('270','10'),('271','10'),('272','4'),('273','4'),('274','4'),('275','14'),('276','18'),('277','18'),('278','18'),('279','18'),('280','2'),('281','2'),('282','3'),('283','4'),('284','5'),('285','5'),('286','5'),('287','5'),('288','5'),('289','5'),('290','9'),('291','9'),('292','9'),('293','9'),('294','9'),('295','9'),('296','10'),('297','10'),('298','11'),('299','11'),('300','11'),('301','11'),('302','11'),('303','11'),('304','14'),('305','15'),('306','15'),('307','15'),('308','17'),('309','17'),('310','17'),('311','17'),('312','17'),('313','17'),('314','16'),('315','16'),('316','16'),('317','18'),('318','19'),('319','19'),('320','19'),('321','19'),('322','19'),('323','19'),('324','3'),('325','4'),('326','5'),('327','9'),('328','10'),('329','11'),('330','14'),('331','15'),('332','16'),('333','17'),('334','18'),('335','19');
/*!40000 ALTER TABLE `catalog_product_price_cl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_relation`
--

DROP TABLE IF EXISTS `catalog_product_relation`;
CREATE TABLE `catalog_product_relation` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  PRIMARY KEY (`parent_id`,`child_id`),
  KEY `CATALOG_PRODUCT_RELATION_CHILD_ID` (`child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Relation Table';

--
-- Table structure for table `catalog_product_super_attribute`
--

DROP TABLE IF EXISTS `catalog_product_super_attribute`;
CREATE TABLE `catalog_product_super_attribute` (
  `product_super_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product Super Attribute ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`product_super_attribute_id`),
  UNIQUE KEY `CATALOG_PRODUCT_SUPER_ATTRIBUTE_PRODUCT_ID_ATTRIBUTE_ID` (`product_id`,`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Table';

--
-- Table structure for table `catalog_product_super_attribute_label`
--

DROP TABLE IF EXISTS `catalog_product_super_attribute_label`;
CREATE TABLE `catalog_product_super_attribute_label` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_super_attribute_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Super Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `use_default` smallint(5) unsigned DEFAULT '0' COMMENT 'Use Default Value',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_SPR_ATTR_LBL_PRD_SPR_ATTR_ID_STORE_ID` (`product_super_attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Label Table';

--
-- Table structure for table `catalog_product_super_link`
--

DROP TABLE IF EXISTS `catalog_product_super_link`;
CREATE TABLE `catalog_product_super_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent ID',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `CATALOG_PRODUCT_SUPER_LINK_PRODUCT_ID_PARENT_ID` (`product_id`,`parent_id`),
  KEY `CATALOG_PRODUCT_SUPER_LINK_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Link Table';

--
-- Table structure for table `catalog_product_website`
--

DROP TABLE IF EXISTS `catalog_product_website`;
CREATE TABLE `catalog_product_website` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  PRIMARY KEY (`product_id`,`website_id`),
  KEY `CATALOG_PRODUCT_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Website Linkage Table';

--
-- Dumping data for table `catalog_product_website`
--

LOCK TABLES `catalog_product_website` WRITE;
/*!40000 ALTER TABLE `catalog_product_website` DISABLE KEYS */;
INSERT INTO `catalog_product_website` VALUES ('2',1),('3',1),('4',1),('5',1),('9',1),('10',1),('11',1),('14',1),('15',1),('16',1),('17',1),('18',1),('19',1);
/*!40000 ALTER TABLE `catalog_product_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_url_rewrite_product_category`
--

DROP TABLE IF EXISTS `catalog_url_rewrite_product_category`;
CREATE TABLE `catalog_url_rewrite_product_category` (
  `url_rewrite_id` int(10) unsigned NOT NULL COMMENT 'url_rewrite_id',
  `category_id` int(10) unsigned NOT NULL COMMENT 'category_id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'product_id',
  KEY `CATALOG_URL_REWRITE_PRODUCT_CATEGORY_CATEGORY_ID_PRODUCT_ID` (`category_id`,`product_id`),
  KEY `CAT_URL_REWRITE_PRD_CTGR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` (`product_id`),
  KEY `FK_BB79E64705D7F17FE181F23144528FC8` (`url_rewrite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='url_rewrite_relation';

--
-- Dumping data for table `catalog_url_rewrite_product_category`
--

LOCK TABLES `catalog_url_rewrite_product_category` WRITE;
/*!40000 ALTER TABLE `catalog_url_rewrite_product_category` DISABLE KEYS */;
INSERT INTO `catalog_url_rewrite_product_category` VALUES ('2805','4','11'),('2806','5','11'),('2807','2','11'),('2813','4','11'),('2814','5','11'),('2815','2','11'),('2817','4','11'),('2818','5','11'),('2819','2','11'),('3181','4','10'),('3182','5','10'),('3183','6','10'),('3184','8','10'),('3185','2','10'),('3193','4','10'),('3194','5','10'),('3195','6','10'),('3196','8','10'),('3197','2','10'),('3199','4','10'),('3200','5','10'),('3201','6','10'),('3202','8','10'),('3203','2','10'),('3397','3','2'),('3398','4','2'),('3399','5','2'),('3400','6','2'),('3401','7','2'),('3402','8','2'),('3403','2','2'),('3413','3','2'),('3414','4','2'),('3415','5','2'),('3416','6','2'),('3417','7','2'),('3418','8','2'),('3419','2','2'),('3421','3','2'),('3422','4','2'),('3423','5','2'),('3424','6','2'),('3425','7','2'),('3426','8','2'),('3427','2','2'),('3537','4','9'),('3538','5','9'),('3539','6','9'),('3540','7','9'),('3541','8','9'),('3542','2','9'),('3551','4','9'),('3552','5','9'),('3553','6','9'),('3554','7','9'),('3555','8','9'),('3556','2','9'),('3558','4','9'),('3559','5','9'),('3560','6','9'),('3561','7','9'),('3562','8','9'),('3563','2','9'),('3565','4','14'),('3566','5','14'),('3567','3','14'),('3568','6','14'),('3569','7','14'),('3570','8','14'),('3571','2','14'),('3581','4','14'),('3582','5','14'),('3583','3','14'),('3584','6','14'),('3585','7','14'),('3586','8','14'),('3587','2','14'),('3589','4','14'),('3590','5','14'),('3591','3','14'),('3592','6','14'),('3593','7','14'),('3594','8','14'),('3595','2','14'),('3597','3','15'),('3598','4','15'),('3599','5','15'),('3600','6','15'),('3601','7','15'),('3602','8','15'),('3603','2','15'),('3613','3','15'),('3614','4','15'),('3615','5','15'),('3616','6','15'),('3617','7','15'),('3618','8','15'),('3619','2','15'),('3621','3','15'),('3622','4','15'),('3623','5','15'),('3624','6','15'),('3625','7','15'),('3626','8','15'),('3627','2','15'),('3629','3','16'),('3630','4','16'),('3631','5','16'),('3632','6','16'),('3633','7','16'),('3634','8','16'),('3635','2','16'),('3645','3','16'),('3646','4','16'),('3647','5','16'),('3648','6','16'),('3649','7','16'),('3650','8','16'),('3651','2','16'),('3653','3','16'),('3654','4','16'),('3655','5','16'),('3656','6','16'),('3657','7','16'),('3658','8','16'),('3659','2','16'),('3661','3','17'),('3662','4','17'),('3663','5','17'),('3664','6','17'),('3665','7','17'),('3666','8','17'),('3667','2','17'),('3677','3','17'),('3678','4','17'),('3679','5','17'),('3680','6','17'),('3681','7','17'),('3682','8','17'),('3683','2','17'),('3685','3','17'),('3686','4','17'),('3687','5','17'),('3688','6','17'),('3689','7','17'),('3690','8','17'),('3691','2','17'),('3693','3','18'),('3694','4','18'),('3695','5','18'),('3696','6','18'),('3697','7','18'),('3698','8','18'),('3699','2','18'),('3709','3','18'),('3710','4','18'),('3711','5','18'),('3712','6','18'),('3713','7','18'),('3714','8','18'),('3715','2','18'),('3717','3','18'),('3718','4','18'),('3719','5','18'),('3720','6','18'),('3721','7','18'),('3722','8','18'),('3723','2','18'),('3725','4','19'),('3726','5','19'),('3727','6','19'),('3728','7','19'),('3729','8','19'),('3730','2','19'),('3739','4','19'),('3740','5','19'),('3741','6','19'),('3742','7','19'),('3743','8','19'),('3744','2','19'),('3746','4','19'),('3747','5','19'),('3748','6','19'),('3749','7','19'),('3750','8','19'),('3751','2','19'),('3757','3','3'),('3758','4','3'),('3759','5','3'),('3760','6','3'),('3761','7','3'),('3762','8','3'),('3763','9','3'),('3764','2','3'),('3775','3','3'),('3776','4','3'),('3777','5','3'),('3778','6','3'),('3779','7','3'),('3780','8','3'),('3781','9','3'),('3782','2','3'),('3784','3','3'),('3785','4','3'),('3786','5','3'),('3787','6','3'),('3788','7','3'),('3789','8','3'),('3790','9','3'),('3791','2','3'),('3793','3','4'),('3794','4','4'),('3795','5','4'),('3796','6','4'),('3797','7','4'),('3798','8','4'),('3799','9','4'),('3800','2','4'),('3811','3','4'),('3812','4','4'),('3813','5','4'),('3814','6','4'),('3815','7','4'),('3816','8','4'),('3817','9','4'),('3818','2','4'),('3820','3','4'),('3821','4','4'),('3822','5','4'),('3823','6','4'),('3824','7','4'),('3825','8','4'),('3826','9','4'),('3827','2','4'),('3829','3','5'),('3830','4','5'),('3831','5','5'),('3832','6','5'),('3833','7','5'),('3834','8','5'),('3835','9','5'),('3836','2','5'),('3847','3','5'),('3848','4','5'),('3849','5','5'),('3850','6','5'),('3851','7','5'),('3852','8','5'),('3853','9','5'),('3854','2','5'),('3856','3','5'),('3857','4','5'),('3858','5','5'),('3859','6','5'),('3860','7','5'),('3861','8','5'),('3862','9','5'),('3863','2','5');
/*!40000 ALTER TABLE `catalog_url_rewrite_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock`
--

DROP TABLE IF EXISTS `cataloginventory_stock`;
CREATE TABLE `cataloginventory_stock` (
  `stock_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Stock Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_name` varchar(255) DEFAULT NULL COMMENT 'Stock Name',
  PRIMARY KEY (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock';

--
-- Dumping data for table `cataloginventory_stock`
--

LOCK TABLES `cataloginventory_stock` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock` VALUES (1,0,'Default');
/*!40000 ALTER TABLE `cataloginventory_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_cl`
--

DROP TABLE IF EXISTS `cataloginventory_stock_cl`;
CREATE TABLE `cataloginventory_stock_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='cataloginventory_stock_cl';

--
-- Dumping data for table `cataloginventory_stock_cl`
--

LOCK TABLES `cataloginventory_stock_cl` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_cl` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock_cl` VALUES ('1','1'),('2','2'),('3','2'),('4','3'),('5','4'),('6','4'),('7','5'),('8','9'),('9','10'),('10','11'),('11','12'),('12','13'),('13','14'),('14','15'),('15','16'),('16','16'),('17','17'),('18','18'),('19','19'),('20','3'),('21','2'),('22','4'),('23','5'),('24','9'),('25','10'),('26','11'),('27','14'),('28','15'),('29','16'),('30','18'),('31','14'),('32','2'),('33','3'),('34','14'),('35','2'),('36','2'),('37','2'),('38','2'),('39','10'),('40','4'),('41','14'),('42','4'),('43','10'),('44','14'),('45','17'),('46','18'),('47','2'),('48','2'),('49','3'),('50','4'),('51','5'),('52','9'),('53','10'),('54','11'),('55','14'),('56','15'),('57','17'),('58','16'),('59','18'),('60','19'),('61','3'),('62','4'),('63','5'),('64','9'),('65','10'),('66','11'),('67','14'),('68','15'),('69','16'),('70','17'),('71','18'),('72','19');
/*!40000 ALTER TABLE `cataloginventory_stock_cl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_item`
--

DROP TABLE IF EXISTS `cataloginventory_stock_item`;
CREATE TABLE `cataloginventory_stock_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `stock_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Stock Id',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `min_qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Min Qty',
  `use_config_min_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Min Qty',
  `is_qty_decimal` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Qty Decimal',
  `backorders` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Backorders',
  `use_config_backorders` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Backorders',
  `min_sale_qty` decimal(12,4) NOT NULL DEFAULT '1.0000' COMMENT 'Min Sale Qty',
  `use_config_min_sale_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Min Sale Qty',
  `max_sale_qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Max Sale Qty',
  `use_config_max_sale_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Max Sale Qty',
  `is_in_stock` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is In Stock',
  `low_stock_date` timestamp NULL DEFAULT NULL COMMENT 'Low Stock Date',
  `notify_stock_qty` decimal(12,4) DEFAULT NULL COMMENT 'Notify Stock Qty',
  `use_config_notify_stock_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Notify Stock Qty',
  `manage_stock` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Manage Stock',
  `use_config_manage_stock` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Manage Stock',
  `stock_status_changed_auto` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Stock Status Changed Automatically',
  `use_config_qty_increments` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Qty Increments',
  `qty_increments` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Increments',
  `use_config_enable_qty_inc` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Enable Qty Increments',
  `enable_qty_increments` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Enable Qty Increments',
  `is_decimal_divided` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Divided into Multiple Boxes for Shipping',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Divided into Multiple Boxes for Shipping',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `CATALOGINVENTORY_STOCK_ITEM_PRODUCT_ID_WEBSITE_ID` (`product_id`,`website_id`),
  KEY `CATALOGINVENTORY_STOCK_ITEM_WEBSITE_ID` (`website_id`),
  KEY `CATALOGINVENTORY_STOCK_ITEM_STOCK_ID` (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Item';

--
-- Dumping data for table `cataloginventory_stock_item`
--

LOCK TABLES `cataloginventory_stock_item` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_item` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock_item` VALUES ('2','2',1,'10.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('3','3',1,'10.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('4','4',1,'9.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('5','5',1,'10.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('6','9',1,'10.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('7','10',1,'9.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('8','11',1,'50.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('11','14',1,'9.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('12','15',1,'10.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('13','16',1,'10.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('14','17',1,'19.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('15','18',1,'10.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('16','19',1,'10.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0);
/*!40000 ALTER TABLE `cataloginventory_stock_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_status`
--

DROP TABLE IF EXISTS `cataloginventory_stock_status`;
CREATE TABLE `cataloginventory_stock_status` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status';

--
-- Dumping data for table `cataloginventory_stock_status`
--

LOCK TABLES `cataloginventory_stock_status` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_status` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock_status` VALUES ('2',0,1,'10.0000',1),('3',0,1,'10.0000',1),('4',0,1,'9.0000',1),('5',0,1,'10.0000',1),('9',0,1,'10.0000',1),('10',0,1,'9.0000',1),('11',0,1,'50.0000',1),('14',0,1,'9.0000',1),('15',0,1,'10.0000',1),('16',0,1,'10.0000',1),('17',0,1,'19.0000',1),('18',0,1,'10.0000',1),('19',0,1,'10.0000',1);
/*!40000 ALTER TABLE `cataloginventory_stock_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_status_idx`
--

DROP TABLE IF EXISTS `cataloginventory_stock_status_idx`;
CREATE TABLE `cataloginventory_stock_status_idx` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_IDX_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_IDX_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status Indexer Idx';

--
-- Dumping data for table `cataloginventory_stock_status_idx`
--

LOCK TABLES `cataloginventory_stock_status_idx` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_status_idx` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock_status_idx` VALUES ('2',0,1,'10.0000',1),('3',0,1,'10.0000',1),('4',0,1,'9.0000',1),('5',0,1,'10.0000',1),('9',0,1,'10.0000',1),('10',0,1,'9.0000',1),('11',0,1,'50.0000',1),('14',0,1,'9.0000',1),('15',0,1,'10.0000',1),('16',0,1,'10.0000',1),('17',0,1,'19.0000',1),('18',0,1,'10.0000',1),('19',0,1,'10.0000',1);
/*!40000 ALTER TABLE `cataloginventory_stock_status_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_status_tmp`
--

DROP TABLE IF EXISTS `cataloginventory_stock_status_tmp`;
CREATE TABLE `cataloginventory_stock_status_tmp` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_TMP_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_TMP_WEBSITE_ID` (`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status Indexer Tmp';

--
-- Table structure for table `catalogrule`
--

DROP TABLE IF EXISTS `catalogrule`;
CREATE TABLE `catalogrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From',
  `to_date` date DEFAULT NULL COMMENT 'To',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  PRIMARY KEY (`rule_id`),
  KEY `CATALOGRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule';

--
-- Table structure for table `catalogrule_customer_group`
--

DROP TABLE IF EXISTS `catalogrule_customer_group`;
CREATE TABLE `catalogrule_customer_group` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `CATALOGRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Rules To Customer Groups Relations';

--
-- Table structure for table `catalogrule_group_website`
--

DROP TABLE IF EXISTS `catalogrule_group_website`;
CREATE TABLE `catalogrule_group_website` (
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`,`website_id`),
  KEY `CATALOGRULE_GROUP_WEBSITE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_GROUP_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Group Website';

--
-- Table structure for table `catalogrule_product`
--

DROP TABLE IF EXISTS `catalogrule_product`;
CREATE TABLE `catalogrule_product` (
  `rule_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Product Id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `from_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'From Time',
  `to_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'To time',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `action_operator` varchar(10) DEFAULT 'to_fixed' COMMENT 'Action Operator',
  `action_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Action Amount',
  `action_stop` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Action Stop',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_product_id`),
  UNIQUE KEY `IDX_EAA51B56FF092A0DCB795D1CEF812B7B` (`rule_id`,`from_time`,`to_time`,`website_id`,`customer_group_id`,`product_id`,`sort_order`),
  KEY `CATALOGRULE_PRODUCT_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_PRODUCT_WEBSITE_ID` (`website_id`),
  KEY `CATALOGRULE_PRODUCT_FROM_TIME` (`from_time`),
  KEY `CATALOGRULE_PRODUCT_TO_TIME` (`to_time`),
  KEY `CATALOGRULE_PRODUCT_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Product';

--
-- Table structure for table `catalogrule_product_cl`
--

DROP TABLE IF EXISTS `catalogrule_product_cl`;
CREATE TABLE `catalogrule_product_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1091 DEFAULT CHARSET=utf8 COMMENT='catalogrule_product_cl';

--
-- Dumping data for table `catalogrule_product_cl`
--

LOCK TABLES `catalogrule_product_cl` WRITE;
/*!40000 ALTER TABLE `catalogrule_product_cl` DISABLE KEYS */;
INSERT INTO `catalogrule_product_cl` VALUES ('1','1'),('2','1'),('3','1'),('4','1'),('5','1'),('6','1'),('7','1'),('8','1'),('9','1'),('10','1'),('11','1'),('12','1'),('13','1'),('14','1'),('15','1'),('16','1'),('17','1'),('18','1'),('19','1'),('20','1'),('21','1'),('22','1'),('23','1'),('24','1'),('25','1'),('26','1'),('27','1'),('28','1'),('29','1'),('30','1'),('31','1'),('32','1'),('33','2'),('34','2'),('35','2'),('36','2'),('37','2'),('38','2'),('39','2'),('40','2'),('41','2'),('42','2'),('43','2'),('44','2'),('45','2'),('46','2'),('47','2'),('48','2'),('49','2'),('50','2'),('51','2'),('52','2'),('53','2'),('54','2'),('55','2'),('56','2'),('57','2'),('58','2'),('59','2'),('60','2'),('61','2'),('62','2'),('63','2'),('64','2'),('65','2'),('66','2'),('67','2'),('68','2'),('69','2'),('70','2'),('71','2'),('72','2'),('73','2'),('74','2'),('75','2'),('76','2'),('77','2'),('78','2'),('79','2'),('80','2'),('81','2'),('82','2'),('83','2'),('84','3'),('85','2'),('86','4'),('87','2'),('88','3'),('89','3'),('90','3'),('91','3'),('92','3'),('93','3'),('94','3'),('95','3'),('96','3'),('97','3'),('98','3'),('99','3'),('100','3'),('101','3'),('102','3'),('103','3'),('104','3'),('105','3'),('106','3'),('107','3'),('108','3'),('109','3'),('110','3'),('111','3'),('112','3'),('113','3'),('114','3'),('115','3'),('116','3'),('117','5'),('118','3'),('119','3'),('120','3'),('121','3'),('122','3'),('123','3'),('124','3'),('125','3'),('126','3'),('127','4'),('128','4'),('129','4'),('130','4'),('131','4'),('132','4'),('133','4'),('134','4'),('135','4'),('136','4'),('137','4'),('138','4'),('139','4'),('140','4'),('141','4'),('142','4'),('143','4'),('144','4'),('145','4'),('146','4'),('147','4'),('148','4'),('149','4'),('150','4'),('151','4'),('152','4'),('153','4'),('154','4'),('155','4'),('156','4'),('157','4'),('158','4'),('159','4'),('160','4'),('161','4'),('162','4'),('163','4'),('164','4'),('165','4'),('166','4'),('167','6'),('168','4'),('169','5'),('170','5'),('171','5'),('172','5'),('173','5'),('174','5'),('175','5'),('176','5'),('177','5'),('178','5'),('179','5'),('180','5'),('181','5'),('182','5'),('183','5'),('184','5'),('185','5'),('186','5'),('187','5'),('188','5'),('189','5'),('190','5'),('191','5'),('192','7'),('193','5'),('194','5'),('195','5'),('196','5'),('197','5'),('198','5'),('199','5'),('200','5'),('201','5'),('272','9'),('273','9'),('274','9'),('275','9'),('276','9'),('277','9'),('278','9'),('279','9'),('280','9'),('281','9'),('282','9'),('283','9'),('284','9'),('285','9'),('286','9'),('287','9'),('288','9'),('289','9'),('290','9'),('291','9'),('292','9'),('293','9'),('294','9'),('295','9'),('296','9'),('297','9'),('298','9'),('299','9'),('300','9'),('301','9'),('302','9'),('303','10'),('304','10'),('305','10'),('306','10'),('307','10'),('308','10'),('309','10'),('310','10'),('311','10'),('312','10'),('313','10'),('314','10'),('315','10'),('316','10'),('317','10'),('318','10'),('319','10'),('320','10'),('321','10'),('322','10'),('323','10'),('324','10'),('325','10'),('326','10'),('327','10'),('328','10'),('329','10'),('330','10'),('331','10'),('332','10'),('333','11'),('334','11'),('335','11'),('336','11'),('337','11'),('338','11'),('339','11'),('340','11'),('341','11'),('342','11'),('343','11'),('344','11'),('345','11'),('346','11'),('347','11'),('348','11'),('349','11'),('350','11'),('351','11'),('352','11'),('353','11'),('354','11'),('355','11'),('356','11'),('357','11'),('358','11'),('359','11'),('360','11'),('361','12'),('362','12'),('363','12'),('364','12'),('365','12'),('366','12'),('367','12'),('368','12'),('369','12'),('370','12'),('371','12'),('372','12'),('373','12'),('374','12'),('375','12'),('376','12'),('377','12'),('378','12'),('379','12'),('380','12'),('381','12'),('382','12'),('383','12'),('384','12'),('385','12'),('386','13'),('387','12'),('388','14'),('389','12'),('390','12'),('391','12'),('392','12'),('393','13'),('394','13'),('395','13'),('396','13'),('397','13'),('398','13'),('399','13'),('400','13'),('401','13'),('402','13'),('403','13'),('404','13'),('405','13'),('406','13'),('407','13'),('408','13'),('409','13'),('410','13'),('411','13'),('412','13'),('413','13'),('414','13'),('415','13'),('416','15'),('417','13'),('418','16'),('419','13'),('420','13'),('421','13'),('422','13'),('423','14'),('424','14'),('425','14'),('426','14'),('427','14'),('428','14'),('429','14'),('430','14'),('431','14'),('432','14'),('433','14'),('434','14'),('435','14'),('436','14'),('437','14'),('438','14'),('439','14'),('440','14'),('441','14'),('442','14'),('443','14'),('444','14'),('445','14'),('446','17'),('447','14'),('448','18'),('449','14'),('450','14'),('451','14'),('452','14'),('453','1'),('454','12'),('455','13'),('456','15'),('457','15'),('458','15'),('459','15'),('460','15'),('461','15'),('462','15'),('463','15'),('464','15'),('465','15'),('466','15'),('467','15'),('468','15'),('469','15'),('470','15'),('471','15'),('472','15'),('473','15'),('474','15'),('475','15'),('476','15'),('477','15'),('478','15'),('479','15'),('480','15'),('481','15'),('482','15'),('483','15'),('484','15'),('485','15'),('486','15'),('487','15'),('488','19'),('489','15'),('490','15'),('491','15'),('492','15'),('493','15'),('494','15'),('495','15'),('496','15'),('497','16'),('498','16'),('499','16'),('500','16'),('501','16'),('502','16'),('503','16'),('504','16'),('505','16'),('506','16'),('507','16'),('508','16'),('509','16'),('510','16'),('511','16'),('512','16'),('513','16'),('514','16'),('515','16'),('516','16'),('517','16'),('518','16'),('519','16'),('520','16'),('521','16'),('522','16'),('523','16'),('524','16'),('525','20'),('526','16'),('527','16'),('528','16'),('529','16'),('530','16'),('531','16'),('532','16'),('533','16'),('534','16'),('535','16'),('536','16'),('537','16'),('538','16'),('539','16'),('540','16'),('541','16'),('542','17'),('543','17'),('544','17'),('545','17'),('546','17'),('547','17'),('548','17'),('549','17'),('550','17'),('551','17'),('552','17'),('553','17'),('554','17'),('555','17'),('556','17'),('557','17'),('558','17'),('559','17'),('560','17'),('561','17'),('562','17'),('563','17'),('564','17'),('565','17'),('566','17'),('567','17'),('568','17'),('569','17'),('570','17'),('571','21'),('572','17'),('573','17'),('574','17'),('575','17'),('576','17'),('577','17'),('578','17'),('579','17'),('580','18'),('581','18'),('582','18'),('583','18'),('584','18'),('585','18'),('586','18'),('587','18'),('588','18'),('589','18'),('590','18'),('591','18'),('592','18'),('593','18'),('594','18'),('595','18'),('596','18'),('597','18'),('598','18'),('599','18'),('600','18'),('601','18'),('602','18'),('603','18'),('604','18'),('605','18'),('606','18'),('607','18'),('608','22'),('609','18'),('610','18'),('611','18'),('612','18'),('613','18'),('614','18'),('615','18'),('616','18'),('617','19'),('618','19'),('619','19'),('620','19'),('621','19'),('622','19'),('623','19'),('624','19'),('625','19'),('626','19'),('627','19'),('628','19'),('629','19'),('630','19'),('631','19'),('632','19'),('633','19'),('634','19'),('635','19'),('636','19'),('637','19'),('638','19'),('639','19'),('640','19'),('641','19'),('642','19'),('643','19'),('644','19'),('645','19'),('646','19'),('647','23'),('648','19'),('649','19'),('650','19'),('651','19'),('652','19'),('653','19'),('654','19'),('655','19'),('656','19'),('657','3'),('658','3'),('659','3'),('660','3'),('661','3'),('662','3'),('663','3'),('664','3'),('665','3'),('666','2'),('667','2'),('668','2'),('669','2'),('670','2'),('671','2'),('672','2'),('673','2'),('674','2'),('675','4'),('676','4'),('677','4'),('678','4'),('679','4'),('680','5'),('681','5'),('682','5'),('683','5'),('684','5'),('685','5'),('686','5'),('687','5'),('688','5'),('689','5'),('690','9'),('691','9'),('692','9'),('693','9'),('694','9'),('695','9'),('696','9'),('697','9'),('698','9'),('699','9'),('700','10'),('701','10'),('702','10'),('703','10'),('704','10'),('705','10'),('706','10'),('707','10'),('708','10'),('709','11'),('710','11'),('711','11'),('712','11'),('713','11'),('714','11'),('715','11'),('716','11'),('717','11'),('718','11'),('719','14'),('720','14'),('721','14'),('722','14'),('723','14'),('724','14'),('725','14'),('726','14'),('727','14'),('728','14'),('729','14'),('730','14'),('731','14'),('732','14'),('733','14'),('734','15'),('735','15'),('736','15'),('737','15'),('738','15'),('739','15'),('740','15'),('741','15'),('742','16'),('743','16'),('744','16'),('745','18'),('746','18'),('747','18'),('748','18'),('749','18'),('750','18'),('751','18'),('752','14'),('753','14'),('754','14'),('755','14'),('756','14'),('757','14'),('758','14'),('759','14'),('760','14'),('761','14'),('762','14'),('763','14'),('764','14'),('765','17'),('766','18'),('767','24'),('768','14'),('769','25'),('770','14'),('771','2'),('772','2'),('773','2'),('774','2'),('775','2'),('776','2'),('777','2'),('778','2'),('779','3'),('780','3'),('781','3'),('782','3'),('783','3'),('784','14'),('785','14'),('786','14'),('787','14'),('788','14'),('789','14'),('790','14'),('791','14'),('792','2'),('793','2'),('794','2'),('795','2'),('796','2'),('797','2'),('798','2'),('799','2'),('800','2'),('801','2'),('802','2'),('803','2'),('804','2'),('805','2'),('806','2'),('807','2'),('808','2'),('809','2'),('810','2'),('811','2'),('812','2'),('813','2'),('814','2'),('815','2'),('816','2'),('817','2'),('818','2'),('819','2'),('820','2'),('821','10'),('822','10'),('823','10'),('824','10'),('825','10'),('826','10'),('827','10'),('828','10'),('829','10'),('830','4'),('831','4'),('832','4'),('833','4'),('834','4'),('835','4'),('836','14'),('837','14'),('838','14'),('839','14'),('840','14'),('841','14'),('842','18'),('843','18'),('844','18'),('845','18'),('846','18'),('847','18'),('848','18'),('849','2'),('850','2'),('851','2'),('852','2'),('853','2'),('854','2'),('855','2'),('856','26'),('857','2'),('858','27'),('859','2'),('860','28'),('861','2'),('862','2'),('863','2'),('864','2'),('865','2'),('866','2'),('867','2'),('868','2'),('869','2'),('870','2'),('871','2'),('872','2'),('873','2'),('874','3'),('875','3'),('876','3'),('877','3'),('878','4'),('879','4'),('880','4'),('881','4'),('882','5'),('883','5'),('884','5'),('885','5'),('886','5'),('887','5'),('888','5'),('889','5'),('890','5'),('891','9'),('892','9'),('893','9'),('894','9'),('895','9'),('896','9'),('897','9'),('898','9'),('899','9'),('900','9'),('901','10'),('902','10'),('903','10'),('904','10'),('905','10'),('906','11'),('907','11'),('908','11'),('909','11'),('910','11'),('911','11'),('912','11'),('913','11'),('914','11'),('915','11'),('916','14'),('917','14'),('918','14'),('919','14'),('920','14'),('921','14'),('922','14'),('923','15'),('924','15'),('925','15'),('926','15'),('927','15'),('928','15'),('929','17'),('930','17'),('931','17'),('932','17'),('933','17'),('934','17'),('935','17'),('936','17'),('937','17'),('938','16'),('939','16'),('940','16'),('941','16'),('942','16'),('943','16'),('944','18'),('945','18'),('946','18'),('947','18'),('948','19'),('949','19'),('950','19'),('951','19'),('952','19'),('953','19'),('954','19'),('955','19'),('956','19'),('957','19'),('958','3'),('959','3'),('960','3'),('961','29'),('962','3'),('963','30'),('964','3'),('965','31'),('966','3'),('967','32'),('968','3'),('969','33'),('970','3'),('971','4'),('972','4'),('973','4'),('974','34'),('975','4'),('976','35'),('977','4'),('978','36'),('979','4'),('980','37'),('981','4'),('982','5'),('983','5'),('984','5'),('985','5'),('986','5'),('987','38'),('988','5'),('989','39'),('990','5'),('991','40'),('992','5'),('993','9'),('994','9'),('995','9'),('996','41'),('997','9'),('998','42'),('999','9'),('1000','43'),('1001','9'),('1002','44'),('1003','9'),('1004','10'),('1005','10'),('1006','10'),('1007','45'),('1008','10'),('1009','46'),('1010','10'),('1011','47'),('1012','10'),('1013','48'),('1014','10'),('1015','11'),('1016','11'),('1017','11'),('1018','49'),('1019','11'),('1020','50'),('1021','11'),('1022','51'),('1023','11'),('1024','52'),('1025','11'),('1026','14'),('1027','14'),('1028','14'),('1029','14'),('1030','14'),('1031','14'),('1032','53'),('1033','14'),('1034','54'),('1035','14'),('1036','15'),('1037','15'),('1038','15'),('1039','55'),('1040','15'),('1041','56'),('1042','15'),('1043','57'),('1044','15'),('1045','16'),('1046','16'),('1047','16'),('1048','58'),('1049','16'),('1050','59'),('1051','16'),('1052','60'),('1053','16'),('1054','61'),('1055','16'),('1056','17'),('1057','17'),('1058','17'),('1059','17'),('1060','17'),('1061','62'),('1062','17'),('1063','63'),('1064','17'),('1065','64'),('1066','17'),('1067','65'),('1068','17'),('1069','18'),('1070','18'),('1071','18'),('1072','18'),('1073','18'),('1074','66'),('1075','18'),('1076','67'),('1077','18'),('1078','68'),('1079','18'),('1080','19'),('1081','19'),('1082','19'),('1083','69'),('1084','19'),('1085','70'),('1086','19'),('1087','71'),('1088','19'),('1089','72'),('1090','19');
/*!40000 ALTER TABLE `catalogrule_product_cl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogrule_product_price`
--

DROP TABLE IF EXISTS `catalogrule_product_price`;
CREATE TABLE `catalogrule_product_price` (
  `rule_product_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Product PriceId',
  `rule_date` date NOT NULL COMMENT 'Rule Date',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `rule_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rule Price',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `latest_start_date` date DEFAULT NULL COMMENT 'Latest StartDate',
  `earliest_end_date` date DEFAULT NULL COMMENT 'Earliest EndDate',
  PRIMARY KEY (`rule_product_price_id`),
  UNIQUE KEY `CATRULE_PRD_PRICE_RULE_DATE_WS_ID_CSTR_GROUP_ID_PRD_ID` (`rule_date`,`website_id`,`customer_group_id`,`product_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_WEBSITE_ID` (`website_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Product Price';

--
-- Table structure for table `catalogrule_rule_cl`
--

DROP TABLE IF EXISTS `catalogrule_rule_cl`;
CREATE TABLE `catalogrule_rule_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogrule_rule_cl';

--
-- Table structure for table `catalogrule_website`
--

DROP TABLE IF EXISTS `catalogrule_website`;
CREATE TABLE `catalogrule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `CATALOGRULE_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Rules To Websites Relations';

--
-- Table structure for table `catalogsearch_fulltext_cl`
--

DROP TABLE IF EXISTS `catalogsearch_fulltext_cl`;
CREATE TABLE `catalogsearch_fulltext_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1025 DEFAULT CHARSET=utf8 COMMENT='catalogsearch_fulltext_cl';

--
-- Dumping data for table `catalogsearch_fulltext_cl`
--

LOCK TABLES `catalogsearch_fulltext_cl` WRITE;
/*!40000 ALTER TABLE `catalogsearch_fulltext_cl` DISABLE KEYS */;
INSERT INTO `catalogsearch_fulltext_cl` VALUES ('1','1'),('2','1'),('3','1'),('4','1'),('5','1'),('6','1'),('7','1'),('8','1'),('9','1'),('10','1'),('11','1'),('12','1'),('13','1'),('14','1'),('15','1'),('16','1'),('17','1'),('18','1'),('19','1'),('20','1'),('21','1'),('22','1'),('23','1'),('24','1'),('25','1'),('26','1'),('27','2'),('28','2'),('29','2'),('30','2'),('31','2'),('32','2'),('33','2'),('34','2'),('35','2'),('36','2'),('37','2'),('38','2'),('39','2'),('40','2'),('41','2'),('42','2'),('43','2'),('44','2'),('45','2'),('46','2'),('47','2'),('48','2'),('49','2'),('50','2'),('51','2'),('52','2'),('53','2'),('54','2'),('55','2'),('56','2'),('57','2'),('58','2'),('59','2'),('60','2'),('61','2'),('62','2'),('63','2'),('64','2'),('65','2'),('66','2'),('67','2'),('68','2'),('69','2'),('70','2'),('71','3'),('72','3'),('73','3'),('74','3'),('75','3'),('76','3'),('77','3'),('78','3'),('79','3'),('80','3'),('81','3'),('82','3'),('83','3'),('84','3'),('85','3'),('86','3'),('87','3'),('88','3'),('89','3'),('90','3'),('91','3'),('92','3'),('93','3'),('94','3'),('95','3'),('96','3'),('97','3'),('98','3'),('99','3'),('100','3'),('101','3'),('102','4'),('103','4'),('104','4'),('105','4'),('106','4'),('107','4'),('108','4'),('109','4'),('110','4'),('111','4'),('112','4'),('113','4'),('114','4'),('115','4'),('116','4'),('117','4'),('118','4'),('119','4'),('120','4'),('121','4'),('122','4'),('123','4'),('124','4'),('125','4'),('126','4'),('127','4'),('128','4'),('129','4'),('130','4'),('131','4'),('132','4'),('133','4'),('134','4'),('135','4'),('136','4'),('137','5'),('138','5'),('139','5'),('140','5'),('141','5'),('142','5'),('143','5'),('144','5'),('145','5'),('146','5'),('147','5'),('148','5'),('149','5'),('150','5'),('151','5'),('152','5'),('153','5'),('154','5'),('155','5'),('156','5'),('157','5'),('158','5'),('159','5'),('160','5'),('161','5'),('224','9'),('225','9'),('226','9'),('227','9'),('228','9'),('229','9'),('230','9'),('231','9'),('232','9'),('233','9'),('234','9'),('235','9'),('236','9'),('237','9'),('238','9'),('239','9'),('240','9'),('241','9'),('242','9'),('243','9'),('244','9'),('245','9'),('246','9'),('247','9'),('248','9'),('249','10'),('250','10'),('251','10'),('252','10'),('253','10'),('254','10'),('255','10'),('256','10'),('257','10'),('258','10'),('259','10'),('260','10'),('261','10'),('262','10'),('263','10'),('264','10'),('265','10'),('266','10'),('267','10'),('268','10'),('269','10'),('270','10'),('271','10'),('272','10'),('273','10'),('274','11'),('275','11'),('276','11'),('277','11'),('278','11'),('279','11'),('280','11'),('281','11'),('282','11'),('283','11'),('284','11'),('285','11'),('286','11'),('287','11'),('288','11'),('289','11'),('290','11'),('291','11'),('292','11'),('293','11'),('294','11'),('295','11'),('296','11'),('297','11'),('298','11'),('299','12'),('300','12'),('301','12'),('302','12'),('303','12'),('304','12'),('305','12'),('306','12'),('307','12'),('308','12'),('309','12'),('310','12'),('311','12'),('312','12'),('313','12'),('314','12'),('315','12'),('316','12'),('317','12'),('318','12'),('319','12'),('320','12'),('321','12'),('322','12'),('323','12'),('324','13'),('325','13'),('326','13'),('327','13'),('328','13'),('329','13'),('330','13'),('331','13'),('332','13'),('333','13'),('334','13'),('335','13'),('336','13'),('337','13'),('338','13'),('339','13'),('340','13'),('341','13'),('342','13'),('343','13'),('344','13'),('345','13'),('346','13'),('347','13'),('348','13'),('349','14'),('350','14'),('351','14'),('352','14'),('353','14'),('354','14'),('355','14'),('356','14'),('357','14'),('358','14'),('359','14'),('360','14'),('361','14'),('362','14'),('363','14'),('364','14'),('365','14'),('366','14'),('367','14'),('368','14'),('369','14'),('370','14'),('371','14'),('372','14'),('373','14'),('374','1'),('375','12'),('376','13'),('377','15'),('378','15'),('379','15'),('380','15'),('381','15'),('382','15'),('383','15'),('384','15'),('385','15'),('386','15'),('387','15'),('388','15'),('389','15'),('390','15'),('391','15'),('392','15'),('393','15'),('394','15'),('395','15'),('396','15'),('397','15'),('398','15'),('399','15'),('400','15'),('401','15'),('402','15'),('403','15'),('404','15'),('405','15'),('406','15'),('407','15'),('408','15'),('409','15'),('410','15'),('411','16'),('412','16'),('413','16'),('414','16'),('415','16'),('416','16'),('417','16'),('418','16'),('419','16'),('420','16'),('421','16'),('422','16'),('423','16'),('424','16'),('425','16'),('426','16'),('427','16'),('428','16'),('429','16'),('430','16'),('431','16'),('432','16'),('433','16'),('434','16'),('435','16'),('436','16'),('437','16'),('438','16'),('439','16'),('440','16'),('441','16'),('442','16'),('443','16'),('444','16'),('445','16'),('446','16'),('447','16'),('448','17'),('449','17'),('450','17'),('451','17'),('452','17'),('453','17'),('454','17'),('455','17'),('456','17'),('457','17'),('458','17'),('459','17'),('460','17'),('461','17'),('462','17'),('463','17'),('464','17'),('465','17'),('466','17'),('467','17'),('468','17'),('469','17'),('470','17'),('471','17'),('472','17'),('473','17'),('474','17'),('475','17'),('476','17'),('477','17'),('478','17'),('479','18'),('480','18'),('481','18'),('482','18'),('483','18'),('484','18'),('485','18'),('486','18'),('487','18'),('488','18'),('489','18'),('490','18'),('491','18'),('492','18'),('493','18'),('494','18'),('495','18'),('496','18'),('497','18'),('498','18'),('499','18'),('500','18'),('501','18'),('502','18'),('503','18'),('504','18'),('505','18'),('506','18'),('507','18'),('508','18'),('509','19'),('510','19'),('511','19'),('512','19'),('513','19'),('514','19'),('515','19'),('516','19'),('517','19'),('518','19'),('519','19'),('520','19'),('521','19'),('522','19'),('523','19'),('524','19'),('525','19'),('526','19'),('527','19'),('528','19'),('529','19'),('530','19'),('531','19'),('532','19'),('533','19'),('534','19'),('535','19'),('536','19'),('537','19'),('538','19'),('539','19'),('540','19'),('541','3'),('542','3'),('543','3'),('544','3'),('545','3'),('546','3'),('547','3'),('548','3'),('549','2'),('550','2'),('551','2'),('552','2'),('553','2'),('554','2'),('555','4'),('556','4'),('557','4'),('558','4'),('559','5'),('560','5'),('561','5'),('562','5'),('563','5'),('564','5'),('565','5'),('566','5'),('567','5'),('568','9'),('569','9'),('570','9'),('571','9'),('572','9'),('573','9'),('574','9'),('575','9'),('576','9'),('577','10'),('578','10'),('579','10'),('580','10'),('581','10'),('582','10'),('583','10'),('584','10'),('585','11'),('586','11'),('587','11'),('588','11'),('589','11'),('590','11'),('591','11'),('592','11'),('593','11'),('594','14'),('595','14'),('596','14'),('597','14'),('598','14'),('599','14'),('600','14'),('601','14'),('602','15'),('603','15'),('604','15'),('605','15'),('606','15'),('607','15'),('608','15'),('609','16'),('610','16'),('611','18'),('612','18'),('613','18'),('614','18'),('615','18'),('616','18'),('617','14'),('618','14'),('619','14'),('620','14'),('621','14'),('622','14'),('623','14'),('624','14'),('625','14'),('626','14'),('627','14'),('628','14'),('629','14'),('630','14'),('631','2'),('632','2'),('633','2'),('634','2'),('635','2'),('636','3'),('637','3'),('638','3'),('639','3'),('640','14'),('641','14'),('642','14'),('643','14'),('644','14'),('645','2'),('646','2'),('647','2'),('648','2'),('649','2'),('650','2'),('651','2'),('652','2'),('653','2'),('654','2'),('655','2'),('656','2'),('657','2'),('658','2'),('659','2'),('660','2'),('661','2'),('662','10'),('663','10'),('664','10'),('665','10'),('666','10'),('667','10'),('668','10'),('669','10'),('670','4'),('671','4'),('672','4'),('673','4'),('674','4'),('675','14'),('676','14'),('677','14'),('678','4'),('679','10'),('680','14'),('681','17'),('682','18'),('683','18'),('684','18'),('685','18'),('686','18'),('687','18'),('688','2'),('689','2'),('690','2'),('691','2'),('692','2'),('693','2'),('694','2'),('695','2'),('696','2'),('697','2'),('698','2'),('699','2'),('700','2'),('701','2'),('702','2'),('703','2'),('704','2'),('705','2'),('706','2'),('707','2'),('708','2'),('709','2'),('710','2'),('711','2'),('712','2'),('713','2'),('714','2'),('715','2'),('716','2'),('717','2'),('718','2'),('719','2'),('720','2'),('721','2'),('722','2'),('723','2'),('724','2'),('725','2'),('726','2'),('727','2'),('728','2'),('729','2'),('730','2'),('731','2'),('732','2'),('733','2'),('734','2'),('735','2'),('736','2'),('737','2'),('738','2'),('739','2'),('740','2'),('741','2'),('742','2'),('743','2'),('744','2'),('745','2'),('746','2'),('747','2'),('748','2'),('749','2'),('750','2'),('751','2'),('752','2'),('753','2'),('754','2'),('755','2'),('756','2'),('757','2'),('758','2'),('759','2'),('760','2'),('761','2'),('762','2'),('763','2'),('764','2'),('765','2'),('766','2'),('767','3'),('768','3'),('769','3'),('770','4'),('771','4'),('772','4'),('773','5'),('774','5'),('775','5'),('776','5'),('777','5'),('778','5'),('779','5'),('780','5'),('781','9'),('782','9'),('783','9'),('784','9'),('785','9'),('786','9'),('787','9'),('788','9'),('789','9'),('790','10'),('791','10'),('792','10'),('793','10'),('794','11'),('795','11'),('796','11'),('797','11'),('798','11'),('799','11'),('800','11'),('801','11'),('802','11'),('803','14'),('804','14'),('805','14'),('806','14'),('807','15'),('808','15'),('809','15'),('810','15'),('811','15'),('812','17'),('813','17'),('814','17'),('815','17'),('816','17'),('817','17'),('818','17'),('819','17'),('820','16'),('821','16'),('822','16'),('823','16'),('824','16'),('825','18'),('826','18'),('827','18'),('828','19'),('829','19'),('830','19'),('831','19'),('832','19'),('833','19'),('834','19'),('835','19'),('836','19'),('837','3'),('838','3'),('839','3'),('840','3'),('841','3'),('842','3'),('843','3'),('844','3'),('845','3'),('846','3'),('847','3'),('848','3'),('849','3'),('850','3'),('851','3'),('852','3'),('853','3'),('854','3'),('855','3'),('856','3'),('857','4'),('858','4'),('859','4'),('860','4'),('861','4'),('862','4'),('863','4'),('864','4'),('865','4'),('866','4'),('867','4'),('868','4'),('869','4'),('870','4'),('871','5'),('872','5'),('873','5'),('874','5'),('875','5'),('876','5'),('877','5'),('878','5'),('879','5'),('880','5'),('881','5'),('882','5'),('883','5'),('884','5'),('885','5'),('886','5'),('887','5'),('888','9'),('889','9'),('890','9'),('891','9'),('892','9'),('893','9'),('894','9'),('895','9'),('896','9'),('897','9'),('898','9'),('899','9'),('900','9'),('901','9'),('902','9'),('903','9'),('904','10'),('905','10'),('906','10'),('907','10'),('908','10'),('909','10'),('910','10'),('911','10'),('912','10'),('913','10'),('914','10'),('915','10'),('916','10'),('917','10'),('918','10'),('919','10'),('920','10'),('921','10'),('922','10'),('923','10'),('924','11'),('925','11'),('926','11'),('927','11'),('928','11'),('929','11'),('930','11'),('931','11'),('932','11'),('933','11'),('934','11'),('935','11'),('936','11'),('937','11'),('938','11'),('939','14'),('940','14'),('941','14'),('942','14'),('943','14'),('944','14'),('945','14'),('946','14'),('947','14'),('948','14'),('949','14'),('950','14'),('951','14'),('952','15'),('953','15'),('954','15'),('955','15'),('956','15'),('957','15'),('958','15'),('959','15'),('960','15'),('961','15'),('962','15'),('963','15'),('964','15'),('965','16'),('966','16'),('967','16'),('968','16'),('969','16'),('970','16'),('971','16'),('972','16'),('973','16'),('974','16'),('975','16'),('976','16'),('977','16'),('978','16'),('979','16'),('980','17'),('981','17'),('982','17'),('983','17'),('984','17'),('985','17'),('986','17'),('987','17'),('988','17'),('989','17'),('990','17'),('991','17'),('992','17'),('993','17'),('994','17'),('995','17'),('996','18'),('997','18'),('998','18'),('999','18'),('1000','18'),('1001','18'),('1002','18'),('1003','18'),('1004','18'),('1005','18'),('1006','18'),('1007','18'),('1008','18'),('1009','18'),('1010','18'),('1011','18'),('1012','19'),('1013','19'),('1014','19'),('1015','19'),('1016','19'),('1017','19'),('1018','19'),('1019','19'),('1020','19'),('1021','19'),('1022','19'),('1023','19'),('1024','19');
/*!40000 ALTER TABLE `catalogsearch_fulltext_cl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogsearch_fulltext_scope1`
--

DROP TABLE IF EXISTS `catalogsearch_fulltext_scope1`;
CREATE TABLE `catalogsearch_fulltext_scope1` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` int(10) unsigned NOT NULL COMMENT 'Attribute_id',
  `data_index` longtext COMMENT 'Data index',
  PRIMARY KEY (`entity_id`,`attribute_id`),
  FULLTEXT KEY `FTI_FULLTEXT_DATA_INDEX` (`data_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogsearch_fulltext_scope1';

--
-- Dumping data for table `catalogsearch_fulltext_scope1`
--

LOCK TABLES `catalogsearch_fulltext_scope1` WRITE;
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope1` DISABLE KEYS */;
INSERT INTO `catalogsearch_fulltext_scope1` VALUES ('2','73','Accumsan Elit'),('2','74','Accumsan Elit'),('2','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('2','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('2','83','Gear'),('2','93','Green'),('3','73','Nunc Facilisis'),('3','74','Nunc Facilisis'),('3','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('3','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('3','83','Gear'),('3','93','Red'),('4','73','Etiam Gravida'),('4','74','Etiam Gravida'),('4','75','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque libero. Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget veli'),('4','76','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper.'),('4','83','Accessories'),('4','93','Red'),('5','73','Donec Non Est'),('5','74','Donec Non Est'),('5','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('5','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('9','73','Habitant'),('9','74','Habitant'),('9','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('9','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','73','Tempus'),('10','74','Tempus'),('10','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','83','Accessories'),('10','93','Red'),('11','73','Aliquam'),('11','74','Aliquam'),('11','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('11','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('14','73','squa'),('14','74','squa'),('14','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue'),('14','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue'),('14','83','Accessories'),('14','93','Red'),('15','73','Levis'),('15','74','Levis'),('15','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('15','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('16','73','puma'),('16','74','puma'),('16','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('16','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('17','73','adidas'),('17','74','adidas'),('17','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('17','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('18','73','nike'),('18','74','nike'),('18','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('19','73','violet'),('19','74','violet'),('19','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('19','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk');
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogsearch_fulltext_scope2`
--

DROP TABLE IF EXISTS `catalogsearch_fulltext_scope2`;
CREATE TABLE `catalogsearch_fulltext_scope2` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` int(10) unsigned NOT NULL COMMENT 'Attribute_id',
  `data_index` longtext COMMENT 'Data index',
  PRIMARY KEY (`entity_id`,`attribute_id`),
  FULLTEXT KEY `FTI_FULLTEXT_DATA_INDEX` (`data_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogsearch_fulltext_scope2';

--
-- Dumping data for table `catalogsearch_fulltext_scope2`
--

LOCK TABLES `catalogsearch_fulltext_scope2` WRITE;
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope2` DISABLE KEYS */;
INSERT INTO `catalogsearch_fulltext_scope2` VALUES ('2','73','Accumsan Elit'),('2','74','Accumsan Elit'),('2','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('2','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('2','83','Gear'),('2','93','Green'),('3','73','Nunc Facilisis'),('3','74','Nunc Facilisis'),('3','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('3','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('3','83','Gear'),('3','93','Red'),('4','73','Etiam Gravida'),('4','74','Etiam Gravida'),('4','75','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque libero. Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget veli'),('4','76','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper.'),('4','83','Accessories'),('4','93','Red'),('5','73','Donec Non Est'),('5','74','Donec Non Est'),('5','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('5','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('9','73','Habitant'),('9','74','Habitant'),('9','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('9','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','73','Tempus'),('10','74','Tempus'),('10','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','83','Accessories'),('10','93','Red'),('11','73','Aliquam'),('11','74','Aliquam'),('11','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('11','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('14','73','squa'),('14','74','squa'),('14','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue'),('14','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue'),('14','83','Accessories'),('14','93','Red'),('15','73','Levis'),('15','74','Levis'),('15','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('15','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('16','73','puma'),('16','74','puma'),('16','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('16','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('17','73','adidas'),('17','74','adidas'),('17','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('17','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('18','73','nike'),('18','74','nike'),('18','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('19','73','violet'),('19','74','violet'),('19','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('19','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk');
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogsearch_fulltext_scope3`
--

DROP TABLE IF EXISTS `catalogsearch_fulltext_scope3`;
CREATE TABLE `catalogsearch_fulltext_scope3` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` int(10) unsigned NOT NULL COMMENT 'Attribute_id',
  `data_index` longtext COMMENT 'Data index',
  PRIMARY KEY (`entity_id`,`attribute_id`),
  FULLTEXT KEY `FTI_FULLTEXT_DATA_INDEX` (`data_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogsearch_fulltext_scope3';

--
-- Dumping data for table `catalogsearch_fulltext_scope3`
--

LOCK TABLES `catalogsearch_fulltext_scope3` WRITE;
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope3` DISABLE KEYS */;
INSERT INTO `catalogsearch_fulltext_scope3` VALUES ('2','73','Accumsan Elit'),('2','74','Accumsan Elit'),('2','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('2','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('2','83','Gear'),('2','93','Green'),('3','73','Nunc Facilisis'),('3','74','Nunc Facilisis'),('3','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('3','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('3','83','Gear'),('3','93','Red'),('4','73','Etiam Gravida'),('4','74','Etiam Gravida'),('4','75','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque libero. Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget veli'),('4','76','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper.'),('4','83','Accessories'),('4','93','Red'),('5','73','Donec Non Est'),('5','74','Donec Non Est'),('5','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('5','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('9','73','Habitant'),('9','74','Habitant'),('9','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('9','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','73','Tempus'),('10','74','Tempus'),('10','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','83','Accessories'),('10','93','Red'),('11','73','Aliquam'),('11','74','Aliquam'),('11','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('11','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('14','73','squa'),('14','74','squa'),('14','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue'),('14','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue'),('14','83','Accessories'),('14','93','Red'),('15','73','Levis'),('15','74','Levis'),('15','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('15','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('16','73','puma'),('16','74','puma'),('16','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('16','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('17','73','adidas'),('17','74','adidas'),('17','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('17','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('18','73','nike'),('18','74','nike'),('18','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('19','73','violet'),('19','74','violet'),('19','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('19','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk');
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogsearch_fulltext_scope4`
--

DROP TABLE IF EXISTS `catalogsearch_fulltext_scope4`;
CREATE TABLE `catalogsearch_fulltext_scope4` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` int(10) unsigned NOT NULL COMMENT 'Attribute_id',
  `data_index` longtext COMMENT 'Data index',
  PRIMARY KEY (`entity_id`,`attribute_id`),
  FULLTEXT KEY `FTI_FULLTEXT_DATA_INDEX` (`data_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogsearch_fulltext_scope4';

--
-- Dumping data for table `catalogsearch_fulltext_scope4`
--

LOCK TABLES `catalogsearch_fulltext_scope4` WRITE;
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope4` DISABLE KEYS */;
INSERT INTO `catalogsearch_fulltext_scope4` VALUES ('2','73','Accumsan Elit'),('2','74','Accumsan Elit'),('2','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('2','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('2','83','Gear'),('2','93','Green'),('3','73','Nunc Facilisis'),('3','74','Nunc Facilisis'),('3','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('3','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('3','83','Gear'),('3','93','Red'),('4','73','Etiam Gravida'),('4','74','Etiam Gravida'),('4','75','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque libero. Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget veli'),('4','76','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper.'),('4','83','Accessories'),('4','93','Red'),('5','73','Donec Non Est'),('5','74','Donec Non Est'),('5','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('5','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('9','73','Habitant'),('9','74','Habitant'),('9','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('9','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','73','Tempus'),('10','74','Tempus'),('10','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('10','83','Accessories'),('10','93','Red'),('11','73','Aliquam'),('11','74','Aliquam'),('11','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('11','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('14','73','squa'),('14','74','squa'),('14','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue'),('14','76','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue'),('14','83','Accessories'),('14','93','Red'),('15','73','Levis'),('15','74','Levis'),('15','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('15','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('16','73','puma'),('16','74','puma'),('16','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('16','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('17','73','adidas'),('17','74','adidas'),('17','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('17','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('18','73','nike'),('18','74','nike'),('18','75','Nulla iaculis purus nulla, condimentum facilisis ligula congue eget. Nulla iaculis purus nulla, condime ntum facilisis ligula congue eget. Nulla iaculis purus nulla, condimentum facilisis ligula congue.'),('19','73','violet'),('19','74','violet'),('19','75','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk'),('19','76','Text & call online - free! ... Unlimited talk, text & 2G Data. LG Tribute $9.99 (was $99.99). TextNow back to school sale. Get social this semester. Unlimited talk');
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checkout_agreement`
--

DROP TABLE IF EXISTS `checkout_agreement`;
CREATE TABLE `checkout_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Agreement Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `content` text COMMENT 'Content',
  `content_height` varchar(25) DEFAULT NULL COMMENT 'Content Height',
  `checkbox_text` text COMMENT 'Checkbox Text',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `is_html` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Html',
  `mode` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Applied mode',
  PRIMARY KEY (`agreement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkout Agreement';

--
-- Table structure for table `checkout_agreement_store`
--

DROP TABLE IF EXISTS `checkout_agreement_store`;
CREATE TABLE `checkout_agreement_store` (
  `agreement_id` int(10) unsigned NOT NULL COMMENT 'Agreement Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`agreement_id`,`store_id`),
  KEY `CHECKOUT_AGREEMENT_STORE_STORE_ID_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkout Agreement Store';

--
-- Table structure for table `cms_block`
--

DROP TABLE IF EXISTS `cms_block`;
CREATE TABLE `cms_block` (
  `block_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Block ID',
  `title` varchar(255) NOT NULL COMMENT 'Block Title',
  `identifier` varchar(255) NOT NULL COMMENT 'Block String Identifier',
  `content` mediumtext COMMENT 'Block Content',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Block Creation Time',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Block Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Block Active',
  PRIMARY KEY (`block_id`),
  FULLTEXT KEY `CMS_BLOCK_TITLE_IDENTIFIER_CONTENT` (`title`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='CMS Block Table';

--
-- Dumping data for table `cms_block`
--

LOCK TABLES `cms_block` WRITE;
/*!40000 ALTER TABLE `cms_block` DISABLE KEYS */;
INSERT INTO `cms_block` VALUES (1,'Header Phone','header_phone','<div class=\"header-phone\">Call us for free:  <span> 84. 888 888 686</span></div>','2016-10-06 03:52:30','2016-10-06 04:16:26',1),(2,'Message','message','<div class=\"message\">Default welcome msg!</div>','2016-10-06 03:56:22','2016-10-06 03:56:22',1),(5,'Home Banner Static1','home_banner_static1','\r\n<div class=\"home-banner-static1 row\">\r\n<div class=\"ca-box ca-box7 col-sm-4 col-md-4 col-sms-12 col-smb-12\">\r\n<div class=\"box-content\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_banner01.jpg\"}}\" alt=\"\" /></a></div>\r\n<div class=\"ca-content\">\r\n<h3 class=\"ca-sub1\">for women</h3>\r\n<h3 class=\"ca-sub2\">new colection</h3>\r\n<h3 class=\"ca-sub3\">Designed by thomas jeam</h3>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"ca-box ca-box8 col-sm-5 col-md-5 col-sms-12 col-smb-12\">\r\n<div class=\"box-content\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_banner02.jpg\"}}\" alt=\"\" /></a></div>\r\n<div class=\"ca-content\">\r\n<h3 class=\"ca-sub1\">Collection</h3>\r\n<h3 class=\"ca-sub2\">victory secreat</h3>\r\n<h3 class=\"ca-sub3\">street fashion</h3>\r\n</div>\r\n</div>\r\n<div class=\"box-content box-content2\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_banner03.jpg\"}}\" alt=\"\" /></a></div>\r\n<div class=\"ca-content\">\r\n<h3 class=\"ca-sub1\">Hello</h3>\r\n<h3 class=\"ca-sub2\">this summer</h3>\r\n<h3 class=\"ca-sub3\">Designed by MarCorae</h3>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"ca-box ca-box9 col-sm-3 col-md-3 col-sms-12 col-smb-12\">\r\n<div class=\"box-content\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_banner04.jpg\"}}\" alt=\"\" /></a></div>\r\n<div class=\"ca-content\">\r\n<h3 class=\"ca-sub1\">Collection</h3>\r\n<h3 class=\"ca-sub2\">Designed by Mar</h3>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n','2016-10-07 09:39:08','2016-10-08 02:28:37',1),(6,'Footer Static','footer_static','<div class=\"footer-static row\">\r\n<div class=\"f-col col-sm-6 col-md-3 col-sms-6 col-smb-12\">\r\n<div class=\"footer-static-title\">\r\n<h3>information</h3>\r\n</div>\r\n<div class=\"footer-static-content\">\r\n<ul>\r\n<li class=\"first\"><a href=\"#\">About us</a></li>\r\n<li><a href=\"#\">Delivery Information</a></li>\r\n<li><a href=\"#\">Privacy Policy</a></li>\r\n<li><a href=\"#\">Discount</a></li>\r\n<li><a href=\"#\">Custom Service</a></li>\r\n<li class=\"last\"><a href=\"#\">Terms &amp; Condition</a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"f-col f-col2 col-sm-6 col-md-3 col-sms-6 col-smb-12\">\r\n<div class=\"footer-static-title\">\r\n<h3>our support</h3>\r\n</div>\r\n<div class=\"footer-static-content\">\r\n<ul>\r\n<li class=\"first\"><a href=\"#\">Sitemap</a></li>\r\n<li><a href=\"#\">Privacy Policy</a></li>\r\n<li><a href=\"#\">Your Account</a></li>\r\n<li><a href=\"#\">Advanced Search</a></li>\r\n<li><a href=\"#\">Terms &amp; Condition</a></li>\r\n<li class=\"last\"><a href=\"#\">Contact Us</a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"f-col f-col3 col-sm-6 col-md-3 col-sms-6 col-smb-12\">\r\n<div class=\"footer-static-title\">\r\n<h3>our service</h3>\r\n</div>\r\n<div class=\"footer-static-content\">\r\n<ul>\r\n<li class=\"first\"><a href=\"#\">My Account</a></li>\r\n<li><a href=\"#\">Order History</a></li>\r\n<li><a href=\"#\">Returns</a></li>\r\n<li><a href=\"#\">Specials</a></li>\r\n<li><a href=\"#\">Mobiles</a></li>\r\n<li class=\"last\"><a href=\"#\">Site Map</a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"f-col f-col4 col-sm-6 col-md-3 col-sms-6 col-smb-12\">\r\n<div class=\"footer-static-title\">\r\n<h3>about unis Fashion</h3>\r\n</div>\r\n<div class=\"footer-static-content\">\r\n<div class=\"box-img\"><img src=\"{{media url=\"wysiwyg/fashion.png\"}}\" alt=\"\" /></div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipis cing elit. Suspendisse laoreet dictum orci eget varius. Curabitur ligula felis, vulputate at.</p>\r\n</div>\r\n<ul class=\"link-follow \">\r\n<li class=\"first\"><a class=\"facebook fa fa-facebook\" href=\"#\"><span>facebook</span></a></li>\r\n<li><a class=\"rss fa fa-rss\" href=\"#\"><span>rss</span></a></li>\r\n<li><a class=\"twitter fa fa-twitter\" href=\"#\"><span>twitter</span></a></li>\r\n<li><a class=\"linkedin fa fa-linkedin\" href=\"#\"><span>linkedin</span></a></li>\r\n<li class=\"last\"><a class=\"dribbble fa fa-dribbble\" href=\"#\"><span>dribbble</span></a></li>\r\n</ul>\r\n</div>\r\n</div>','2016-10-10 09:12:16','2016-10-12 04:40:47',1),(7,'Footer End','footer_end','<div class=\"col-xs-12 col-sm-12\">\r\n	<address>\r\n		Copyright © 2014 <a href=\"http://www.plazathemes.com/\">Plazathemes.com</a>. All Rights Reserved.\r\n	</address>\r\n	<ul>\r\n		<li><a href=\"#\">About Us</a></li>\r\n		<li><a href=\"#\">Customer Service</a></li>\r\n		<li class=\"last privacy\"><a href=\"#\">Privacy Policy</a></li>\r\n	</ul>\r\n	<ul class=\"links\">\r\n		<li class=\"first\"><a href=\"#\" title=\"Site Map\">Site Map</a></li>\r\n		<li><a href=\"#\" title=\"Search Terms\">Search Terms</a></li>\r\n		<li><a href=\"#\" title=\"Advanced Search\">Advanced Search</a></li>\r\n		<li><a href=\"#\" title=\"Orders and Returns\">Orders and Returns</a></li>\r\n		<li class=\" last\"><a href=\"#\" title=\"Contact Us\">Contact Us</a></li>\r\n	</ul>\r\n</div>','2016-10-11 01:33:51','2016-10-11 01:33:51',1),(8,'Banner Left1','banner_left1','<div class=\"banner-left\"><a class=\"overlay\" href=\"#\"></a> <a href=\"#\"><img src=\"{{media url=\"wysiwyg/banner-left.jpg\"}}\" alt=\"\" /></a></div>','2016-10-11 08:01:13','2016-10-11 08:34:35',1),(9,'Custom Menu','pt_menu_idcat_9','<div class=\"static_block_custom_menu\">\r\n<div class=\"custom-inner\">\r\n<div class=\"box-html\">\r\n<div class=\"menu-title\"><span>VIDEO WIDGETS </span></div>\r\n<div class=\"menu-content\">\r\n<div class=\"menu-inner\"><object width=\"320\" height=\"200\" data=\"http://www.youtube.com/v/TZADnkwNEO8?wmode=transparent\" type=\"application/x-shockwave-flash\"><param name=\"src\" value=\"http://www.youtube.com/v/TZADnkwNEO8?wmode=transparent\" /></object>\r\n<p>This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>\r\n<a href=\"#\"><img src=\"{{media url=\"wysiwyg/menuidcart.png\"}}\" alt=\"\" /></a></div>\r\n</div>\r\n</div>\r\n<div class=\"box-video\">\r\n<div class=\"menu-title\"><span>Images Widgets</span></div>\r\n<div class=\"menu-content\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/menuidcaright.png\"}}\" alt=\"\" /></a></div>\r\n</div>\r\n</div>\r\n</div>','2016-10-12 07:56:54','2016-11-27 14:29:57',0),(10,'Banner Home Content','banner_home_content','<div class=\"banner-home-content\">\r\n<div class=\"banner-home-content-inner\"><a class=\"overlay1\"></a> \r\n<a href=\"#\">\r\n<img src=\"{{media url=\"wysiwyg/img23.jpg\"}}\" alt=\"\" />\r\n</a></div>\r\n</div>','2016-10-17 02:30:53','2016-10-17 02:55:07',1),(11,'Home Banner Static2','home_banner_static2','<div class=\"home-banner-static2 row\">\r\n<div class=\"ca-box ca-box1 col-sm-4 col-md-4 col-sms-12 col-smb-12\">\r\n<div class=\"box-content\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_static01.jpg\"}}\" alt=\"\" /></a></div>\r\n<div class=\"ca-content\">\r\n<h3 class=\"ca-sub1\">Hello</h3>\r\n<h3 class=\"ca-sub2\">this summer</h3>\r\n<h3 class=\"ca-sub3\">Designed by MarCorae</h3>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"ca-box ca-box2 col-sm-4 col-md-4 col-sms-12 col-smb-12\">\r\n<div class=\"box-content\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_static02.jpg\"}}\" alt=\"\" /></a></div>\r\n<div class=\"ca-content\">\r\n<h3 class=\"ca-sub1\">Collection</h3>\r\n<h3 class=\"ca-sub2\">victory secreat</h3>\r\n<h3 class=\"ca-sub3\">street fashion</h3>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"ca-box ca-box3 col-sm-4 col-md-4 col-sms-12 col-smb-12\">\r\n<div class=\"box-content\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_static03.jpg\"}}\" alt=\"\" /></a></div>\r\n<div class=\"ca-content\">\r\n<h3 class=\"ca-sub1\">Hello</h3>\r\n<h3 class=\"ca-sub2\">this summer</h3>\r\n<h3 class=\"ca-sub3\">Designed by MarCorae</h3>\r\n</div>\r\n</div>\r\n</div>\r\n</div>','2016-10-17 02:40:16','2016-10-17 03:15:55',1),(12,'Home Banner Static3','home_banner_static3','<div class=\"home-banner-static3 row\">\r\n<div class=\"ca-box ca-box4 col-sm-4 col-md-4 col-sms-12 col-smb-12\">\r\n<div class=\"box-content\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_static04.jpg\"}}\" alt=\"\" /></a></div>\r\n<div class=\"ca-content \">\r\n<h3 class=\"ca-sub1\">Hello</h3>\r\n<h3 class=\"ca-sub2\">this summer</h3>\r\n<h3 class=\"ca-sub3\">Designed by MarCorae</h3>\r\n</div>\r\n</div>\r\n<div class=\"box-content box-content1\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_static05.jpg\"}}\" alt=\"\" /></a></div>\r\n</div>\r\n</div>\r\n<div class=\"ca-box ca-box5 col-sm-4 col-md-4 col-sms-12 col-smb-12\">\r\n<div class=\"box-content\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_static06.jpg\"}}\" alt=\"\" /></a></div>\r\n<div class=\"ca-content\">\r\n<h3 class=\"ca-sub1\">Collection</h3>\r\n<h3 class=\"ca-sub2\">victory secreat</h3>\r\n<h3 class=\"ca-sub3\">street fashion</h3>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"ca-box ca-box6 col-sm-4 col-md-4 col-sms-12 col-smb-12\">\r\n<div class=\"box-content\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_static07.jpg\"}}\" alt=\"\" /></a></div>\r\n<div class=\"ca-content\">\r\n<h3 class=\"ca-sub1\">Hello</h3>\r\n<h3 class=\"ca-sub2\">this summer</h3>\r\n<h3 class=\"ca-sub3\">Designed by MarCorae</h3>\r\n</div>\r\n</div>\r\n<div class=\"box-content  box-content1\">\r\n<div class=\"ca-img\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/images_static08.jpg\"}}\" alt=\"\" /></a></div>\r\n</div>\r\n</div>\r\n</div>','2016-10-17 08:06:16','2016-10-17 09:38:31',1),(13,'Pt Menu Custom','pt_menu_custom','<div class=\"pt_menu\">\r\n<div class=\"parentMenu\"><a>Custom Menu</a></div>\r\n<div id=\"popup_pt_item_menu_custom_menu\" class=\"popup cmsblock\">\r\n<div id=\"block2_pt_item_menu_custom_menu\" class=\"block2\">\r\n<div class=\"static_block_custom_menu\">\r\n<div class=\"custom-inner\">\r\n<div class=\"box-html\">\r\n<div class=\"menu-title\"><span>VIDEO WIDGETS </span></div>\r\n<div class=\"menu-content\">\r\n<div class=\"menu-inner\"><object width=\"320\" height=\"200\" data=\"http://www.youtube.com/v/TZADnkwNEO8?wmode=transparent\" type=\"application/x-shockwave-flash\"><param name=\"src\" value=\"http://www.youtube.com/v/TZADnkwNEO8?wmode=transparent\" /></object>\r\n<p>This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>\r\n<a href=\"#\"><img src=\"{{media url=\"wysiwyg/menuidcart.png\"}}\" alt=\"\" /></a></div>\r\n</div>\r\n</div>\r\n<div class=\"box-video\">\r\n<div class=\"menu-title\"><span>Images Widgets</span></div>\r\n<div class=\"menu-content\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/menuidcaright.png\"}}\" alt=\"\" /></a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>','2016-11-26 04:57:23','2016-11-27 14:33:14',1);
/*!40000 ALTER TABLE `cms_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_block_store`
--

DROP TABLE IF EXISTS `cms_block_store`;
CREATE TABLE `cms_block_store` (
  `block_id` smallint(6) NOT NULL COMMENT 'Block ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`block_id`,`store_id`),
  KEY `CMS_BLOCK_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Block To Store Linkage Table';

--
-- Dumping data for table `cms_block_store`
--

LOCK TABLES `cms_block_store` WRITE;
/*!40000 ALTER TABLE `cms_block_store` DISABLE KEYS */;
INSERT INTO `cms_block_store` VALUES (1,0),(2,0),(5,0),(6,0),(7,0),(8,0),(9,0),(10,0),(11,0),(12,0),(13,0);
/*!40000 ALTER TABLE `cms_block_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_page`
--

DROP TABLE IF EXISTS `cms_page`;
CREATE TABLE `cms_page` (
  `page_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Page ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Page Title',
  `page_layout` varchar(255) DEFAULT NULL COMMENT 'Page Layout',
  `meta_keywords` text COMMENT 'Page Meta Keywords',
  `meta_description` text COMMENT 'Page Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Page String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Page Content Heading',
  `content` mediumtext COMMENT 'Page Content',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Page Creation Time',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Page Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Page Active',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Page Sort Order',
  `layout_update_xml` text COMMENT 'Page Layout Update Content',
  `custom_theme` varchar(100) DEFAULT NULL COMMENT 'Page Custom Theme',
  `custom_root_template` varchar(255) DEFAULT NULL COMMENT 'Page Custom Template',
  `custom_layout_update_xml` text COMMENT 'Page Custom Layout Update Content',
  `custom_theme_from` date DEFAULT NULL COMMENT 'Page Custom Theme Active From Date',
  `custom_theme_to` date DEFAULT NULL COMMENT 'Page Custom Theme Active To Date',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Page Meta Title',
  PRIMARY KEY (`page_id`),
  KEY `CMS_PAGE_IDENTIFIER` (`identifier`),
  FULLTEXT KEY `CMS_PAGE_TITLE_META_KEYWORDS_META_DESCRIPTION_IDENTIFIER_CONTENT` (`title`,`meta_keywords`,`meta_description`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='CMS Page Table';

--
-- Dumping data for table `cms_page`
--

LOCK TABLES `cms_page` WRITE;
/*!40000 ALTER TABLE `cms_page` DISABLE KEYS */;
INSERT INTO `cms_page` VALUES (1,'404 Not Found','2columns-right','Page keywords','Page description','no-route','Whoops, our bad...','<dl>\r\n<dt>The page you requested was not found, and we have a fine guess why.</dt>\r\n<dd>\r\n<ul class=\"disc\">\r\n<li>If you typed the URL directly, please make sure the spelling is correct.</li>\r\n<li>If you clicked on a link to get here, the link is outdated.</li>\r\n</ul></dd>\r\n</dl>\r\n<dl>\r\n<dt>What can you do?</dt>\r\n<dd>Have no fear, help is near! There are many ways you can get back on track with Magento Store.</dd>\r\n<dd>\r\n<ul class=\"disc\">\r\n<li><a href=\"#\" onclick=\"history.go(-1); return false;\">Go back</a> to the previous page.</li>\r\n<li>Use the search bar at the top of the page to search for your products.</li>\r\n<li>Follow these links to get you back on track!<br /><a href=\"{{store url=\"\"}}\">Store Home</a> <span class=\"separator\">|</span> <a href=\"{{store url=\"customer/account\"}}\">My Account</a></li></ul></dd></dl>\r\n','2016-10-06 02:16:05','2016-10-06 02:16:05',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Home page','1column','','','home','Home Page','<p>CMS homepage content goes here.</p>\r\n','2016-10-06 02:16:07','2016-10-06 10:02:39',1,0,'<!--\n    <referenceContainer name=\"right\">\n        <action method=\"unsetChild\"><argument name=\"alias\" xsi:type=\"string\">right.reports.product.viewed</argument></action>\n        <action method=\"unsetChild\"><argument name=\"alias\" xsi:type=\"string\">right.reports.product.compared</argument></action>\n    </referenceContainer>-->',NULL,NULL,NULL,NULL,NULL,''),(3,'Enable Cookies','1column',NULL,NULL,'enable-cookies','What are Cookies?','<div class=\"enable-cookies cms-content\">\r\n<p>\"Cookies\" are little pieces of data we send when you visit our store. Cookies help us get to know you better and personalize your experience. Plus they help protect you and other shoppers from fraud.</p>\r\n<p style=\"margin-bottom: 20px;\">Set your browser to accept cookies so you can buy items, save items, and receive customized recommendations. Here’s how:</p>\r\n<ul>\r\n<li><a href=\"https://support.google.com/accounts/answer/61416?hl=en\" target=\"_blank\">Google Chrome</a></li>\r\n<li><a href=\"http://windows.microsoft.com/en-us/internet-explorer/delete-manage-cookies\" target=\"_blank\">Internet Explorer</a></li>\r\n<li><a href=\"http://support.apple.com/kb/PH19214\" target=\"_blank\">Safari</a></li>\r\n<li><a href=\"https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences\" target=\"_blank\">Mozilla/Firefox</a></li>\r\n</ul>\r\n</div>','2016-10-06 02:16:07','2016-10-06 02:16:07',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Privacy and Cookie Policy','1column',NULL,NULL,'privacy-policy-cookie-restriction-mode','Privacy and Cookie Policy','<div class=\"privacy-policy cms-content\">\n    <div class=\"message info\">\n        <span>\n            Please replace this text with you Privacy Policy.\n            Please add any additional cookies your website uses below (e.g. Google Analytics).\n        </span>\n    </div>\n    <p>\n        This privacy policy sets out how this website (hereafter \"the Store\") uses and protects any information that\n        you give the Store while using this website. The Store is committed to ensuring that your privacy is protected.\n        Should we ask you to provide certain information by which you can be identified when using this website, then\n        you can be assured that it will only be used in accordance with this privacy statement. The Store may change\n        this policy from time to time by updating this page. You should check this page from time to time to ensure\n        that you are happy with any changes.\n    </p>\n    <h2>What we collect</h2>\n    <p>We may collect the following information:</p>\n    <ul>\n        <li>name</li>\n        <li>contact information including email address</li>\n        <li>demographic information such as postcode, preferences and interests</li>\n        <li>other information relevant to customer surveys and/or offers</li>\n    </ul>\n    <p>\n        For the exhaustive list of cookies we collect see the <a href=\"#list\">List of cookies we collect</a> section.\n    </p>\n    <h2>What we do with the information we gather</h2>\n    <p>\n        We require this information to understand your needs and provide you with a better service,\n        and in particular for the following reasons:\n    </p>\n    <ul>\n        <li>Internal record keeping.</li>\n        <li>We may use the information to improve our products and services.</li>\n        <li>\n            We may periodically send promotional emails about new products, special offers or other information which we\n            think you may find interesting using the email address which you have provided.\n        </li>\n        <li>\n            From time to time, we may also use your information to contact you for market research purposes.\n            We may contact you by email, phone, fax or mail. We may use the information to customise the website\n            according to your interests.\n        </li>\n    </ul>\n    <h2>Security</h2>\n    <p>\n        We are committed to ensuring that your information is secure. In order to prevent unauthorised access or\n        disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and\n        secure the information we collect online.\n    </p>\n    <h2>How we use cookies</h2>\n    <p>\n        A cookie is a small file which asks permission to be placed on your computer\'s hard drive.\n        Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit\n        a particular site. Cookies allow web applications to respond to you as an individual. The web application\n        can tailor its operations to your needs, likes and dislikes by gathering and remembering information about\n        your preferences.\n    </p>\n    <p>\n        We use traffic log cookies to identify which pages are being used. This helps us analyse data about web page\n        traffic and improve our website in order to tailor it to customer needs. We only use this information for\n        statistical analysis purposes and then the data is removed from the system.\n    </p>\n    <p>\n        Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find\n        useful and which you do not. A cookie in no way gives us access to your computer or any information about you,\n        other than the data you choose to share with us. You can choose to accept or decline cookies.\n        Most web browsers automatically accept cookies, but you can usually modify your browser setting\n        to decline cookies if you prefer. This may prevent you from taking full advantage of the website.\n    </p>\n    <h2>Links to other websites</h2>\n    <p>\n        Our website may contain links to other websites of interest. However, once you have used these links\n        to leave our site, you should note that we do not have any control over that other website.\n        Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst\n        visiting such sites and such sites are not governed by this privacy statement.\n        You should exercise caution and look at the privacy statement applicable to the website in question.\n    </p>\n    <h2>Controlling your personal information</h2>\n    <p>You may choose to restrict the collection or use of your personal information in the following ways:</p>\n    <ul>\n        <li>\n            whenever you are asked to fill in a form on the website, look for the box that you can click to indicate\n            that you do not want the information to be used by anybody for direct marketing purposes\n        </li>\n        <li>\n            if you have previously agreed to us using your personal information for direct marketing purposes,\n            you may change your mind at any time by letting us know using our Contact Us information\n        </li>\n    </ul>\n    <p>\n        We will not sell, distribute or lease your personal information to third parties unless we have your permission\n        or are required by law to do so. We may use your personal information to send you promotional information\n        about third parties which we think you may find interesting if you tell us that you wish this to happen.\n    </p>\n    <p>\n        You may request details of personal information which we hold about you under the Data Protection Act 1998.\n        A small fee will be payable. If you would like a copy of the information held on you please email us this\n        request using our Contact Us information.\n    </p>\n    <p>\n        If you believe that any information we are holding on you is incorrect or incomplete,\n        please write to or email us as soon as possible, at the above address.\n        We will promptly correct any information found to be incorrect.\n    </p>\n    <h2><a name=\"list\"></a>List of cookies we collect</h2>\n    <p>The table below lists the cookies we collect and what information they store.</p>\n    <table class=\"data-table data-table-definition-list\">\n        <thead>\n        <tr>\n            <th>Cookie Name</th>\n            <th>Cookie Description</th>\n        </tr>\n        </thead>\n        <tbody>\n            <tr>\n                <th>FORM_KEY</th>\n                <td>Stores randomly generated key used to prevent forged requests.</td>\n            </tr>\n            <tr>\n                <th>PHPSESSID</th>\n                <td>Your session ID on the server.</td>\n            </tr>\n            <tr>\n                <th>GUEST-VIEW</th>\n                <td>Allows guests to view and edit their orders.</td>\n            </tr>\n            <tr>\n                <th>PERSISTENT_SHOPPING_CART</th>\n                <td>A link to information about your cart and viewing history, if you have asked for this.</td>\n            </tr>\n            <tr>\n                <th>STF</th>\n                <td>Information on products you have emailed to friends.</td>\n            </tr>\n            <tr>\n                <th>STORE</th>\n                <td>The store view or language you have selected.</td>\n            </tr>\n            <tr>\n                <th>USER_ALLOWED_SAVE_COOKIE</th>\n                <td>Indicates whether a customer allowed to use cookies.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-SESSID</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-STORAGE</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-STORAGE-SECTION-INVALIDATION</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-TIMEOUT</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>SECTION-DATA-IDS</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>PRIVATE_CONTENT_VERSION</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>X-MAGENTO-VARY</th>\n                <td>Facilitates caching of content on the server to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-TRANSLATION-FILE-VERSION</th>\n                <td>Facilitates translation of content to other languages.</td>\n            </tr>\n            <tr>\n                <th>MAGE-TRANSLATION-STORAGE</th>\n                <td>Facilitates translation of content to other languages.</td>\n            </tr>\n        </tbody>\n    </table>\n</div>','2016-10-06 02:16:07','2016-10-06 02:16:09',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Sahara Fashion8 - Responsive Magento Theme','1column','','','sahara-fashion8','','<div class=\"home-content\">{{block class=\"Magento\\\\Cms\\\\Block\\\\Block\" block_id=\"home_banner_static1\"}}\r\n {{block class=\"Plazathemes\\Producttab\\Block\\Producttab\" name=\"producttab\" template=\"producttab.phtml\"}}\r\n</div>','2016-10-06 03:36:52','2016-11-28 05:26:40',1,0,'','','',NULL,NULL,NULL,''),(6,'Sahara Fashion9 - Responsive Magento Theme','1column','','','sahara-fashion9','','<div class=\"home-content\">{{block class=\"Magento\\\\Cms\\\\Block\\\\Block\" block_id=\"home_banner_static2\"}} {{block class=\"Plazathemes\\Producttab\\Block\\Producttab\" name=\"producttab\" template=\"producttab.phtml\"}} {{block class=\"Magento\\\\Cms\\\\Block\\\\Block\" block_id=\"banner_home_content\"}} {{block class=\"Plazathemes\\Newproductslider\\Block\\Newproductslider\" name=\"newproductslider\" template=\"newproductslider.phtml\" }}</div>','2016-10-17 01:38:54','2016-11-28 05:28:29',1,0,'','','',NULL,NULL,NULL,''),(7,'Sahara Fashion10 - Responsive Magento Theme','1column','','','sahara-fashion10','','<div class=\"home-content\">{{block class=\"Magento\\\\Cms\\\\Block\\\\Block\" block_id=\"home_banner_static3\"}} {{block class=\"Plazathemes\\Producttab\\Block\\Producttab\" name=\"producttab\" template=\"producttab.phtml\"}} {{block class=\"Magento\\\\Cms\\\\Block\\\\Block\" block_id=\"banner_home_content\"}} {{block class=\"Plazathemes\\Newproductslider\\Block\\Newproductslider\" name=\"newproductslider\" template=\"newproductslider.phtml\" }}</div>','2016-10-17 08:03:06','2016-11-28 05:29:48',1,0,'','','',NULL,NULL,NULL,'');
/*!40000 ALTER TABLE `cms_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_page_store`
--

DROP TABLE IF EXISTS `cms_page_store`;
CREATE TABLE `cms_page_store` (
  `page_id` smallint(6) NOT NULL COMMENT 'Page ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`page_id`,`store_id`),
  KEY `CMS_PAGE_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Page To Store Linkage Table';

--
-- Dumping data for table `cms_page_store`
--

LOCK TABLES `cms_page_store` WRITE;
/*!40000 ALTER TABLE `cms_page_store` DISABLE KEYS */;
INSERT INTO `cms_page_store` VALUES (1,0),(2,0),(3,0),(4,0),(5,1),(6,3),(7,4);
/*!40000 ALTER TABLE `cms_page_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_config_data`
--

DROP TABLE IF EXISTS `core_config_data`;
CREATE TABLE `core_config_data` (
  `config_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Config Id',
  `scope` varchar(8) NOT NULL DEFAULT 'default' COMMENT 'Config Scope',
  `scope_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Config Scope Id',
  `path` varchar(255) NOT NULL DEFAULT 'general' COMMENT 'Config Path',
  `value` text COMMENT 'Config Value',
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `CORE_CONFIG_DATA_SCOPE_SCOPE_ID_PATH` (`scope`,`scope_id`,`path`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8 COMMENT='Config Data';

--
-- Dumping data for table `core_config_data`
--

LOCK TABLES `core_config_data` WRITE;
/*!40000 ALTER TABLE `core_config_data` DISABLE KEYS */;
INSERT INTO `core_config_data` VALUES ('1','default','0','web/seo/use_rewrites','1'),('2','default','0','web/unsecure/base_url','http://192.168.1.99:8080/ma_sahara_fashion8910/'),('3','default','0','web/secure/base_url','http://192.168.1.99:8080/ma_sahara_fashion8910/'),('4','default','0','general/locale/code','en_US'),('5','default','0','web/secure/use_in_frontend','0'),('6','default','0','web/secure/use_in_adminhtml','0'),('7','default','0','general/locale/timezone','UTC'),('8','default','0','currency/options/base','USD'),('9','default','0','currency/options/default','USD'),('10','default','0','currency/options/allow','EUR,USD'),('11','default','0','general/region/display_all','1'),('12','default','0','general/region/state_required','AT,BR,CA,CH,EE,ES,FI,LT,LV,RO,US'),('13','default','0','catalog/category/root_id','2'),('14','stores','1','design/head/title_prefix',NULL),('15','stores','1','design/head/title_suffix',NULL),('16','stores','1','design/head/includes',NULL),('17','stores','1','design/header/logo_src','stores/1/logotf8.png'),('20','stores','1','design/footer/absolute_footer',NULL),('22','stores','1','design/pagination/pagination_frame_skip',NULL),('23','stores','1','design/pagination/anchor_text_for_previous',NULL),('24','stores','1','design/pagination/anchor_text_for_next',NULL),('25','stores','1','design/watermark/image_size',NULL),('26','stores','1','design/watermark/image_imageOpacity',NULL),('27','stores','1','design/watermark/small_image_size',NULL),('28','stores','1','design/watermark/small_image_imageOpacity',NULL),('29','stores','1','design/watermark/thumbnail_size',NULL),('30','stores','1','design/watermark/thumbnail_imageOpacity',NULL),('31','stores','1','design/email/logo_alt',NULL),('32','stores','1','design/email/logo_width',NULL),('33','stores','1','design/email/logo_height',NULL),('34','stores','1','design/watermark/swatch_image_size',NULL),('35','stores','1','design/watermark/swatch_image_imageOpacity',NULL),('36','websites','1','design/head/title_prefix',NULL),('37','websites','1','design/head/title_suffix',NULL),('38','websites','1','design/head/includes',NULL),('39','websites','1','design/header/logo_src','websites/1/logotf8.png'),('40','websites','1','design/header/logo_width','212'),('41','websites','1','design/header/logo_height','67'),('42','websites','1','design/footer/absolute_footer',NULL),('44','websites','1','design/pagination/pagination_frame_skip',NULL),('45','websites','1','design/pagination/anchor_text_for_previous',NULL),('46','websites','1','design/pagination/anchor_text_for_next',NULL),('47','websites','1','design/watermark/image_size',NULL),('48','websites','1','design/watermark/image_imageOpacity',NULL),('49','websites','1','design/watermark/small_image_size',NULL),('50','websites','1','design/watermark/small_image_imageOpacity',NULL),('51','websites','1','design/watermark/thumbnail_size',NULL),('52','websites','1','design/watermark/thumbnail_imageOpacity',NULL),('53','websites','1','design/email/logo_alt',NULL),('54','websites','1','design/email/logo_width',NULL),('55','websites','1','design/email/logo_height',NULL),('56','websites','1','design/watermark/swatch_image_size',NULL),('57','websites','1','design/watermark/swatch_image_imageOpacity',NULL),('58','default','0','currency/yahoofinance/timeout','100'),('59','default','0','currency/fixerio/timeout','100'),('60','default','0','currency/import/service','yahoofinance'),('61','default','0','crontab/default/jobs/currency_rates_update/schedule/cron_expr','0 0 * * *'),('62','default','0','currency/import/time','00,00,00'),('63','default','0','currency/import/frequency','D'),('64','default','0','currency/import/error_email',NULL),('66','stores','3','design/head/title_prefix',NULL),('67','stores','3','design/head/title_suffix',NULL),('68','stores','3','design/head/includes',NULL),('69','stores','3','design/footer/absolute_footer',NULL),('70','stores','3','design/theme/theme_id','5'),('71','stores','3','design/pagination/pagination_frame_skip',NULL),('72','stores','3','design/pagination/anchor_text_for_previous',NULL),('73','stores','3','design/pagination/anchor_text_for_next',NULL),('74','stores','3','design/watermark/image_size',NULL),('75','stores','3','design/watermark/image_imageOpacity',NULL),('76','stores','3','design/watermark/small_image_size',NULL),('77','stores','3','design/watermark/small_image_imageOpacity',NULL),('78','stores','3','design/watermark/thumbnail_size',NULL),('79','stores','3','design/watermark/thumbnail_imageOpacity',NULL),('80','stores','3','design/email/logo_alt',NULL),('81','stores','3','design/email/logo_width',NULL),('82','stores','3','design/email/logo_height',NULL),('83','stores','3','design/watermark/swatch_image_size',NULL),('84','stores','3','design/watermark/swatch_image_imageOpacity',NULL),('85','default','0','producttab/new_status/enabled','1'),('86','default','0','producttab/new_status/row','1'),('87','default','0','producttab/new_status/items','4'),('88','default','0','producttab/new_status/speed','1000'),('89','default','0','producttab/new_status/qty','10'),('90','default','0','producttab/new_status/wishlist','1'),('91','default','0','producttab/new_status/addtocart','1'),('92','default','0','producttab/new_status/compare','1'),('93','default','0','producttab/new_status/navigation','1'),('94','default','0','producttab/new_status/pagination','0'),('95','default','0','producttab/new_status/auto','0'),('96','default','0','categorytab/new_status/enabled','1'),('97','default','0','categorytab/new_status/row','1'),('98','default','0','categorytab/new_status/items','4'),('99','default','0','categorytab/new_status/speed','1000'),('100','default','0','categorytab/new_status/qty','12'),('101','default','0','categorytab/new_status/wishlist','1'),('102','default','0','categorytab/new_status/addtocart','1'),('103','default','0','categorytab/new_status/compare','1'),('104','default','0','categorytab/new_status/navigation','1'),('105','default','0','categorytab/new_status/pagination','0'),('106','default','0','categorytab/new_status/auto','0'),('107','default','0','brandslider/general/enable_frontend','1'),('108','default','0','brandslider/general/title','Our Brands'),('109','default','0','brandslider/general/auto','0'),('110','default','0','brandslider/general/pagination','500'),('111','default','0','brandslider/general/qty','12'),('112','default','0','brandslider/general/default','6'),('113','default','0','brandslider/general/desktop','6'),('114','default','0','brandslider/general/desktop_small','4'),('115','default','0','brandslider/general/tablet','3'),('116','default','0','brandslider/general/mobile','2'),('117','default','0','brandslider/general/rows','1'),('118','default','0','brandslider/general/show_description','0'),('119','default','0','brandslider/general/show_next_back','1'),('120','default','0','brandslider/general/show_navigation_control','0'),('121','default','0','quickview/general/enabled','0'),('122','default','0','quickview/general/loading_image',NULL),('123','default','0','quickview/general/quickview_init','a:0:{}'),('124','default','0','quickview/general/insertion','after'),('125','stores','1','design/footer/copyright','Copyright © 2014 Plazathemes.com. All Rights Reserved.'),('126','default','0','catalog/frontend/list_per_page_values','4,8,12,16,20'),('127','default','0','catalog/frontend/list_per_page','4'),('128','default','0','catalog/frontend/list_allow_all','0'),('129','default','0','catalog/frontend/flat_catalog_product','0'),('130','default','0','catalog/productalert_cron/frequency','D'),('131','default','0','crontab/default/jobs/catalog_product_alert/schedule/cron_expr','0 0 * * *'),('132','default','0','crontab/default/jobs/catalog_product_alert/run/model',NULL),('133','default','0','catalog/productalert_cron/time','00,00,00'),('134','default','0','catalog/productalert_cron/error_email',NULL),('135','default','0','catalog/product_video/youtube_api_key',NULL),('136','default','0','catalog/price/scope','0'),('137','default','0','catalog/downloadable/shareable','0'),('138','default','0','catalog/downloadable/content_disposition','inline'),('139','default','0','catalog/custom_options/use_calendar','0'),('140','default','0','catalog/custom_options/year_range',','),('141','default','0','catalog/placeholder/image_placeholder',NULL),('142','default','0','catalog/placeholder/small_image_placeholder',NULL),('143','default','0','catalog/placeholder/swatch_image_placeholder',NULL),('144','default','0','catalog/placeholder/thumbnail_placeholder',NULL),('145','default','0','saleproductslider/new_status/enabled','1'),('146','default','0','saleproductslider/new_status/row','1'),('147','default','0','saleproductslider/new_status/title','Sale off products'),('148','default','0','saleproductslider/new_status/itemsDefault','1'),('149','default','0','saleproductslider/new_status/itemsDesktop','1'),('150','default','0','saleproductslider/new_status/itemsDesktopSmall','1'),('151','default','0','saleproductslider/new_status/itemsTablet','1'),('152','default','0','saleproductslider/new_status/itemsMobile','1'),('153','default','0','saleproductslider/new_status/speed','1000'),('154','default','0','saleproductslider/new_status/qty','6'),('155','default','0','saleproductslider/new_status/addtocart','0'),('156','default','0','saleproductslider/new_status/wishlist','0'),('157','default','0','saleproductslider/new_status/compare','0'),('158','default','0','saleproductslider/new_status/navigation','0'),('159','default','0','saleproductslider/new_status/pagination','1'),('160','default','0','saleproductslider/new_status/auto','0'),('161','default','0','pricecountdown/general/enabled','1'),('162','default','0','pricecountdown/countdown_slider/enabled','1'),('163','default','0','pricecountdown/countdown_slider/use_countdown','1'),('164','default','0','pricecountdown/countdown_slider/title','Today Deals'),('165','default','0','pricecountdown/countdown_slider/show_price','1'),('166','default','0','pricecountdown/countdown_slider/show_add_cart','1'),('167','default','0','pricecountdown/countdown_slider/show_add_wishlist','0'),('168','default','0','pricecountdown/countdown_slider/show_add_compare','0'),('169','default','0','pricecountdown/countdown_slider/show_rating','1'),('170','default','0','pricecountdown/countdown_slider/show_short_description','0'),('171','default','0','pricecountdown/countdown_slider/short_description_length','100'),('172','default','0','pricecountdown/countdown_slider/quantity','5'),('173','default','0','pricecountdown/countdown_slider/auto','0'),('174','default','0','pricecountdown/countdown_slider/speed','1000'),('175','default','0','pricecountdown/countdown_slider/item_default','1'),('176','default','0','pricecountdown/countdown_slider/item_desktop','1'),('177','default','0','pricecountdown/countdown_slider/item_desktop_small','1'),('178','default','0','pricecountdown/countdown_slider/item_tablet','3'),('179','default','0','pricecountdown/countdown_slider/item_mobile','1'),('180','default','0','pricecountdown/countdown_slider/row_number','1'),('181','default','0','pricecountdown/countdown_slider/show_navigation','1'),('182','default','0','pricecountdown/countdown_slider/show_pagination','0'),('183','default','0','pricecountdown/in_product_list/enabled','1'),('184','default','0','pricecountdown/in_product_view/enabled','1'),('185','default','0','pricecountdown/in_product_view/insertion','after'),('186','default','0','pricecountdown/in_product_view/parent_element','.product-info-main'),('187','default','0','pricecountdown/in_product_view/children_element','.product-info-price'),('188','stores','3','design/header/logo_src','stores/3/logotf8.png'),('189','stores','3','web/default/cms_home_page','sahara-fashion9'),('190','default','0','newproductslider/new_status/enabled','1'),('191','default','0','newproductslider/new_status/row','2'),('192','default','0','newproductslider/new_status/title','New Products'),('193','default','0','newproductslider/new_status/itemsDefault','4'),('194','default','0','newproductslider/new_status/itemsDesktop','4'),('195','default','0','newproductslider/new_status/itemsDesktopSmall','3'),('196','default','0','newproductslider/new_status/itemsTablet','2'),('197','default','0','newproductslider/new_status/itemsMobile','1'),('198','default','0','newproductslider/new_status/speed','1000'),('199','default','0','newproductslider/new_status/qty','10'),('200','default','0','newproductslider/new_status/addtocart','1'),('201','default','0','newproductslider/new_status/wishlist','1'),('202','default','0','newproductslider/new_status/compare','1'),('203','default','0','newproductslider/new_status/navigation','1'),('204','default','0','newproductslider/new_status/pagination','0'),('205','default','0','newproductslider/new_status/auto','0'),('206','stores','3','newproductslider/new_status/itemsDefault','8'),('207','stores','3','newproductslider/new_status/itemsDesktop','8'),('208','stores','4','design/head/title_prefix',NULL),('209','stores','4','design/head/title_suffix',NULL),('210','stores','4','design/head/includes',NULL),('211','stores','4','design/header/logo_src','stores/4/logotf8.png'),('212','stores','4','design/footer/absolute_footer',NULL),('213','stores','4','design/theme/theme_id','6'),('214','stores','4','design/pagination/pagination_frame_skip',NULL),('215','stores','4','design/pagination/anchor_text_for_previous',NULL),('216','stores','4','design/pagination/anchor_text_for_next',NULL),('217','stores','4','design/watermark/image_size',NULL),('218','stores','4','design/watermark/image_imageOpacity',NULL),('219','stores','4','design/watermark/small_image_size',NULL),('220','stores','4','design/watermark/small_image_imageOpacity',NULL),('221','stores','4','design/watermark/thumbnail_size',NULL),('222','stores','4','design/watermark/thumbnail_imageOpacity',NULL),('223','stores','4','design/email/logo_alt',NULL),('224','stores','4','design/email/logo_width',NULL),('225','stores','4','design/email/logo_height',NULL),('226','stores','4','design/watermark/swatch_image_size',NULL),('227','stores','4','design/watermark/swatch_image_imageOpacity',NULL),('228','stores','4','web/default/cms_home_page','sahara-fashion10'),('229','default','0','web/unsecure/base_static_url',NULL),('230','default','0','web/unsecure/base_media_url',NULL),('231','default','0','web/secure/base_static_url',NULL),('232','default','0','web/secure/base_media_url',NULL),('233','default','0','web/default/cms_home_page','sahara-fashion8'),('234','default','0','web/cookie/cookie_path',NULL),('235','default','0','web/cookie/cookie_domain',NULL),('236','default','0','web/cookie/cookie_httponly','1'),('237','stores','1','design/theme/theme_id','4'),('238','default','0','web/url/use_store','1'),('239','default','0','recentproductslider/general/enable_frontend','1'),('240','default','0','recentproductslider/general/title','Most View'),('241','default','0','recentproductslider/general/auto','0'),('242','default','0','recentproductslider/general/pagination','1000'),('243','default','0','recentproductslider/general/qty','10'),('244','default','0','recentproductslider/general/default','1'),('245','default','0','recentproductslider/general/desktop','1'),('246','default','0','recentproductslider/general/desktop_small','1'),('247','default','0','recentproductslider/general/tablet','1'),('248','default','0','recentproductslider/general/mobile','1'),('249','default','0','recentproductslider/general/rows','2'),('250','default','0','recentproductslider/general/show_next_back','1'),('251','default','0','recentproductslider/general/show_navigation_control','0'),('252','default','0','newsletterpopup/popup_group/enabled','1'),('253','default','0','newsletterpopup/popup_group/title','NEWSLETTER'),('254','default','0','newsletterpopup/popup_group/content','Subscribe to the fashion mailing list to receive updates on new arrivals, special offers and other discount information.'),('255','default','0','newsletterpopup/popup_group/width','790'),('256','default','0','newsletterpopup/popup_group/height','390'),('257','default','0','newsletterpopup/popup_group/speed','600'),('258','default','0','newsletterpopup/popup_group/margin_top',NULL),('259','default','0','newsletterpopup/popup_group/cookie_timeout','1'),('260','default','0','newsletterpopup/popup_group/background',NULL);
/*!40000 ALTER TABLE `core_config_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cron_schedule`
--

DROP TABLE IF EXISTS `cron_schedule`;
CREATE TABLE `cron_schedule` (
  `schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Schedule Id',
  `job_code` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Job Code',
  `status` varchar(7) NOT NULL DEFAULT 'pending' COMMENT 'Status',
  `messages` text COMMENT 'Messages',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `scheduled_at` timestamp NULL DEFAULT NULL COMMENT 'Scheduled At',
  `executed_at` timestamp NULL DEFAULT NULL COMMENT 'Executed At',
  `finished_at` timestamp NULL DEFAULT NULL COMMENT 'Finished At',
  PRIMARY KEY (`schedule_id`),
  KEY `CRON_SCHEDULE_JOB_CODE` (`job_code`),
  KEY `CRON_SCHEDULE_SCHEDULED_AT_STATUS` (`scheduled_at`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cron Schedule';

--
-- Table structure for table `customer_address_entity`
--

DROP TABLE IF EXISTS `customer_address_entity`;
CREATE TABLE `customer_address_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Active',
  `city` varchar(255) NOT NULL COMMENT 'City',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `country_id` varchar(255) NOT NULL COMMENT 'Country',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `firstname` varchar(255) NOT NULL COMMENT 'First Name',
  `lastname` varchar(255) NOT NULL COMMENT 'Last Name',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middle Name',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Zip/Postal Code',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `region` varchar(255) DEFAULT NULL COMMENT 'State/Province',
  `region_id` int(10) unsigned DEFAULT NULL COMMENT 'State/Province',
  `street` text NOT NULL COMMENT 'Street Address',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `telephone` varchar(255) NOT NULL COMMENT 'Phone Number',
  `vat_id` varchar(255) DEFAULT NULL COMMENT 'VAT number',
  `vat_is_valid` int(10) unsigned DEFAULT NULL COMMENT 'VAT number validity',
  `vat_request_date` varchar(255) DEFAULT NULL COMMENT 'VAT number validation request date',
  `vat_request_id` varchar(255) DEFAULT NULL COMMENT 'VAT number validation request ID',
  `vat_request_success` int(10) unsigned DEFAULT NULL COMMENT 'VAT number validation request success',
  PRIMARY KEY (`entity_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity';

--
-- Table structure for table `customer_address_entity_datetime`
--

DROP TABLE IF EXISTS `customer_address_entity_datetime`;
CREATE TABLE `customer_address_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Datetime';

--
-- Table structure for table `customer_address_entity_decimal`
--

DROP TABLE IF EXISTS `customer_address_entity_decimal`;
CREATE TABLE `customer_address_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Decimal';

--
-- Table structure for table `customer_address_entity_int`
--

DROP TABLE IF EXISTS `customer_address_entity_int`;
CREATE TABLE `customer_address_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Int';

--
-- Table structure for table `customer_address_entity_text`
--

DROP TABLE IF EXISTS `customer_address_entity_text`;
CREATE TABLE `customer_address_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Text';

--
-- Table structure for table `customer_address_entity_varchar`
--

DROP TABLE IF EXISTS `customer_address_entity_varchar`;
CREATE TABLE `customer_address_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Varchar';

--
-- Table structure for table `customer_dummy_cl`
--

DROP TABLE IF EXISTS `customer_dummy_cl`;
CREATE TABLE `customer_dummy_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='customer_dummy_cl';

--
-- Table structure for table `customer_eav_attribute`
--

DROP TABLE IF EXISTS `customer_eav_attribute`;
CREATE TABLE `customer_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `input_filter` varchar(255) DEFAULT NULL COMMENT 'Input Filter',
  `multiline_count` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Multiline Count',
  `validate_rules` text COMMENT 'Validate Rules',
  `is_system` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is System',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `data_model` varchar(255) DEFAULT NULL COMMENT 'Data Model',
  `is_used_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used in Grid',
  `is_visible_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible in Grid',
  `is_filterable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable in Grid',
  `is_searchable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable in Grid',
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Eav Attribute';

--
-- Dumping data for table `customer_eav_attribute`
--

LOCK TABLES `customer_eav_attribute` WRITE;
/*!40000 ALTER TABLE `customer_eav_attribute` DISABLE KEYS */;
INSERT INTO `customer_eav_attribute` VALUES (1,1,NULL,0,NULL,1,'10',NULL,1,1,1,0),(2,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(3,1,NULL,0,NULL,1,'20',NULL,1,1,0,1),(4,0,NULL,0,NULL,0,'30',NULL,0,0,0,0),(5,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'40',NULL,0,0,0,0),(6,0,NULL,0,NULL,0,'50',NULL,0,0,0,0),(7,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'60',NULL,0,0,0,0),(8,0,NULL,0,NULL,0,'70',NULL,0,0,0,0),(9,1,NULL,0,'a:1:{s:16:\"input_validation\";s:5:\"email\";}',1,'80',NULL,1,1,1,1),(10,1,NULL,0,NULL,1,'25',NULL,1,1,1,0),(11,0,'date',0,'a:1:{s:16:\"input_validation\";s:4:\"date\";}',0,'90',NULL,1,1,1,0),(12,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(13,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(14,0,NULL,0,'a:1:{s:16:\"input_validation\";s:4:\"date\";}',1,'0',NULL,0,0,0,0),(15,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(16,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(17,0,NULL,0,'a:1:{s:15:\"max_text_length\";i:255;}',0,'100',NULL,1,1,0,1),(18,0,NULL,0,NULL,1,'0',NULL,1,1,1,0),(19,0,NULL,0,NULL,0,'0',NULL,1,1,1,0),(20,0,NULL,0,'a:0:{}',0,'110',NULL,1,1,1,0),(21,1,NULL,0,NULL,1,'28',NULL,0,0,0,0),(22,0,NULL,0,NULL,0,'10',NULL,0,0,0,0),(23,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'20',NULL,1,0,0,1),(24,0,NULL,0,NULL,0,'30',NULL,0,0,0,0),(25,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'40',NULL,1,0,0,1),(26,0,NULL,0,NULL,0,'50',NULL,0,0,0,0),(27,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'60',NULL,1,0,0,1),(28,1,NULL,2,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'70',NULL,1,0,0,1),(29,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'80',NULL,1,0,0,1),(30,1,NULL,0,NULL,1,'90',NULL,1,1,1,0),(31,1,NULL,0,NULL,1,'100',NULL,1,1,0,1),(32,1,NULL,0,NULL,1,'100',NULL,0,0,0,0),(33,1,NULL,0,'a:0:{}',1,'110','Magento\\Customer\\Model\\Attribute\\Data\\Postcode',1,1,1,1),(34,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'120',NULL,1,1,1,1),(35,0,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',0,'130',NULL,1,0,0,1),(36,1,NULL,0,NULL,1,'140',NULL,0,0,0,0),(37,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(38,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(39,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(40,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(41,0,NULL,0,NULL,0,'0',NULL,0,0,0,0),(42,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(43,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(44,0,NULL,0,NULL,1,'0',NULL,0,0,0,0);
/*!40000 ALTER TABLE `customer_eav_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_eav_attribute_website`
--

DROP TABLE IF EXISTS `customer_eav_attribute_website`;
CREATE TABLE `customer_eav_attribute_website` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `is_visible` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Visible',
  `is_required` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Required',
  `default_value` text COMMENT 'Default Value',
  `multiline_count` smallint(5) unsigned DEFAULT NULL COMMENT 'Multiline Count',
  PRIMARY KEY (`attribute_id`,`website_id`),
  KEY `CUSTOMER_EAV_ATTRIBUTE_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Eav Attribute Website';

--
-- Dumping data for table `customer_eav_attribute_website`
--

LOCK TABLES `customer_eav_attribute_website` WRITE;
/*!40000 ALTER TABLE `customer_eav_attribute_website` DISABLE KEYS */;
INSERT INTO `customer_eav_attribute_website` VALUES (1,1,NULL,NULL,NULL,NULL),(3,1,NULL,NULL,NULL,NULL),(9,1,NULL,NULL,NULL,NULL),(10,1,NULL,NULL,NULL,NULL),(11,1,NULL,NULL,NULL,NULL),(17,1,NULL,NULL,NULL,NULL),(18,1,NULL,NULL,NULL,NULL),(19,1,NULL,NULL,NULL,NULL),(20,1,NULL,NULL,NULL,NULL),(21,1,NULL,NULL,NULL,NULL),(23,1,NULL,NULL,NULL,NULL),(25,1,NULL,NULL,NULL,NULL),(27,1,NULL,NULL,NULL,NULL),(28,1,NULL,NULL,NULL,NULL),(29,1,NULL,NULL,NULL,NULL),(30,1,NULL,NULL,NULL,NULL),(31,1,NULL,NULL,NULL,NULL),(32,1,NULL,NULL,NULL,NULL),(33,1,NULL,NULL,NULL,NULL),(34,1,NULL,NULL,NULL,NULL),(35,1,NULL,NULL,NULL,NULL),(36,1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `customer_eav_attribute_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_entity`
--

DROP TABLE IF EXISTS `customer_entity`;
CREATE TABLE `customer_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Website Id',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Active',
  `disable_auto_group_change` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Disable automatic group change based on VAT ID',
  `created_in` varchar(255) DEFAULT NULL COMMENT 'Created From',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'First Name',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middle Name/Initial',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Last Name',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `dob` date DEFAULT NULL COMMENT 'Date of Birth',
  `password_hash` varchar(128) DEFAULT NULL COMMENT 'Password_hash',
  `rp_token` varchar(128) DEFAULT NULL COMMENT 'Reset password token',
  `rp_token_created_at` datetime DEFAULT NULL COMMENT 'Reset password token creation time',
  `default_billing` int(10) unsigned DEFAULT NULL COMMENT 'Default Billing Address',
  `default_shipping` int(10) unsigned DEFAULT NULL COMMENT 'Default Shipping Address',
  `taxvat` varchar(50) DEFAULT NULL COMMENT 'Tax/VAT Number',
  `confirmation` varchar(64) DEFAULT NULL COMMENT 'Is Confirmed',
  `gender` smallint(5) unsigned DEFAULT NULL COMMENT 'Gender',
  `failures_num` smallint(6) DEFAULT '0' COMMENT 'Failure Number',
  `first_failure` timestamp NULL DEFAULT NULL COMMENT 'First Failure',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Lock Expiration Date',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_EMAIL_WEBSITE_ID` (`email`,`website_id`),
  KEY `CUSTOMER_ENTITY_STORE_ID` (`store_id`),
  KEY `CUSTOMER_ENTITY_WEBSITE_ID` (`website_id`),
  KEY `CUSTOMER_ENTITY_FIRSTNAME` (`firstname`),
  KEY `CUSTOMER_ENTITY_LASTNAME` (`lastname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity';

--
-- Table structure for table `customer_entity_datetime`
--

DROP TABLE IF EXISTS `customer_entity_datetime`;
CREATE TABLE `customer_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Datetime';

--
-- Table structure for table `customer_entity_decimal`
--

DROP TABLE IF EXISTS `customer_entity_decimal`;
CREATE TABLE `customer_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Decimal';

--
-- Table structure for table `customer_entity_int`
--

DROP TABLE IF EXISTS `customer_entity_int`;
CREATE TABLE `customer_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Int';

--
-- Table structure for table `customer_entity_text`
--

DROP TABLE IF EXISTS `customer_entity_text`;
CREATE TABLE `customer_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Text';

--
-- Table structure for table `customer_entity_varchar`
--

DROP TABLE IF EXISTS `customer_entity_varchar`;
CREATE TABLE `customer_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Varchar';

--
-- Table structure for table `customer_form_attribute`
--

DROP TABLE IF EXISTS `customer_form_attribute`;
CREATE TABLE `customer_form_attribute` (
  `form_code` varchar(32) NOT NULL COMMENT 'Form Code',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`form_code`,`attribute_id`),
  KEY `CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Form Attribute';

--
-- Dumping data for table `customer_form_attribute`
--

LOCK TABLES `customer_form_attribute` WRITE;
/*!40000 ALTER TABLE `customer_form_attribute` DISABLE KEYS */;
INSERT INTO `customer_form_attribute` VALUES ('adminhtml_checkout',9),('adminhtml_checkout',10),('adminhtml_checkout',11),('adminhtml_checkout',17),('adminhtml_checkout',20),('adminhtml_customer',1),('adminhtml_customer',3),('adminhtml_customer',4),('adminhtml_customer',5),('adminhtml_customer',6),('adminhtml_customer',7),('adminhtml_customer',8),('adminhtml_customer',9),('adminhtml_customer',10),('adminhtml_customer',11),('adminhtml_customer',17),('adminhtml_customer',19),('adminhtml_customer',20),('adminhtml_customer',21),('adminhtml_customer_address',22),('adminhtml_customer_address',23),('adminhtml_customer_address',24),('adminhtml_customer_address',25),('adminhtml_customer_address',26),('adminhtml_customer_address',27),('adminhtml_customer_address',28),('adminhtml_customer_address',29),('adminhtml_customer_address',30),('adminhtml_customer_address',31),('adminhtml_customer_address',32),('adminhtml_customer_address',33),('adminhtml_customer_address',34),('adminhtml_customer_address',35),('adminhtml_customer_address',36),('customer_account_create',4),('customer_account_create',5),('customer_account_create',6),('customer_account_create',7),('customer_account_create',8),('customer_account_create',9),('customer_account_create',11),('customer_account_create',17),('customer_account_create',19),('customer_account_create',20),('customer_account_edit',4),('customer_account_edit',5),('customer_account_edit',6),('customer_account_edit',7),('customer_account_edit',8),('customer_account_edit',9),('customer_account_edit',11),('customer_account_edit',17),('customer_account_edit',19),('customer_account_edit',20),('customer_address_edit',22),('customer_address_edit',23),('customer_address_edit',24),('customer_address_edit',25),('customer_address_edit',26),('customer_address_edit',27),('customer_address_edit',28),('customer_address_edit',29),('customer_address_edit',30),('customer_address_edit',31),('customer_address_edit',32),('customer_address_edit',33),('customer_address_edit',34),('customer_address_edit',35),('customer_address_edit',36),('customer_register_address',22),('customer_register_address',23),('customer_register_address',24),('customer_register_address',25),('customer_register_address',26),('customer_register_address',27),('customer_register_address',28),('customer_register_address',29),('customer_register_address',30),('customer_register_address',31),('customer_register_address',32),('customer_register_address',33),('customer_register_address',34),('customer_register_address',35),('customer_register_address',36);
/*!40000 ALTER TABLE `customer_form_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_grid_flat`
--

DROP TABLE IF EXISTS `customer_grid_flat`;
CREATE TABLE `customer_grid_flat` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `name` text COMMENT 'Name',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `group_id` int(11) DEFAULT NULL COMMENT 'Group_id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created_at',
  `website_id` int(11) DEFAULT NULL COMMENT 'Website_id',
  `confirmation` varchar(255) DEFAULT NULL COMMENT 'Confirmation',
  `created_in` text COMMENT 'Created_in',
  `dob` date DEFAULT NULL COMMENT 'Dob',
  `gender` int(11) DEFAULT NULL COMMENT 'Gender',
  `taxvat` varchar(255) DEFAULT NULL COMMENT 'Taxvat',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Lock_expires',
  `billing_full` text COMMENT 'Billing_full',
  `billing_firstname` varchar(255) DEFAULT NULL COMMENT 'Billing_firstname',
  `billing_lastname` varchar(255) DEFAULT NULL COMMENT 'Billing_lastname',
  `billing_telephone` varchar(255) DEFAULT NULL COMMENT 'Billing_telephone',
  `billing_postcode` varchar(255) DEFAULT NULL COMMENT 'Billing_postcode',
  `billing_country_id` varchar(255) DEFAULT NULL COMMENT 'Billing_country_id',
  `billing_region` varchar(255) DEFAULT NULL COMMENT 'Billing_region',
  `billing_street` varchar(255) DEFAULT NULL COMMENT 'Billing_street',
  `billing_city` varchar(255) DEFAULT NULL COMMENT 'Billing_city',
  `billing_fax` varchar(255) DEFAULT NULL COMMENT 'Billing_fax',
  `billing_vat_id` varchar(255) DEFAULT NULL COMMENT 'Billing_vat_id',
  `billing_company` varchar(255) DEFAULT NULL COMMENT 'Billing_company',
  `shipping_full` text COMMENT 'Shipping_full',
  PRIMARY KEY (`entity_id`),
  KEY `CUSTOMER_GRID_FLAT_GROUP_ID` (`group_id`),
  KEY `CUSTOMER_GRID_FLAT_CREATED_AT` (`created_at`),
  KEY `CUSTOMER_GRID_FLAT_WEBSITE_ID` (`website_id`),
  KEY `CUSTOMER_GRID_FLAT_CONFIRMATION` (`confirmation`),
  KEY `CUSTOMER_GRID_FLAT_DOB` (`dob`),
  KEY `CUSTOMER_GRID_FLAT_GENDER` (`gender`),
  KEY `CUSTOMER_GRID_FLAT_BILLING_COUNTRY_ID` (`billing_country_id`),
  FULLTEXT KEY `FTI_B691CA777399890C71AC8A4CDFB8EA99` (`name`,`email`,`created_in`,`taxvat`,`billing_full`,`billing_firstname`,`billing_lastname`,`billing_telephone`,`billing_postcode`,`billing_region`,`billing_city`,`billing_fax`,`billing_company`,`shipping_full`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='customer_grid_flat';

--
-- Table structure for table `customer_group`
--

DROP TABLE IF EXISTS `customer_group`;
CREATE TABLE `customer_group` (
  `customer_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Customer Group Id',
  `customer_group_code` varchar(32) NOT NULL COMMENT 'Customer Group Code',
  `tax_class_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Tax Class Id',
  PRIMARY KEY (`customer_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Customer Group';

--
-- Dumping data for table `customer_group`
--

LOCK TABLES `customer_group` WRITE;
/*!40000 ALTER TABLE `customer_group` DISABLE KEYS */;
INSERT INTO `customer_group` VALUES (0,'NOT LOGGED IN','3'),(1,'General','3'),(2,'Wholesale','3'),(3,'Retailer','3');
/*!40000 ALTER TABLE `customer_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_log`
--

DROP TABLE IF EXISTS `customer_log`;
CREATE TABLE `customer_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `customer_id` int(11) NOT NULL COMMENT 'Customer ID',
  `last_login_at` timestamp NULL DEFAULT NULL COMMENT 'Last Login Time',
  `last_logout_at` timestamp NULL DEFAULT NULL COMMENT 'Last Logout Time',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `CUSTOMER_LOG_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Log Table';

--
-- Table structure for table `customer_visitor`
--

DROP TABLE IF EXISTS `customer_visitor`;
CREATE TABLE `customer_visitor` (
  `visitor_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Visitor ID',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `session_id` varchar(64) DEFAULT NULL COMMENT 'Session ID',
  `last_visit_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Last Visit Time',
  PRIMARY KEY (`visitor_id`),
  KEY `CUSTOMER_VISITOR_CUSTOMER_ID` (`customer_id`),
  KEY `CUSTOMER_VISITOR_LAST_VISIT_AT` (`last_visit_at`)
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8 COMMENT='Visitor Table';

--
-- Dumping data for table `customer_visitor`
--

LOCK TABLES `customer_visitor` WRITE;
/*!40000 ALTER TABLE `customer_visitor` DISABLE KEYS */;
INSERT INTO `customer_visitor` VALUES (1,NULL,'b72vracg4l6d77s8ohdc8fhf27','2016-10-06 02:20:29'),(2,NULL,'kuros5ekvh95ahahcb0r5iqq17','2016-10-06 02:56:35'),(3,NULL,'8fj0r9n91p7aj9457j7ept6p60','2016-10-06 04:27:08'),(4,NULL,'bd4qd69vfk2iu8ltun2fg6p5u6','2016-10-06 04:35:33'),(5,NULL,'2dtbtjtm78htap4ueht6fs7784','2016-10-06 07:02:17'),(6,NULL,'n46v0k9eahois9h1c0i33a09f4','2016-10-06 08:14:20'),(7,NULL,'jh02dl5kcu232d2qmvivtet9v2','2016-10-06 09:23:25'),(8,NULL,'huothe59e1ev8r406433q09ss2','2016-10-06 10:28:05'),(9,NULL,'ipd5a1ggf39mn1ktf3eiok4s87','2016-10-06 10:30:41'),(10,NULL,'el2eearqfhnr9hp56eag2j7fk6','2016-10-07 02:34:40'),(11,NULL,'kht0ksdsfsvptgk9v6tdqpqov3','2016-10-07 03:40:19'),(12,NULL,'as0ok7d83k167ltp317baumtb0','2016-10-07 04:30:55'),(13,NULL,'cn9ptqsdtjtc85tor4r1495uc7','2016-10-07 04:54:33'),(14,NULL,'4ada9lqsjrlkcsmiovd73r6pl1','2016-10-07 07:16:46'),(15,NULL,'6b6lu7o7ojr3j3hq6nft8migp5','2016-10-07 08:21:10'),(16,NULL,'ur6av5vejktu91ct2pqnar6rg0','2016-10-07 09:30:59'),(17,NULL,'et5577lgr0ivkbp03t465fiko1','2016-10-07 10:31:06'),(18,NULL,'86ovtcjj59vsi08hrlkdr8n0l0','2016-10-08 01:40:45'),(19,NULL,'kb7b61qr63qi61luh14rc2l282','2016-10-08 03:32:31'),(20,NULL,'4jmmmkkhredd829lujpepmm8h5','2016-10-08 04:33:02'),(21,NULL,'73cd421oapit9qre1t3bin1bc0','2016-10-08 05:01:22'),(22,NULL,'ueha6tunfdvgg4hbafodb43vk4','2016-10-10 02:10:49'),(23,NULL,'csrhleahc66oq0534l9fvs9035','2016-10-10 03:28:11'),(24,NULL,'rhe9bhafu2ai2ctj26p6ojm711','2016-10-10 04:19:59'),(25,NULL,'ertoqlr5cbias3anbmju45sds1','2016-10-10 04:33:02'),(26,NULL,'n0e9q25mmb0ubl258oas6ohf04','2016-10-10 07:33:54'),(27,NULL,'ge5a2si4bi7o0ld5kqgord8cj5','2016-10-10 08:36:30'),(28,NULL,'47rrnv0qkefbubo2hof0esvtl1','2016-10-10 09:35:58'),(29,NULL,'4ebldtpo446quk5a200atstds4','2016-10-10 10:26:14'),(30,NULL,'i5spi76bsvo2iavqkenciqnv47','2016-10-11 02:21:21'),(31,NULL,'5jvvab4ihpufpebjbd8h4ord15','2016-10-11 03:22:32'),(32,NULL,'iaufpn6cdq41jp12qnbc2om043','2016-10-11 04:22:01'),(33,NULL,'cc69qcf2hge4mda24q0psl2sm5','2016-10-11 04:35:18'),(34,NULL,'0ce8daeqg4cldib8od5udsk5d1','2016-10-11 07:25:05'),(35,NULL,'g164csc8brssng77ltqp6ed7m7','2016-10-11 08:31:10'),(36,NULL,'nj5tpbck5asult6leqf5jfl265','2016-10-11 09:38:58'),(37,NULL,'etn5p7te1cbkb9m9vbap2md2e7','2016-10-11 10:35:59'),(38,NULL,'86uqnq8iml98u32lt5klnd5d26','2016-10-12 02:06:43'),(39,NULL,'rcod1phthmssmpjpsla0vh3g30','2016-10-12 03:13:23'),(40,NULL,'pvkjcnbmlgdrv8ioffguqv6l00','2016-10-12 03:18:47'),(41,NULL,'tbpin7064tklqqk47f7btcig72','2016-10-12 04:21:31'),(42,NULL,'vap7ot7cfso8hbvo0quhqtc7k6','2016-10-12 04:41:23'),(43,NULL,'6d12i41olb7pbofvlh9ha26vu6','2016-10-12 07:12:58'),(44,NULL,'tpq0gv32ceku8hq897jqoo4n26','2016-10-12 08:24:59'),(45,NULL,'hsc36n3qqdn7mlsr1an0b9stv5','2016-10-12 09:35:37'),(46,NULL,'9ill50jtbvk35lkdnq846na9b2','2016-10-12 10:31:40'),(47,NULL,'410f1jrrs4sg09o32tl3p0frh4','2016-10-13 01:57:21'),(48,NULL,'h0vrtoefr9hh80na4vg3oo3ih1','2016-10-13 00:56:08'),(49,NULL,'mehdtu70sbk177p3ksrinfqfq1','2016-10-13 02:57:32'),(50,NULL,'2b0750dcb5a4rsq6s6qotv93h3','2016-10-13 03:42:59'),(51,NULL,'emqv9phdfdfaph5sj6lnu8qqo7','2016-10-13 04:32:04'),(52,NULL,'ctb4ke95lo7mqrkhpbdpq26mp1','2016-10-13 07:20:54'),(53,NULL,'mtjrglmrsckbk4cd299tm6mpg6','2016-10-13 08:20:43'),(54,NULL,'c6l5iemel4dfitg0otf5i4kfl0','2016-10-13 09:22:47'),(55,NULL,'bbrmg1e0f7h0i8hoshclc937c6','2016-10-13 10:35:20'),(56,NULL,'dbdhceba790f2m3qb105up13o6','2016-10-14 02:14:10'),(57,NULL,'k0ri27bv3mjgsic4ppa7r6i0r0','2016-10-14 01:17:42'),(58,NULL,'212na8ts3147t4d7uqckjcv6i2','2016-10-14 03:30:25'),(59,NULL,'2q166a481h49fs0rt4ltpuu4i6','2016-10-14 04:22:19'),(60,NULL,'kvccfc7ifuuqi8p7rm2gdpjih4','2016-10-14 05:18:46'),(61,NULL,'omcrer681qorapgcggtoc0lqg1','2016-10-14 07:28:30'),(62,NULL,'nbt3528avkojgitrqbu5rtpq45','2016-10-14 07:41:02'),(63,NULL,'r3logvlnuvquhuamdip776vm06','2016-10-14 08:39:09'),(64,NULL,'7ve44tml2o7uu5jdemvcjaspc6','2016-10-14 09:45:38'),(65,NULL,'nrhpnlv23utoldd7rv5ojl38e3','2016-10-14 10:26:21'),(66,NULL,'o2hj57u5icsm8fcriduitq0s20','2016-10-15 02:00:35'),(67,NULL,'nm6i3qd3jm6m6d2ghsorsc2p13','2016-10-15 01:06:54'),(68,NULL,'51o8k5t568a4j7bvvltvf3q7a1','2016-10-15 03:09:33'),(69,NULL,'bi8jfl0fmp6pgtr3f0rupm9gj7','2016-10-15 04:20:26'),(70,NULL,'dj45pnfqag6f2fv6kgbfn5arm5','2016-10-15 04:25:12'),(71,NULL,'tf2acmecl8d2nifia8bbq9jes5','2016-10-15 10:11:08'),(72,NULL,'n774lirt4ailosgpp3euu0f6m4','2016-10-17 01:33:09'),(73,NULL,'8e1km3t5dd130uv674bt59o4k0','2016-10-17 01:08:41'),(74,NULL,'ar2ajb6oe1mrcrbf7e1jonj3v0','2016-10-17 04:02:07'),(75,NULL,'blemr0mh3uq9mu3ndo6bnnjjr5','2016-10-17 04:32:13'),(76,NULL,'6fn61prenr2u4c226vcmq7dbq5','2016-10-17 07:24:35'),(77,NULL,'a78dqjfjjdniuqn7thlidiea53','2016-10-17 06:22:50'),(78,NULL,'7amebqtpq6p897dne0h3r6f3a7','2016-10-17 07:42:09'),(79,NULL,'5qnl9qq07bibqreiatk8qca916','2016-10-17 10:06:35'),(80,NULL,'7r3dpuu56sehti4cbeln6jhho1','2016-10-17 10:29:23'),(81,NULL,'an68lnbtih8siupifk0vqf7lv4','2016-10-17 10:15:11'),(82,NULL,'40hpu5c53at9mbhe2vl63645d7','2016-10-18 02:19:25'),(83,NULL,'1fc7u42304kn734b6o46qgjqr0','2016-10-18 03:10:54'),(84,NULL,'3a285serfk07qvsip7tdu3flv5','2016-10-18 03:36:51'),(85,NULL,'6iuvrtonri642qlb0fq7v42ai0','2016-10-18 07:54:18'),(86,NULL,'sm9rv5umop470msvoh7n85r2v4','2016-10-18 10:06:01'),(87,NULL,'te8pm8fglktjcbbr1or37jrq95','2016-10-19 01:42:09'),(88,NULL,'rovk60lcjo4o1fp0q36umacme4','2016-10-19 02:55:17'),(89,NULL,'8sslhjn6cbhm97ieoeh8rb4b92','2016-10-19 07:21:55'),(90,NULL,'q6nkhv4pltsbsh400pnhpln5a6','2016-10-19 07:22:27'),(91,NULL,'8gto49csjiaep98su03hvua2c6','2016-10-19 07:25:16'),(92,NULL,'hkr3pbs9rdncgcf38phj00far2','2016-10-19 07:26:55'),(93,NULL,'ki1g7jr8cebv4ng3gvpn0eqba0','2016-10-19 07:31:09'),(94,NULL,'tf6d063fnulqi6p4a659cld2v1','2016-10-19 07:31:27'),(95,NULL,'phhclp7ji8ernc0ho6dasdc3l6','2016-11-03 07:08:57'),(96,NULL,'cbaeq6a2dsfu5kep3k5ei0kb20','2016-11-05 03:39:09'),(97,NULL,'knrgdjvn8etgrd160iqorr8bp7','2016-11-05 03:08:06'),(98,NULL,'apst8sqpkm27m7mnpqr48flea4','2016-11-05 03:16:43'),(99,NULL,'8egk6lva7gsstjoij8l6d8j8o0','2016-11-05 04:38:40'),(100,NULL,'kuao6ns342lqvroqlbcmcc0l75','2016-11-07 01:23:55'),(101,NULL,'tges80sjrc0244h7o9113uumd3','2016-11-07 02:56:58'),(102,NULL,'eg2qua54ho2uogni9rg9ajo024','2016-11-07 08:21:26'),(103,NULL,'i459mfehj4r6bdrded86u0iqm6','2016-11-07 08:21:26'),(104,NULL,'bkqk6ll9peg4rtel555ebv4af4','2016-11-07 08:21:26'),(105,NULL,'1s1soao95m0irdqpj7sc2eajc1','2016-11-07 08:21:26'),(106,NULL,'kvitrtagde0tac7vg8kdvp0lc3','2016-11-07 08:21:26'),(107,NULL,'95qotoem3t0unhmdtkg38lobi6','2016-11-07 08:21:26'),(108,NULL,'7rn0i44db7lp3tco7g9vbto8p4','2016-11-07 08:21:37'),(109,NULL,'bnhfk7vg7f9lhoic1mooaofkl0','2016-11-07 08:21:37'),(110,NULL,'6pqml9so2h2rqpe60usp88f8n3','2016-11-07 08:21:37'),(111,NULL,'sc58jdf1k0jvf5sgvu22t8vb41','2016-11-07 08:21:37'),(112,NULL,'k57lph3bpnigfs0on36jd0hsf2','2016-11-07 08:21:37'),(113,NULL,'cc0o45kv5cshoq8t3n8n5v7jc2','2016-11-07 08:21:38'),(114,NULL,'8rbs9ihsor0483i2rsbbgbj005','2016-11-07 08:21:41'),(115,NULL,'sra9vk2e0j4mnf4dp53bde5683','2016-11-07 08:21:41'),(116,NULL,'go2nfptu67jr1duhkglt0sntq2','2016-11-07 08:21:41'),(117,NULL,'9c73lfifsra0kvce82kumkr540','2016-11-07 08:21:41'),(118,NULL,'vag32co8jhoutagqbk33s9en35','2016-11-07 08:21:41'),(119,NULL,'jbaas73m37u7mv67s4eqim5nt2','2016-11-07 08:21:42'),(120,NULL,'vjm3v0ou45ki62284cv3q32jn5','2016-11-07 08:21:44'),(121,NULL,'samerklp0655krugfupupa92h2','2016-11-07 08:21:44'),(122,NULL,'mnc1tk6qqhgbc0c14g0kv1a285','2016-11-07 08:21:44'),(123,NULL,'s1o90hrlq93ua5bi7qni41un94','2016-11-07 08:21:44'),(124,NULL,'95fs3l5qna3ue1qgh1h3rk4fr5','2016-11-07 08:21:44'),(125,NULL,'kqbreimqusrudm792kguk9ioj3','2016-11-07 08:21:44'),(126,NULL,'s4ehjvvife01kgllhfk388a454','2016-11-07 08:21:47'),(127,NULL,'asf9j9alpru7cvosbu8riegve7','2016-11-07 08:21:47'),(128,NULL,'8b4ahb768q1eqfhfkroq21duk0','2016-11-07 08:21:47'),(129,NULL,'i5tl944hhq604qvj3e1877nv50','2016-11-07 08:21:47'),(130,NULL,'m799juu2e3a4unj373nvu1d1t1','2016-11-07 08:21:47'),(131,NULL,'05rrljqn3f8fgmuakobeoofio0','2016-11-08 01:56:32'),(132,NULL,'sak6vf2f7jfgdmj3v20shho155','2016-11-08 03:55:17'),(133,NULL,'iggc02ta37bbvj4ehu587rf3b3','2016-11-08 08:05:47'),(134,NULL,'qrm3qch6187thea0vl4jrt7lq7','2016-11-10 10:01:51'),(135,NULL,'7a4st6lh0ecrp3fkqr9oqdl7o5','2016-11-10 10:13:44'),(136,NULL,'v3gvdrilugkjj0sejqttfll832','2016-11-11 01:47:16'),(137,NULL,'ccftnvros47ip361dg5sbinqo7','2016-11-11 01:46:08'),(138,NULL,'q4c2boujmb87folob7j80aj122','2016-11-11 05:23:55'),(139,NULL,'hjsapct146dek3vs0hdgu2m1i5','2016-11-11 07:16:18'),(140,NULL,'b685ai12bo8n5flnp52lt458k5','2016-11-11 07:33:09'),(141,NULL,'dvr592ck3soa38co0oftniq5a6','2016-11-11 08:54:18'),(142,NULL,'ige6c2v820glqnbgcu54fe23h4','2016-11-11 10:07:07'),(143,NULL,'nb1icec2g5l1otfhprmgapgb16','2016-11-11 10:25:33'),(144,NULL,'j9re7dcqg06qlsiibpmdccc457','2016-11-12 01:49:34'),(145,NULL,'ujd0sv4hkqmjuhd6ggssfp6953','2016-11-12 04:43:50'),(146,NULL,'vvh4859n6c7pgve2j6e11pu530','2016-11-12 05:03:10'),(147,NULL,'2kfgaj4s6f3bngk81shfp15ol1','2016-11-14 02:13:04'),(148,NULL,'f9ssalu4nume81kbbt9cbe69p3','2016-11-14 06:47:36'),(149,NULL,'mfs0ngvn5tu8f1alacf59r4o63','2016-11-15 09:04:34'),(150,NULL,'5k8k9e12jdhbjmlem1r3j50mv2','2016-11-15 09:40:59'),(151,NULL,'5djm77g62rt3gnm7ge14idtqo0','2016-11-15 10:46:28'),(152,NULL,'vf2dgeeffmk7mbavfttsc0lqa2','2016-11-16 02:23:26'),(153,NULL,'0uoeachuc5bv9h1s22djntt830','2016-11-16 03:19:22'),(154,NULL,'g8nfm2vrt31mg2ihg1qer51e70','2016-11-16 04:35:38'),(155,NULL,'2tg7od5pr706m64t7ft3lp7dr5','2016-11-16 04:42:59'),(156,NULL,'arf5mqpvisvbea57l26mg5sde4','2016-11-16 07:06:56'),(157,NULL,'s5ol34dv47kc04omqq62vc0b14','2016-11-16 06:21:15'),(158,NULL,'cr0ct79talatvo0oa8ri20qlg7','2016-11-16 06:54:28'),(159,NULL,'1kja9ofptc9jfbkh2iknlm3t21','2016-11-16 09:27:52'),(160,NULL,'gfutecto3l1bqk46hhkosebgf0','2016-11-17 01:25:38'),(161,NULL,'chv6n9639pglm1h9abg59tvck5','2016-11-17 04:21:59'),(162,NULL,'2a9b6v86qqdi5lfrhekglm3c10','2016-11-17 08:29:18'),(163,NULL,'lk8albe0g7udbl9fr8vf3tadp6','2016-11-17 09:52:43'),(164,NULL,'gcfpu1rl5brnqqo52c9ls87ei2','2016-11-17 10:32:59'),(165,NULL,'198r408b3cp1sv99kse8vrgq75','2016-11-17 10:32:54'),(166,NULL,'ogahi355pek47ht84j2vv3jta5','2016-11-21 01:46:19'),(167,NULL,'fi5k6gq0csr1f6lsb5772gdsd7','2016-11-21 03:51:42'),(168,NULL,'kulp64n8037n5lrchsjm4kkm70','2016-11-21 06:14:32'),(169,NULL,'md1a7kfggsviooieg0cp6qedk5','2016-11-22 04:07:22'),(170,NULL,'llcm4k4gj8obj7qjdn6tb80v60','2016-11-22 07:34:57'),(171,NULL,'vbtba6r9borqoj4bi9plnfqd95','2016-11-22 07:35:53'),(172,NULL,'tgt19upmvvta78nd49rppmmc90','2016-11-22 07:36:06'),(173,NULL,'01i8sadaaonqmrbsn24l6i7m63','2016-11-22 10:22:14'),(174,NULL,'go10dprnd6akel398l182vnrg6','2016-11-22 10:04:53'),(175,NULL,'j8mf6ob3bv6rnv84ale8bufot4','2016-11-22 13:02:40'),(176,NULL,'o926d5061prd07uivh7vr3lgm5','2016-11-23 07:27:57'),(177,NULL,'oln7j945kahoad33p6i5kodst0','2016-11-23 08:13:17'),(178,NULL,'njlv68udkplg2h028lbbimi8u6','2016-11-23 07:59:57'),(179,NULL,'5arm674rtmpqs41nv1strhtvo3','2016-11-23 09:33:01'),(180,NULL,'1945ga94e3vi6j2rui00u8pe57','2016-11-23 10:21:11'),(181,NULL,'12n8c6ak5ksqiemqj6f6pdbrc0','2016-11-23 14:44:36'),(182,NULL,'teikse2qh984fe9mmbbc02rhu7','2016-11-23 15:58:13'),(183,NULL,'llgqgfjt06cm93uu9ackjnnbp2','2016-11-23 16:59:41'),(184,NULL,'74bg224288orc1me91omljsha3','2016-11-23 17:07:47'),(185,NULL,'tt0tp0rrmbnhbqma03hs4ruo23','2016-11-23 17:00:40'),(186,NULL,'g78mc37vjgq7jk4vmoru2b4bg0','2016-11-24 03:10:24'),(187,NULL,'pt2bd9fc358h6mn71c8lvuitm2','2016-11-24 04:25:07'),(188,NULL,'lgt86pvk3rlmkve0kgbj34i5n2','2016-11-24 03:27:41'),(189,NULL,'flsruompb7660m3e4055er9k36','2016-11-24 05:27:41'),(190,NULL,'ua78s59deh1fkn8i66qvhrvkl1','2016-11-24 06:24:34'),(191,NULL,'l5ja48s6qtgc58vjism2kf8386','2016-11-24 07:30:49'),(192,NULL,'90usj528p83k9rnmbbdu6qoqe6','2016-11-24 08:30:36'),(193,NULL,'27kgkgeqv7n20l7rv7i6a4tru4','2016-11-24 09:03:20'),(194,NULL,'p2hqanh97a3683ubehmdefi785','2016-11-24 09:06:17'),(195,NULL,'nagmv5ilag5dtqfd0n6gufpr86','2016-11-24 10:03:31'),(196,NULL,'freol1g115gc6mm55949qj75e0','2016-11-24 09:50:17'),(197,NULL,'ac3ijifq1d4sth0eppl471dqm1','2016-11-24 10:04:10'),(198,NULL,'b5i6dg84b8g46rmni3jdn7mgp5','2016-11-25 02:22:36'),(199,NULL,'n4kqin5kqca46okljovkpib147','2016-11-25 04:07:32'),(200,NULL,'2ao7gj1h4vc9ipmom18vhigsm1','2016-11-25 09:08:37'),(201,NULL,'mppdnt8g30q1dij5h70ef3o0c4','2016-11-26 03:40:51'),(202,NULL,'o36gnk606nqg0mu2m3fds318a0','2016-11-26 03:13:44'),(203,NULL,'o36gnk606nqg0mu2m3fds318a0','2016-11-26 03:56:03'),(204,NULL,'2lispcgjvi5vomtvhhdh8guu44','2016-11-26 04:17:57'),(205,NULL,'2t9sh8q53khql8u6g7evffd7o7','2016-11-26 04:14:28'),(206,NULL,'atp4ee52unjpt52aq6dq7ckqs5','2016-11-26 05:07:08'),(207,NULL,'g1kqe6otkbv517asrfvoht8i41','2016-11-26 04:46:23'),(208,NULL,'u9r9dqohvq35kl5alma34de8c1','2016-11-27 14:46:05'),(209,NULL,'u0guf81btc92od1iao9aitu695','2016-11-27 16:40:41'),(210,NULL,'qe3581l909gocvr9m5i41unbc4','2016-11-28 02:58:14'),(211,NULL,'0qig0b965so12a5ig9t77ivin5','2016-11-28 01:53:45'),(212,NULL,'8sjdehpm4c4u9q297me8eah602','2016-11-28 02:59:51'),(213,NULL,'idv84i5p2klv7qh0i0ombnmmd6','2016-11-28 02:45:57'),(214,NULL,'om2n4ppo414ovro3qg5p9prch3','2016-11-28 04:14:51'),(215,NULL,'e5h1d0e8f3c0ggclmsi4rg4g12','2016-11-28 06:07:08'),(216,NULL,'nevd48ua7vqiknvlppd0g78ok6','2016-11-28 07:48:10'),(217,NULL,'ftpj48tuirsem6ock00lungvs1','2016-11-28 08:25:38');
/*!40000 ALTER TABLE `customer_visitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `design_change`
--

DROP TABLE IF EXISTS `design_change`;
CREATE TABLE `design_change` (
  `design_change_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Design Change Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `design` varchar(255) DEFAULT NULL COMMENT 'Design',
  `date_from` date DEFAULT NULL COMMENT 'First Date of Design Activity',
  `date_to` date DEFAULT NULL COMMENT 'Last Date of Design Activity',
  PRIMARY KEY (`design_change_id`),
  KEY `DESIGN_CHANGE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Design Changes';

--
-- Table structure for table `design_config_dummy_cl`
--

DROP TABLE IF EXISTS `design_config_dummy_cl`;
CREATE TABLE `design_config_dummy_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='design_config_dummy_cl';

--
-- Table structure for table `design_config_grid_flat`
--

DROP TABLE IF EXISTS `design_config_grid_flat`;
CREATE TABLE `design_config_grid_flat` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `store_website_id` int(11) DEFAULT NULL COMMENT 'Store_website_id',
  `store_group_id` int(11) DEFAULT NULL COMMENT 'Store_group_id',
  `store_id` int(11) DEFAULT NULL COMMENT 'Store_id',
  `theme_theme_id` varchar(255) DEFAULT NULL COMMENT 'Theme_theme_id',
  PRIMARY KEY (`entity_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_WEBSITE_ID` (`store_website_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_GROUP_ID` (`store_group_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_ID` (`store_id`),
  FULLTEXT KEY `DESIGN_CONFIG_GRID_FLAT_THEME_THEME_ID` (`theme_theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='design_config_grid_flat';

--
-- Dumping data for table `design_config_grid_flat`
--

LOCK TABLES `design_config_grid_flat` WRITE;
/*!40000 ALTER TABLE `design_config_grid_flat` DISABLE KEYS */;
INSERT INTO `design_config_grid_flat` VALUES ('0',NULL,NULL,NULL,''),('1','1',NULL,NULL,''),('2','1','1','1','4'),('3','1','1','3','5'),('4','1','1','4','6'),('5','1','1','2','');
/*!40000 ALTER TABLE `design_config_grid_flat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_country`
--

DROP TABLE IF EXISTS `directory_country`;
CREATE TABLE `directory_country` (
  `country_id` varchar(2) NOT NULL COMMENT 'Country Id in ISO-2',
  `iso2_code` varchar(2) DEFAULT NULL COMMENT 'Country ISO-2 format',
  `iso3_code` varchar(3) DEFAULT NULL COMMENT 'Country ISO-3',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country';

--
-- Dumping data for table `directory_country`
--

LOCK TABLES `directory_country` WRITE;
/*!40000 ALTER TABLE `directory_country` DISABLE KEYS */;
INSERT INTO `directory_country` VALUES ('AD','AD','AND'),('AE','AE','ARE'),('AF','AF','AFG'),('AG','AG','ATG'),('AI','AI','AIA'),('AL','AL','ALB'),('AM','AM','ARM'),('AN','AN','ANT'),('AO','AO','AGO'),('AQ','AQ','ATA'),('AR','AR','ARG'),('AS','AS','ASM'),('AT','AT','AUT'),('AU','AU','AUS'),('AW','AW','ABW'),('AX','AX','ALA'),('AZ','AZ','AZE'),('BA','BA','BIH'),('BB','BB','BRB'),('BD','BD','BGD'),('BE','BE','BEL'),('BF','BF','BFA'),('BG','BG','BGR'),('BH','BH','BHR'),('BI','BI','BDI'),('BJ','BJ','BEN'),('BL','BL','BLM'),('BM','BM','BMU'),('BN','BN','BRN'),('BO','BO','BOL'),('BR','BR','BRA'),('BS','BS','BHS'),('BT','BT','BTN'),('BV','BV','BVT'),('BW','BW','BWA'),('BY','BY','BLR'),('BZ','BZ','BLZ'),('CA','CA','CAN'),('CC','CC','CCK'),('CD','CD','COD'),('CF','CF','CAF'),('CG','CG','COG'),('CH','CH','CHE'),('CI','CI','CIV'),('CK','CK','COK'),('CL','CL','CHL'),('CM','CM','CMR'),('CN','CN','CHN'),('CO','CO','COL'),('CR','CR','CRI'),('CU','CU','CUB'),('CV','CV','CPV'),('CX','CX','CXR'),('CY','CY','CYP'),('CZ','CZ','CZE'),('DE','DE','DEU'),('DJ','DJ','DJI'),('DK','DK','DNK'),('DM','DM','DMA'),('DO','DO','DOM'),('DZ','DZ','DZA'),('EC','EC','ECU'),('EE','EE','EST'),('EG','EG','EGY'),('EH','EH','ESH'),('ER','ER','ERI'),('ES','ES','ESP'),('ET','ET','ETH'),('FI','FI','FIN'),('FJ','FJ','FJI'),('FK','FK','FLK'),('FM','FM','FSM'),('FO','FO','FRO'),('FR','FR','FRA'),('GA','GA','GAB'),('GB','GB','GBR'),('GD','GD','GRD'),('GE','GE','GEO'),('GF','GF','GUF'),('GG','GG','GGY'),('GH','GH','GHA'),('GI','GI','GIB'),('GL','GL','GRL'),('GM','GM','GMB'),('GN','GN','GIN'),('GP','GP','GLP'),('GQ','GQ','GNQ'),('GR','GR','GRC'),('GS','GS','SGS'),('GT','GT','GTM'),('GU','GU','GUM'),('GW','GW','GNB'),('GY','GY','GUY'),('HK','HK','HKG'),('HM','HM','HMD'),('HN','HN','HND'),('HR','HR','HRV'),('HT','HT','HTI'),('HU','HU','HUN'),('ID','ID','IDN'),('IE','IE','IRL'),('IL','IL','ISR'),('IM','IM','IMN'),('IN','IN','IND'),('IO','IO','IOT'),('IQ','IQ','IRQ'),('IR','IR','IRN'),('IS','IS','ISL'),('IT','IT','ITA'),('JE','JE','JEY'),('JM','JM','JAM'),('JO','JO','JOR'),('JP','JP','JPN'),('KE','KE','KEN'),('KG','KG','KGZ'),('KH','KH','KHM'),('KI','KI','KIR'),('KM','KM','COM'),('KN','KN','KNA'),('KP','KP','PRK'),('KR','KR','KOR'),('KW','KW','KWT'),('KY','KY','CYM'),('KZ','KZ','KAZ'),('LA','LA','LAO'),('LB','LB','LBN'),('LC','LC','LCA'),('LI','LI','LIE'),('LK','LK','LKA'),('LR','LR','LBR'),('LS','LS','LSO'),('LT','LT','LTU'),('LU','LU','LUX'),('LV','LV','LVA'),('LY','LY','LBY'),('MA','MA','MAR'),('MC','MC','MCO'),('MD','MD','MDA'),('ME','ME','MNE'),('MF','MF','MAF'),('MG','MG','MDG'),('MH','MH','MHL'),('MK','MK','MKD'),('ML','ML','MLI'),('MM','MM','MMR'),('MN','MN','MNG'),('MO','MO','MAC'),('MP','MP','MNP'),('MQ','MQ','MTQ'),('MR','MR','MRT'),('MS','MS','MSR'),('MT','MT','MLT'),('MU','MU','MUS'),('MV','MV','MDV'),('MW','MW','MWI'),('MX','MX','MEX'),('MY','MY','MYS'),('MZ','MZ','MOZ'),('NA','NA','NAM'),('NC','NC','NCL'),('NE','NE','NER'),('NF','NF','NFK'),('NG','NG','NGA'),('NI','NI','NIC'),('NL','NL','NLD'),('NO','NO','NOR'),('NP','NP','NPL'),('NR','NR','NRU'),('NU','NU','NIU'),('NZ','NZ','NZL'),('OM','OM','OMN'),('PA','PA','PAN'),('PE','PE','PER'),('PF','PF','PYF'),('PG','PG','PNG'),('PH','PH','PHL'),('PK','PK','PAK'),('PL','PL','POL'),('PM','PM','SPM'),('PN','PN','PCN'),('PS','PS','PSE'),('PT','PT','PRT'),('PW','PW','PLW'),('PY','PY','PRY'),('QA','QA','QAT'),('RE','RE','REU'),('RO','RO','ROU'),('RS','RS','SRB'),('RU','RU','RUS'),('RW','RW','RWA'),('SA','SA','SAU'),('SB','SB','SLB'),('SC','SC','SYC'),('SD','SD','SDN'),('SE','SE','SWE'),('SG','SG','SGP'),('SH','SH','SHN'),('SI','SI','SVN'),('SJ','SJ','SJM'),('SK','SK','SVK'),('SL','SL','SLE'),('SM','SM','SMR'),('SN','SN','SEN'),('SO','SO','SOM'),('SR','SR','SUR'),('ST','ST','STP'),('SV','SV','SLV'),('SY','SY','SYR'),('SZ','SZ','SWZ'),('TC','TC','TCA'),('TD','TD','TCD'),('TF','TF','ATF'),('TG','TG','TGO'),('TH','TH','THA'),('TJ','TJ','TJK'),('TK','TK','TKL'),('TL','TL','TLS'),('TM','TM','TKM'),('TN','TN','TUN'),('TO','TO','TON'),('TR','TR','TUR'),('TT','TT','TTO'),('TV','TV','TUV'),('TW','TW','TWN'),('TZ','TZ','TZA'),('UA','UA','UKR'),('UG','UG','UGA'),('UM','UM','UMI'),('US','US','USA'),('UY','UY','URY'),('UZ','UZ','UZB'),('VA','VA','VAT'),('VC','VC','VCT'),('VE','VE','VEN'),('VG','VG','VGB'),('VI','VI','VIR'),('VN','VN','VNM'),('VU','VU','VUT'),('WF','WF','WLF'),('WS','WS','WSM'),('YE','YE','YEM'),('YT','YT','MYT'),('ZA','ZA','ZAF'),('ZM','ZM','ZMB'),('ZW','ZW','ZWE');
/*!40000 ALTER TABLE `directory_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_country_format`
--

DROP TABLE IF EXISTS `directory_country_format`;
CREATE TABLE `directory_country_format` (
  `country_format_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Country Format Id',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id in ISO-2',
  `type` varchar(30) DEFAULT NULL COMMENT 'Country Format Type',
  `format` text NOT NULL COMMENT 'Country Format',
  PRIMARY KEY (`country_format_id`),
  UNIQUE KEY `DIRECTORY_COUNTRY_FORMAT_COUNTRY_ID_TYPE` (`country_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Format';

--
-- Table structure for table `directory_country_region`
--

DROP TABLE IF EXISTS `directory_country_region`;
CREATE TABLE `directory_country_region` (
  `region_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Region Id',
  `country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Country Id in ISO-2',
  `code` varchar(32) DEFAULT NULL COMMENT 'Region code',
  `default_name` varchar(255) DEFAULT NULL COMMENT 'Region Name',
  PRIMARY KEY (`region_id`),
  KEY `DIRECTORY_COUNTRY_REGION_COUNTRY_ID` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8 COMMENT='Directory Country Region';

--
-- Dumping data for table `directory_country_region`
--

LOCK TABLES `directory_country_region` WRITE;
/*!40000 ALTER TABLE `directory_country_region` DISABLE KEYS */;
INSERT INTO `directory_country_region` VALUES ('1','US','AL','Alabama'),('2','US','AK','Alaska'),('3','US','AS','American Samoa'),('4','US','AZ','Arizona'),('5','US','AR','Arkansas'),('6','US','AE','Armed Forces Africa'),('7','US','AA','Armed Forces Americas'),('8','US','AE','Armed Forces Canada'),('9','US','AE','Armed Forces Europe'),('10','US','AE','Armed Forces Middle East'),('11','US','AP','Armed Forces Pacific'),('12','US','CA','California'),('13','US','CO','Colorado'),('14','US','CT','Connecticut'),('15','US','DE','Delaware'),('16','US','DC','District of Columbia'),('17','US','FM','Federated States Of Micronesia'),('18','US','FL','Florida'),('19','US','GA','Georgia'),('20','US','GU','Guam'),('21','US','HI','Hawaii'),('22','US','ID','Idaho'),('23','US','IL','Illinois'),('24','US','IN','Indiana'),('25','US','IA','Iowa'),('26','US','KS','Kansas'),('27','US','KY','Kentucky'),('28','US','LA','Louisiana'),('29','US','ME','Maine'),('30','US','MH','Marshall Islands'),('31','US','MD','Maryland'),('32','US','MA','Massachusetts'),('33','US','MI','Michigan'),('34','US','MN','Minnesota'),('35','US','MS','Mississippi'),('36','US','MO','Missouri'),('37','US','MT','Montana'),('38','US','NE','Nebraska'),('39','US','NV','Nevada'),('40','US','NH','New Hampshire'),('41','US','NJ','New Jersey'),('42','US','NM','New Mexico'),('43','US','NY','New York'),('44','US','NC','North Carolina'),('45','US','ND','North Dakota'),('46','US','MP','Northern Mariana Islands'),('47','US','OH','Ohio'),('48','US','OK','Oklahoma'),('49','US','OR','Oregon'),('50','US','PW','Palau'),('51','US','PA','Pennsylvania'),('52','US','PR','Puerto Rico'),('53','US','RI','Rhode Island'),('54','US','SC','South Carolina'),('55','US','SD','South Dakota'),('56','US','TN','Tennessee'),('57','US','TX','Texas'),('58','US','UT','Utah'),('59','US','VT','Vermont'),('60','US','VI','Virgin Islands'),('61','US','VA','Virginia'),('62','US','WA','Washington'),('63','US','WV','West Virginia'),('64','US','WI','Wisconsin'),('65','US','WY','Wyoming'),('66','CA','AB','Alberta'),('67','CA','BC','British Columbia'),('68','CA','MB','Manitoba'),('69','CA','NL','Newfoundland and Labrador'),('70','CA','NB','New Brunswick'),('71','CA','NS','Nova Scotia'),('72','CA','NT','Northwest Territories'),('73','CA','NU','Nunavut'),('74','CA','ON','Ontario'),('75','CA','PE','Prince Edward Island'),('76','CA','QC','Quebec'),('77','CA','SK','Saskatchewan'),('78','CA','YT','Yukon Territory'),('79','DE','NDS','Niedersachsen'),('80','DE','BAW','Baden-Württemberg'),('81','DE','BAY','Bayern'),('82','DE','BER','Berlin'),('83','DE','BRG','Brandenburg'),('84','DE','BRE','Bremen'),('85','DE','HAM','Hamburg'),('86','DE','HES','Hessen'),('87','DE','MEC','Mecklenburg-Vorpommern'),('88','DE','NRW','Nordrhein-Westfalen'),('89','DE','RHE','Rheinland-Pfalz'),('90','DE','SAR','Saarland'),('91','DE','SAS','Sachsen'),('92','DE','SAC','Sachsen-Anhalt'),('93','DE','SCN','Schleswig-Holstein'),('94','DE','THE','Thüringen'),('95','AT','WI','Wien'),('96','AT','NO','Niederösterreich'),('97','AT','OO','Oberösterreich'),('98','AT','SB','Salzburg'),('99','AT','KN','Kärnten'),('100','AT','ST','Steiermark'),('101','AT','TI','Tirol'),('102','AT','BL','Burgenland'),('103','AT','VB','Vorarlberg'),('104','CH','AG','Aargau'),('105','CH','AI','Appenzell Innerrhoden'),('106','CH','AR','Appenzell Ausserrhoden'),('107','CH','BE','Bern'),('108','CH','BL','Basel-Landschaft'),('109','CH','BS','Basel-Stadt'),('110','CH','FR','Freiburg'),('111','CH','GE','Genf'),('112','CH','GL','Glarus'),('113','CH','GR','Graubünden'),('114','CH','JU','Jura'),('115','CH','LU','Luzern'),('116','CH','NE','Neuenburg'),('117','CH','NW','Nidwalden'),('118','CH','OW','Obwalden'),('119','CH','SG','St. Gallen'),('120','CH','SH','Schaffhausen'),('121','CH','SO','Solothurn'),('122','CH','SZ','Schwyz'),('123','CH','TG','Thurgau'),('124','CH','TI','Tessin'),('125','CH','UR','Uri'),('126','CH','VD','Waadt'),('127','CH','VS','Wallis'),('128','CH','ZG','Zug'),('129','CH','ZH','Zürich'),('130','ES','A Coruсa','A Coruña'),('131','ES','Alava','Alava'),('132','ES','Albacete','Albacete'),('133','ES','Alicante','Alicante'),('134','ES','Almeria','Almeria'),('135','ES','Asturias','Asturias'),('136','ES','Avila','Avila'),('137','ES','Badajoz','Badajoz'),('138','ES','Baleares','Baleares'),('139','ES','Barcelona','Barcelona'),('140','ES','Burgos','Burgos'),('141','ES','Caceres','Caceres'),('142','ES','Cadiz','Cadiz'),('143','ES','Cantabria','Cantabria'),('144','ES','Castellon','Castellon'),('145','ES','Ceuta','Ceuta'),('146','ES','Ciudad Real','Ciudad Real'),('147','ES','Cordoba','Cordoba'),('148','ES','Cuenca','Cuenca'),('149','ES','Girona','Girona'),('150','ES','Granada','Granada'),('151','ES','Guadalajara','Guadalajara'),('152','ES','Guipuzcoa','Guipuzcoa'),('153','ES','Huelva','Huelva'),('154','ES','Huesca','Huesca'),('155','ES','Jaen','Jaen'),('156','ES','La Rioja','La Rioja'),('157','ES','Las Palmas','Las Palmas'),('158','ES','Leon','Leon'),('159','ES','Lleida','Lleida'),('160','ES','Lugo','Lugo'),('161','ES','Madrid','Madrid'),('162','ES','Malaga','Malaga'),('163','ES','Melilla','Melilla'),('164','ES','Murcia','Murcia'),('165','ES','Navarra','Navarra'),('166','ES','Ourense','Ourense'),('167','ES','Palencia','Palencia'),('168','ES','Pontevedra','Pontevedra'),('169','ES','Salamanca','Salamanca'),('170','ES','Santa Cruz de Tenerife','Santa Cruz de Tenerife'),('171','ES','Segovia','Segovia'),('172','ES','Sevilla','Sevilla'),('173','ES','Soria','Soria'),('174','ES','Tarragona','Tarragona'),('175','ES','Teruel','Teruel'),('176','ES','Toledo','Toledo'),('177','ES','Valencia','Valencia'),('178','ES','Valladolid','Valladolid'),('179','ES','Vizcaya','Vizcaya'),('180','ES','Zamora','Zamora'),('181','ES','Zaragoza','Zaragoza'),('182','FR','1','Ain'),('183','FR','2','Aisne'),('184','FR','3','Allier'),('185','FR','4','Alpes-de-Haute-Provence'),('186','FR','5','Hautes-Alpes'),('187','FR','6','Alpes-Maritimes'),('188','FR','7','Ardèche'),('189','FR','8','Ardennes'),('190','FR','9','Ariège'),('191','FR','10','Aube'),('192','FR','11','Aude'),('193','FR','12','Aveyron'),('194','FR','13','Bouches-du-Rhône'),('195','FR','14','Calvados'),('196','FR','15','Cantal'),('197','FR','16','Charente'),('198','FR','17','Charente-Maritime'),('199','FR','18','Cher'),('200','FR','19','Corrèze'),('201','FR','2A','Corse-du-Sud'),('202','FR','2B','Haute-Corse'),('203','FR','21','Côte-d\'Or'),('204','FR','22','Côtes-d\'Armor'),('205','FR','23','Creuse'),('206','FR','24','Dordogne'),('207','FR','25','Doubs'),('208','FR','26','Drôme'),('209','FR','27','Eure'),('210','FR','28','Eure-et-Loir'),('211','FR','29','Finistère'),('212','FR','30','Gard'),('213','FR','31','Haute-Garonne'),('214','FR','32','Gers'),('215','FR','33','Gironde'),('216','FR','34','Hérault'),('217','FR','35','Ille-et-Vilaine'),('218','FR','36','Indre'),('219','FR','37','Indre-et-Loire'),('220','FR','38','Isère'),('221','FR','39','Jura'),('222','FR','40','Landes'),('223','FR','41','Loir-et-Cher'),('224','FR','42','Loire'),('225','FR','43','Haute-Loire'),('226','FR','44','Loire-Atlantique'),('227','FR','45','Loiret'),('228','FR','46','Lot'),('229','FR','47','Lot-et-Garonne'),('230','FR','48','Lozère'),('231','FR','49','Maine-et-Loire'),('232','FR','50','Manche'),('233','FR','51','Marne'),('234','FR','52','Haute-Marne'),('235','FR','53','Mayenne'),('236','FR','54','Meurthe-et-Moselle'),('237','FR','55','Meuse'),('238','FR','56','Morbihan'),('239','FR','57','Moselle'),('240','FR','58','Nièvre'),('241','FR','59','Nord'),('242','FR','60','Oise'),('243','FR','61','Orne'),('244','FR','62','Pas-de-Calais'),('245','FR','63','Puy-de-Dôme'),('246','FR','64','Pyrénées-Atlantiques'),('247','FR','65','Hautes-Pyrénées'),('248','FR','66','Pyrénées-Orientales'),('249','FR','67','Bas-Rhin'),('250','FR','68','Haut-Rhin'),('251','FR','69','Rhône'),('252','FR','70','Haute-Saône'),('253','FR','71','Saône-et-Loire'),('254','FR','72','Sarthe'),('255','FR','73','Savoie'),('256','FR','74','Haute-Savoie'),('257','FR','75','Paris'),('258','FR','76','Seine-Maritime'),('259','FR','77','Seine-et-Marne'),('260','FR','78','Yvelines'),('261','FR','79','Deux-Sèvres'),('262','FR','80','Somme'),('263','FR','81','Tarn'),('264','FR','82','Tarn-et-Garonne'),('265','FR','83','Var'),('266','FR','84','Vaucluse'),('267','FR','85','Vendée'),('268','FR','86','Vienne'),('269','FR','87','Haute-Vienne'),('270','FR','88','Vosges'),('271','FR','89','Yonne'),('272','FR','90','Territoire-de-Belfort'),('273','FR','91','Essonne'),('274','FR','92','Hauts-de-Seine'),('275','FR','93','Seine-Saint-Denis'),('276','FR','94','Val-de-Marne'),('277','FR','95','Val-d\'Oise'),('278','RO','AB','Alba'),('279','RO','AR','Arad'),('280','RO','AG','Argeş'),('281','RO','BC','Bacău'),('282','RO','BH','Bihor'),('283','RO','BN','Bistriţa-Năsăud'),('284','RO','BT','Botoşani'),('285','RO','BV','Braşov'),('286','RO','BR','Brăila'),('287','RO','B','Bucureşti'),('288','RO','BZ','Buzău'),('289','RO','CS','Caraş-Severin'),('290','RO','CL','Călăraşi'),('291','RO','CJ','Cluj'),('292','RO','CT','Constanţa'),('293','RO','CV','Covasna'),('294','RO','DB','Dâmboviţa'),('295','RO','DJ','Dolj'),('296','RO','GL','Galaţi'),('297','RO','GR','Giurgiu'),('298','RO','GJ','Gorj'),('299','RO','HR','Harghita'),('300','RO','HD','Hunedoara'),('301','RO','IL','Ialomiţa'),('302','RO','IS','Iaşi'),('303','RO','IF','Ilfov'),('304','RO','MM','Maramureş'),('305','RO','MH','Mehedinţi'),('306','RO','MS','Mureş'),('307','RO','NT','Neamţ'),('308','RO','OT','Olt'),('309','RO','PH','Prahova'),('310','RO','SM','Satu-Mare'),('311','RO','SJ','Sălaj'),('312','RO','SB','Sibiu'),('313','RO','SV','Suceava'),('314','RO','TR','Teleorman'),('315','RO','TM','Timiş'),('316','RO','TL','Tulcea'),('317','RO','VS','Vaslui'),('318','RO','VL','Vâlcea'),('319','RO','VN','Vrancea'),('320','FI','Lappi','Lappi'),('321','FI','Pohjois-Pohjanmaa','Pohjois-Pohjanmaa'),('322','FI','Kainuu','Kainuu'),('323','FI','Pohjois-Karjala','Pohjois-Karjala'),('324','FI','Pohjois-Savo','Pohjois-Savo'),('325','FI','Etelä-Savo','Etelä-Savo'),('326','FI','Etelä-Pohjanmaa','Etelä-Pohjanmaa'),('327','FI','Pohjanmaa','Pohjanmaa'),('328','FI','Pirkanmaa','Pirkanmaa'),('329','FI','Satakunta','Satakunta'),('330','FI','Keski-Pohjanmaa','Keski-Pohjanmaa'),('331','FI','Keski-Suomi','Keski-Suomi'),('332','FI','Varsinais-Suomi','Varsinais-Suomi'),('333','FI','Etelä-Karjala','Etelä-Karjala'),('334','FI','Päijät-Häme','Päijät-Häme'),('335','FI','Kanta-Häme','Kanta-Häme'),('336','FI','Uusimaa','Uusimaa'),('337','FI','Itä-Uusimaa','Itä-Uusimaa'),('338','FI','Kymenlaakso','Kymenlaakso'),('339','FI','Ahvenanmaa','Ahvenanmaa'),('340','EE','EE-37','Harjumaa'),('341','EE','EE-39','Hiiumaa'),('342','EE','EE-44','Ida-Virumaa'),('343','EE','EE-49','Jõgevamaa'),('344','EE','EE-51','Järvamaa'),('345','EE','EE-57','Läänemaa'),('346','EE','EE-59','Lääne-Virumaa'),('347','EE','EE-65','Põlvamaa'),('348','EE','EE-67','Pärnumaa'),('349','EE','EE-70','Raplamaa'),('350','EE','EE-74','Saaremaa'),('351','EE','EE-78','Tartumaa'),('352','EE','EE-82','Valgamaa'),('353','EE','EE-84','Viljandimaa'),('354','EE','EE-86','Võrumaa'),('355','LV','LV-DGV','Daugavpils'),('356','LV','LV-JEL','Jelgava'),('357','LV','Jēkabpils','Jēkabpils'),('358','LV','LV-JUR','Jūrmala'),('359','LV','LV-LPX','Liepāja'),('360','LV','LV-LE','Liepājas novads'),('361','LV','LV-REZ','Rēzekne'),('362','LV','LV-RIX','Rīga'),('363','LV','LV-RI','Rīgas novads'),('364','LV','Valmiera','Valmiera'),('365','LV','LV-VEN','Ventspils'),('366','LV','Aglonas novads','Aglonas novads'),('367','LV','LV-AI','Aizkraukles novads'),('368','LV','Aizputes novads','Aizputes novads'),('369','LV','Aknīstes novads','Aknīstes novads'),('370','LV','Alojas novads','Alojas novads'),('371','LV','Alsungas novads','Alsungas novads'),('372','LV','LV-AL','Alūksnes novads'),('373','LV','Amatas novads','Amatas novads'),('374','LV','Apes novads','Apes novads'),('375','LV','Auces novads','Auces novads'),('376','LV','Babītes novads','Babītes novads'),('377','LV','Baldones novads','Baldones novads'),('378','LV','Baltinavas novads','Baltinavas novads'),('379','LV','LV-BL','Balvu novads'),('380','LV','LV-BU','Bauskas novads'),('381','LV','Beverīnas novads','Beverīnas novads'),('382','LV','Brocēnu novads','Brocēnu novads'),('383','LV','Burtnieku novads','Burtnieku novads'),('384','LV','Carnikavas novads','Carnikavas novads'),('385','LV','Cesvaines novads','Cesvaines novads'),('386','LV','Ciblas novads','Ciblas novads'),('387','LV','LV-CE','Cēsu novads'),('388','LV','Dagdas novads','Dagdas novads'),('389','LV','LV-DA','Daugavpils novads'),('390','LV','LV-DO','Dobeles novads'),('391','LV','Dundagas novads','Dundagas novads'),('392','LV','Durbes novads','Durbes novads'),('393','LV','Engures novads','Engures novads'),('394','LV','Garkalnes novads','Garkalnes novads'),('395','LV','Grobiņas novads','Grobiņas novads'),('396','LV','LV-GU','Gulbenes novads'),('397','LV','Iecavas novads','Iecavas novads'),('398','LV','Ikšķiles novads','Ikšķiles novads'),('399','LV','Ilūkstes novads','Ilūkstes novads'),('400','LV','Inčukalna novads','Inčukalna novads'),('401','LV','Jaunjelgavas novads','Jaunjelgavas novads'),('402','LV','Jaunpiebalgas novads','Jaunpiebalgas novads'),('403','LV','Jaunpils novads','Jaunpils novads'),('404','LV','LV-JL','Jelgavas novads'),('405','LV','LV-JK','Jēkabpils novads'),('406','LV','Kandavas novads','Kandavas novads'),('407','LV','Kokneses novads','Kokneses novads'),('408','LV','Krimuldas novads','Krimuldas novads'),('409','LV','Krustpils novads','Krustpils novads'),('410','LV','LV-KR','Krāslavas novads'),('411','LV','LV-KU','Kuldīgas novads'),('412','LV','Kārsavas novads','Kārsavas novads'),('413','LV','Lielvārdes novads','Lielvārdes novads'),('414','LV','LV-LM','Limbažu novads'),('415','LV','Lubānas novads','Lubānas novads'),('416','LV','LV-LU','Ludzas novads'),('417','LV','Līgatnes novads','Līgatnes novads'),('418','LV','Līvānu novads','Līvānu novads'),('419','LV','LV-MA','Madonas novads'),('420','LV','Mazsalacas novads','Mazsalacas novads'),('421','LV','Mālpils novads','Mālpils novads'),('422','LV','Mārupes novads','Mārupes novads'),('423','LV','Naukšēnu novads','Naukšēnu novads'),('424','LV','Neretas novads','Neretas novads'),('425','LV','Nīcas novads','Nīcas novads'),('426','LV','LV-OG','Ogres novads'),('427','LV','Olaines novads','Olaines novads'),('428','LV','Ozolnieku novads','Ozolnieku novads'),('429','LV','LV-PR','Preiļu novads'),('430','LV','Priekules novads','Priekules novads'),('431','LV','Priekuļu novads','Priekuļu novads'),('432','LV','Pārgaujas novads','Pārgaujas novads'),('433','LV','Pāvilostas novads','Pāvilostas novads'),('434','LV','Pļaviņu novads','Pļaviņu novads'),('435','LV','Raunas novads','Raunas novads'),('436','LV','Riebiņu novads','Riebiņu novads'),('437','LV','Rojas novads','Rojas novads'),('438','LV','Ropažu novads','Ropažu novads'),('439','LV','Rucavas novads','Rucavas novads'),('440','LV','Rugāju novads','Rugāju novads'),('441','LV','Rundāles novads','Rundāles novads'),('442','LV','LV-RE','Rēzeknes novads'),('443','LV','Rūjienas novads','Rūjienas novads'),('444','LV','Salacgrīvas novads','Salacgrīvas novads'),('445','LV','Salas novads','Salas novads'),('446','LV','Salaspils novads','Salaspils novads'),('447','LV','LV-SA','Saldus novads'),('448','LV','Saulkrastu novads','Saulkrastu novads'),('449','LV','Siguldas novads','Siguldas novads'),('450','LV','Skrundas novads','Skrundas novads'),('451','LV','Skrīveru novads','Skrīveru novads'),('452','LV','Smiltenes novads','Smiltenes novads'),('453','LV','Stopiņu novads','Stopiņu novads'),('454','LV','Strenču novads','Strenču novads'),('455','LV','Sējas novads','Sējas novads'),('456','LV','LV-TA','Talsu novads'),('457','LV','LV-TU','Tukuma novads'),('458','LV','Tērvetes novads','Tērvetes novads'),('459','LV','Vaiņodes novads','Vaiņodes novads'),('460','LV','LV-VK','Valkas novads'),('461','LV','LV-VM','Valmieras novads'),('462','LV','Varakļānu novads','Varakļānu novads'),('463','LV','Vecpiebalgas novads','Vecpiebalgas novads'),('464','LV','Vecumnieku novads','Vecumnieku novads'),('465','LV','LV-VE','Ventspils novads'),('466','LV','Viesītes novads','Viesītes novads'),('467','LV','Viļakas novads','Viļakas novads'),('468','LV','Viļānu novads','Viļānu novads'),('469','LV','Vārkavas novads','Vārkavas novads'),('470','LV','Zilupes novads','Zilupes novads'),('471','LV','Ādažu novads','Ādažu novads'),('472','LV','Ērgļu novads','Ērgļu novads'),('473','LV','Ķeguma novads','Ķeguma novads'),('474','LV','Ķekavas novads','Ķekavas novads'),('475','LT','LT-AL','Alytaus Apskritis'),('476','LT','LT-KU','Kauno Apskritis'),('477','LT','LT-KL','Klaipėdos Apskritis'),('478','LT','LT-MR','Marijampolės Apskritis'),('479','LT','LT-PN','Panevėžio Apskritis'),('480','LT','LT-SA','Šiaulių Apskritis'),('481','LT','LT-TA','Tauragės Apskritis'),('482','LT','LT-TE','Telšių Apskritis'),('483','LT','LT-UT','Utenos Apskritis'),('484','LT','LT-VL','Vilniaus Apskritis'),('485','BR','AC','Acre'),('486','BR','AL','Alagoas'),('487','BR','AP','Amapá'),('488','BR','AM','Amazonas'),('489','BR','BA','Bahia'),('490','BR','CE','Ceará'),('491','BR','ES','Espírito Santo'),('492','BR','GO','Goiás'),('493','BR','MA','Maranhão'),('494','BR','MT','Mato Grosso'),('495','BR','MS','Mato Grosso do Sul'),('496','BR','MG','Minas Gerais'),('497','BR','PA','Pará'),('498','BR','PB','Paraíba'),('499','BR','PR','Paraná'),('500','BR','PE','Pernambuco'),('501','BR','PI','Piauí'),('502','BR','RJ','Rio de Janeiro'),('503','BR','RN','Rio Grande do Norte'),('504','BR','RS','Rio Grande do Sul'),('505','BR','RO','Rondônia'),('506','BR','RR','Roraima'),('507','BR','SC','Santa Catarina'),('508','BR','SP','São Paulo'),('509','BR','SE','Sergipe'),('510','BR','TO','Tocantins'),('511','BR','DF','Distrito Federal');
/*!40000 ALTER TABLE `directory_country_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_country_region_name`
--

DROP TABLE IF EXISTS `directory_country_region_name`;
CREATE TABLE `directory_country_region_name` (
  `locale` varchar(8) NOT NULL COMMENT 'Locale',
  `region_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Region Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Region Name',
  PRIMARY KEY (`locale`,`region_id`),
  KEY `DIRECTORY_COUNTRY_REGION_NAME_REGION_ID` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Region Name';

--
-- Dumping data for table `directory_country_region_name`
--

LOCK TABLES `directory_country_region_name` WRITE;
/*!40000 ALTER TABLE `directory_country_region_name` DISABLE KEYS */;
INSERT INTO `directory_country_region_name` VALUES ('en_US','1','Alabama'),('en_US','2','Alaska'),('en_US','3','American Samoa'),('en_US','4','Arizona'),('en_US','5','Arkansas'),('en_US','6','Armed Forces Africa'),('en_US','7','Armed Forces Americas'),('en_US','8','Armed Forces Canada'),('en_US','9','Armed Forces Europe'),('en_US','10','Armed Forces Middle East'),('en_US','11','Armed Forces Pacific'),('en_US','12','California'),('en_US','13','Colorado'),('en_US','14','Connecticut'),('en_US','15','Delaware'),('en_US','16','District of Columbia'),('en_US','17','Federated States Of Micronesia'),('en_US','18','Florida'),('en_US','19','Georgia'),('en_US','20','Guam'),('en_US','21','Hawaii'),('en_US','22','Idaho'),('en_US','23','Illinois'),('en_US','24','Indiana'),('en_US','25','Iowa'),('en_US','26','Kansas'),('en_US','27','Kentucky'),('en_US','28','Louisiana'),('en_US','29','Maine'),('en_US','30','Marshall Islands'),('en_US','31','Maryland'),('en_US','32','Massachusetts'),('en_US','33','Michigan'),('en_US','34','Minnesota'),('en_US','35','Mississippi'),('en_US','36','Missouri'),('en_US','37','Montana'),('en_US','38','Nebraska'),('en_US','39','Nevada'),('en_US','40','New Hampshire'),('en_US','41','New Jersey'),('en_US','42','New Mexico'),('en_US','43','New York'),('en_US','44','North Carolina'),('en_US','45','North Dakota'),('en_US','46','Northern Mariana Islands'),('en_US','47','Ohio'),('en_US','48','Oklahoma'),('en_US','49','Oregon'),('en_US','50','Palau'),('en_US','51','Pennsylvania'),('en_US','52','Puerto Rico'),('en_US','53','Rhode Island'),('en_US','54','South Carolina'),('en_US','55','South Dakota'),('en_US','56','Tennessee'),('en_US','57','Texas'),('en_US','58','Utah'),('en_US','59','Vermont'),('en_US','60','Virgin Islands'),('en_US','61','Virginia'),('en_US','62','Washington'),('en_US','63','West Virginia'),('en_US','64','Wisconsin'),('en_US','65','Wyoming'),('en_US','66','Alberta'),('en_US','67','British Columbia'),('en_US','68','Manitoba'),('en_US','69','Newfoundland and Labrador'),('en_US','70','New Brunswick'),('en_US','71','Nova Scotia'),('en_US','72','Northwest Territories'),('en_US','73','Nunavut'),('en_US','74','Ontario'),('en_US','75','Prince Edward Island'),('en_US','76','Quebec'),('en_US','77','Saskatchewan'),('en_US','78','Yukon Territory'),('en_US','79','Niedersachsen'),('en_US','80','Baden-Württemberg'),('en_US','81','Bayern'),('en_US','82','Berlin'),('en_US','83','Brandenburg'),('en_US','84','Bremen'),('en_US','85','Hamburg'),('en_US','86','Hessen'),('en_US','87','Mecklenburg-Vorpommern'),('en_US','88','Nordrhein-Westfalen'),('en_US','89','Rheinland-Pfalz'),('en_US','90','Saarland'),('en_US','91','Sachsen'),('en_US','92','Sachsen-Anhalt'),('en_US','93','Schleswig-Holstein'),('en_US','94','Thüringen'),('en_US','95','Wien'),('en_US','96','Niederösterreich'),('en_US','97','Oberösterreich'),('en_US','98','Salzburg'),('en_US','99','Kärnten'),('en_US','100','Steiermark'),('en_US','101','Tirol'),('en_US','102','Burgenland'),('en_US','103','Vorarlberg'),('en_US','104','Aargau'),('en_US','105','Appenzell Innerrhoden'),('en_US','106','Appenzell Ausserrhoden'),('en_US','107','Bern'),('en_US','108','Basel-Landschaft'),('en_US','109','Basel-Stadt'),('en_US','110','Freiburg'),('en_US','111','Genf'),('en_US','112','Glarus'),('en_US','113','Graubünden'),('en_US','114','Jura'),('en_US','115','Luzern'),('en_US','116','Neuenburg'),('en_US','117','Nidwalden'),('en_US','118','Obwalden'),('en_US','119','St. Gallen'),('en_US','120','Schaffhausen'),('en_US','121','Solothurn'),('en_US','122','Schwyz'),('en_US','123','Thurgau'),('en_US','124','Tessin'),('en_US','125','Uri'),('en_US','126','Waadt'),('en_US','127','Wallis'),('en_US','128','Zug'),('en_US','129','Zürich'),('en_US','130','A Coruña'),('en_US','131','Alava'),('en_US','132','Albacete'),('en_US','133','Alicante'),('en_US','134','Almeria'),('en_US','135','Asturias'),('en_US','136','Avila'),('en_US','137','Badajoz'),('en_US','138','Baleares'),('en_US','139','Barcelona'),('en_US','140','Burgos'),('en_US','141','Caceres'),('en_US','142','Cadiz'),('en_US','143','Cantabria'),('en_US','144','Castellon'),('en_US','145','Ceuta'),('en_US','146','Ciudad Real'),('en_US','147','Cordoba'),('en_US','148','Cuenca'),('en_US','149','Girona'),('en_US','150','Granada'),('en_US','151','Guadalajara'),('en_US','152','Guipuzcoa'),('en_US','153','Huelva'),('en_US','154','Huesca'),('en_US','155','Jaen'),('en_US','156','La Rioja'),('en_US','157','Las Palmas'),('en_US','158','Leon'),('en_US','159','Lleida'),('en_US','160','Lugo'),('en_US','161','Madrid'),('en_US','162','Malaga'),('en_US','163','Melilla'),('en_US','164','Murcia'),('en_US','165','Navarra'),('en_US','166','Ourense'),('en_US','167','Palencia'),('en_US','168','Pontevedra'),('en_US','169','Salamanca'),('en_US','170','Santa Cruz de Tenerife'),('en_US','171','Segovia'),('en_US','172','Sevilla'),('en_US','173','Soria'),('en_US','174','Tarragona'),('en_US','175','Teruel'),('en_US','176','Toledo'),('en_US','177','Valencia'),('en_US','178','Valladolid'),('en_US','179','Vizcaya'),('en_US','180','Zamora'),('en_US','181','Zaragoza'),('en_US','182','Ain'),('en_US','183','Aisne'),('en_US','184','Allier'),('en_US','185','Alpes-de-Haute-Provence'),('en_US','186','Hautes-Alpes'),('en_US','187','Alpes-Maritimes'),('en_US','188','Ardèche'),('en_US','189','Ardennes'),('en_US','190','Ariège'),('en_US','191','Aube'),('en_US','192','Aude'),('en_US','193','Aveyron'),('en_US','194','Bouches-du-Rhône'),('en_US','195','Calvados'),('en_US','196','Cantal'),('en_US','197','Charente'),('en_US','198','Charente-Maritime'),('en_US','199','Cher'),('en_US','200','Corrèze'),('en_US','201','Corse-du-Sud'),('en_US','202','Haute-Corse'),('en_US','203','Côte-d\'Or'),('en_US','204','Côtes-d\'Armor'),('en_US','205','Creuse'),('en_US','206','Dordogne'),('en_US','207','Doubs'),('en_US','208','Drôme'),('en_US','209','Eure'),('en_US','210','Eure-et-Loir'),('en_US','211','Finistère'),('en_US','212','Gard'),('en_US','213','Haute-Garonne'),('en_US','214','Gers'),('en_US','215','Gironde'),('en_US','216','Hérault'),('en_US','217','Ille-et-Vilaine'),('en_US','218','Indre'),('en_US','219','Indre-et-Loire'),('en_US','220','Isère'),('en_US','221','Jura'),('en_US','222','Landes'),('en_US','223','Loir-et-Cher'),('en_US','224','Loire'),('en_US','225','Haute-Loire'),('en_US','226','Loire-Atlantique'),('en_US','227','Loiret'),('en_US','228','Lot'),('en_US','229','Lot-et-Garonne'),('en_US','230','Lozère'),('en_US','231','Maine-et-Loire'),('en_US','232','Manche'),('en_US','233','Marne'),('en_US','234','Haute-Marne'),('en_US','235','Mayenne'),('en_US','236','Meurthe-et-Moselle'),('en_US','237','Meuse'),('en_US','238','Morbihan'),('en_US','239','Moselle'),('en_US','240','Nièvre'),('en_US','241','Nord'),('en_US','242','Oise'),('en_US','243','Orne'),('en_US','244','Pas-de-Calais'),('en_US','245','Puy-de-Dôme'),('en_US','246','Pyrénées-Atlantiques'),('en_US','247','Hautes-Pyrénées'),('en_US','248','Pyrénées-Orientales'),('en_US','249','Bas-Rhin'),('en_US','250','Haut-Rhin'),('en_US','251','Rhône'),('en_US','252','Haute-Saône'),('en_US','253','Saône-et-Loire'),('en_US','254','Sarthe'),('en_US','255','Savoie'),('en_US','256','Haute-Savoie'),('en_US','257','Paris'),('en_US','258','Seine-Maritime'),('en_US','259','Seine-et-Marne'),('en_US','260','Yvelines'),('en_US','261','Deux-Sèvres'),('en_US','262','Somme'),('en_US','263','Tarn'),('en_US','264','Tarn-et-Garonne'),('en_US','265','Var'),('en_US','266','Vaucluse'),('en_US','267','Vendée'),('en_US','268','Vienne'),('en_US','269','Haute-Vienne'),('en_US','270','Vosges'),('en_US','271','Yonne'),('en_US','272','Territoire-de-Belfort'),('en_US','273','Essonne'),('en_US','274','Hauts-de-Seine'),('en_US','275','Seine-Saint-Denis'),('en_US','276','Val-de-Marne'),('en_US','277','Val-d\'Oise'),('en_US','278','Alba'),('en_US','279','Arad'),('en_US','280','Argeş'),('en_US','281','Bacău'),('en_US','282','Bihor'),('en_US','283','Bistriţa-Năsăud'),('en_US','284','Botoşani'),('en_US','285','Braşov'),('en_US','286','Brăila'),('en_US','287','Bucureşti'),('en_US','288','Buzău'),('en_US','289','Caraş-Severin'),('en_US','290','Călăraşi'),('en_US','291','Cluj'),('en_US','292','Constanţa'),('en_US','293','Covasna'),('en_US','294','Dâmboviţa'),('en_US','295','Dolj'),('en_US','296','Galaţi'),('en_US','297','Giurgiu'),('en_US','298','Gorj'),('en_US','299','Harghita'),('en_US','300','Hunedoara'),('en_US','301','Ialomiţa'),('en_US','302','Iaşi'),('en_US','303','Ilfov'),('en_US','304','Maramureş'),('en_US','305','Mehedinţi'),('en_US','306','Mureş'),('en_US','307','Neamţ'),('en_US','308','Olt'),('en_US','309','Prahova'),('en_US','310','Satu-Mare'),('en_US','311','Sălaj'),('en_US','312','Sibiu'),('en_US','313','Suceava'),('en_US','314','Teleorman'),('en_US','315','Timiş'),('en_US','316','Tulcea'),('en_US','317','Vaslui'),('en_US','318','Vâlcea'),('en_US','319','Vrancea'),('en_US','320','Lappi'),('en_US','321','Pohjois-Pohjanmaa'),('en_US','322','Kainuu'),('en_US','323','Pohjois-Karjala'),('en_US','324','Pohjois-Savo'),('en_US','325','Etelä-Savo'),('en_US','326','Etelä-Pohjanmaa'),('en_US','327','Pohjanmaa'),('en_US','328','Pirkanmaa'),('en_US','329','Satakunta'),('en_US','330','Keski-Pohjanmaa'),('en_US','331','Keski-Suomi'),('en_US','332','Varsinais-Suomi'),('en_US','333','Etelä-Karjala'),('en_US','334','Päijät-Häme'),('en_US','335','Kanta-Häme'),('en_US','336','Uusimaa'),('en_US','337','Itä-Uusimaa'),('en_US','338','Kymenlaakso'),('en_US','339','Ahvenanmaa'),('en_US','340','Harjumaa'),('en_US','341','Hiiumaa'),('en_US','342','Ida-Virumaa'),('en_US','343','Jõgevamaa'),('en_US','344','Järvamaa'),('en_US','345','Läänemaa'),('en_US','346','Lääne-Virumaa'),('en_US','347','Põlvamaa'),('en_US','348','Pärnumaa'),('en_US','349','Raplamaa'),('en_US','350','Saaremaa'),('en_US','351','Tartumaa'),('en_US','352','Valgamaa'),('en_US','353','Viljandimaa'),('en_US','354','Võrumaa'),('en_US','355','Daugavpils'),('en_US','356','Jelgava'),('en_US','357','Jēkabpils'),('en_US','358','Jūrmala'),('en_US','359','Liepāja'),('en_US','360','Liepājas novads'),('en_US','361','Rēzekne'),('en_US','362','Rīga'),('en_US','363','Rīgas novads'),('en_US','364','Valmiera'),('en_US','365','Ventspils'),('en_US','366','Aglonas novads'),('en_US','367','Aizkraukles novads'),('en_US','368','Aizputes novads'),('en_US','369','Aknīstes novads'),('en_US','370','Alojas novads'),('en_US','371','Alsungas novads'),('en_US','372','Alūksnes novads'),('en_US','373','Amatas novads'),('en_US','374','Apes novads'),('en_US','375','Auces novads'),('en_US','376','Babītes novads'),('en_US','377','Baldones novads'),('en_US','378','Baltinavas novads'),('en_US','379','Balvu novads'),('en_US','380','Bauskas novads'),('en_US','381','Beverīnas novads'),('en_US','382','Brocēnu novads'),('en_US','383','Burtnieku novads'),('en_US','384','Carnikavas novads'),('en_US','385','Cesvaines novads'),('en_US','386','Ciblas novads'),('en_US','387','Cēsu novads'),('en_US','388','Dagdas novads'),('en_US','389','Daugavpils novads'),('en_US','390','Dobeles novads'),('en_US','391','Dundagas novads'),('en_US','392','Durbes novads'),('en_US','393','Engures novads'),('en_US','394','Garkalnes novads'),('en_US','395','Grobiņas novads'),('en_US','396','Gulbenes novads'),('en_US','397','Iecavas novads'),('en_US','398','Ikšķiles novads'),('en_US','399','Ilūkstes novads'),('en_US','400','Inčukalna novads'),('en_US','401','Jaunjelgavas novads'),('en_US','402','Jaunpiebalgas novads'),('en_US','403','Jaunpils novads'),('en_US','404','Jelgavas novads'),('en_US','405','Jēkabpils novads'),('en_US','406','Kandavas novads'),('en_US','407','Kokneses novads'),('en_US','408','Krimuldas novads'),('en_US','409','Krustpils novads'),('en_US','410','Krāslavas novads'),('en_US','411','Kuldīgas novads'),('en_US','412','Kārsavas novads'),('en_US','413','Lielvārdes novads'),('en_US','414','Limbažu novads'),('en_US','415','Lubānas novads'),('en_US','416','Ludzas novads'),('en_US','417','Līgatnes novads'),('en_US','418','Līvānu novads'),('en_US','419','Madonas novads'),('en_US','420','Mazsalacas novads'),('en_US','421','Mālpils novads'),('en_US','422','Mārupes novads'),('en_US','423','Naukšēnu novads'),('en_US','424','Neretas novads'),('en_US','425','Nīcas novads'),('en_US','426','Ogres novads'),('en_US','427','Olaines novads'),('en_US','428','Ozolnieku novads'),('en_US','429','Preiļu novads'),('en_US','430','Priekules novads'),('en_US','431','Priekuļu novads'),('en_US','432','Pārgaujas novads'),('en_US','433','Pāvilostas novads'),('en_US','434','Pļaviņu novads'),('en_US','435','Raunas novads'),('en_US','436','Riebiņu novads'),('en_US','437','Rojas novads'),('en_US','438','Ropažu novads'),('en_US','439','Rucavas novads'),('en_US','440','Rugāju novads'),('en_US','441','Rundāles novads'),('en_US','442','Rēzeknes novads'),('en_US','443','Rūjienas novads'),('en_US','444','Salacgrīvas novads'),('en_US','445','Salas novads'),('en_US','446','Salaspils novads'),('en_US','447','Saldus novads'),('en_US','448','Saulkrastu novads'),('en_US','449','Siguldas novads'),('en_US','450','Skrundas novads'),('en_US','451','Skrīveru novads'),('en_US','452','Smiltenes novads'),('en_US','453','Stopiņu novads'),('en_US','454','Strenču novads'),('en_US','455','Sējas novads'),('en_US','456','Talsu novads'),('en_US','457','Tukuma novads'),('en_US','458','Tērvetes novads'),('en_US','459','Vaiņodes novads'),('en_US','460','Valkas novads'),('en_US','461','Valmieras novads'),('en_US','462','Varakļānu novads'),('en_US','463','Vecpiebalgas novads'),('en_US','464','Vecumnieku novads'),('en_US','465','Ventspils novads'),('en_US','466','Viesītes novads'),('en_US','467','Viļakas novads'),('en_US','468','Viļānu novads'),('en_US','469','Vārkavas novads'),('en_US','470','Zilupes novads'),('en_US','471','Ādažu novads'),('en_US','472','Ērgļu novads'),('en_US','473','Ķeguma novads'),('en_US','474','Ķekavas novads'),('en_US','475','Alytaus Apskritis'),('en_US','476','Kauno Apskritis'),('en_US','477','Klaipėdos Apskritis'),('en_US','478','Marijampolės Apskritis'),('en_US','479','Panevėžio Apskritis'),('en_US','480','Šiaulių Apskritis'),('en_US','481','Tauragės Apskritis'),('en_US','482','Telšių Apskritis'),('en_US','483','Utenos Apskritis'),('en_US','484','Vilniaus Apskritis'),('en_US','485','Acre'),('en_US','486','Alagoas'),('en_US','487','Amapá'),('en_US','488','Amazonas'),('en_US','489','Bahia'),('en_US','490','Ceará'),('en_US','491','Espírito Santo'),('en_US','492','Goiás'),('en_US','493','Maranhão'),('en_US','494','Mato Grosso'),('en_US','495','Mato Grosso do Sul'),('en_US','496','Minas Gerais'),('en_US','497','Pará'),('en_US','498','Paraíba'),('en_US','499','Paraná'),('en_US','500','Pernambuco'),('en_US','501','Piauí'),('en_US','502','Rio de Janeiro'),('en_US','503','Rio Grande do Norte'),('en_US','504','Rio Grande do Sul'),('en_US','505','Rondônia'),('en_US','506','Roraima'),('en_US','507','Santa Catarina'),('en_US','508','São Paulo'),('en_US','509','Sergipe'),('en_US','510','Tocantins'),('en_US','511','Distrito Federal');
/*!40000 ALTER TABLE `directory_country_region_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_currency_rate`
--

DROP TABLE IF EXISTS `directory_currency_rate`;
CREATE TABLE `directory_currency_rate` (
  `currency_from` varchar(3) NOT NULL COMMENT 'Currency Code Convert From',
  `currency_to` varchar(3) NOT NULL COMMENT 'Currency Code Convert To',
  `rate` decimal(24,12) NOT NULL DEFAULT '0.000000000000' COMMENT 'Currency Conversion Rate',
  PRIMARY KEY (`currency_from`,`currency_to`),
  KEY `DIRECTORY_CURRENCY_RATE_CURRENCY_TO` (`currency_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Currency Rate';

--
-- Dumping data for table `directory_currency_rate`
--

LOCK TABLES `directory_currency_rate` WRITE;
/*!40000 ALTER TABLE `directory_currency_rate` DISABLE KEYS */;
INSERT INTO `directory_currency_rate` VALUES ('EUR','EUR','1.000000000000'),('EUR','USD','1.415000000000'),('USD','EUR','0.706700000000'),('USD','USD','1.000000000000');
/*!40000 ALTER TABLE `directory_currency_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloadable_link`
--

DROP TABLE IF EXISTS `downloadable_link`;
CREATE TABLE `downloadable_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order',
  `number_of_downloads` int(11) DEFAULT NULL COMMENT 'Number of downloads',
  `is_shareable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Shareable flag',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'Link Url',
  `link_file` varchar(255) DEFAULT NULL COMMENT 'Link File',
  `link_type` varchar(20) DEFAULT NULL COMMENT 'Link Type',
  `sample_url` varchar(255) DEFAULT NULL COMMENT 'Sample Url',
  `sample_file` varchar(255) DEFAULT NULL COMMENT 'Sample File',
  `sample_type` varchar(20) DEFAULT NULL COMMENT 'Sample Type',
  PRIMARY KEY (`link_id`),
  KEY `DOWNLOADABLE_LINK_PRODUCT_ID_SORT_ORDER` (`product_id`,`sort_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Table';

--
-- Table structure for table `downloadable_link_price`
--

DROP TABLE IF EXISTS `downloadable_link_price`;
CREATE TABLE `downloadable_link_price` (
  `price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Price ID',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  PRIMARY KEY (`price_id`),
  KEY `DOWNLOADABLE_LINK_PRICE_LINK_ID` (`link_id`),
  KEY `DOWNLOADABLE_LINK_PRICE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Price Table';

--
-- Table structure for table `downloadable_link_purchased`
--

DROP TABLE IF EXISTS `downloadable_link_purchased`;
CREATE TABLE `downloadable_link_purchased` (
  `purchased_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Purchased ID',
  `order_id` int(10) unsigned DEFAULT '0' COMMENT 'Order ID',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment ID',
  `order_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Item ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of creation',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of modification',
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer ID',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product name',
  `product_sku` varchar(255) DEFAULT NULL COMMENT 'Product sku',
  `link_section_title` varchar(255) DEFAULT NULL COMMENT 'Link_section_title',
  PRIMARY KEY (`purchased_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ORDER_ID` (`order_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ORDER_ITEM_ID` (`order_item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Purchased Table';

--
-- Table structure for table `downloadable_link_purchased_item`
--

DROP TABLE IF EXISTS `downloadable_link_purchased_item`;
CREATE TABLE `downloadable_link_purchased_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item ID',
  `purchased_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Purchased ID',
  `order_item_id` int(10) unsigned DEFAULT '0' COMMENT 'Order Item ID',
  `product_id` int(10) unsigned DEFAULT '0' COMMENT 'Product ID',
  `link_hash` varchar(255) DEFAULT NULL COMMENT 'Link hash',
  `number_of_downloads_bought` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of downloads bought',
  `number_of_downloads_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of downloads used',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `link_title` varchar(255) DEFAULT NULL COMMENT 'Link Title',
  `is_shareable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Shareable Flag',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'Link Url',
  `link_file` varchar(255) DEFAULT NULL COMMENT 'Link File',
  `link_type` varchar(255) DEFAULT NULL COMMENT 'Link Type',
  `status` varchar(50) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  PRIMARY KEY (`item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_LINK_HASH` (`link_hash`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_ORDER_ITEM_ID` (`order_item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_PURCHASED_ID` (`purchased_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Purchased Item Table';

--
-- Table structure for table `downloadable_link_title`
--

DROP TABLE IF EXISTS `downloadable_link_title`;
CREATE TABLE `downloadable_link_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Title ID',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `DOWNLOADABLE_LINK_TITLE_LINK_ID_STORE_ID` (`link_id`,`store_id`),
  KEY `DOWNLOADABLE_LINK_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link Title Table';

--
-- Table structure for table `downloadable_sample`
--

DROP TABLE IF EXISTS `downloadable_sample`;
CREATE TABLE `downloadable_sample` (
  `sample_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Sample ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `sample_url` varchar(255) DEFAULT NULL COMMENT 'Sample URL',
  `sample_file` varchar(255) DEFAULT NULL COMMENT 'Sample file',
  `sample_type` varchar(20) DEFAULT NULL COMMENT 'Sample Type',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`sample_id`),
  KEY `DOWNLOADABLE_SAMPLE_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Sample Table';

--
-- Table structure for table `downloadable_sample_title`
--

DROP TABLE IF EXISTS `downloadable_sample_title`;
CREATE TABLE `downloadable_sample_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Title ID',
  `sample_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sample ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `DOWNLOADABLE_SAMPLE_TITLE_SAMPLE_ID_STORE_ID` (`sample_id`,`store_id`),
  KEY `DOWNLOADABLE_SAMPLE_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Sample Title Table';

--
-- Table structure for table `eav_attribute`
--

DROP TABLE IF EXISTS `eav_attribute`;
CREATE TABLE `eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_code` varchar(255) DEFAULT NULL COMMENT 'Attribute Code',
  `attribute_model` varchar(255) DEFAULT NULL COMMENT 'Attribute Model',
  `backend_model` varchar(255) DEFAULT NULL COMMENT 'Backend Model',
  `backend_type` varchar(8) NOT NULL DEFAULT 'static' COMMENT 'Backend Type',
  `backend_table` varchar(255) DEFAULT NULL COMMENT 'Backend Table',
  `frontend_model` varchar(255) DEFAULT NULL COMMENT 'Frontend Model',
  `frontend_input` varchar(50) DEFAULT NULL COMMENT 'Frontend Input',
  `frontend_label` varchar(255) DEFAULT NULL COMMENT 'Frontend Label',
  `frontend_class` varchar(255) DEFAULT NULL COMMENT 'Frontend Class',
  `source_model` varchar(255) DEFAULT NULL COMMENT 'Source Model',
  `is_required` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is Required',
  `is_user_defined` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is User Defined',
  `default_value` text COMMENT 'Default Value',
  `is_unique` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is Unique',
  `note` varchar(255) DEFAULT NULL COMMENT 'Note',
  PRIMARY KEY (`attribute_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_ENTITY_TYPE_ID_ATTRIBUTE_CODE` (`entity_type_id`,`attribute_code`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute';

--
-- Dumping data for table `eav_attribute`
--

LOCK TABLES `eav_attribute` WRITE;
/*!40000 ALTER TABLE `eav_attribute` DISABLE KEYS */;
INSERT INTO `eav_attribute` VALUES (1,1,'website_id',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Website','static',NULL,NULL,'select','Associate to Website',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Website',1,0,NULL,0,NULL),(2,1,'store_id',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Store','static',NULL,NULL,'select','Create In',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Store',1,0,NULL,0,NULL),(3,1,'created_in',NULL,NULL,'static',NULL,NULL,'text','Created From',NULL,NULL,0,0,NULL,0,NULL),(4,1,'prefix',NULL,NULL,'static',NULL,NULL,'text','Prefix',NULL,NULL,0,0,NULL,0,NULL),(5,1,'firstname',NULL,NULL,'static',NULL,NULL,'text','First Name',NULL,NULL,1,0,NULL,0,NULL),(6,1,'middlename',NULL,NULL,'static',NULL,NULL,'text','Middle Name/Initial',NULL,NULL,0,0,NULL,0,NULL),(7,1,'lastname',NULL,NULL,'static',NULL,NULL,'text','Last Name',NULL,NULL,1,0,NULL,0,NULL),(8,1,'suffix',NULL,NULL,'static',NULL,NULL,'text','Suffix',NULL,NULL,0,0,NULL,0,NULL),(9,1,'email',NULL,NULL,'static',NULL,NULL,'text','Email',NULL,NULL,1,0,NULL,0,NULL),(10,1,'group_id',NULL,NULL,'static',NULL,NULL,'select','Group',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Group',1,0,NULL,0,NULL),(11,1,'dob',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','static',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Frontend\\Datetime','date','Date of Birth',NULL,NULL,0,0,NULL,0,NULL),(12,1,'password_hash',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Password','static',NULL,NULL,'hidden',NULL,NULL,NULL,0,0,NULL,0,NULL),(13,1,'rp_token',NULL,NULL,'static',NULL,NULL,'hidden',NULL,NULL,NULL,0,0,NULL,0,NULL),(14,1,'rp_token_created_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,0,0,NULL,0,NULL),(15,1,'default_billing',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Billing','static',NULL,NULL,'text','Default Billing Address',NULL,NULL,0,0,NULL,0,NULL),(16,1,'default_shipping',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Shipping','static',NULL,NULL,'text','Default Shipping Address',NULL,NULL,0,0,NULL,0,NULL),(17,1,'taxvat',NULL,NULL,'static',NULL,NULL,'text','Tax/VAT Number',NULL,NULL,0,0,NULL,0,NULL),(18,1,'confirmation',NULL,NULL,'static',NULL,NULL,'text','Is Confirmed',NULL,NULL,0,0,NULL,0,NULL),(19,1,'created_at',NULL,NULL,'static',NULL,NULL,'date','Created At',NULL,NULL,0,0,NULL,0,NULL),(20,1,'gender',NULL,NULL,'static',NULL,NULL,'select','Gender',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,0,NULL,0,NULL),(21,1,'disable_auto_group_change',NULL,'Magento\\Customer\\Model\\Attribute\\Backend\\Data\\Boolean','static',NULL,NULL,'boolean','Disable Automatic Group Change Based on VAT ID',NULL,NULL,0,0,NULL,0,NULL),(22,2,'prefix',NULL,NULL,'static',NULL,NULL,'text','Prefix',NULL,NULL,0,0,NULL,0,NULL),(23,2,'firstname',NULL,NULL,'static',NULL,NULL,'text','First Name',NULL,NULL,1,0,NULL,0,NULL),(24,2,'middlename',NULL,NULL,'static',NULL,NULL,'text','Middle Name/Initial',NULL,NULL,0,0,NULL,0,NULL),(25,2,'lastname',NULL,NULL,'static',NULL,NULL,'text','Last Name',NULL,NULL,1,0,NULL,0,NULL),(26,2,'suffix',NULL,NULL,'static',NULL,NULL,'text','Suffix',NULL,NULL,0,0,NULL,0,NULL),(27,2,'company',NULL,NULL,'static',NULL,NULL,'text','Company',NULL,NULL,0,0,NULL,0,NULL),(28,2,'street',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\DefaultBackend','static',NULL,NULL,'multiline','Street Address',NULL,NULL,1,0,NULL,0,NULL),(29,2,'city',NULL,NULL,'static',NULL,NULL,'text','City',NULL,NULL,1,0,NULL,0,NULL),(30,2,'country_id',NULL,NULL,'static',NULL,NULL,'select','Country',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Source\\Country',1,0,NULL,0,NULL),(31,2,'region',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Backend\\Region','static',NULL,NULL,'text','State/Province',NULL,NULL,0,0,NULL,0,NULL),(32,2,'region_id',NULL,NULL,'static',NULL,NULL,'hidden','State/Province',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Source\\Region',0,0,NULL,0,NULL),(33,2,'postcode',NULL,NULL,'static',NULL,NULL,'text','Zip/Postal Code',NULL,NULL,0,0,NULL,0,NULL),(34,2,'telephone',NULL,NULL,'static',NULL,NULL,'text','Phone Number',NULL,NULL,1,0,NULL,0,NULL),(35,2,'fax',NULL,NULL,'static',NULL,NULL,'text','Fax',NULL,NULL,0,0,NULL,0,NULL),(36,2,'vat_id',NULL,NULL,'static',NULL,NULL,'text','VAT number',NULL,NULL,0,0,NULL,0,NULL),(37,2,'vat_is_valid',NULL,NULL,'static',NULL,NULL,'text','VAT number validity',NULL,NULL,0,0,NULL,0,NULL),(38,2,'vat_request_id',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request ID',NULL,NULL,0,0,NULL,0,NULL),(39,2,'vat_request_date',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request date',NULL,NULL,0,0,NULL,0,NULL),(40,2,'vat_request_success',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request success',NULL,NULL,0,0,NULL,0,NULL),(41,1,'updated_at',NULL,NULL,'static',NULL,NULL,'date','Updated At',NULL,NULL,0,0,NULL,0,NULL),(42,1,'failures_num',NULL,NULL,'static',NULL,NULL,'hidden','Failures Number',NULL,NULL,0,0,NULL,0,NULL),(43,1,'first_failure',NULL,NULL,'static',NULL,NULL,'date','First Failure Date',NULL,NULL,0,0,NULL,0,NULL),(44,1,'lock_expires',NULL,NULL,'static',NULL,NULL,'date','Failures Number',NULL,NULL,0,0,NULL,0,NULL),(45,3,'name',NULL,NULL,'varchar',NULL,NULL,'text','Name',NULL,NULL,1,0,NULL,0,NULL),(46,3,'is_active',NULL,NULL,'int',NULL,NULL,'select','Is Active',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,NULL,0,NULL),(47,3,'description',NULL,NULL,'text',NULL,NULL,'textarea','Description',NULL,NULL,0,0,NULL,0,NULL),(48,3,'image',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Image',NULL,NULL,0,0,NULL,0,NULL),(49,3,'meta_title',NULL,NULL,'varchar',NULL,NULL,'text','Page Title',NULL,NULL,0,0,NULL,0,NULL),(50,3,'meta_keywords',NULL,NULL,'text',NULL,NULL,'textarea','Meta Keywords',NULL,NULL,0,0,NULL,0,NULL),(51,3,'meta_description',NULL,NULL,'text',NULL,NULL,'textarea','Meta Description',NULL,NULL,0,0,NULL,0,NULL),(52,3,'display_mode',NULL,NULL,'varchar',NULL,NULL,'select','Display Mode',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Mode',0,0,NULL,0,NULL),(53,3,'landing_page',NULL,NULL,'int',NULL,NULL,'select','CMS Block',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Page',0,0,NULL,0,NULL),(54,3,'is_anchor',NULL,NULL,'int',NULL,NULL,'select','Is Anchor',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,'1',0,NULL),(55,3,'path',NULL,NULL,'static',NULL,NULL,'text','Path',NULL,NULL,0,0,NULL,0,NULL),(56,3,'position',NULL,NULL,'static',NULL,NULL,'text','Position',NULL,NULL,0,0,NULL,0,NULL),(57,3,'all_children',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(58,3,'path_in_store',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(59,3,'children',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(60,3,'custom_design',NULL,NULL,'varchar',NULL,NULL,'select','Custom Design',NULL,'Magento\\Theme\\Model\\Theme\\Source\\Theme',0,0,NULL,0,NULL),(61,3,'custom_design_from','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Frontend\\Datetime','date','Active From',NULL,NULL,0,0,NULL,0,NULL),(62,3,'custom_design_to',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Active To',NULL,NULL,0,0,NULL,0,NULL),(63,3,'page_layout',NULL,NULL,'varchar',NULL,NULL,'select','Page Layout',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(64,3,'custom_layout_update',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Customlayoutupdate','text',NULL,NULL,'textarea','Custom Layout Update',NULL,NULL,0,0,NULL,0,NULL),(65,3,'level',NULL,NULL,'static',NULL,NULL,'text','Level',NULL,NULL,0,0,NULL,0,NULL),(66,3,'children_count',NULL,NULL,'static',NULL,NULL,'text','Children Count',NULL,NULL,0,0,NULL,0,NULL),(67,3,'available_sort_by',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Sortby','text',NULL,NULL,'multiselect','Available Product Listing Sort By',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Sortby',1,0,NULL,0,NULL),(68,3,'default_sort_by',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Sortby','varchar',NULL,NULL,'select','Default Product Listing Sort By',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Sortby',1,0,NULL,0,NULL),(69,3,'include_in_menu',NULL,NULL,'int',NULL,NULL,'select','Include in Navigation Menu',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,'1',0,NULL),(70,3,'custom_use_parent_settings',NULL,NULL,'int',NULL,NULL,'select','Use Parent Category Settings',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(71,3,'custom_apply_to_products',NULL,NULL,'int',NULL,NULL,'select','Apply To Products',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(72,3,'filter_price_range',NULL,NULL,'decimal',NULL,NULL,'text','Layered Navigation Price Step',NULL,NULL,0,0,NULL,0,NULL),(73,4,'name',NULL,NULL,'varchar',NULL,NULL,'text','Product Name','validate-length maximum-length-255',NULL,1,0,NULL,0,NULL),(74,4,'sku',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Sku','static',NULL,NULL,'text','SKU','validate-length maximum-length-64',NULL,1,0,NULL,1,NULL),(75,4,'description',NULL,NULL,'text',NULL,NULL,'textarea','Description',NULL,NULL,0,0,NULL,0,NULL),(76,4,'short_description',NULL,NULL,'text',NULL,NULL,'textarea','Short Description',NULL,NULL,0,0,NULL,0,NULL),(77,4,'price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Price',NULL,NULL,1,0,NULL,0,NULL),(78,4,'special_price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Special Price',NULL,NULL,0,0,NULL,0,NULL),(79,4,'special_from_date',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Special Price From Date',NULL,NULL,0,0,NULL,0,NULL),(80,4,'special_to_date',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Special Price To Date',NULL,NULL,0,0,NULL,0,NULL),(81,4,'cost',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Cost',NULL,NULL,0,1,NULL,0,NULL),(82,4,'weight',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Weight','decimal',NULL,NULL,'weight','Weight',NULL,NULL,0,0,NULL,0,NULL),(83,4,'manufacturer',NULL,NULL,'int',NULL,NULL,'select','Manufacturer',NULL,NULL,0,1,'9',0,NULL),(84,4,'meta_title',NULL,NULL,'varchar',NULL,NULL,'text','Meta Title',NULL,NULL,0,0,NULL,0,NULL),(85,4,'meta_keyword',NULL,NULL,'text',NULL,NULL,'textarea','Meta Keywords',NULL,NULL,0,0,NULL,0,NULL),(86,4,'meta_description',NULL,NULL,'varchar',NULL,NULL,'textarea','Meta Description',NULL,NULL,0,0,NULL,0,'Maximum 255 chars. Meta Description should optimally be between 150-160 characters'),(87,4,'image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Base',NULL,NULL,0,0,NULL,0,NULL),(88,4,'small_image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Small',NULL,NULL,0,0,NULL,0,NULL),(89,4,'thumbnail',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Thumbnail',NULL,NULL,0,0,NULL,0,NULL),(90,4,'media_gallery',NULL,NULL,'static',NULL,NULL,'gallery','Media Gallery',NULL,NULL,0,0,NULL,0,NULL),(91,4,'old_id',NULL,NULL,'int',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(92,4,'tier_price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Tierprice','decimal',NULL,NULL,'text','Tier Price',NULL,NULL,0,0,NULL,0,NULL),(93,4,'color',NULL,NULL,'int',NULL,NULL,'select','Color',NULL,NULL,0,1,'4',0,NULL),(94,4,'news_from_date',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Set Product as New from Date',NULL,NULL,0,0,NULL,0,NULL),(95,4,'news_to_date',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Set Product as New to Date',NULL,NULL,0,0,NULL,0,NULL),(96,4,'gallery',NULL,NULL,'varchar',NULL,NULL,'gallery','Image Gallery',NULL,NULL,0,0,NULL,0,NULL),(97,4,'status',NULL,NULL,'int',NULL,NULL,'select','Enable Product',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Status',0,0,'1',0,NULL),(98,4,'minimal_price',NULL,NULL,'decimal',NULL,NULL,'price','Minimal Price',NULL,NULL,0,0,NULL,0,NULL),(99,4,'visibility',NULL,NULL,'int',NULL,NULL,'select','Visibility',NULL,'Magento\\Catalog\\Model\\Product\\Visibility',0,0,'4',0,NULL),(100,4,'custom_design',NULL,NULL,'varchar',NULL,NULL,'select','New Theme',NULL,'Magento\\Theme\\Model\\Theme\\Source\\Theme',0,0,NULL,0,NULL),(101,4,'custom_design_from',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Active From',NULL,NULL,0,0,NULL,0,NULL),(102,4,'custom_design_to',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Active To',NULL,NULL,0,0,NULL,0,NULL),(103,4,'custom_layout_update',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Customlayoutupdate','text',NULL,NULL,'textarea','Layout Update XML',NULL,NULL,0,0,NULL,0,NULL),(104,4,'page_layout',NULL,NULL,'varchar',NULL,NULL,'select','Layout',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(105,4,'category_ids',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Category','static',NULL,NULL,'text','Categories',NULL,NULL,0,0,NULL,0,NULL),(106,4,'options_container',NULL,NULL,'varchar',NULL,NULL,'select','Display Product Options In',NULL,'Magento\\Catalog\\Model\\Entity\\Product\\Attribute\\Design\\Options\\Container',0,0,'container2',0,NULL),(107,4,'required_options',NULL,NULL,'static',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(108,4,'has_options',NULL,NULL,'static',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(109,4,'image_label',NULL,NULL,'varchar',NULL,NULL,'text','Image Label',NULL,NULL,0,0,NULL,0,NULL),(110,4,'small_image_label',NULL,NULL,'varchar',NULL,NULL,'text','Small Image Label',NULL,NULL,0,0,NULL,0,NULL),(111,4,'thumbnail_label',NULL,NULL,'varchar',NULL,NULL,'text','Thumbnail Label',NULL,NULL,0,0,NULL,0,NULL),(112,4,'created_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,1,0,NULL,0,NULL),(113,4,'updated_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,1,0,NULL,0,NULL),(114,4,'country_of_manufacture',NULL,NULL,'varchar',NULL,NULL,'select','Country of Manufacture',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Countryofmanufacture',0,0,NULL,0,NULL),(115,4,'quantity_and_stock_status',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Stock','int',NULL,NULL,'select','Quantity',NULL,'Magento\\CatalogInventory\\Model\\Source\\Stock',0,0,'1',0,NULL),(116,4,'custom_layout',NULL,NULL,'varchar',NULL,NULL,'select','New Layout',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(117,3,'url_key',NULL,NULL,'varchar',NULL,NULL,'text','URL Key',NULL,NULL,0,0,NULL,0,NULL),(118,3,'url_path',NULL,NULL,'varchar',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(119,4,'url_key',NULL,NULL,'varchar',NULL,NULL,'text','URL Key',NULL,NULL,0,0,NULL,0,NULL),(120,4,'url_path',NULL,NULL,'varchar',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(121,4,'msrp',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Manufacturer\'s Suggested Retail Price',NULL,NULL,0,0,NULL,0,NULL),(122,4,'msrp_display_actual_price_type',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Boolean','varchar',NULL,NULL,'select','Display Actual Price',NULL,'Magento\\Msrp\\Model\\Product\\Attribute\\Source\\Type\\Price',0,0,'0',0,NULL),(123,4,'price_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic Price',NULL,NULL,1,0,'0',0,NULL),(124,4,'sku_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic SKU',NULL,NULL,1,0,'0',0,NULL),(125,4,'weight_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic Weight',NULL,NULL,1,0,'0',0,NULL),(126,4,'price_view',NULL,NULL,'int',NULL,NULL,'select','Price View',NULL,'Magento\\Bundle\\Model\\Product\\Attribute\\Source\\Price\\View',1,0,NULL,0,NULL),(127,4,'shipment_type',NULL,NULL,'int',NULL,NULL,'select','Ship Bundle Items',NULL,'Magento\\Bundle\\Model\\Product\\Attribute\\Source\\Shipment\\Type',1,0,'0',0,NULL),(128,4,'links_purchased_separately',NULL,NULL,'int',NULL,NULL,NULL,'Links can be purchased separately',NULL,NULL,1,0,NULL,0,NULL),(129,4,'samples_title',NULL,NULL,'varchar',NULL,NULL,NULL,'Samples title',NULL,NULL,1,0,NULL,0,NULL),(130,4,'links_title',NULL,NULL,'varchar',NULL,NULL,NULL,'Links title',NULL,NULL,1,0,NULL,0,NULL),(131,4,'links_exist',NULL,NULL,'int',NULL,NULL,NULL,NULL,NULL,NULL,0,0,'0',0,NULL),(132,4,'swatch_image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Swatch',NULL,NULL,0,0,NULL,0,NULL),(133,4,'tax_class_id',NULL,NULL,'int',NULL,NULL,'select','Tax Class',NULL,'Magento\\Tax\\Model\\TaxClass\\Source\\Product',0,0,'2',0,NULL),(134,4,'gift_message_available',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Boolean','varchar',NULL,NULL,'select','Allow Gift Message',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(135,3,'thumb_popular',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Thumbnail Popular',NULL,NULL,0,0,NULL,0,NULL),(136,3,'cate_popular',NULL,NULL,'int',NULL,NULL,'select','Show Category Popular',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(137,4,'featured',NULL,NULL,'int',NULL,NULL,'select','Featured',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,'0',0,NULL),(138,3,'thumb_nail',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Thumbnail',NULL,NULL,0,0,NULL,0,NULL),(139,3,'is_new',NULL,NULL,'int',NULL,NULL,'select','Is New',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,NULL,0,NULL),(140,3,'is_sale',NULL,NULL,'int',NULL,NULL,'select','Is Sale',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,NULL,0,NULL),(141,4,'show_countdown',NULL,NULL,'varchar',NULL,NULL,'select','Show Price Countdown',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Boolean',0,0,'1',0,NULL);
/*!40000 ALTER TABLE `eav_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_group`
--

DROP TABLE IF EXISTS `eav_attribute_group`;
CREATE TABLE `eav_attribute_group` (
  `attribute_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Group Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `attribute_group_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Group Name',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Default Id',
  `attribute_group_code` varchar(255) NOT NULL COMMENT 'Attribute Group Code',
  `tab_group_code` varchar(255) DEFAULT NULL COMMENT 'Tab Group Code',
  PRIMARY KEY (`attribute_group_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_GROUP_ATTRIBUTE_SET_ID_ATTRIBUTE_GROUP_NAME` (`attribute_set_id`,`attribute_group_name`),
  KEY `EAV_ATTRIBUTE_GROUP_ATTRIBUTE_SET_ID_SORT_ORDER` (`attribute_set_id`,`sort_order`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Group';

--
-- Dumping data for table `eav_attribute_group`
--

LOCK TABLES `eav_attribute_group` WRITE;
/*!40000 ALTER TABLE `eav_attribute_group` DISABLE KEYS */;
INSERT INTO `eav_attribute_group` VALUES (1,1,'General',1,1,'general',NULL),(2,2,'General',1,1,'general',NULL),(3,3,'General',10,1,'general',NULL),(4,3,'General Information',2,0,'general-information',NULL),(5,3,'Display Settings',20,0,'display-settings',NULL),(6,3,'Custom Design',30,0,'custom-design',NULL),(7,4,'Product Details',1,1,'product-details','basic'),(8,4,'Advanced Pricing',6,0,'advanced-pricing','advanced'),(9,4,'Search Engine Optimization',5,0,'search-engine-optimization','basic'),(10,4,'Images',4,0,'image-management','basic'),(11,4,'Design',7,0,'design','advanced'),(12,4,'Autosettings',9,0,'autosettings','advanced'),(13,4,'Content',2,0,'content','basic'),(14,4,'Schedule Design Update',8,0,'schedule-design-update','advanced'),(15,4,'Bundle Items',3,0,'bundle-items',NULL),(16,5,'General',1,1,'general',NULL),(17,6,'General',1,1,'general',NULL),(18,7,'General',1,1,'general',NULL),(19,8,'General',1,1,'general',NULL),(20,4,'Gift Options',10,0,'gift-options',NULL),(21,4,'Attributes',11,0,'attributes',NULL);
/*!40000 ALTER TABLE `eav_attribute_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_label`
--

DROP TABLE IF EXISTS `eav_attribute_label`;
CREATE TABLE `eav_attribute_label` (
  `attribute_label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Label Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`attribute_label_id`),
  KEY `EAV_ATTRIBUTE_LABEL_STORE_ID` (`store_id`),
  KEY `EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID_STORE_ID` (`attribute_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Label';

--
-- Table structure for table `eav_attribute_option`
--

DROP TABLE IF EXISTS `eav_attribute_option`;
CREATE TABLE `eav_attribute_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Option';

--
-- Dumping data for table `eav_attribute_option`
--

LOCK TABLES `eav_attribute_option` WRITE;
/*!40000 ALTER TABLE `eav_attribute_option` DISABLE KEYS */;
INSERT INTO `eav_attribute_option` VALUES ('1',20,0),('2',20,1),('3',20,3),('4',93,1),('5',93,2),('6',93,3),('7',93,4),('8',93,5),('9',83,1),('10',83,2),('11',83,3),('12',83,4),('13',83,5),('14',83,6);
/*!40000 ALTER TABLE `eav_attribute_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_option_swatch`
--

DROP TABLE IF EXISTS `eav_attribute_option_swatch`;
CREATE TABLE `eav_attribute_option_swatch` (
  `swatch_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Swatch ID',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `type` smallint(5) unsigned NOT NULL COMMENT 'Swatch type: 0 - text, 1 - visual color, 2 - visual image',
  `value` varchar(255) DEFAULT NULL COMMENT 'Swatch Value',
  PRIMARY KEY (`swatch_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_OPTION_SWATCH_STORE_ID_OPTION_ID` (`store_id`,`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_SWATCH_SWATCH_ID` (`swatch_id`),
  KEY `EAV_ATTR_OPT_SWATCH_OPT_ID_EAV_ATTR_OPT_OPT_ID` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magento Swatches table';

--
-- Table structure for table `eav_attribute_option_value`
--

DROP TABLE IF EXISTS `eav_attribute_option_value`;
CREATE TABLE `eav_attribute_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  KEY `EAV_ATTRIBUTE_OPTION_VALUE_OPTION_ID` (`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_VALUE_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Option Value';

--
-- Dumping data for table `eav_attribute_option_value`
--

LOCK TABLES `eav_attribute_option_value` WRITE;
/*!40000 ALTER TABLE `eav_attribute_option_value` DISABLE KEYS */;
INSERT INTO `eav_attribute_option_value` VALUES ('1','1',0,'Male'),('2','2',0,'Female'),('3','3',0,'Not Specified'),('37','4',1,'Red'),('38','4',0,'Red'),('39','4',3,'Red'),('40','4',4,'Red'),('42','5',1,'Green'),('43','5',0,'Green'),('44','5',3,'Green'),('45','5',4,'Green'),('47','6',1,'Blue'),('48','6',0,'Blue'),('49','6',3,'Blue'),('50','6',4,'Blue'),('52','7',1,'Black'),('53','7',0,'Black'),('54','7',3,'Black'),('55','7',4,'Black'),('57','8',1,'White'),('58','8',0,'White'),('59','8',3,'White'),('60','8',4,'White'),('62','9',1,'Accessories'),('63','9',0,'Accessories'),('64','9',3,'Accessories'),('65','9',4,'Accessories'),('67','10',1,'Fashion'),('68','10',0,'Fashion'),('69','10',3,'Fashion'),('70','10',4,'Fashion'),('72','11',1,'Gear'),('73','11',0,'Gear'),('74','11',3,'Gear'),('75','11',4,'Gear'),('77','12',1,'Kid'),('78','12',0,'Kid'),('79','12',3,'Kid'),('80','12',4,'Kid'),('82','13',1,'beautifull'),('83','13',0,'beautifull'),('84','13',3,'beautifull'),('85','13',4,'beautifull'),('87','14',1,'dress'),('88','14',0,'dress'),('89','14',3,'dress'),('90','14',4,'dress');
/*!40000 ALTER TABLE `eav_attribute_option_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_set`
--

DROP TABLE IF EXISTS `eav_attribute_set`;
CREATE TABLE `eav_attribute_set` (
  `attribute_set_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Set Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Set Name',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`attribute_set_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_ATTRIBUTE_SET_NAME` (`entity_type_id`,`attribute_set_name`),
  KEY `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_SORT_ORDER` (`entity_type_id`,`sort_order`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Set';

--
-- Dumping data for table `eav_attribute_set`
--

LOCK TABLES `eav_attribute_set` WRITE;
/*!40000 ALTER TABLE `eav_attribute_set` DISABLE KEYS */;
INSERT INTO `eav_attribute_set` VALUES (1,1,'Default',2),(2,2,'Default',2),(3,3,'Default',1),(4,4,'Default',1),(5,5,'Default',1),(6,6,'Default',1),(7,7,'Default',1),(8,8,'Default',1);
/*!40000 ALTER TABLE `eav_attribute_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity`
--

DROP TABLE IF EXISTS `eav_entity`;
CREATE TABLE `eav_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Defines Is Entity Active',
  PRIMARY KEY (`entity_id`),
  KEY `EAV_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity';

--
-- Table structure for table `eav_entity_attribute`
--

DROP TABLE IF EXISTS `eav_entity_attribute`;
CREATE TABLE `eav_entity_attribute` (
  `entity_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `attribute_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`entity_attribute_id`),
  UNIQUE KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_SET_ID_ATTRIBUTE_ID` (`attribute_set_id`,`attribute_id`),
  UNIQUE KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_GROUP_ID_ATTRIBUTE_ID` (`attribute_group_id`,`attribute_id`),
  KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_SET_ID_SORT_ORDER` (`attribute_set_id`,`sort_order`),
  KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=716 DEFAULT CHARSET=utf8 COMMENT='Eav Entity Attributes';

--
-- Dumping data for table `eav_entity_attribute`
--

LOCK TABLES `eav_entity_attribute` WRITE;
/*!40000 ALTER TABLE `eav_entity_attribute` DISABLE KEYS */;
INSERT INTO `eav_entity_attribute` VALUES ('1',1,1,1,1,10),('2',1,1,1,2,20),('3',1,1,1,3,20),('4',1,1,1,4,30),('5',1,1,1,5,40),('6',1,1,1,6,50),('7',1,1,1,7,60),('8',1,1,1,8,70),('9',1,1,1,9,80),('10',1,1,1,10,25),('11',1,1,1,11,90),('12',1,1,1,12,81),('13',1,1,1,13,115),('14',1,1,1,14,120),('15',1,1,1,15,82),('16',1,1,1,16,83),('17',1,1,1,17,100),('18',1,1,1,18,85),('19',1,1,1,19,86),('20',1,1,1,20,110),('21',1,1,1,21,121),('22',2,2,2,22,10),('23',2,2,2,23,20),('24',2,2,2,24,30),('25',2,2,2,25,40),('26',2,2,2,26,50),('27',2,2,2,27,60),('28',2,2,2,28,70),('29',2,2,2,29,80),('30',2,2,2,30,90),('31',2,2,2,31,100),('32',2,2,2,32,100),('33',2,2,2,33,110),('34',2,2,2,34,120),('35',2,2,2,35,130),('36',2,2,2,36,131),('37',2,2,2,37,132),('38',2,2,2,38,133),('39',2,2,2,39,134),('40',2,2,2,40,135),('41',1,1,1,41,87),('42',1,1,1,42,100),('43',1,1,1,43,110),('44',1,1,1,44,120),('45',3,3,4,45,1),('46',3,3,4,46,2),('47',3,3,4,47,4),('48',3,3,4,48,5),('49',3,3,4,49,6),('50',3,3,4,50,7),('51',3,3,4,51,8),('52',3,3,5,52,10),('53',3,3,5,53,20),('54',3,3,5,54,30),('55',3,3,4,55,12),('56',3,3,4,56,13),('57',3,3,4,57,14),('58',3,3,4,58,15),('59',3,3,4,59,16),('60',3,3,6,60,10),('61',3,3,6,61,30),('62',3,3,6,62,40),('63',3,3,6,63,50),('64',3,3,6,64,60),('65',3,3,4,65,24),('66',3,3,4,66,25),('67',3,3,5,67,40),('68',3,3,5,68,50),('69',3,3,4,69,10),('70',3,3,6,70,5),('71',3,3,6,71,6),('72',3,3,5,72,51),('90',4,4,7,91,6),('96',4,4,8,98,8),('105',4,4,7,107,14),('106',4,4,7,108,15),('107',4,4,7,109,16),('108',4,4,7,110,17),('109',4,4,7,111,18),('110',4,4,7,112,19),('111',4,4,7,113,20),('115',3,3,4,117,3),('116',3,3,4,118,17),('118',4,4,7,120,11),('126',4,4,7,128,111),('127',4,4,7,129,112),('128',4,4,7,130,113),('129',4,4,7,131,114),('133',3,3,4,135,5),('134',3,3,4,136,5),('136',3,3,4,138,50),('137',3,3,4,139,51),('138',3,3,4,140,52),('621',4,4,7,73,2),('623',4,4,7,74,3),('625',4,4,7,77,5),('627',4,4,7,82,9),('629',4,4,7,94,13),('631',4,4,7,95,14),('633',4,4,7,97,1),('635',4,4,7,99,12),('637',4,4,7,105,11),('639',4,4,7,114,15),('641',4,4,7,115,8),('643',4,4,7,123,6),('645',4,4,7,124,4),('647',4,4,7,125,10),('649',4,4,7,133,7),('651',4,4,7,137,16),('653',4,4,13,75,1),('655',4,4,13,76,2),('657',4,4,15,127,1),('659',4,4,10,87,1),('661',4,4,10,88,2),('663',4,4,10,89,3),('665',4,4,10,90,5),('667',4,4,10,96,6),('669',4,4,10,132,4),('671',4,4,9,84,2),('673',4,4,9,85,3),('675',4,4,9,86,4),('677',4,4,9,119,1),('679',4,4,8,78,1),('681',4,4,8,79,2),('683',4,4,8,80,3),('685',4,4,8,81,4),('687',4,4,8,92,5),('689',4,4,8,121,6),('691',4,4,8,122,7),('693',4,4,8,126,8),('695',4,4,8,141,9),('697',4,4,11,103,3),('699',4,4,11,104,1),('701',4,4,11,106,2),('703',4,4,14,100,3),('705',4,4,14,101,1),('707',4,4,14,102,2),('709',4,4,14,116,4),('711',4,4,20,134,1),('713',4,4,21,83,1),('715',4,4,21,93,2);
/*!40000 ALTER TABLE `eav_entity_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_datetime`
--

DROP TABLE IF EXISTS `eav_entity_datetime`;
CREATE TABLE `eav_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_DATETIME_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_DATETIME_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_DATETIME_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

--
-- Table structure for table `eav_entity_decimal`
--

DROP TABLE IF EXISTS `eav_entity_decimal`;
CREATE TABLE `eav_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_DECIMAL_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_DECIMAL_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

--
-- Table structure for table `eav_entity_int`
--

DROP TABLE IF EXISTS `eav_entity_int`;
CREATE TABLE `eav_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_INT_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_INT_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_INT_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

--
-- Table structure for table `eav_entity_store`
--

DROP TABLE IF EXISTS `eav_entity_store`;
CREATE TABLE `eav_entity_store` (
  `entity_store_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Store Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `increment_prefix` varchar(20) DEFAULT NULL COMMENT 'Increment Prefix',
  `increment_last_id` varchar(50) DEFAULT NULL COMMENT 'Last Incremented Id',
  PRIMARY KEY (`entity_store_id`),
  KEY `EAV_ENTITY_STORE_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Store';

--
-- Table structure for table `eav_entity_text`
--

DROP TABLE IF EXISTS `eav_entity_text`;
CREATE TABLE `eav_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_TEXT_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `EAV_ENTITY_TEXT_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

--
-- Table structure for table `eav_entity_type`
--

DROP TABLE IF EXISTS `eav_entity_type`;
CREATE TABLE `eav_entity_type` (
  `entity_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Type Id',
  `entity_type_code` varchar(50) NOT NULL COMMENT 'Entity Type Code',
  `entity_model` varchar(255) NOT NULL COMMENT 'Entity Model',
  `attribute_model` varchar(255) DEFAULT NULL COMMENT 'Attribute Model',
  `entity_table` varchar(255) DEFAULT NULL COMMENT 'Entity Table',
  `value_table_prefix` varchar(255) DEFAULT NULL COMMENT 'Value Table Prefix',
  `entity_id_field` varchar(255) DEFAULT NULL COMMENT 'Entity Id Field',
  `is_data_sharing` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Defines Is Data Sharing',
  `data_sharing_key` varchar(100) DEFAULT 'default' COMMENT 'Data Sharing Key',
  `default_attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Attribute Set Id',
  `increment_model` varchar(255) DEFAULT NULL COMMENT 'Increment Model',
  `increment_per_store` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Increment Per Store',
  `increment_pad_length` smallint(5) unsigned NOT NULL DEFAULT '8' COMMENT 'Increment Pad Length',
  `increment_pad_char` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Increment Pad Char',
  `additional_attribute_table` varchar(255) DEFAULT NULL COMMENT 'Additional Attribute Table',
  `entity_attribute_collection` varchar(255) DEFAULT NULL COMMENT 'Entity Attribute Collection',
  PRIMARY KEY (`entity_type_id`),
  KEY `EAV_ENTITY_TYPE_ENTITY_TYPE_CODE` (`entity_type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Eav Entity Type';

--
-- Dumping data for table `eav_entity_type`
--

LOCK TABLES `eav_entity_type` WRITE;
/*!40000 ALTER TABLE `eav_entity_type` DISABLE KEYS */;
INSERT INTO `eav_entity_type` VALUES (1,'customer','Magento\\Customer\\Model\\ResourceModel\\Customer','Magento\\Customer\\Model\\Attribute','customer_entity',NULL,NULL,1,'default',1,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',0,8,'0','customer_eav_attribute','Magento\\Customer\\Model\\ResourceModel\\Attribute\\Collection'),(2,'customer_address','Magento\\Customer\\Model\\ResourceModel\\Address','Magento\\Customer\\Model\\Attribute','customer_address_entity',NULL,NULL,1,'default',2,NULL,0,8,'0','customer_eav_attribute','Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Collection'),(3,'catalog_category','Magento\\Catalog\\Model\\ResourceModel\\Category','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','catalog_category_entity',NULL,NULL,1,'default',3,NULL,0,8,'0','catalog_eav_attribute','Magento\\Catalog\\Model\\ResourceModel\\Category\\Attribute\\Collection'),(4,'catalog_product','Magento\\Catalog\\Model\\ResourceModel\\Product','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','catalog_product_entity',NULL,NULL,1,'default',4,NULL,0,8,'0','catalog_eav_attribute','Magento\\Catalog\\Model\\ResourceModel\\Product\\Attribute\\Collection'),(5,'order','Magento\\Sales\\Model\\ResourceModel\\Order',NULL,'sales_order',NULL,NULL,1,'default',5,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(6,'invoice','Magento\\Sales\\Model\\ResourceModel\\Order',NULL,'sales_invoice',NULL,NULL,1,'default',6,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(7,'creditmemo','Magento\\Sales\\Model\\ResourceModel\\Order\\Creditmemo',NULL,'sales_creditmemo',NULL,NULL,1,'default',7,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(8,'shipment','Magento\\Sales\\Model\\ResourceModel\\Order\\Shipment',NULL,'sales_shipment',NULL,NULL,1,'default',8,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL);
/*!40000 ALTER TABLE `eav_entity_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_varchar`
--

DROP TABLE IF EXISTS `eav_entity_varchar`;
CREATE TABLE `eav_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_VARCHAR_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_VARCHAR_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

--
-- Table structure for table `eav_form_element`
--

DROP TABLE IF EXISTS `eav_form_element`;
CREATE TABLE `eav_form_element` (
  `element_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Element Id',
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `fieldset_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Fieldset Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`element_id`),
  UNIQUE KEY `EAV_FORM_ELEMENT_TYPE_ID_ATTRIBUTE_ID` (`type_id`,`attribute_id`),
  KEY `EAV_FORM_ELEMENT_FIELDSET_ID` (`fieldset_id`),
  KEY `EAV_FORM_ELEMENT_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='Eav Form Element';

--
-- Dumping data for table `eav_form_element`
--

LOCK TABLES `eav_form_element` WRITE;
/*!40000 ALTER TABLE `eav_form_element` DISABLE KEYS */;
INSERT INTO `eav_form_element` VALUES ('1',1,NULL,23,'0'),('2',1,NULL,25,'1'),('3',1,NULL,27,'2'),('4',1,NULL,9,'3'),('5',1,NULL,28,'4'),('6',1,NULL,29,'5'),('7',1,NULL,31,'6'),('8',1,NULL,33,'7'),('9',1,NULL,30,'8'),('10',1,NULL,34,'9'),('11',1,NULL,35,'10'),('12',2,NULL,23,'0'),('13',2,NULL,25,'1'),('14',2,NULL,27,'2'),('15',2,NULL,9,'3'),('16',2,NULL,28,'4'),('17',2,NULL,29,'5'),('18',2,NULL,31,'6'),('19',2,NULL,33,'7'),('20',2,NULL,30,'8'),('21',2,NULL,34,'9'),('22',2,NULL,35,'10'),('23',3,NULL,23,'0'),('24',3,NULL,25,'1'),('25',3,NULL,27,'2'),('26',3,NULL,28,'3'),('27',3,NULL,29,'4'),('28',3,NULL,31,'5'),('29',3,NULL,33,'6'),('30',3,NULL,30,'7'),('31',3,NULL,34,'8'),('32',3,NULL,35,'9'),('33',4,NULL,23,'0'),('34',4,NULL,25,'1'),('35',4,NULL,27,'2'),('36',4,NULL,28,'3'),('37',4,NULL,29,'4'),('38',4,NULL,31,'5'),('39',4,NULL,33,'6'),('40',4,NULL,30,'7'),('41',4,NULL,34,'8'),('42',4,NULL,35,'9');
/*!40000 ALTER TABLE `eav_form_element` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_form_fieldset`
--

DROP TABLE IF EXISTS `eav_form_fieldset`;
CREATE TABLE `eav_form_fieldset` (
  `fieldset_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Fieldset Id',
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `code` varchar(64) NOT NULL COMMENT 'Code',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`fieldset_id`),
  UNIQUE KEY `EAV_FORM_FIELDSET_TYPE_ID_CODE` (`type_id`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Fieldset';

--
-- Table structure for table `eav_form_fieldset_label`
--

DROP TABLE IF EXISTS `eav_form_fieldset_label`;
CREATE TABLE `eav_form_fieldset_label` (
  `fieldset_id` smallint(5) unsigned NOT NULL COMMENT 'Fieldset Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`fieldset_id`,`store_id`),
  KEY `EAV_FORM_FIELDSET_LABEL_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Fieldset Label';

--
-- Table structure for table `eav_form_type`
--

DROP TABLE IF EXISTS `eav_form_type`;
CREATE TABLE `eav_form_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Type Id',
  `code` varchar(64) NOT NULL COMMENT 'Code',
  `label` varchar(255) NOT NULL COMMENT 'Label',
  `is_system` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is System',
  `theme` varchar(64) DEFAULT NULL COMMENT 'Theme',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `EAV_FORM_TYPE_CODE_THEME_STORE_ID` (`code`,`theme`,`store_id`),
  KEY `EAV_FORM_TYPE_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Eav Form Type';

--
-- Dumping data for table `eav_form_type`
--

LOCK TABLES `eav_form_type` WRITE;
/*!40000 ALTER TABLE `eav_form_type` DISABLE KEYS */;
INSERT INTO `eav_form_type` VALUES (1,'checkout_onepage_register','checkout_onepage_register',1,'',0),(2,'checkout_onepage_register_guest','checkout_onepage_register_guest',1,'',0),(3,'checkout_onepage_billing_address','checkout_onepage_billing_address',1,'',0),(4,'checkout_onepage_shipping_address','checkout_onepage_shipping_address',1,'',0);
/*!40000 ALTER TABLE `eav_form_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_form_type_entity`
--

DROP TABLE IF EXISTS `eav_form_type_entity`;
CREATE TABLE `eav_form_type_entity` (
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `entity_type_id` smallint(5) unsigned NOT NULL COMMENT 'Entity Type Id',
  PRIMARY KEY (`type_id`,`entity_type_id`),
  KEY `EAV_FORM_TYPE_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Type Entity';

--
-- Dumping data for table `eav_form_type_entity`
--

LOCK TABLES `eav_form_type_entity` WRITE;
/*!40000 ALTER TABLE `eav_form_type_entity` DISABLE KEYS */;
INSERT INTO `eav_form_type_entity` VALUES (1,1),(1,2),(2,1),(2,2),(3,2),(4,2);
/*!40000 ALTER TABLE `eav_form_type_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_template`
--

DROP TABLE IF EXISTS `email_template`;
CREATE TABLE `email_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template ID',
  `template_code` varchar(150) NOT NULL COMMENT 'Template Name',
  `template_text` text NOT NULL COMMENT 'Template Content',
  `template_styles` text COMMENT 'Templste Styles',
  `template_type` int(10) unsigned DEFAULT NULL COMMENT 'Template Type',
  `template_subject` varchar(200) NOT NULL COMMENT 'Template Subject',
  `template_sender_name` varchar(200) DEFAULT NULL COMMENT 'Template Sender Name',
  `template_sender_email` varchar(200) DEFAULT NULL COMMENT 'Template Sender Email',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of Template Creation',
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Template Modification',
  `orig_template_code` varchar(200) DEFAULT NULL COMMENT 'Original Template Code',
  `orig_template_variables` text COMMENT 'Original Template Variables',
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `EMAIL_TEMPLATE_TEMPLATE_CODE` (`template_code`),
  KEY `EMAIL_TEMPLATE_ADDED_AT` (`added_at`),
  KEY `EMAIL_TEMPLATE_MODIFIED_AT` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Email Templates';

--
-- Table structure for table `flag`
--

DROP TABLE IF EXISTS `flag`;
CREATE TABLE `flag` (
  `flag_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Flag Id',
  `flag_code` varchar(255) NOT NULL COMMENT 'Flag Code',
  `state` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag State',
  `flag_data` text COMMENT 'Flag Data',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Last Flag Update',
  PRIMARY KEY (`flag_id`),
  KEY `FLAG_LAST_UPDATE` (`last_update`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Flag';

--
-- Table structure for table `gift_message`
--

DROP TABLE IF EXISTS `gift_message`;
CREATE TABLE `gift_message` (
  `gift_message_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'GiftMessage Id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `sender` varchar(255) DEFAULT NULL COMMENT 'Sender',
  `recipient` varchar(255) DEFAULT NULL COMMENT 'Registrant',
  `message` text COMMENT 'Message',
  PRIMARY KEY (`gift_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Gift Message';

--
-- Table structure for table `googleoptimizer_code`
--

DROP TABLE IF EXISTS `googleoptimizer_code`;
CREATE TABLE `googleoptimizer_code` (
  `code_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Google experiment code id',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Optimized entity id product id or catalog id',
  `entity_type` varchar(50) DEFAULT NULL COMMENT 'Optimized entity type',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store id',
  `experiment_script` text COMMENT 'Google experiment script',
  PRIMARY KEY (`code_id`),
  UNIQUE KEY `GOOGLEOPTIMIZER_CODE_STORE_ID_ENTITY_ID_ENTITY_TYPE` (`store_id`,`entity_id`,`entity_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Google Experiment code';

--
-- Table structure for table `hozmegamenu`
--

DROP TABLE IF EXISTS `hozmegamenu`;
CREATE TABLE `hozmegamenu` (
  `hozmegamenu_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'hozmegamenu ID',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'hozmegamenu status',
  `type_menu` smallint(6) NOT NULL DEFAULT '1' COMMENT 'hozmegamenu type_menu',
  `is_home` smallint(6) NOT NULL DEFAULT '1' COMMENT 'hozmegamenu is_home',
  `is_mobile` smallint(6) NOT NULL DEFAULT '1' COMMENT 'hozmegamenu is_home',
  `is_new` varchar(255) DEFAULT '' COMMENT 'hozmegamenu is_new',
  `is_sale` varchar(255) DEFAULT '' COMMENT 'hozmegamenu is_sale',
  `is_level` varchar(255) DEFAULT '' COMMENT 'hozmegamenu Level',
  `is_column` varchar(255) DEFAULT '' COMMENT 'hozmegamenu Column',
  `items` varchar(255) DEFAULT '' COMMENT 'hozmegamenu items',
  `is_link` varchar(255) DEFAULT '' COMMENT 'hozmegamenu is_link',
  `effect` varchar(255) DEFAULT '' COMMENT 'hozmegamenu Effect',
  `image` varchar(255) DEFAULT NULL COMMENT 'hozmegamenu image',
  `store` smallint(6) NOT NULL DEFAULT '0' COMMENT 'store',
  PRIMARY KEY (`hozmegamenu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='hozmegamenu';

--
-- Dumping data for table `hozmegamenu`
--

LOCK TABLES `hozmegamenu` WRITE;
/*!40000 ALTER TABLE `hozmegamenu` DISABLE KEYS */;
INSERT INTO `hozmegamenu` VALUES ('1',1,1,1,1,NULL,NULL,'4','4','[\"category_3\",\"category_4\",\"category_5\",\"category_6\",\"category_7\",\"category_8\"]','[\"9\"]','0',NULL,0);
/*!40000 ALTER TABLE `hozmegamenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_history`
--

DROP TABLE IF EXISTS `import_history`;
CREATE TABLE `import_history` (
  `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'History record Id',
  `started_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Started at',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID',
  `imported_file` varchar(255) DEFAULT NULL COMMENT 'Imported file',
  `execution_time` varchar(255) DEFAULT NULL COMMENT 'Execution time',
  `summary` varchar(255) DEFAULT NULL COMMENT 'Summary',
  `error_file` varchar(255) NOT NULL COMMENT 'Imported file with errors',
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Import history table';

--
-- Table structure for table `importexport_importdata`
--

DROP TABLE IF EXISTS `importexport_importdata`;
CREATE TABLE `importexport_importdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `entity` varchar(50) NOT NULL COMMENT 'Entity',
  `behavior` varchar(10) NOT NULL DEFAULT 'append' COMMENT 'Behavior',
  `data` longtext COMMENT 'Data',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Import Data Table';

--
-- Table structure for table `indexer_state`
--

DROP TABLE IF EXISTS `indexer_state`;
CREATE TABLE `indexer_state` (
  `state_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Indexer State Id',
  `indexer_id` varchar(255) DEFAULT NULL COMMENT 'Indexer Id',
  `status` varchar(16) DEFAULT 'invalid' COMMENT 'Indexer Status',
  `updated` datetime DEFAULT NULL COMMENT 'Indexer Status',
  `hash_config` varchar(32) NOT NULL COMMENT 'Hash of indexer config',
  PRIMARY KEY (`state_id`),
  KEY `INDEXER_STATE_INDEXER_ID` (`indexer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Indexer State';

--
-- Dumping data for table `indexer_state`
--

LOCK TABLES `indexer_state` WRITE;
/*!40000 ALTER TABLE `indexer_state` DISABLE KEYS */;
INSERT INTO `indexer_state` VALUES ('1','design_config_grid','invalid','2016-11-05 03:05:40','27baa8fe6a5369f52c8b7cbd54a3c3c4'),('2','customer_grid','valid','2016-10-17 03:38:55','a1bbcab4c6368d654719ccf6cf0e55a8'),('3','catalog_category_product','valid','2016-10-17 03:38:59','57b48d3cf1fcd64abe6b01dea3173d02'),('4','catalog_product_category','valid','2016-10-06 09:51:01','9957f66909342cc58ff2703dcd268bf4'),('5','catalog_product_price','valid','2016-10-17 03:39:05','15a819a577a149220cd0722c291de721'),('6','catalog_product_attribute','valid','2016-10-17 03:39:07','77eed0bf72b16099d299d0ab47b74910'),('7','cataloginventory_stock','valid','2016-10-17 03:39:09','78a405fd852458c326c85096099d7d5e'),('8','catalogrule_rule','valid','2016-10-17 03:39:11','5afe3cacdcb52ec3a7e68dc245679021'),('9','catalogrule_product','valid','2016-10-06 09:51:09','0ebee9e52ed424273132e8227fe646f3'),('10','catalogsearch_fulltext','invalid','2016-11-05 03:05:40','4486b57e2021aa78b526c68c9af2dcab');
/*!40000 ALTER TABLE `indexer_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integration`
--

DROP TABLE IF EXISTS `integration`;
CREATE TABLE `integration` (
  `integration_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Integration ID',
  `name` varchar(255) NOT NULL COMMENT 'Integration name is displayed in the admin interface',
  `email` varchar(255) NOT NULL COMMENT 'Email address of the contact person',
  `endpoint` varchar(255) DEFAULT NULL COMMENT 'Endpoint for posting consumer credentials',
  `status` smallint(5) unsigned NOT NULL COMMENT 'Integration status',
  `consumer_id` int(10) unsigned DEFAULT NULL COMMENT 'Oauth consumer',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `setup_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Integration type - manual or config file',
  `identity_link_url` varchar(255) DEFAULT NULL COMMENT 'Identity linking Url',
  PRIMARY KEY (`integration_id`),
  UNIQUE KEY `INTEGRATION_NAME` (`name`),
  UNIQUE KEY `INTEGRATION_CONSUMER_ID` (`consumer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='integration';

--
-- Table structure for table `layout_link`
--

DROP TABLE IF EXISTS `layout_link`;
CREATE TABLE `layout_link` (
  `layout_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme id',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Layout Update Id',
  `is_temporary` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Defines whether Layout Update is Temporary',
  PRIMARY KEY (`layout_link_id`),
  KEY `LAYOUT_LINK_LAYOUT_UPDATE_ID` (`layout_update_id`),
  KEY `LAYOUT_LINK_STORE_ID_THEME_ID_LAYOUT_UPDATE_ID_IS_TEMPORARY` (`store_id`,`theme_id`,`layout_update_id`,`is_temporary`),
  KEY `LAYOUT_LINK_THEME_ID_THEME_THEME_ID` (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout Link';

--
-- Table structure for table `layout_update`
--

DROP TABLE IF EXISTS `layout_update`;
CREATE TABLE `layout_update` (
  `layout_update_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Layout Update Id',
  `handle` varchar(255) DEFAULT NULL COMMENT 'Handle',
  `xml` text COMMENT 'Xml',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Last Update Timestamp',
  PRIMARY KEY (`layout_update_id`),
  KEY `LAYOUT_UPDATE_HANDLE` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout Updates';

--
-- Table structure for table `mview_state`
--

DROP TABLE IF EXISTS `mview_state`;
CREATE TABLE `mview_state` (
  `state_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'View State Id',
  `view_id` varchar(255) DEFAULT NULL COMMENT 'View Id',
  `mode` varchar(16) DEFAULT 'disabled' COMMENT 'View Mode',
  `status` varchar(16) DEFAULT 'idle' COMMENT 'View Status',
  `updated` datetime DEFAULT NULL COMMENT 'View updated time',
  `version_id` int(10) unsigned DEFAULT NULL COMMENT 'View Version Id',
  PRIMARY KEY (`state_id`),
  KEY `MVIEW_STATE_VIEW_ID` (`view_id`),
  KEY `MVIEW_STATE_MODE` (`mode`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='View State';

--
-- Dumping data for table `mview_state`
--

LOCK TABLES `mview_state` WRITE;
/*!40000 ALTER TABLE `mview_state` DISABLE KEYS */;
INSERT INTO `mview_state` VALUES ('1','design_config_dummy','enabled','idle','2016-11-05 03:09:30','0'),('2','customer_dummy','enabled','idle','2016-11-05 03:09:30','0'),('3','catalog_category_product','enabled','idle','2016-11-05 03:09:32','392'),('4','catalog_product_category','enabled','idle','2016-11-05 03:09:36',NULL),('5','catalog_product_price','enabled','idle','2016-11-05 03:09:39','275'),('6','catalog_product_attribute','enabled','idle','2016-11-05 03:09:41','446'),('7','cataloginventory_stock','enabled','idle','2016-11-05 03:09:41','45'),('8','catalogrule_rule','enabled','idle','2016-11-05 03:09:42','0'),('9','catalogrule_product','enabled','idle','2016-11-05 03:09:50',NULL),('10','catalogsearch_fulltext','enabled','idle','2016-11-05 03:10:02','681');
/*!40000 ALTER TABLE `mview_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_problem`
--

DROP TABLE IF EXISTS `newsletter_problem`;
CREATE TABLE `newsletter_problem` (
  `problem_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Problem Id',
  `subscriber_id` int(10) unsigned DEFAULT NULL COMMENT 'Subscriber Id',
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `problem_error_code` int(10) unsigned DEFAULT '0' COMMENT 'Problem Error Code',
  `problem_error_text` varchar(200) DEFAULT NULL COMMENT 'Problem Error Text',
  PRIMARY KEY (`problem_id`),
  KEY `NEWSLETTER_PROBLEM_SUBSCRIBER_ID` (`subscriber_id`),
  KEY `NEWSLETTER_PROBLEM_QUEUE_ID` (`queue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Problems';

--
-- Table structure for table `newsletter_queue`
--

DROP TABLE IF EXISTS `newsletter_queue`;
CREATE TABLE `newsletter_queue` (
  `queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Queue Id',
  `template_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Template ID',
  `newsletter_type` int(11) DEFAULT NULL COMMENT 'Newsletter Type',
  `newsletter_text` text COMMENT 'Newsletter Text',
  `newsletter_styles` text COMMENT 'Newsletter Styles',
  `newsletter_subject` varchar(200) DEFAULT NULL COMMENT 'Newsletter Subject',
  `newsletter_sender_name` varchar(200) DEFAULT NULL COMMENT 'Newsletter Sender Name',
  `newsletter_sender_email` varchar(200) DEFAULT NULL COMMENT 'Newsletter Sender Email',
  `queue_status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Status',
  `queue_start_at` timestamp NULL DEFAULT NULL COMMENT 'Queue Start At',
  `queue_finish_at` timestamp NULL DEFAULT NULL COMMENT 'Queue Finish At',
  PRIMARY KEY (`queue_id`),
  KEY `NEWSLETTER_QUEUE_TEMPLATE_ID` (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue';

--
-- Table structure for table `newsletter_queue_link`
--

DROP TABLE IF EXISTS `newsletter_queue_link`;
CREATE TABLE `newsletter_queue_link` (
  `queue_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Queue Link Id',
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `subscriber_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subscriber Id',
  `letter_sent_at` timestamp NULL DEFAULT NULL COMMENT 'Letter Sent At',
  PRIMARY KEY (`queue_link_id`),
  KEY `NEWSLETTER_QUEUE_LINK_SUBSCRIBER_ID` (`subscriber_id`),
  KEY `NEWSLETTER_QUEUE_LINK_QUEUE_ID_LETTER_SENT_AT` (`queue_id`,`letter_sent_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue Link';

--
-- Table structure for table `newsletter_queue_store_link`
--

DROP TABLE IF EXISTS `newsletter_queue_store_link`;
CREATE TABLE `newsletter_queue_store_link` (
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  PRIMARY KEY (`queue_id`,`store_id`),
  KEY `NEWSLETTER_QUEUE_STORE_LINK_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue Store Link';

--
-- Table structure for table `newsletter_subscriber`
--

DROP TABLE IF EXISTS `newsletter_subscriber`;
CREATE TABLE `newsletter_subscriber` (
  `subscriber_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Subscriber Id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store Id',
  `change_status_at` timestamp NULL DEFAULT NULL COMMENT 'Change Status At',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `subscriber_email` varchar(150) DEFAULT NULL COMMENT 'Subscriber Email',
  `subscriber_status` int(11) NOT NULL DEFAULT '0' COMMENT 'Subscriber Status',
  `subscriber_confirm_code` varchar(32) DEFAULT 'NULL' COMMENT 'Subscriber Confirm Code',
  PRIMARY KEY (`subscriber_id`),
  KEY `NEWSLETTER_SUBSCRIBER_CUSTOMER_ID` (`customer_id`),
  KEY `NEWSLETTER_SUBSCRIBER_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Subscriber';

--
-- Table structure for table `newsletter_template`
--

DROP TABLE IF EXISTS `newsletter_template`;
CREATE TABLE `newsletter_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template ID',
  `template_code` varchar(150) DEFAULT NULL COMMENT 'Template Code',
  `template_text` text COMMENT 'Template Text',
  `template_styles` text COMMENT 'Template Styles',
  `template_type` int(10) unsigned DEFAULT NULL COMMENT 'Template Type',
  `template_subject` varchar(200) DEFAULT NULL COMMENT 'Template Subject',
  `template_sender_name` varchar(200) DEFAULT NULL COMMENT 'Template Sender Name',
  `template_sender_email` varchar(200) DEFAULT NULL COMMENT 'Template Sender Email',
  `template_actual` smallint(5) unsigned DEFAULT '1' COMMENT 'Template Actual',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Added At',
  `modified_at` timestamp NULL DEFAULT NULL COMMENT 'Modified At',
  PRIMARY KEY (`template_id`),
  KEY `NEWSLETTER_TEMPLATE_TEMPLATE_ACTUAL` (`template_actual`),
  KEY `NEWSLETTER_TEMPLATE_ADDED_AT` (`added_at`),
  KEY `NEWSLETTER_TEMPLATE_MODIFIED_AT` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Template';

--
-- Table structure for table `oauth_consumer`
--

DROP TABLE IF EXISTS `oauth_consumer`;
CREATE TABLE `oauth_consumer` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `name` varchar(255) NOT NULL COMMENT 'Name of consumer',
  `key` varchar(32) NOT NULL COMMENT 'Key code',
  `secret` varchar(32) NOT NULL COMMENT 'Secret code',
  `callback_url` text COMMENT 'Callback URL',
  `rejected_callback_url` text NOT NULL COMMENT 'Rejected callback URL',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `OAUTH_CONSUMER_KEY` (`key`),
  UNIQUE KEY `OAUTH_CONSUMER_SECRET` (`secret`),
  KEY `OAUTH_CONSUMER_CREATED_AT` (`created_at`),
  KEY `OAUTH_CONSUMER_UPDATED_AT` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Consumers';

--
-- Table structure for table `oauth_nonce`
--

DROP TABLE IF EXISTS `oauth_nonce`;
CREATE TABLE `oauth_nonce` (
  `nonce` varchar(32) NOT NULL COMMENT 'Nonce String',
  `timestamp` int(10) unsigned NOT NULL COMMENT 'Nonce Timestamp',
  `consumer_id` int(10) unsigned NOT NULL COMMENT 'Consumer ID',
  UNIQUE KEY `OAUTH_NONCE_NONCE_CONSUMER_ID` (`nonce`,`consumer_id`),
  KEY `OAUTH_NONCE_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` (`consumer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Nonce';

--
-- Table structure for table `oauth_token`
--

DROP TABLE IF EXISTS `oauth_token`;
CREATE TABLE `oauth_token` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `consumer_id` int(10) unsigned DEFAULT NULL COMMENT 'Oauth Consumer ID',
  `admin_id` int(10) unsigned DEFAULT NULL COMMENT 'Admin user ID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer user ID',
  `type` varchar(16) NOT NULL COMMENT 'Token Type',
  `token` varchar(32) NOT NULL COMMENT 'Token',
  `secret` varchar(32) NOT NULL COMMENT 'Token Secret',
  `verifier` varchar(32) DEFAULT NULL COMMENT 'Token Verifier',
  `callback_url` text NOT NULL COMMENT 'Token Callback URL',
  `revoked` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Token revoked',
  `authorized` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Token authorized',
  `user_type` int(11) DEFAULT NULL COMMENT 'User type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Token creation timestamp',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `OAUTH_TOKEN_TOKEN` (`token`),
  KEY `OAUTH_TOKEN_CONSUMER_ID` (`consumer_id`),
  KEY `OAUTH_TOKEN_ADMIN_ID_ADMIN_USER_USER_ID` (`admin_id`),
  KEY `OAUTH_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Tokens';

--
-- Table structure for table `oauth_token_request_log`
--

DROP TABLE IF EXISTS `oauth_token_request_log`;
CREATE TABLE `oauth_token_request_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log Id',
  `user_name` varchar(255) NOT NULL COMMENT 'Customer email or admin login',
  `user_type` smallint(5) unsigned NOT NULL COMMENT 'User type (admin or customer)',
  `failures_count` smallint(5) unsigned DEFAULT '0' COMMENT 'Number of failed authentication attempts in a row',
  `lock_expires_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Lock expiration time',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `OAUTH_TOKEN_REQUEST_LOG_USER_NAME_USER_TYPE` (`user_name`,`user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log of token request authentication failures.';

--
-- Table structure for table `password_reset_request_event`
--

DROP TABLE IF EXISTS `password_reset_request_event`;
CREATE TABLE `password_reset_request_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `request_type` smallint(5) unsigned NOT NULL COMMENT 'Type of the event under a security control',
  `account_reference` varchar(255) DEFAULT NULL COMMENT 'An identifier for existing account or another target',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp when the event occurs',
  `ip` varchar(15) NOT NULL COMMENT 'Remote user IP',
  PRIMARY KEY (`id`),
  KEY `PASSWORD_RESET_REQUEST_EVENT_ACCOUNT_REFERENCE` (`account_reference`),
  KEY `PASSWORD_RESET_REQUEST_EVENT_CREATED_AT` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Password Reset Request Event under a security control';

--
-- Table structure for table `paypal_billing_agreement`
--

DROP TABLE IF EXISTS `paypal_billing_agreement`;
CREATE TABLE `paypal_billing_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Agreement Id',
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Id',
  `method_code` varchar(32) NOT NULL COMMENT 'Method Code',
  `reference_id` varchar(32) NOT NULL COMMENT 'Reference Id',
  `status` varchar(20) NOT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `agreement_label` varchar(255) DEFAULT NULL COMMENT 'Agreement Label',
  PRIMARY KEY (`agreement_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_CUSTOMER_ID` (`customer_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Billing Agreement';

--
-- Table structure for table `paypal_billing_agreement_order`
--

DROP TABLE IF EXISTS `paypal_billing_agreement_order`;
CREATE TABLE `paypal_billing_agreement_order` (
  `agreement_id` int(10) unsigned NOT NULL COMMENT 'Agreement Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  PRIMARY KEY (`agreement_id`,`order_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_ORDER_ORDER_ID` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Billing Agreement Order';

--
-- Table structure for table `paypal_cert`
--

DROP TABLE IF EXISTS `paypal_cert`;
CREATE TABLE `paypal_cert` (
  `cert_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Cert Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `content` text COMMENT 'Content',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`cert_id`),
  KEY `PAYPAL_CERT_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Certificate Table';

--
-- Table structure for table `paypal_payment_transaction`
--

DROP TABLE IF EXISTS `paypal_payment_transaction`;
CREATE TABLE `paypal_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `PAYPAL_PAYMENT_TRANSACTION_TXN_ID` (`txn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='PayPal Payflow Link Payment Transaction';

--
-- Table structure for table `paypal_settlement_report`
--

DROP TABLE IF EXISTS `paypal_settlement_report`;
CREATE TABLE `paypal_settlement_report` (
  `report_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Report Id',
  `report_date` timestamp NULL DEFAULT NULL COMMENT 'Report Date',
  `account_id` varchar(64) DEFAULT NULL COMMENT 'Account Id',
  `filename` varchar(24) DEFAULT NULL COMMENT 'Filename',
  `last_modified` timestamp NULL DEFAULT NULL COMMENT 'Last Modified',
  PRIMARY KEY (`report_id`),
  UNIQUE KEY `PAYPAL_SETTLEMENT_REPORT_REPORT_DATE_ACCOUNT_ID` (`report_date`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Settlement Report Table';

--
-- Table structure for table `paypal_settlement_report_row`
--

DROP TABLE IF EXISTS `paypal_settlement_report_row`;
CREATE TABLE `paypal_settlement_report_row` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Row Id',
  `report_id` int(10) unsigned NOT NULL COMMENT 'Report Id',
  `transaction_id` varchar(19) DEFAULT NULL COMMENT 'Transaction Id',
  `invoice_id` varchar(127) DEFAULT NULL COMMENT 'Invoice Id',
  `paypal_reference_id` varchar(19) DEFAULT NULL COMMENT 'Paypal Reference Id',
  `paypal_reference_id_type` varchar(3) DEFAULT NULL COMMENT 'Paypal Reference Id Type',
  `transaction_event_code` varchar(5) DEFAULT NULL COMMENT 'Transaction Event Code',
  `transaction_initiation_date` timestamp NULL DEFAULT NULL COMMENT 'Transaction Initiation Date',
  `transaction_completion_date` timestamp NULL DEFAULT NULL COMMENT 'Transaction Completion Date',
  `transaction_debit_or_credit` varchar(2) NOT NULL DEFAULT 'CR' COMMENT 'Transaction Debit Or Credit',
  `gross_transaction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000' COMMENT 'Gross Transaction Amount',
  `gross_transaction_currency` varchar(3) DEFAULT NULL COMMENT 'Gross Transaction Currency',
  `fee_debit_or_credit` varchar(2) DEFAULT NULL COMMENT 'Fee Debit Or Credit',
  `fee_amount` decimal(20,6) NOT NULL DEFAULT '0.000000' COMMENT 'Fee Amount',
  `fee_currency` varchar(3) DEFAULT NULL COMMENT 'Fee Currency',
  `custom_field` varchar(255) DEFAULT NULL COMMENT 'Custom Field',
  `consumer_id` varchar(127) DEFAULT NULL COMMENT 'Consumer Id',
  `payment_tracking_id` varchar(255) DEFAULT NULL COMMENT 'Payment Tracking ID',
  `store_id` varchar(50) DEFAULT NULL COMMENT 'Store ID',
  PRIMARY KEY (`row_id`),
  KEY `PAYPAL_SETTLEMENT_REPORT_ROW_REPORT_ID` (`report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Settlement Report Row Table';

--
-- Table structure for table `persistent_session`
--

DROP TABLE IF EXISTS `persistent_session`;
CREATE TABLE `persistent_session` (
  `persistent_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Session id',
  `key` varchar(50) NOT NULL COMMENT 'Unique cookie key',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  `info` text COMMENT 'Session Data',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`persistent_id`),
  UNIQUE KEY `PERSISTENT_SESSION_KEY` (`key`),
  UNIQUE KEY `PERSISTENT_SESSION_CUSTOMER_ID` (`customer_id`),
  KEY `PERSISTENT_SESSION_UPDATED_AT` (`updated_at`),
  KEY `PERSISTENT_SESSION_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Persistent Session';

--
-- Table structure for table `plazathemes_blog_category`
--

DROP TABLE IF EXISTS `plazathemes_blog_category`;
CREATE TABLE `plazathemes_blog_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Category ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Category Title',
  `meta_keywords` text COMMENT 'Category Meta Keywords',
  `meta_description` text COMMENT 'Category Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Category String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Category Content Heading',
  `content` mediumtext COMMENT 'Category Content',
  `path` varchar(255) DEFAULT NULL COMMENT 'Category Path',
  `position` smallint(6) NOT NULL COMMENT 'Category Position',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Category Active',
  PRIMARY KEY (`category_id`),
  KEY `PLAZATHEMES_BLOG_CATEGORY_IDENTIFIER` (`identifier`),
  FULLTEXT KEY `FTI_0E5A42C75595B5A4A5CC4FE14800BBC2` (`title`,`meta_keywords`,`meta_description`,`identifier`,`content`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Category Table';

--
-- Table structure for table `plazathemes_blog_category_store`
--

DROP TABLE IF EXISTS `plazathemes_blog_category_store`;
CREATE TABLE `plazathemes_blog_category_store` (
  `category_id` int(11) NOT NULL COMMENT 'Category ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`category_id`,`store_id`),
  KEY `PLAZATHEMES_BLOG_CATEGORY_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Category To Store Linkage Table';

--
-- Table structure for table `plazathemes_blog_post`
--

DROP TABLE IF EXISTS `plazathemes_blog_post`;
CREATE TABLE `plazathemes_blog_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Post ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Post Title',
  `thumbnailimage` varchar(255) DEFAULT NULL COMMENT 'Thumbnail Image',
  `meta_keywords` text COMMENT 'Post Meta Keywords',
  `meta_description` text COMMENT 'Post Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Post String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Post Content Heading',
  `short_content` text COMMENT 'Short Content',
  `content` mediumtext COMMENT 'Post Content',
  `creation_time` timestamp NULL DEFAULT NULL COMMENT 'Post Creation Time',
  `update_time` timestamp NULL DEFAULT NULL COMMENT 'Post Modification Time',
  `publish_time` timestamp NULL DEFAULT NULL COMMENT 'Post Publish Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Post Active',
  PRIMARY KEY (`post_id`),
  KEY `PLAZATHEMES_BLOG_POST_IDENTIFIER` (`identifier`),
  FULLTEXT KEY `FTI_3A962F890DAF9E9D7B6277086623FD2B` (`title`,`meta_keywords`,`meta_description`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Post Table';

--
-- Dumping data for table `plazathemes_blog_post`
--

LOCK TABLES `plazathemes_blog_post` WRITE;
/*!40000 ALTER TABLE `plazathemes_blog_post` DISABLE KEYS */;
INSERT INTO `plazathemes_blog_post` VALUES ('1','Hello world!',NULL,'magento 2 blog','Magento 2 blog default post.','hello-world','Hello world!',NULL,'Welcome to <a target=\"_blank\" href=\"http://www.plazathemes.com/\" title=\"plazathemes - solutions for Magento 2\">plazathemes</a> blog extension for Magento&reg; 2. This is your first post. Edit or delete it, then start blogging!','2016-10-06 02:25:57','2016-10-06 02:25:57','2016-10-06 02:25:57',1);
/*!40000 ALTER TABLE `plazathemes_blog_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plazathemes_blog_post_category`
--

DROP TABLE IF EXISTS `plazathemes_blog_post_category`;
CREATE TABLE `plazathemes_blog_post_category` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `category_id` int(11) NOT NULL COMMENT 'Category ID',
  PRIMARY KEY (`post_id`,`category_id`),
  KEY `PLAZATHEMES_BLOG_POST_CATEGORY_CATEGORY_ID` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Post To Category Linkage Table';

--
-- Table structure for table `plazathemes_blog_post_relatedpost`
--

DROP TABLE IF EXISTS `plazathemes_blog_post_relatedpost`;
CREATE TABLE `plazathemes_blog_post_relatedpost` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `related_id` int(11) NOT NULL COMMENT 'Related Post ID',
  PRIMARY KEY (`post_id`,`related_id`),
  KEY `PLAZATHEMES_BLOG_POST_RELATEDPRODUCT_RELATED_ID` (`related_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Post To Post Linkage Table';

--
-- Table structure for table `plazathemes_blog_post_relatedproduct`
--

DROP TABLE IF EXISTS `plazathemes_blog_post_relatedproduct`;
CREATE TABLE `plazathemes_blog_post_relatedproduct` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `related_id` int(10) unsigned NOT NULL COMMENT 'Related Product ID',
  PRIMARY KEY (`post_id`,`related_id`),
  KEY `PLAZATHEMES_BLOG_POST_RELATEDPRODUCT_RELATED_ID` (`related_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Post To Product Linkage Table';

--
-- Table structure for table `plazathemes_blog_post_store`
--

DROP TABLE IF EXISTS `plazathemes_blog_post_store`;
CREATE TABLE `plazathemes_blog_post_store` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`post_id`,`store_id`),
  KEY `PLAZATHEMES_BLOG_POST_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Post To Store Linkage Table';

--
-- Dumping data for table `plazathemes_blog_post_store`
--

LOCK TABLES `plazathemes_blog_post_store` WRITE;
/*!40000 ALTER TABLE `plazathemes_blog_post_store` DISABLE KEYS */;
INSERT INTO `plazathemes_blog_post_store` VALUES ('1',0);
/*!40000 ALTER TABLE `plazathemes_blog_post_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_alert_price`
--

DROP TABLE IF EXISTS `product_alert_price`;
CREATE TABLE `product_alert_price` (
  `alert_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product alert price id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price amount',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website id',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Product alert add date',
  `last_send_date` timestamp NULL DEFAULT NULL COMMENT 'Product alert last send date',
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert send count',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert status',
  PRIMARY KEY (`alert_price_id`),
  KEY `PRODUCT_ALERT_PRICE_CUSTOMER_ID` (`customer_id`),
  KEY `PRODUCT_ALERT_PRICE_PRODUCT_ID` (`product_id`),
  KEY `PRODUCT_ALERT_PRICE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Alert Price';

--
-- Table structure for table `product_alert_stock`
--

DROP TABLE IF EXISTS `product_alert_stock`;
CREATE TABLE `product_alert_stock` (
  `alert_stock_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product alert stock id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website id',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Product alert add date',
  `send_date` timestamp NULL DEFAULT NULL COMMENT 'Product alert send date',
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Send Count',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert status',
  PRIMARY KEY (`alert_stock_id`),
  KEY `PRODUCT_ALERT_STOCK_CUSTOMER_ID` (`customer_id`),
  KEY `PRODUCT_ALERT_STOCK_PRODUCT_ID` (`product_id`),
  KEY `PRODUCT_ALERT_STOCK_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Alert Stock';

--
-- Table structure for table `pt_bannerslider`
--

DROP TABLE IF EXISTS `pt_bannerslider`;
CREATE TABLE `pt_bannerslider` (
  `banner_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Banner ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Banner name',
  `store_id` varchar(255) NOT NULL DEFAULT '' COMMENT 'Store Id',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Banner status',
  `click_url` varchar(255) DEFAULT '' COMMENT 'Banner click url',
  `image` varchar(255) DEFAULT NULL COMMENT 'Banner image',
  `image_alt` varchar(255) DEFAULT NULL COMMENT 'Banner image alt',
  `title1` varchar(255) DEFAULT NULL COMMENT 'Title 1',
  `title2` varchar(255) DEFAULT NULL COMMENT 'Title 2',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Order',
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='pt_bannerslider';

--
-- Dumping data for table `pt_bannerslider`
--

LOCK TABLES `pt_bannerslider` WRITE;
/*!40000 ALTER TABLE `pt_bannerslider` DISABLE KEYS */;
INSERT INTO `pt_bannerslider` VALUES ('1','New fashion 2016','0',1,'#','Plazathemes/bannerslider/images/i/m/img_banner01_1.png',NULL,'Donec vitae est placerat','Donec vitae est placerat',NULL,0),('2','New fashion 2016','0',1,'#','Plazathemes/bannerslider/images/i/m/img_banner02_1.png',NULL,'Donec vitae est placerat','Donec vitae est placerat',NULL,0),('3','New fashion 2016','0',1,'#','Plazathemes/bannerslider/images/i/m/img_banner7.png',NULL,'Donec vitae est placerat','Donec vitae est placerat',NULL,0);
/*!40000 ALTER TABLE `pt_bannerslider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pt_brandslider`
--

DROP TABLE IF EXISTS `pt_brandslider`;
CREATE TABLE `pt_brandslider` (
  `brand_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Brand ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'Title',
  `store_id` varchar(255) NOT NULL DEFAULT '' COMMENT 'Store Id',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Brand status',
  `link` varchar(255) DEFAULT '' COMMENT 'Link',
  `image` varchar(255) DEFAULT NULL COMMENT 'Brand image',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Order',
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='pt_brandslider';

--
-- Dumping data for table `pt_brandslider`
--

LOCK TABLES `pt_brandslider` WRITE;
/*!40000 ALTER TABLE `pt_brandslider` DISABLE KEYS */;
INSERT INTO `pt_brandslider` VALUES ('1','brand1','0',1,'#','Plazathemes/brandslider/images/1/_/1.jpg',NULL,0),('2','brand2','0',1,'#','Plazathemes/brandslider/images/2/_/2.jpg',NULL,0),('3','brand3','0',1,'#','Plazathemes/brandslider/images/3/_/3.jpg',NULL,0),('4','brand4','0',1,'#','Plazathemes/brandslider/images/4/_/4.jpg',NULL,0),('5','brand5','0',1,'#','Plazathemes/brandslider/images/5/_/5.jpg',NULL,0),('6','brand6','0',1,'#','Plazathemes/brandslider/images/6/_/6_1_.jpg',NULL,0),('7','brand7','0',1,'#','Plazathemes/brandslider/images/7/_/7.jpg',NULL,0);
/*!40000 ALTER TABLE `pt_brandslider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote`
--

DROP TABLE IF EXISTS `quote`;
CREATE TABLE `quote` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `converted_at` timestamp NULL DEFAULT NULL COMMENT 'Converted At',
  `is_active` smallint(5) unsigned DEFAULT '1' COMMENT 'Is Active',
  `is_virtual` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Virtual',
  `is_multi_shipping` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Multi Shipping',
  `items_count` int(10) unsigned DEFAULT '0' COMMENT 'Items Count',
  `items_qty` decimal(12,4) DEFAULT '0.0000' COMMENT 'Items Qty',
  `orig_order_id` int(10) unsigned DEFAULT '0' COMMENT 'Orig Order Id',
  `store_to_base_rate` decimal(12,4) DEFAULT '0.0000' COMMENT 'Store To Base Rate',
  `store_to_quote_rate` decimal(12,4) DEFAULT '0.0000' COMMENT 'Store To Quote Rate',
  `base_currency_code` varchar(255) DEFAULT NULL COMMENT 'Base Currency Code',
  `store_currency_code` varchar(255) DEFAULT NULL COMMENT 'Store Currency Code',
  `quote_currency_code` varchar(255) DEFAULT NULL COMMENT 'Quote Currency Code',
  `grand_total` decimal(12,4) DEFAULT '0.0000' COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Grand Total',
  `checkout_method` varchar(255) DEFAULT NULL COMMENT 'Checkout Method',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `customer_tax_class_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Tax Class Id',
  `customer_group_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Group Id',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_prefix` varchar(40) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_firstname` varchar(255) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_middlename` varchar(40) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_lastname` varchar(255) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_suffix` varchar(40) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `customer_note` varchar(255) DEFAULT NULL COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT '1' COMMENT 'Customer Note Notify',
  `customer_is_guest` smallint(5) unsigned DEFAULT '0' COMMENT 'Customer Is Guest',
  `remote_ip` varchar(32) DEFAULT NULL COMMENT 'Remote Ip',
  `applied_rule_ids` varchar(255) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `reserved_order_id` varchar(64) DEFAULT NULL COMMENT 'Reserved Order Id',
  `password_hash` varchar(255) DEFAULT NULL COMMENT 'Password Hash',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `global_currency_code` varchar(255) DEFAULT NULL COMMENT 'Global Currency Code',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_quote_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Quote Rate',
  `customer_taxvat` varchar(255) DEFAULT NULL COMMENT 'Customer Taxvat',
  `customer_gender` varchar(255) DEFAULT NULL COMMENT 'Customer Gender',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `subtotal_with_discount` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal With Discount',
  `base_subtotal_with_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal With Discount',
  `is_changed` int(10) unsigned DEFAULT NULL COMMENT 'Is Changed',
  `trigger_recollect` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Trigger Recollect',
  `ext_shipping_info` text COMMENT 'Ext Shipping Info',
  `is_persistent` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Quote Persistent',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`entity_id`),
  KEY `QUOTE_CUSTOMER_ID_STORE_ID_IS_ACTIVE` (`customer_id`,`store_id`,`is_active`),
  KEY `QUOTE_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote';

--
-- Dumping data for table `quote`
--

LOCK TABLES `quote` WRITE;
/*!40000 ALTER TABLE `quote` DISABLE KEYS */;
INSERT INTO `quote` VALUES ('11',1,'2016-11-05 03:07:57','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','50.0000','50.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'50.0000','50.0000','50.0000','50.0000','1',0,NULL,0,NULL),('12',1,'2016-11-08 01:56:27','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','60.0000','60.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'60.0000','60.0000','60.0000','60.0000','1',0,NULL,0,NULL),('13',1,'2016-11-10 09:17:21','2016-11-10 09:34:00',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','160.0000','160.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'160.0000','160.0000','160.0000','160.0000','1',0,NULL,0,NULL),('14',1,'2016-11-11 01:46:06','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','80.0000','80.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'80.0000','80.0000','80.0000','80.0000','1',0,NULL,0,NULL),('15',1,'2016-11-11 01:47:14','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','60.0000','60.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'60.0000','60.0000','60.0000','60.0000','1',0,NULL,0,NULL),('16',4,'2016-11-11 08:53:51','2016-11-11 08:54:13',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','230.0000','230.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'230.0000','230.0000','230.0000','230.0000','1',0,NULL,0,NULL),('17',4,'2016-11-11 10:24:16','2016-11-11 10:25:32',NULL,1,0,0,'0','0.0000','0','0.0000','0.0000','USD','USD','USD','0.0000','0.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000','1',0,NULL,0,NULL),('18',4,'2016-11-15 08:11:02','2016-11-15 09:04:31',NULL,1,0,0,'1','5.0000','0','0.0000','0.0000','USD','USD','USD','250.0000','250.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'250.0000','250.0000','250.0000','250.0000','1',0,NULL,0,NULL),('19',4,'2016-11-16 02:01:29','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','100.0000','100.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'100.0000','100.0000','100.0000','100.0000','1',0,NULL,0,NULL),('20',4,'2016-11-16 03:44:12','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','150.0000','150.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'150.0000','150.0000','150.0000','150.0000','1',0,NULL,0,NULL),('21',3,'2016-11-16 06:24:06','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','60.0000','60.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'60.0000','60.0000','60.0000','60.0000','1',0,NULL,0,NULL),('22',4,'2016-11-17 07:45:12','2016-11-17 07:45:49',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','80.0000','80.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'80.0000','80.0000','80.0000','80.0000','1',0,NULL,0,NULL),('23',1,'2016-11-17 09:56:21','2016-11-17 10:29:19',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','250.0000','250.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'250.0000','250.0000','250.0000','250.0000','1',0,NULL,0,NULL),('24',1,'2016-11-17 10:29:19','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','50.0000','50.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'50.0000','50.0000','50.0000','50.0000','1',0,NULL,0,NULL),('25',1,'2016-11-22 10:02:48','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','60.0000','60.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.2',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'60.0000','60.0000','60.0000','60.0000','1',0,NULL,0,NULL),('26',1,'2016-11-23 09:54:55','2016-11-23 09:55:08',NULL,1,0,0,'3','3.0000','0','0.0000','0.0000','USD','USD','USD','320.0000','320.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'320.0000','320.0000','320.0000','320.0000','1',0,NULL,0,NULL),('27',1,'2016-11-24 04:12:16','2016-11-24 04:23:12',NULL,1,0,0,'1','2.0000','0','0.0000','0.0000','USD','USD','USD','300.0000','300.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'::1',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'300.0000','300.0000','300.0000','300.0000','1',0,NULL,0,NULL),('28',3,'2016-11-24 04:32:45','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','50.0000','50.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'::1',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'50.0000','50.0000','50.0000','50.0000','1',0,NULL,0,NULL),('29',1,'2016-11-24 07:23:00','2016-11-24 07:28:36',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','180.0000','180.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'::1',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'180.0000','180.0000','180.0000','180.0000','1',0,NULL,0,NULL),('30',1,'2016-11-24 07:47:09','2016-11-24 07:47:28',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','100.0000','100.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'::1',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'100.0000','100.0000','100.0000','100.0000','1',0,NULL,0,NULL),('31',1,'2016-11-24 08:41:25','2016-11-24 08:51:07',NULL,1,0,0,'3','4.0000','0','0.0000','0.0000','USD','USD','USD','390.0000','390.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'::1',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'390.0000','390.0000','390.0000','390.0000','1',0,NULL,0,NULL),('32',1,'2016-11-26 03:15:10','2016-11-26 03:40:31',NULL,1,0,0,'2','3.0000','0','0.0000','0.0000','USD','USD','USD','380.0000','380.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'380.0000','380.0000','380.0000','380.0000','1',0,NULL,0,NULL),('33',1,'2016-11-27 14:45:39','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','150.0000','150.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'::1',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'150.0000','150.0000','150.0000','150.0000','1',0,NULL,0,NULL),('34',4,'2016-11-28 02:10:16','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','150.0000','150.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'192.168.1.99',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'150.0000','150.0000','150.0000','150.0000','1',0,NULL,0,NULL);
/*!40000 ALTER TABLE `quote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_address`
--

DROP TABLE IF EXISTS `quote_address`;
CREATE TABLE `quote_address` (
  `address_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Address Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `save_in_address_book` smallint(6) DEFAULT '0' COMMENT 'Save In Address Book',
  `customer_address_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Address Id',
  `address_type` varchar(10) DEFAULT NULL COMMENT 'Address Type',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `firstname` varchar(20) DEFAULT NULL COMMENT 'Firstname',
  `middlename` varchar(20) DEFAULT NULL COMMENT 'Middlename',
  `lastname` varchar(20) DEFAULT NULL COMMENT 'Lastname',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(40) DEFAULT NULL COMMENT 'City',
  `region` varchar(40) DEFAULT NULL COMMENT 'Region',
  `region_id` int(10) unsigned DEFAULT NULL COMMENT 'Region Id',
  `postcode` varchar(20) DEFAULT NULL COMMENT 'Postcode',
  `country_id` varchar(30) DEFAULT NULL COMMENT 'Country Id',
  `telephone` varchar(20) DEFAULT NULL COMMENT 'Phone Number',
  `fax` varchar(20) DEFAULT NULL COMMENT 'Fax',
  `same_as_billing` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Same As Billing',
  `collect_shipping_rates` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Collect Shipping Rates',
  `shipping_method` varchar(40) DEFAULT NULL COMMENT 'Shipping Method',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `weight` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Weight',
  `subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Subtotal',
  `subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal With Discount',
  `base_subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Subtotal With Discount',
  `tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Shipping Amount',
  `base_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Shipping Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Grand Total',
  `customer_notes` text COMMENT 'Customer Notes',
  `applied_taxes` text COMMENT 'Applied Taxes',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `free_shipping` smallint(6) DEFAULT NULL,
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`address_id`),
  KEY `QUOTE_ADDRESS_QUOTE_ID` (`quote_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Address';

--
-- Dumping data for table `quote_address`
--

LOCK TABLES `quote_address` WRITE;
/*!40000 ALTER TABLE `quote_address` DISABLE KEYS */;
INSERT INTO `quote_address` VALUES ('22','11','2016-11-05 03:07:58','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('23','11','2016-11-05 03:07:58','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','50.0000','50.0000','50.0000','50.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','50.0000','50.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','50.0000','50.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('24','12','2016-11-08 01:56:27','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('25','12','2016-11-08 01:56:27','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','60.0000','60.0000','60.0000','60.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('26','13','2016-11-10 09:17:23','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('27','13','2016-11-10 09:17:23','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','160.0000','160.0000','160.0000','160.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','160.0000','160.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','160.0000','160.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('28','14','2016-11-11 01:46:06','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('29','14','2016-11-11 01:46:06','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','80.0000','80.0000','80.0000','80.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','80.0000','80.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','80.0000','80.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('30','15','2016-11-11 01:47:14','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('31','15','2016-11-11 01:47:14','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','60.0000','60.0000','60.0000','60.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('32','16','2016-11-11 08:53:52','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('33','16','2016-11-11 08:53:52','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','230.0000','230.0000','230.0000','230.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','230.0000','230.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','230.0000','230.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('34','17','2016-11-11 10:24:16','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('35','17','2016-11-11 10:24:16','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,'0.0000','0.0000','0.0000','60.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('36','18','2016-11-15 08:11:03','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('37','18','2016-11-15 08:11:03','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','250.0000','250.0000','250.0000','250.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','250.0000','250.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','250.0000','250.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('38','19','2016-11-16 02:01:29','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('39','19','2016-11-16 02:01:29','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','100.0000','100.0000','100.0000','100.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','100.0000','100.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','100.0000','100.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('40','20','2016-11-16 03:44:12','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('41','20','2016-11-16 03:44:12','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','150.0000','150.0000','150.0000','150.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','150.0000','150.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','150.0000','150.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('42','21','2016-11-16 06:24:06','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('43','21','2016-11-16 06:24:06','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','60.0000','60.0000','60.0000','60.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('44','22','2016-11-17 07:45:13','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('45','22','2016-11-17 07:45:13','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','80.0000','80.0000','80.0000','80.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','80.0000','80.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','80.0000','80.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('46','23','2016-11-17 09:56:22','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('47','23','2016-11-17 09:56:22','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','250.0000','250.0000','250.0000','250.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','250.0000','250.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','250.0000','250.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('48','24','2016-11-17 10:29:19','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('49','24','2016-11-17 10:29:19','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','50.0000','50.0000','50.0000','50.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','50.0000','50.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','50.0000','50.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('50','25','2016-11-22 10:02:48','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('51','25','2016-11-22 10:02:48','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','60.0000','60.0000','60.0000','60.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('52','26','2016-11-23 09:54:55','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('53','26','2016-11-23 09:54:55','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','320.0000','320.0000','320.0000','320.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','320.0000','320.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','320.0000','320.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('54','27','2016-11-24 04:12:16','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('55','27','2016-11-24 04:12:16','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','300.0000','300.0000','300.0000','300.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','300.0000','300.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','300.0000','300.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('56','28','2016-11-24 04:32:46','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('57','28','2016-11-24 04:32:46','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','50.0000','50.0000','50.0000','50.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','50.0000','50.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','50.0000','50.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('58','29','2016-11-24 07:23:00','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('59','29','2016-11-24 07:23:00','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','180.0000','180.0000','180.0000','180.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','180.0000','180.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','180.0000','180.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('60','30','2016-11-24 07:47:09','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('61','30','2016-11-24 07:47:09','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','100.0000','100.0000','100.0000','100.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','100.0000','100.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','100.0000','100.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('62','31','2016-11-24 08:41:25','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('63','31','2016-11-24 08:41:26','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','390.0000','390.0000','390.0000','390.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','390.0000','390.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','390.0000','390.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('64','32','2016-11-26 03:15:10','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('65','32','2016-11-26 03:15:10','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','380.0000','380.0000','380.0000','380.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','380.0000','380.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','380.0000','380.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('66','33','2016-11-27 14:45:39','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('67','33','2016-11-27 14:45:39','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','150.0000','150.0000','150.0000','150.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','150.0000','150.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','150.0000','150.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('68','34','2016-11-28 02:10:16','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('69','34','2016-11-28 02:10:16','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.0000','150.0000','150.0000','150.0000','150.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','150.0000','150.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','150.0000','150.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `quote_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_address_item`
--

DROP TABLE IF EXISTS `quote_address_item`;
CREATE TABLE `quote_address_item` (
  `address_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Address Item Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Address Id',
  `quote_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Item Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Total With Discount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `super_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Super Product Id',
  `parent_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Product Id',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `image` varchar(255) DEFAULT NULL COMMENT 'Image',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `is_qty_decimal` int(10) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `discount_percent` decimal(12,4) DEFAULT NULL COMMENT 'Discount Percent',
  `no_discount` int(10) unsigned DEFAULT NULL COMMENT 'No Discount',
  `tax_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tax Percent',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `free_shipping` int(11) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`address_item_id`),
  KEY `QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS_ID` (`quote_address_id`),
  KEY `QUOTE_ADDRESS_ITEM_PARENT_ITEM_ID` (`parent_item_id`),
  KEY `QUOTE_ADDRESS_ITEM_QUOTE_ITEM_ID` (`quote_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Address Item';

--
-- Table structure for table `quote_id_mask`
--

DROP TABLE IF EXISTS `quote_id_mask`;
CREATE TABLE `quote_id_mask` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `quote_id` int(10) unsigned NOT NULL COMMENT 'Quote ID',
  `masked_id` varchar(32) DEFAULT NULL COMMENT 'Masked ID',
  PRIMARY KEY (`entity_id`,`quote_id`),
  KEY `QUOTE_ID_MASK_QUOTE_ID` (`quote_id`),
  KEY `QUOTE_ID_MASK_MASKED_ID` (`masked_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Quote ID and masked ID mapping';

--
-- Dumping data for table `quote_id_mask`
--

LOCK TABLES `quote_id_mask` WRITE;
/*!40000 ALTER TABLE `quote_id_mask` DISABLE KEYS */;
INSERT INTO `quote_id_mask` VALUES ('6','18','0c68dd89dd62a250824a3e9b75776294'),('20','33','11f3a57ff5fb21fcf85174000af3fba8'),('12','25','13171f2eab8593e190a4262939c49843'),('1','13','169f02ae338212ead3480784a29ab04a'),('13','26','1e47e62da8384e5b5102181888d3584a'),('19','32','26485289ac4ca68146926a13a738391a'),('8','21','294cdbcf747e87b6f97a87b67b0c630a'),('2','14','3aed8e2e45314040993d1e833991ba21'),('21','34','560b2d6d1acc53e3ee006543ecaa540a'),('10','23','5f5dde3e5cdb6c3ab2276afb97c111e1'),('9','22','8acc8cf1b4ab7b8e281a1febbb60bd12'),('18','31','8e1477d0043e0dde9e0f4de53335ea54'),('11','24','981b5800ab125be1409d3f5856328334'),('7','20','c438d0a730786619c40611b37bf2aff6'),('5','17','c6dd72dd06dbb190e5489fcd20a372e8'),('16','29','c7ae1e8bb869afc494850f7a54f80462'),('14','27','d9f6f5157b20b203194f858ecfaca13f'),('17','30','e2d2d487fd2b8d47aa3ab4385533ab8f'),('3','15','f649d6a79b135e4b70cbac449da30e14'),('15','28','f7287a46a4c4dde123c886164ae7b43f'),('4','16','f9e87a73af7478aa83bb404fb0f30299');
/*!40000 ALTER TABLE `quote_id_mask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_item`
--

DROP TABLE IF EXISTS `quote_item`;
CREATE TABLE `quote_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `is_qty_decimal` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) unsigned DEFAULT '0' COMMENT 'No Discount',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `custom_price` decimal(12,4) DEFAULT NULL COMMENT 'Custom Price',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Total With Discount',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `original_custom_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Custom Price',
  `redirect_url` varchar(255) DEFAULT NULL COMMENT 'Redirect Url',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `free_shipping` smallint(6) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`item_id`),
  KEY `QUOTE_ITEM_PARENT_ITEM_ID` (`parent_item_id`),
  KEY `QUOTE_ITEM_PRODUCT_ID` (`product_id`),
  KEY `QUOTE_ITEM_QUOTE_ID` (`quote_id`),
  KEY `QUOTE_ITEM_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Item';

--
-- Dumping data for table `quote_item`
--

LOCK TABLES `quote_item` WRITE;
/*!40000 ALTER TABLE `quote_item` DISABLE KEYS */;
INSERT INTO `quote_item` VALUES ('21','11','2016-11-05 03:08:00','0000-00-00 00:00:00','9',1,NULL,0,'Habitant','Habitant',NULL,NULL,NULL,0,0,NULL,'1.0000','50.0000','50.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','50.0000','50.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'50.0000','50.0000','50.0000','50.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('22','12','2016-11-08 01:56:28','0000-00-00 00:00:00','4',1,NULL,0,'Etiam Gravida','Etiam Gravida',NULL,NULL,NULL,0,0,NULL,'1.0000','60.0000','60.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'60.0000','60.0000','60.0000','60.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('23','13','2016-11-10 09:17:25','0000-00-00 00:00:00','4',1,NULL,0,'Etiam Gravida','Etiam Gravida',NULL,NULL,NULL,0,0,NULL,'1.0000','60.0000','60.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'60.0000','60.0000','60.0000','60.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('24','13','2016-11-10 09:34:00','0000-00-00 00:00:00','17',1,NULL,0,'adidas','adidas',NULL,NULL,NULL,0,0,NULL,'1.0000','100.0000','100.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','100.0000','100.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'100.0000','100.0000','100.0000','100.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('25','14','2016-11-11 01:46:06','0000-00-00 00:00:00','16',1,NULL,0,'puma','puma',NULL,NULL,NULL,0,0,NULL,'1.0000','80.0000','80.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','80.0000','80.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'80.0000','80.0000','80.0000','80.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('26','15','2016-11-11 01:47:14','0000-00-00 00:00:00','15',1,NULL,0,'Levis','Levis',NULL,NULL,NULL,0,0,NULL,'1.0000','60.0000','60.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'60.0000','60.0000','60.0000','60.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('27','16','2016-11-11 08:53:52','0000-00-00 00:00:00','3',4,NULL,0,'Nunc Facilisis','Nunc Facilisis',NULL,NULL,NULL,0,0,NULL,'1.0000','150.0000','150.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','150.0000','150.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'150.0000','150.0000','150.0000','150.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('28','16','2016-11-11 08:54:13','0000-00-00 00:00:00','16',4,NULL,0,'puma','puma',NULL,NULL,NULL,0,0,NULL,'1.0000','80.0000','80.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','80.0000','80.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'80.0000','80.0000','80.0000','80.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('29','18','2016-11-15 08:11:05','2016-11-15 09:04:31','9',4,NULL,0,'Habitant','Habitant',NULL,NULL,NULL,0,0,NULL,'5.0000','50.0000','50.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','250.0000','250.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'50.0000','50.0000','250.0000','250.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('30','19','2016-11-16 02:01:29','0000-00-00 00:00:00','17',4,NULL,0,'adidas','adidas',NULL,NULL,NULL,0,0,NULL,'1.0000','100.0000','100.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','100.0000','100.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'100.0000','100.0000','100.0000','100.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('31','20','2016-11-16 03:44:12','0000-00-00 00:00:00','3',4,NULL,0,'Nunc Facilisis','Nunc Facilisis',NULL,NULL,NULL,0,0,NULL,'1.0000','150.0000','150.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','150.0000','150.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'150.0000','150.0000','150.0000','150.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('32','21','2016-11-16 06:24:07','0000-00-00 00:00:00','15',3,NULL,0,'Levis','Levis',NULL,NULL,NULL,0,0,NULL,'1.0000','60.0000','60.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'60.0000','60.0000','60.0000','60.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('33','22','2016-11-17 07:45:13','0000-00-00 00:00:00','19',4,NULL,0,'violet','violet',NULL,NULL,NULL,0,0,NULL,'1.0000','30.0000','30.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','30.0000','30.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'30.0000','30.0000','30.0000','30.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('34','22','2016-11-17 07:45:49','0000-00-00 00:00:00','14',4,NULL,0,'squa','squa',NULL,NULL,NULL,0,0,NULL,'1.0000','50.0000','50.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','50.0000','50.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'50.0000','50.0000','50.0000','50.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('35','23','2016-11-17 09:56:23','0000-00-00 00:00:00','17',4,NULL,0,'adidas','adidas',NULL,NULL,NULL,0,0,NULL,'1.0000','100.0000','100.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','100.0000','100.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'100.0000','100.0000','100.0000','100.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('36','23','2016-11-17 10:29:20','0000-00-00 00:00:00','3',1,NULL,0,'Nunc Facilisis','Nunc Facilisis',NULL,NULL,NULL,0,0,NULL,'1.0000','150.0000','150.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','150.0000','150.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'150.0000','150.0000','150.0000','150.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('37','24','2016-11-17 10:29:20','0000-00-00 00:00:00','9',1,NULL,0,'Habitant','Habitant',NULL,NULL,NULL,0,0,NULL,'1.0000','50.0000','50.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','50.0000','50.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'50.0000','50.0000','50.0000','50.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('38','25','2016-11-22 10:02:48','0000-00-00 00:00:00','4',1,NULL,0,'Etiam Gravida','Etiam Gravida',NULL,NULL,NULL,0,0,NULL,'1.0000','60.0000','60.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'60.0000','60.0000','60.0000','60.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('39','26','2016-11-23 09:54:55','0000-00-00 00:00:00','10',1,NULL,0,'Tempus','Tempus',NULL,NULL,NULL,0,0,NULL,'1.0000','70.0000','70.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','70.0000','70.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'70.0000','70.0000','70.0000','70.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('40','26','2016-11-23 09:55:04','0000-00-00 00:00:00','17',1,NULL,0,'adidas','adidas',NULL,NULL,NULL,0,0,NULL,'1.0000','100.0000','100.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','100.0000','100.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'100.0000','100.0000','100.0000','100.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('41','26','2016-11-23 09:55:08','0000-00-00 00:00:00','18',1,NULL,0,'nike','nike',NULL,NULL,NULL,0,0,NULL,'1.0000','150.0000','150.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','150.0000','150.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'150.0000','150.0000','150.0000','150.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('42','27','2016-11-24 04:12:16','2016-11-24 04:23:12','2',1,NULL,0,'Accumsan Elit','Accumsan Elit',NULL,NULL,NULL,0,0,NULL,'2.0000','150.0000','150.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','300.0000','300.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'150.0000','150.0000','300.0000','300.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('43','28','2016-11-24 04:32:46','0000-00-00 00:00:00','14',3,NULL,0,'squa','squa',NULL,NULL,NULL,0,0,NULL,'1.0000','50.0000','50.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','50.0000','50.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'50.0000','50.0000','50.0000','50.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('44','29','2016-11-24 07:23:00','0000-00-00 00:00:00','2',1,NULL,0,'Accumsan Elit','Accumsan Elit',NULL,NULL,NULL,0,0,NULL,'1.0000','150.0000','150.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','150.0000','150.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'150.0000','150.0000','150.0000','150.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('45','29','2016-11-24 07:28:36','0000-00-00 00:00:00','19',1,NULL,0,'violet','violet',NULL,NULL,NULL,0,0,NULL,'1.0000','30.0000','30.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','30.0000','30.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'30.0000','30.0000','30.0000','30.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('46','30','2016-11-24 07:47:10','0000-00-00 00:00:00','14',1,NULL,0,'squa','squa',NULL,NULL,NULL,0,0,NULL,'1.0000','50.0000','50.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','50.0000','50.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'50.0000','50.0000','50.0000','50.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('47','30','2016-11-24 07:47:28','0000-00-00 00:00:00','5',1,NULL,0,'Donec Non Est','Donec Non Est',NULL,NULL,NULL,0,0,NULL,'1.0000','50.0000','50.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','50.0000','50.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'50.0000','50.0000','50.0000','50.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('48','31','2016-11-24 08:41:26','2016-11-24 08:41:51','18',4,NULL,0,'nike','nike',NULL,NULL,NULL,0,0,NULL,'2.0000','150.0000','150.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','300.0000','300.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'150.0000','150.0000','300.0000','300.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('49','31','2016-11-24 08:41:32','0000-00-00 00:00:00','11',4,NULL,0,'Aliquam','Aliquam',NULL,NULL,NULL,0,0,NULL,'1.0000','60.0000','60.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'60.0000','60.0000','60.0000','60.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('50','31','2016-11-24 08:51:08','0000-00-00 00:00:00','19',1,NULL,0,'violet','violet',NULL,NULL,NULL,0,0,NULL,'1.0000','30.0000','30.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','30.0000','30.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'30.0000','30.0000','30.0000','30.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('55','32','2016-11-26 03:37:59','2016-11-26 03:40:10','2',1,NULL,0,'Accumsan Elit','Accumsan Elit',NULL,NULL,NULL,0,0,NULL,'2.0000','150.0000','150.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','300.0000','300.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'150.0000','150.0000','300.0000','300.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('56','32','2016-11-26 03:40:31','0000-00-00 00:00:00','16',1,NULL,0,'puma','puma',NULL,NULL,NULL,0,0,NULL,'1.0000','80.0000','80.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','80.0000','80.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'80.0000','80.0000','80.0000','80.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('57','33','2016-11-27 14:45:40','0000-00-00 00:00:00','18',1,NULL,0,'nike','nike',NULL,NULL,NULL,0,0,NULL,'1.0000','150.0000','150.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','150.0000','150.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'150.0000','150.0000','150.0000','150.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('58','34','2016-11-28 02:10:19','0000-00-00 00:00:00','18',4,NULL,0,'nike','nike',NULL,NULL,NULL,0,0,NULL,'1.0000','150.0000','150.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','150.0000','150.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,'150.0000','150.0000','150.0000','150.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `quote_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_item_option`
--

DROP TABLE IF EXISTS `quote_item_option`;
CREATE TABLE `quote_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `item_id` int(10) unsigned NOT NULL COMMENT 'Item Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`option_id`),
  KEY `QUOTE_ITEM_OPTION_ITEM_ID` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Item Option';

--
-- Dumping data for table `quote_item_option`
--

LOCK TABLES `quote_item_option` WRITE;
/*!40000 ALTER TABLE `quote_item_option` DISABLE KEYS */;
INSERT INTO `quote_item_option` VALUES ('21','21','9','info_buyRequest','a:3:{s:4:\"uenc\";s:64:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC8,\";s:7:\"product\";s:1:\"9\";s:3:\"qty\";d:1;}'),('22','22','4','info_buyRequest','a:3:{s:4:\"uenc\";s:64:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC8,\";s:7:\"product\";s:1:\"4\";s:3:\"qty\";d:1;}'),('23','23','4','info_buyRequest','a:3:{s:4:\"uenc\";s:64:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC8,\";s:7:\"product\";s:1:\"4\";s:3:\"qty\";d:1;}'),('24','24','17','info_buyRequest','a:3:{s:4:\"uenc\";s:80:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9mYXNoaW9uLmh0bWw,\";s:7:\"product\";s:2:\"17\";s:3:\"qty\";d:1;}'),('25','25','16','info_buyRequest','a:3:{s:4:\"uenc\";s:108:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC93b21lbi5odG1sP3Byb2R1Y3RfbGlzdF9tb2RlPWxpc3Q,\";s:7:\"product\";s:2:\"16\";s:3:\"qty\";d:1;}'),('26','26','15','info_buyRequest','a:3:{s:4:\"uenc\";s:76:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC93b21lbi5odG1s\";s:7:\"product\";s:2:\"15\";s:3:\"qty\";d:1;}'),('27','27','3','info_buyRequest','a:3:{s:4:\"uenc\";s:76:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC93b21lbi5odG1s\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('28','28','16','info_buyRequest','a:3:{s:4:\"uenc\";s:76:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC93b21lbi5odG1s\";s:7:\"product\";s:2:\"16\";s:3:\"qty\";d:1;}'),('29','29','9','info_buyRequest','a:3:{s:4:\"uenc\";s:76:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC93b21lbi5odG1s\";s:7:\"product\";s:1:\"9\";s:3:\"qty\";d:1;}'),('30','30','17','info_buyRequest','a:3:{s:4:\"uenc\";s:108:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9nZXJtYW55L3dvbWVuL2FjY3Vtc2FuLWVsaXQuaHRtbA,,\";s:7:\"product\";s:2:\"17\";s:3:\"qty\";d:1;}'),('31','31','3','info_buyRequest','a:3:{s:4:\"uenc\";s:84:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9nZXJtYW55L21lbi5odG1s\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('32','32','15','info_buyRequest','a:3:{s:4:\"uenc\";s:88:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9mcmVuY2gvd29tZW4uaHRtbA,,\";s:7:\"product\";s:2:\"15\";s:3:\"qty\";d:1;}'),('33','33','19','info_buyRequest','a:3:{s:4:\"uenc\";s:88:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9nZXJtYW55L3dvbWVuLmh0bWw,\";s:7:\"product\";s:2:\"19\";s:3:\"qty\";d:1;}'),('34','34','14','info_buyRequest','a:3:{s:4:\"uenc\";s:88:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9nZXJtYW55L3dvbWVuLmh0bWw,\";s:7:\"product\";s:2:\"14\";s:3:\"qty\";d:1;}'),('35','35','17','info_buyRequest','a:3:{s:4:\"uenc\";s:88:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9nZXJtYW55L3dvbWVuLmh0bWw,\";s:7:\"product\";s:2:\"17\";s:3:\"qty\";d:1;}'),('36','36','3','info_buyRequest','a:3:{s:4:\"uenc\";s:76:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoLw,,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('37','37','9','info_buyRequest','a:3:{s:4:\"uenc\";s:88:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL3dvbWVuLmh0bWw,\";s:7:\"product\";s:1:\"9\";s:3:\"qty\";d:1;}'),('38','38','4','info_buyRequest','a:3:{s:4:\"uenc\";s:64:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC8,\";s:7:\"product\";s:1:\"4\";s:3:\"qty\";d:1;}'),('39','39','10','info_buyRequest','a:3:{s:4:\"uenc\";s:88:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL3dvbWVuLmh0bWw,\";s:7:\"product\";s:2:\"10\";s:3:\"qty\";d:1;}'),('40','40','17','info_buyRequest','a:3:{s:4:\"uenc\";s:88:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL3dvbWVuLmh0bWw,\";s:7:\"product\";s:2:\"17\";s:3:\"qty\";d:1;}'),('41','41','18','info_buyRequest','a:3:{s:4:\"uenc\";s:88:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL3dvbWVuLmh0bWw,\";s:7:\"product\";s:2:\"18\";s:3:\"qty\";d:1;}'),('42','42','2','info_buyRequest','a:5:{s:4:\"uenc\";s:96:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL2FjY3Vtc2FuLWVsaXQuaHRtbA,,\";s:7:\"product\";s:1:\"2\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}'),('43','43','14','info_buyRequest','a:3:{s:4:\"uenc\";s:84:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9mcmVuY2gvd29tZW4uaHRtbA,,\";s:7:\"product\";s:2:\"14\";s:3:\"qty\";d:1;}'),('44','44','2','info_buyRequest','a:5:{s:4:\"uenc\";s:96:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL2FjY3Vtc2FuLWVsaXQuaHRtbA,,\";s:7:\"product\";s:1:\"2\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}'),('45','45','19','info_buyRequest','a:3:{s:4:\"uenc\";s:84:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL3dvbWVuLmh0bWw,\";s:7:\"product\";s:2:\"19\";s:3:\"qty\";d:1;}'),('46','46','14','info_buyRequest','a:3:{s:4:\"uenc\";s:84:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL3dvbWVuLmh0bWw,\";s:7:\"product\";s:2:\"14\";s:3:\"qty\";d:1;}'),('47','47','5','info_buyRequest','a:3:{s:4:\"uenc\";s:96:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL2FjY3Vtc2FuLWVsaXQuaHRtbA,,\";s:7:\"product\";s:1:\"5\";s:3:\"qty\";d:1;}'),('48','48','18','info_buyRequest','a:3:{s:4:\"uenc\";s:96:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9nZXJtYW55L2FjY3Vtc2FuLWVsaXQuaHRtbA,,\";s:7:\"product\";s:2:\"18\";s:3:\"qty\";d:1;}'),('49','49','11','info_buyRequest','a:3:{s:4:\"uenc\";s:84:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9nZXJtYW55L3dvbWVuLmh0bWw,\";s:7:\"product\";s:2:\"11\";s:3:\"qty\";d:1;}'),('50','50','19','info_buyRequest','a:3:{s:4:\"uenc\";s:84:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL3dvbWVuLmh0bWw,\";s:7:\"product\";s:2:\"19\";s:3:\"qty\";d:1;}'),('55','55','2','info_buyRequest','a:5:{s:4:\"uenc\";s:100:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL2FjY3Vtc2FuLWVsaXQuaHRtbA,,\";s:7:\"product\";s:1:\"2\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}'),('56','56','16','info_buyRequest','a:3:{s:4:\"uenc\";s:100:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoL2FjY3Vtc2FuLWVsaXQuaHRtbA,,\";s:7:\"product\";s:2:\"16\";s:3:\"qty\";d:1;}'),('57','57','18','info_buyRequest','a:3:{s:4:\"uenc\";s:72:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9lbmdsaXNoLw,,\";s:7:\"product\";s:2:\"18\";s:3:\"qty\";d:1;}'),('58','58','18','info_buyRequest','a:5:{s:4:\"uenc\";s:88:\"aHR0cDovLzE5Mi4xNjguMS45OTo4MDgwL21hX3NhaGFyYV9mYXNoaW9uODkxMC9nZXJtYW55L25pa2UuaHRtbA,,\";s:7:\"product\";s:2:\"18\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}');
/*!40000 ALTER TABLE `quote_item_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_payment`
--

DROP TABLE IF EXISTS `quote_payment`;
CREATE TABLE `quote_payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Payment Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `cc_type` varchar(255) DEFAULT NULL COMMENT 'Cc Type',
  `cc_number_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Number Enc',
  `cc_last_4` varchar(255) DEFAULT NULL COMMENT 'Cc Last 4',
  `cc_cid_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Cid Enc',
  `cc_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_exp_month` varchar(255) DEFAULT NULL COMMENT 'Cc Exp Month',
  `cc_exp_year` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Exp Year',
  `cc_ss_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Owner',
  `cc_ss_start_month` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Ss Start Month',
  `cc_ss_start_year` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Ss Start Year',
  `po_number` varchar(255) DEFAULT NULL COMMENT 'Po Number',
  `additional_data` text COMMENT 'Additional Data',
  `cc_ss_issue` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `additional_information` text COMMENT 'Additional Information',
  `paypal_payer_id` varchar(255) DEFAULT NULL COMMENT 'Paypal Payer Id',
  `paypal_payer_status` varchar(255) DEFAULT NULL COMMENT 'Paypal Payer Status',
  `paypal_correlation_id` varchar(255) DEFAULT NULL COMMENT 'Paypal Correlation Id',
  PRIMARY KEY (`payment_id`),
  KEY `QUOTE_PAYMENT_QUOTE_ID` (`quote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Payment';

--
-- Table structure for table `quote_shipping_rate`
--

DROP TABLE IF EXISTS `quote_shipping_rate`;
CREATE TABLE `quote_shipping_rate` (
  `rate_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rate Id',
  `address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Address Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `carrier` varchar(255) DEFAULT NULL COMMENT 'Carrier',
  `carrier_title` varchar(255) DEFAULT NULL COMMENT 'Carrier Title',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `method_description` text COMMENT 'Method Description',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `error_message` text COMMENT 'Error Message',
  `method_title` text COMMENT 'Method Title',
  PRIMARY KEY (`rate_id`),
  KEY `QUOTE_SHIPPING_RATE_ADDRESS_ID` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Shipping Rate';

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
CREATE TABLE `rating` (
  `rating_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rating Id',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `rating_code` varchar(64) NOT NULL COMMENT 'Rating Code',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Position On Storefront',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Rating is active.',
  PRIMARY KEY (`rating_id`),
  UNIQUE KEY `RATING_RATING_CODE` (`rating_code`),
  KEY `RATING_ENTITY_ID` (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Ratings';

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` VALUES (1,1,'Quality',0,1),(2,1,'Value',0,1),(3,1,'Price',0,1);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_entity`
--

DROP TABLE IF EXISTS `rating_entity`;
CREATE TABLE `rating_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_code` varchar(64) NOT NULL COMMENT 'Entity Code',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `RATING_ENTITY_ENTITY_CODE` (`entity_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Rating entities';

--
-- Dumping data for table `rating_entity`
--

LOCK TABLES `rating_entity` WRITE;
/*!40000 ALTER TABLE `rating_entity` DISABLE KEYS */;
INSERT INTO `rating_entity` VALUES (1,'product'),(2,'product_review'),(3,'review');
/*!40000 ALTER TABLE `rating_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_option`
--

DROP TABLE IF EXISTS `rating_option`;
CREATE TABLE `rating_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rating Option Id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Id',
  `code` varchar(32) NOT NULL COMMENT 'Rating Option Code',
  `value` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Option Value',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Ration option position on Storefront',
  PRIMARY KEY (`option_id`),
  KEY `RATING_OPTION_RATING_ID` (`rating_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Rating options';

--
-- Dumping data for table `rating_option`
--

LOCK TABLES `rating_option` WRITE;
/*!40000 ALTER TABLE `rating_option` DISABLE KEYS */;
INSERT INTO `rating_option` VALUES ('1',1,'1',1,1),('2',1,'2',2,2),('3',1,'3',3,3),('4',1,'4',4,4),('5',1,'5',5,5),('6',2,'1',1,1),('7',2,'2',2,2),('8',2,'3',3,3),('9',2,'4',4,4),('10',2,'5',5,5),('11',3,'1',1,1),('12',3,'2',2,2),('13',3,'3',3,3),('14',3,'4',4,4),('15',3,'5',5,5);
/*!40000 ALTER TABLE `rating_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_option_vote`
--

DROP TABLE IF EXISTS `rating_option_vote`;
CREATE TABLE `rating_option_vote` (
  `vote_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Vote id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Vote option id',
  `remote_ip` varchar(16) NOT NULL COMMENT 'Customer IP',
  `remote_ip_long` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Customer IP converted to long integer format',
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Id',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `review_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Review id',
  `percent` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Percent amount',
  `value` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Vote option value',
  PRIMARY KEY (`vote_id`),
  KEY `RATING_OPTION_VOTE_OPTION_ID` (`option_id`),
  KEY `RATING_OPTION_VOTE_REVIEW_ID_REVIEW_REVIEW_ID` (`review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='Rating option values';

--
-- Dumping data for table `rating_option_vote`
--

LOCK TABLES `rating_option_vote` WRITE;
/*!40000 ALTER TABLE `rating_option_vote` DISABLE KEYS */;
INSERT INTO `rating_option_vote` VALUES (1,'8','::1',0,NULL,2,2,2,60,3),(2,'3','::1',0,NULL,2,1,2,60,3),(3,'13','::1',0,NULL,2,3,2,60,3),(4,'8','::1',0,NULL,2,2,1,60,3),(5,'3','::1',0,NULL,2,1,1,60,3),(6,'13','::1',0,NULL,2,3,1,60,3),(7,'8','::1',0,NULL,3,2,3,60,3),(8,'3','::1',0,NULL,3,1,3,60,3),(9,'13','::1',0,NULL,3,3,3,60,3),(10,'8','::1',0,NULL,4,2,4,60,3),(11,'4','::1',0,NULL,4,1,4,80,4),(12,'14','::1',0,NULL,4,3,4,80,4),(13,'9','::1',0,NULL,19,2,5,80,4),(14,'4','::1',0,NULL,19,1,5,80,4),(15,'14','::1',0,NULL,19,3,5,80,4),(16,'3','::1',0,NULL,18,1,6,60,3),(17,'13','::1',0,NULL,18,3,6,60,3),(18,'9','::1',0,NULL,18,2,6,80,4),(19,'3','::1',0,NULL,17,1,7,60,3),(20,'15','::1',0,NULL,17,3,7,100,5),(21,'10','::1',0,NULL,17,2,7,100,5),(22,'3','::1',0,NULL,9,1,9,60,3),(23,'14','::1',0,NULL,9,3,9,80,4),(24,'9','::1',0,NULL,9,2,9,80,4),(25,'3','::1',0,NULL,10,1,10,60,3),(26,'12','::1',0,NULL,10,3,10,40,2),(27,'8','::1',0,NULL,10,2,10,60,3),(28,'3','::1',0,NULL,14,1,11,60,3),(29,'13','::1',0,NULL,14,3,11,60,3),(30,'9','::1',0,NULL,14,2,11,80,4),(31,'3','::1',0,NULL,11,1,12,60,3),(32,'14','::1',0,NULL,11,3,12,80,4),(33,'9','::1',0,NULL,11,2,12,80,4),(34,'3','::1',0,NULL,16,1,13,60,3),(35,'13','::1',0,NULL,16,3,13,60,3),(36,'8','::1',0,NULL,16,2,13,60,3),(37,'4','::1',0,NULL,15,1,14,80,4),(38,'14','::1',0,NULL,15,3,14,80,4),(39,'9','::1',0,NULL,15,2,14,80,4),(40,'8','::1',0,NULL,5,2,8,60,3),(41,'3','::1',0,NULL,5,1,8,60,3),(42,'14','::1',0,NULL,5,3,8,80,4);
/*!40000 ALTER TABLE `rating_option_vote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_option_vote_aggregated`
--

DROP TABLE IF EXISTS `rating_option_vote_aggregated`;
CREATE TABLE `rating_option_vote_aggregated` (
  `primary_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Vote aggregation id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `vote_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Vote dty',
  `vote_value_sum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'General vote sum',
  `percent` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Vote percent',
  `percent_approved` smallint(6) DEFAULT '0' COMMENT 'Vote percent approved by admin',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  PRIMARY KEY (`primary_id`),
  KEY `RATING_OPTION_VOTE_AGGREGATED_RATING_ID` (`rating_id`),
  KEY `RATING_OPTION_VOTE_AGGREGATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8 COMMENT='Rating vote aggregated';

--
-- Dumping data for table `rating_option_vote_aggregated`
--

LOCK TABLES `rating_option_vote_aggregated` WRITE;
/*!40000 ALTER TABLE `rating_option_vote_aggregated` DISABLE KEYS */;
INSERT INTO `rating_option_vote_aggregated` VALUES ('1',2,2,'2','6',60,60,0),('3',1,2,'2','6',60,60,0),('5',3,2,'2','6',60,60,0),('7',1,2,'2','6',60,60,1),('8',1,2,'2','6',60,60,3),('9',1,2,'2','6',60,60,4),('10',2,2,'2','6',60,60,1),('11',2,2,'2','6',60,60,3),('12',2,2,'2','6',60,60,4),('13',3,2,'2','6',60,60,1),('14',3,2,'2','6',60,60,3),('15',3,2,'2','6',60,60,4),('16',2,3,'1','3',60,60,0),('17',2,3,'1','3',60,60,1),('19',2,3,'1','3',60,60,3),('20',2,3,'1','3',60,60,4),('21',1,3,'1','3',60,60,0),('22',1,3,'1','3',60,60,1),('24',1,3,'1','3',60,60,3),('25',1,3,'1','3',60,60,4),('26',3,3,'1','3',60,60,0),('27',3,3,'1','3',60,60,1),('29',3,3,'1','3',60,60,3),('30',3,3,'1','3',60,60,4),('31',2,4,'1','3',60,60,0),('32',2,4,'1','3',60,60,1),('34',2,4,'1','3',60,60,3),('35',2,4,'1','3',60,60,4),('36',1,4,'1','4',80,80,0),('37',1,4,'1','4',80,80,1),('39',1,4,'1','4',80,80,3),('40',1,4,'1','4',80,80,4),('41',3,4,'1','4',80,80,0),('42',3,4,'1','4',80,80,1),('44',3,4,'1','4',80,80,3),('45',3,4,'1','4',80,80,4),('46',2,19,'1','4',80,0,0),('48',1,19,'1','4',80,0,0),('50',3,19,'1','4',80,0,0),('52',1,18,'1','3',60,60,0),('53',1,18,'1','3',60,60,1),('55',1,18,'1','3',60,60,3),('56',1,18,'1','3',60,60,4),('57',3,18,'1','3',60,60,0),('58',3,18,'1','3',60,60,1),('60',3,18,'1','3',60,60,3),('61',3,18,'1','3',60,60,4),('62',2,18,'1','4',80,80,0),('63',2,18,'1','4',80,80,1),('65',2,18,'1','4',80,80,3),('66',2,18,'1','4',80,80,4),('67',1,17,'1','3',60,60,0),('68',1,17,'1','3',60,60,1),('70',1,17,'1','3',60,60,3),('71',1,17,'1','3',60,60,4),('72',3,17,'1','5',100,100,0),('73',3,17,'1','5',100,100,1),('75',3,17,'1','5',100,100,3),('76',3,17,'1','5',100,100,4),('77',2,17,'1','5',100,100,0),('78',2,17,'1','5',100,100,1),('80',2,17,'1','5',100,100,3),('81',2,17,'1','5',100,100,4),('82',1,9,'1','3',60,60,0),('83',1,9,'1','3',60,60,1),('85',1,9,'1','3',60,60,3),('86',1,9,'1','3',60,60,4),('87',3,9,'1','4',80,80,0),('88',3,9,'1','4',80,80,1),('90',3,9,'1','4',80,80,3),('91',3,9,'1','4',80,80,4),('92',2,9,'1','4',80,80,0),('93',2,9,'1','4',80,80,1),('95',2,9,'1','4',80,80,3),('96',2,9,'1','4',80,80,4),('97',1,10,'1','3',60,60,0),('98',1,10,'1','3',60,60,1),('100',1,10,'1','3',60,60,3),('101',1,10,'1','3',60,60,4),('102',3,10,'1','2',40,40,0),('103',3,10,'1','2',40,40,1),('105',3,10,'1','2',40,40,3),('106',3,10,'1','2',40,40,4),('107',2,10,'1','3',60,60,0),('108',2,10,'1','3',60,60,1),('110',2,10,'1','3',60,60,3),('111',2,10,'1','3',60,60,4),('112',1,14,'1','3',60,60,0),('113',1,14,'1','3',60,60,1),('115',1,14,'1','3',60,60,3),('116',1,14,'1','3',60,60,4),('117',3,14,'1','3',60,60,0),('118',3,14,'1','3',60,60,1),('120',3,14,'1','3',60,60,3),('121',3,14,'1','3',60,60,4),('122',2,14,'1','4',80,80,0),('123',2,14,'1','4',80,80,1),('125',2,14,'1','4',80,80,3),('126',2,14,'1','4',80,80,4),('127',1,11,'1','3',60,60,0),('128',1,11,'1','3',60,60,1),('130',1,11,'1','3',60,60,3),('131',1,11,'1','3',60,60,4),('132',3,11,'1','4',80,80,0),('133',3,11,'1','4',80,80,1),('135',3,11,'1','4',80,80,3),('136',3,11,'1','4',80,80,4),('137',2,11,'1','4',80,80,0),('138',2,11,'1','4',80,80,1),('140',2,11,'1','4',80,80,3),('141',2,11,'1','4',80,80,4),('142',1,16,'1','3',60,60,0),('143',1,16,'1','3',60,60,1),('145',1,16,'1','3',60,60,3),('146',1,16,'1','3',60,60,4),('147',3,16,'1','3',60,60,0),('148',3,16,'1','3',60,60,1),('150',3,16,'1','3',60,60,3),('151',3,16,'1','3',60,60,4),('152',2,16,'1','3',60,60,0),('153',2,16,'1','3',60,60,1),('155',2,16,'1','3',60,60,3),('156',2,16,'1','3',60,60,4),('157',1,15,'1','4',80,80,0),('158',1,15,'1','4',80,80,1),('160',1,15,'1','4',80,80,3),('161',1,15,'1','4',80,80,4),('162',3,15,'1','4',80,80,0),('163',3,15,'1','4',80,80,1),('165',3,15,'1','4',80,80,3),('166',3,15,'1','4',80,80,4),('167',2,15,'1','4',80,80,0),('168',2,15,'1','4',80,80,1),('170',2,15,'1','4',80,80,3),('171',2,15,'1','4',80,80,4),('172',2,5,'1','3',60,60,0),('173',2,5,'1','3',60,60,1),('175',2,5,'1','3',60,60,3),('176',2,5,'1','3',60,60,4),('177',1,5,'1','3',60,60,0),('178',1,5,'1','3',60,60,1),('180',1,5,'1','3',60,60,3),('181',1,5,'1','3',60,60,4),('182',3,5,'1','4',80,80,0),('183',3,5,'1','4',80,80,1),('185',3,5,'1','4',80,80,3),('186',3,5,'1','4',80,80,4),('187',1,19,'1','4',80,0,1),('188',1,19,'1','4',80,0,3),('189',1,19,'1','4',80,0,4),('190',2,19,'1','4',80,0,1),('191',2,19,'1','4',80,0,3),('192',2,19,'1','4',80,0,4),('193',3,19,'1','4',80,0,1),('194',3,19,'1','4',80,0,3),('195',3,19,'1','4',80,0,4);
/*!40000 ALTER TABLE `rating_option_vote_aggregated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_store`
--

DROP TABLE IF EXISTS `rating_store`;
CREATE TABLE `rating_store` (
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `RATING_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating Store';

--
-- Dumping data for table `rating_store`
--

LOCK TABLES `rating_store` WRITE;
/*!40000 ALTER TABLE `rating_store` DISABLE KEYS */;
INSERT INTO `rating_store` VALUES (1,0),(1,1),(1,3),(1,4),(2,0),(2,1),(2,3),(2,4),(3,0),(3,1),(3,3),(3,4);
/*!40000 ALTER TABLE `rating_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_title`
--

DROP TABLE IF EXISTS `rating_title`;
CREATE TABLE `rating_title` (
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Rating Label',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `RATING_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating Title';

--
-- Table structure for table `report_compared_product_index`
--

DROP TABLE IF EXISTS `report_compared_product_index`;
CREATE TABLE `report_compared_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Index Id',
  `visitor_id` int(10) unsigned DEFAULT NULL COMMENT 'Visitor Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Added At',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `REPORT_COMPARED_PRODUCT_INDEX_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  UNIQUE KEY `REPORT_COMPARED_PRODUCT_INDEX_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_STORE_ID` (`store_id`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_ADDED_AT` (`added_at`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reports Compared Product Index Table';

--
-- Table structure for table `report_event`
--

DROP TABLE IF EXISTS `report_event`;
CREATE TABLE `report_event` (
  `event_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Id',
  `logged_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Logged At',
  `event_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Event Type Id',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Object Id',
  `subject_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subject Id',
  `subtype` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Subtype',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`event_id`),
  KEY `REPORT_EVENT_EVENT_TYPE_ID` (`event_type_id`),
  KEY `REPORT_EVENT_SUBJECT_ID` (`subject_id`),
  KEY `REPORT_EVENT_OBJECT_ID` (`object_id`),
  KEY `REPORT_EVENT_SUBTYPE` (`subtype`),
  KEY `REPORT_EVENT_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=utf8 COMMENT='Reports Event Table';

--
-- Dumping data for table `report_event`
--

LOCK TABLES `report_event` WRITE;
/*!40000 ALTER TABLE `report_event` DISABLE KEYS */;
INSERT INTO `report_event` VALUES (140,'2016-10-17 07:05:19',1,'10','76',1,3),(141,'2016-10-18 01:30:34',1,'18','82',1,4),(142,'2016-10-18 01:55:20',1,'18','82',1,4),(143,'2016-10-18 01:58:46',1,'18','82',1,4),(144,'2016-10-18 01:59:27',1,'17','82',1,4),(145,'2016-10-18 02:07:35',1,'17','82',1,4),(146,'2016-10-18 03:35:13',1,'18','84',1,4),(147,'2016-11-05 03:07:59',4,'9','97',1,1),(148,'2016-11-08 01:56:28',4,'4','131',1,1),(149,'2016-11-10 09:17:24',4,'4','134',1,1),(150,'2016-11-10 09:34:00',4,'17','134',1,1),(151,'2016-11-11 01:46:06',4,'16','137',1,1),(152,'2016-11-11 01:47:14',4,'15','136',1,1),(153,'2016-11-11 08:53:52',4,'3','141',1,4),(154,'2016-11-11 08:54:13',4,'16','141',1,4),(155,'2016-11-11 10:19:32',1,'4','143',1,4),(156,'2016-11-11 10:24:16',4,'4','143',1,4),(157,'2016-11-11 10:24:47',1,'4','143',1,4),(158,'2016-11-12 03:56:01',1,'15','145',1,4),(159,'2016-11-12 04:20:58',1,'5','145',1,4),(160,'2016-11-12 04:21:32',1,'2','145',1,4),(161,'2016-11-12 04:23:40',1,'2','145',1,4),(162,'2016-11-12 04:38:51',1,'2','145',1,4),(163,'2016-11-12 04:43:25',1,'2','145',1,4),(164,'2016-11-12 05:01:00',1,'2','146',1,4),(165,'2016-11-12 05:02:46',1,'2','146',1,4),(166,'2016-11-14 06:38:00',1,'2','148',1,4),(167,'2016-11-14 06:46:57',1,'2','148',1,4),(168,'2016-11-15 08:11:04',4,'9','149',1,4),(169,'2016-11-15 09:03:37',1,'19','149',1,4),(170,'2016-11-15 09:40:28',1,'2','150',1,4),(171,'2016-11-15 10:46:02',1,'2','151',1,4),(172,'2016-11-16 01:55:36',1,'10','152',1,4),(173,'2016-11-16 01:56:26',1,'4','152',1,4),(174,'2016-11-16 01:57:09',1,'2','152',1,4),(175,'2016-11-16 02:00:31',1,'2','152',1,4),(176,'2016-11-16 02:01:29',4,'17','152',1,4),(177,'2016-11-16 02:01:35',1,'2','152',1,4),(178,'2016-11-16 02:06:21',1,'2','152',1,4),(179,'2016-11-16 02:21:08',1,'2','152',1,4),(180,'2016-11-16 02:22:49',1,'2','152',1,4),(181,'2016-11-16 02:31:16',1,'2','153',1,4),(182,'2016-11-16 02:33:10',1,'2','153',1,4),(183,'2016-11-16 02:40:43',1,'2','153',1,4),(184,'2016-11-16 02:45:16',1,'2','153',1,4),(185,'2016-11-16 03:12:00',1,'2','153',1,4),(186,'2016-11-16 03:44:12',4,'3','154',1,4),(187,'2016-11-16 04:09:20',1,'2','154',1,4),(188,'2016-11-16 04:20:33',1,'2','154',1,4),(189,'2016-11-16 04:27:34',1,'2','154',1,4),(190,'2016-11-16 04:39:15',1,'15','155',1,1),(191,'2016-11-16 04:39:50',1,'2','155',1,1),(192,'2016-11-16 04:41:24',1,'2','155',1,3),(193,'2016-11-16 06:24:07',4,'15','156',1,3),(194,'2016-11-16 07:06:00',1,'2','156',1,4),(195,'2016-11-16 08:43:36',1,'2','159',1,4),(196,'2016-11-16 08:44:42',1,'2','159',1,1),(197,'2016-11-16 08:46:11',1,'2','159',1,1),(198,'2016-11-16 08:49:38',1,'2','159',1,1),(199,'2016-11-16 08:53:54',1,'2','159',1,1),(200,'2016-11-16 08:57:47',1,'2','159',1,1),(201,'2016-11-16 09:20:15',1,'2','159',1,1),(202,'2016-11-16 09:27:22',1,'2','159',1,4),(203,'2016-11-17 01:24:03',1,'2','160',1,4),(204,'2016-11-17 07:45:13',4,'19','162',1,4),(205,'2016-11-17 07:45:49',4,'14','162',1,4),(206,'2016-11-17 09:56:23',4,'17','164',1,4),(207,'2016-11-17 10:29:19',4,'3','164',1,1),(208,'2016-11-17 10:29:19',4,'9','165',1,1),(209,'2016-11-22 10:02:48',4,'4','173',1,1),(210,'2016-11-22 10:15:27',1,'17','173',1,1),(211,'2016-11-22 10:15:52',1,'17','173',1,1),(212,'2016-11-22 10:21:54',1,'4','173',1,1),(213,'2016-11-22 10:22:02',1,'5','173',1,1),(214,'2016-11-23 09:54:55',4,'10','180',1,1),(215,'2016-11-23 09:55:04',4,'17','180',1,1),(216,'2016-11-23 09:55:08',4,'18','180',1,1),(217,'2016-11-23 10:01:38',1,'10','180',1,1),(218,'2016-11-23 10:02:03',1,'2','180',1,1),(219,'2016-11-23 10:02:25',1,'17','180',1,1),(220,'2016-11-23 10:02:51',1,'2','180',1,1),(221,'2016-11-23 10:03:24',1,'4','180',1,1),(222,'2016-11-23 10:03:48',1,'2','180',1,1),(223,'2016-11-23 10:18:01',1,'2','180',1,1),(224,'2016-11-23 10:18:29',1,'2','180',1,3),(225,'2016-11-23 10:20:47',1,'2','180',1,3),(226,'2016-11-23 13:55:29',1,'10','181',1,1),(227,'2016-11-23 13:55:55',1,'2','181',1,1),(228,'2016-11-23 14:44:23',1,'2','181',1,1),(229,'2016-11-24 02:48:09',1,'11','186',1,1),(230,'2016-11-24 02:51:22',1,'11','186',1,1),(231,'2016-11-24 03:39:21',1,'19','187',1,1),(232,'2016-11-24 03:40:47',1,'5','187',1,1),(233,'2016-11-24 03:42:18',1,'2','187',1,1),(234,'2016-11-24 03:59:02',1,'2','187',1,1),(235,'2016-11-24 04:12:16',4,'2','187',1,1),(236,'2016-11-24 04:20:22',1,'2','187',1,1),(237,'2016-11-24 04:21:08',1,'2','187',1,1),(238,'2016-11-24 04:32:46',4,'14','189',1,3),(239,'2016-11-24 06:15:28',1,'2','190',1,1),(240,'2016-11-24 06:35:10',1,'2','191',1,1),(241,'2016-11-24 06:53:15',1,'2','191',1,1),(242,'2016-11-24 06:59:59',1,'2','191',1,1),(243,'2016-11-24 07:00:47',1,'2','191',1,1),(244,'2016-11-24 07:05:10',1,'2','191',1,1),(245,'2016-11-24 07:08:13',1,'2','191',1,1),(246,'2016-11-24 07:09:03',1,'2','191',1,1),(247,'2016-11-24 07:11:28',1,'2','191',1,1),(248,'2016-11-24 07:11:54',1,'2','191',1,1),(249,'2016-11-24 07:13:18',1,'2','191',1,1),(250,'2016-11-24 07:14:00',1,'2','191',1,1),(251,'2016-11-24 07:16:42',1,'2','191',1,1),(252,'2016-11-24 07:17:09',1,'2','191',1,1),(253,'2016-11-24 07:18:17',1,'2','191',1,1),(254,'2016-11-24 07:19:22',1,'2','191',1,1),(255,'2016-11-24 07:23:00',4,'2','191',1,1),(256,'2016-11-24 07:28:36',4,'19','191',1,1),(257,'2016-11-24 07:28:40',1,'2','191',1,1),(258,'2016-11-24 07:44:25',1,'2','192',1,1),(259,'2016-11-24 07:46:06',1,'2','192',1,1),(260,'2016-11-24 07:47:10',4,'14','192',1,1),(261,'2016-11-24 07:47:28',4,'5','192',1,1),(262,'2016-11-24 07:47:35',1,'2','192',1,1),(263,'2016-11-24 07:48:58',1,'2','192',1,3),(264,'2016-11-24 08:30:17',1,'2','192',1,3),(265,'2016-11-24 08:31:57',1,'2','193',1,1),(266,'2016-11-24 08:35:02',1,'2','193',1,1),(267,'2016-11-24 08:37:10',1,'2','193',1,1),(268,'2016-11-24 08:37:56',1,'2','193',1,3),(269,'2016-11-24 08:40:13',1,'2','193',1,4),(270,'2016-11-24 08:41:26',4,'18','193',1,4),(271,'2016-11-24 08:41:32',4,'11','193',1,4),(272,'2016-11-24 08:41:35',1,'2','193',1,4),(273,'2016-11-24 08:51:08',4,'19','193',1,1),(274,'2016-11-24 08:56:48',1,'2','193',1,4),(275,'2016-11-24 09:09:04',1,'14','195',1,1),(276,'2016-11-24 09:12:41',1,'17','196',1,1),(277,'2016-11-24 09:22:38',1,'19','196',1,1),(278,'2016-11-24 09:25:44',1,'17','196',1,1),(279,'2016-11-24 09:26:36',1,'4','196',1,1),(280,'2016-11-24 09:30:12',1,'2','196',1,1),(281,'2016-11-24 09:33:59',1,'2','195',1,1),(282,'2016-11-24 09:41:09',1,'2','195',1,1),(283,'2016-11-24 09:46:50',1,'2','195',1,1),(284,'2016-11-24 09:48:10',1,'2','195',1,1),(285,'2016-11-24 09:50:11',1,'2','195',1,1),(286,'2016-11-24 09:54:00',1,'2','195',1,1),(287,'2016-11-24 09:56:36',1,'2','195',1,1),(288,'2016-11-24 09:57:53',1,'2','195',1,3),(289,'2016-11-25 03:35:54',1,'14','199',1,3),(290,'2016-11-25 04:00:24',1,'14','199',1,4),(291,'2016-11-26 03:15:11',4,'16','201',1,1),(292,'2016-11-26 03:15:16',4,'9','201',1,1),(293,'2016-11-26 03:15:31',4,'19','201',1,1),(294,'2016-11-26 03:22:15',4,'4','201',1,1),(295,'2016-11-26 03:27:08',1,'4','201',1,1),(296,'2016-11-26 03:37:09',1,'2','201',1,1),(297,'2016-11-26 03:37:59',4,'2','201',1,1),(298,'2016-11-26 03:40:31',4,'16','201',1,1),(299,'2016-11-26 03:40:37',1,'2','201',1,1),(300,'2016-11-26 04:18:43',1,'2','206',1,1),(301,'2016-11-26 04:23:24',1,'2','206',1,1),(302,'2016-11-26 04:26:32',1,'2','206',1,1),(303,'2016-11-26 04:34:38',1,'19','207',1,3),(304,'2016-11-26 04:34:44',1,'14','207',1,3),(305,'2016-11-26 04:35:08',1,'11','207',1,4),(306,'2016-11-26 04:35:14',1,'18','207',1,4),(307,'2016-11-26 04:40:37',1,'14','207',1,4),(308,'2016-11-26 04:40:43',1,'5','207',1,4),(309,'2016-11-26 04:41:10',1,'19','206',1,1),(310,'2016-11-26 04:41:29',1,'4','206',1,1),(311,'2016-11-26 04:41:49',1,'2','206',1,1),(312,'2016-11-26 04:46:12',1,'2','207',1,4),(313,'2016-11-26 04:58:14',1,'2','206',1,1),(314,'2016-11-26 05:01:55',1,'2','206',1,1),(315,'2016-11-26 05:06:54',1,'2','206',1,1),(316,'2016-11-27 14:45:39',4,'18','208',1,1),(317,'2016-11-28 01:48:18',1,'5','211',1,1),(318,'2016-11-28 01:48:38',1,'9','211',1,1),(319,'2016-11-28 01:56:59',1,'18','210',1,1),(320,'2016-11-28 02:04:05',1,'18','210',1,3),(321,'2016-11-28 02:05:16',1,'18','210',1,3),(322,'2016-11-28 02:05:39',1,'18','210',1,4),(323,'2016-11-28 02:10:16',4,'18','210',1,4),(324,'2016-11-28 02:57:34',1,'11','210',1,1);
/*!40000 ALTER TABLE `report_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_event_types`
--

DROP TABLE IF EXISTS `report_event_types`;
CREATE TABLE `report_event_types` (
  `event_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Type Id',
  `event_name` varchar(64) NOT NULL COMMENT 'Event Name',
  `customer_login` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Login',
  PRIMARY KEY (`event_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Reports Event Type Table';

--
-- Dumping data for table `report_event_types`
--

LOCK TABLES `report_event_types` WRITE;
/*!40000 ALTER TABLE `report_event_types` DISABLE KEYS */;
INSERT INTO `report_event_types` VALUES (1,'catalog_product_view',0),(2,'sendfriend_product',0),(3,'catalog_product_compare_add_product',0),(4,'checkout_cart_add_product',0),(5,'wishlist_add_product',0),(6,'wishlist_share',0);
/*!40000 ALTER TABLE `report_event_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_viewed_product_aggregated_daily`
--

DROP TABLE IF EXISTS `report_viewed_product_aggregated_daily`;
CREATE TABLE `report_viewed_product_aggregated_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_DAILY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Daily';

--
-- Table structure for table `report_viewed_product_aggregated_monthly`
--

DROP TABLE IF EXISTS `report_viewed_product_aggregated_monthly`;
CREATE TABLE `report_viewed_product_aggregated_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_MONTHLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Monthly';

--
-- Table structure for table `report_viewed_product_aggregated_yearly`
--

DROP TABLE IF EXISTS `report_viewed_product_aggregated_yearly`;
CREATE TABLE `report_viewed_product_aggregated_yearly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_YEARLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Yearly';

--
-- Table structure for table `report_viewed_product_index`
--

DROP TABLE IF EXISTS `report_viewed_product_index`;
CREATE TABLE `report_viewed_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Index Id',
  `visitor_id` int(10) unsigned DEFAULT NULL COMMENT 'Visitor Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Added At',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `REPORT_VIEWED_PRODUCT_INDEX_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  UNIQUE KEY `REPORT_VIEWED_PRODUCT_INDEX_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_ADDED_AT` (`added_at`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8 COMMENT='Reports Viewed Product Index Table';

--
-- Dumping data for table `report_viewed_product_index`
--

LOCK TABLES `report_viewed_product_index` WRITE;
/*!40000 ALTER TABLE `report_viewed_product_index` DISABLE KEYS */;
INSERT INTO `report_viewed_product_index` VALUES (1,'36',NULL,'2',NULL,'2016-10-11 09:39:07'),(2,'39',NULL,'14',NULL,'2016-10-12 02:19:04'),(3,'40',NULL,'14',NULL,'2016-10-12 03:18:49'),(4,'45',NULL,'2',NULL,'2016-10-12 08:39:20'),(6,'45',NULL,'10',NULL,'2016-10-12 08:46:07'),(7,'45',NULL,'19',NULL,'2016-10-12 09:14:58'),(13,'46',NULL,'19',NULL,'2016-10-12 09:38:16'),(14,'47',NULL,'2',NULL,'2016-10-13 01:00:22'),(25,'49',NULL,'2',NULL,'2016-10-13 01:58:22'),(38,'50',NULL,'2',NULL,'2016-10-13 02:59:47'),(46,'51',NULL,'2',NULL,'2016-10-13 04:03:34'),(62,'52',NULL,'2',NULL,'2016-10-13 06:20:18'),(70,'52',NULL,'14',NULL,'2016-10-13 07:08:52'),(71,'53',NULL,'19',NULL,'2016-10-13 07:27:15'),(72,'53',NULL,'2',NULL,'2016-10-13 07:31:00'),(79,'54',NULL,'17',NULL,'2016-10-13 09:13:12'),(80,'58',NULL,'2',NULL,'2016-10-14 02:34:31'),(83,'61',NULL,'14',NULL,'2016-10-14 06:31:56'),(84,'69',NULL,'2',NULL,'2016-10-15 03:50:28'),(89,'69',NULL,'16',NULL,'2016-10-15 04:14:23'),(92,'70',NULL,'15',NULL,'2016-10-15 04:22:53'),(93,'71',NULL,'2',NULL,'2016-10-15 10:01:49'),(94,'72',NULL,'2',NULL,'2016-10-17 01:26:11'),(95,'76',NULL,'10',3,'2016-10-17 07:05:18'),(96,'82',NULL,'18',4,'2016-10-18 01:30:33'),(99,'82',NULL,'17',4,'2016-10-18 01:59:27'),(101,'84',NULL,'18',4,'2016-10-18 03:35:13'),(102,'143',NULL,'4',4,'2016-11-11 10:19:31'),(103,'145',NULL,'15',4,'2016-11-12 03:56:00'),(104,'145',NULL,'5',4,'2016-11-12 04:20:58'),(105,'145',NULL,'2',4,'2016-11-12 04:21:32'),(109,'146',NULL,'2',4,'2016-11-12 05:01:00'),(110,'148',NULL,'2',4,'2016-11-14 06:37:59'),(111,'149',NULL,'19',4,'2016-11-15 09:03:36'),(112,'150',NULL,'2',4,'2016-11-15 09:40:28'),(113,'151',NULL,'2',4,'2016-11-15 10:46:02'),(114,'152',NULL,'10',4,'2016-11-16 01:55:35'),(115,'152',NULL,'4',4,'2016-11-16 01:56:25'),(116,'152',NULL,'2',4,'2016-11-16 01:57:09'),(122,'153',NULL,'2',4,'2016-11-16 02:31:16'),(127,'154',NULL,'2',4,'2016-11-16 04:09:19'),(130,'155',NULL,'15',1,'2016-11-16 04:39:15'),(131,'155',NULL,'2',3,'2016-11-16 04:39:49'),(133,'156',NULL,'2',4,'2016-11-16 07:06:00'),(134,'159',NULL,'2',4,'2016-11-16 08:43:36'),(135,'160',NULL,'2',4,'2016-11-17 01:24:01'),(136,'173',NULL,'17',1,'2016-11-22 10:15:27'),(138,'173',NULL,'4',1,'2016-11-22 10:21:54'),(139,'173',NULL,'5',1,'2016-11-22 10:22:02'),(140,'180',NULL,'10',1,'2016-11-23 10:01:37'),(141,'180',NULL,'2',3,'2016-11-23 10:02:03'),(142,'180',NULL,'17',1,'2016-11-23 10:02:25'),(144,'180',NULL,'4',1,'2016-11-23 10:03:24'),(145,'181',NULL,'10',1,'2016-11-23 13:55:29'),(146,'181',NULL,'2',1,'2016-11-23 13:55:55'),(147,'186',NULL,'11',1,'2016-11-24 02:48:09'),(149,'187',NULL,'19',1,'2016-11-24 03:39:20'),(150,'187',NULL,'5',1,'2016-11-24 03:40:46'),(151,'187',NULL,'2',1,'2016-11-24 03:42:18'),(155,'190',NULL,'2',1,'2016-11-24 06:15:27'),(156,'191',NULL,'2',1,'2016-11-24 06:35:10'),(172,'192',NULL,'2',3,'2016-11-24 07:44:25'),(177,'193',NULL,'2',4,'2016-11-24 08:31:56'),(184,'195',NULL,'14',1,'2016-11-24 09:09:04'),(185,'196',NULL,'17',1,'2016-11-24 09:12:40'),(186,'196',NULL,'19',1,'2016-11-24 09:22:38'),(188,'196',NULL,'4',1,'2016-11-24 09:26:36'),(189,'196',NULL,'2',1,'2016-11-24 09:30:12'),(190,'195',NULL,'2',3,'2016-11-24 09:33:59'),(191,'199',NULL,'14',4,'2016-11-25 03:35:54'),(192,'201',NULL,'4',1,'2016-11-26 03:27:08'),(193,'201',NULL,'2',1,'2016-11-26 03:37:09'),(195,'206',NULL,'2',1,'2016-11-26 04:18:43'),(198,'207',NULL,'19',3,'2016-11-26 04:34:38'),(199,'207',NULL,'14',4,'2016-11-26 04:34:43'),(200,'207',NULL,'11',4,'2016-11-26 04:35:08'),(201,'207',NULL,'18',4,'2016-11-26 04:35:14'),(203,'207',NULL,'5',4,'2016-11-26 04:40:43'),(204,'206',NULL,'19',1,'2016-11-26 04:41:10'),(205,'206',NULL,'4',1,'2016-11-26 04:41:29'),(207,'207',NULL,'2',4,'2016-11-26 04:46:12'),(208,'211',NULL,'5',1,'2016-11-28 01:48:17'),(209,'211',NULL,'9',1,'2016-11-28 01:48:38'),(210,'210',NULL,'18',4,'2016-11-28 01:56:59'),(214,'210',NULL,'11',1,'2016-11-28 02:57:32');
/*!40000 ALTER TABLE `report_viewed_product_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporting_counts`
--

DROP TABLE IF EXISTS `reporting_counts`;
CREATE TABLE `reporting_counts` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'Item Reported',
  `count` int(10) unsigned DEFAULT NULL COMMENT 'Count Value',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for all count related events generated via the cron job';

--
-- Table structure for table `reporting_module_status`
--

DROP TABLE IF EXISTS `reporting_module_status`;
CREATE TABLE `reporting_module_status` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Module Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Module Name',
  `active` varchar(255) DEFAULT NULL COMMENT 'Module Active Status',
  `setup_version` varchar(255) DEFAULT NULL COMMENT 'Module Version',
  `state` varchar(255) DEFAULT NULL COMMENT 'Module State',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module Status Table';

--
-- Table structure for table `reporting_orders`
--

DROP TABLE IF EXISTS `reporting_orders`;
CREATE TABLE `reporting_orders` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `total` decimal(20,2) DEFAULT NULL COMMENT 'Total From Store',
  `total_base` decimal(20,2) DEFAULT NULL COMMENT 'Total From Base Currency',
  `item_count` int(10) unsigned NOT NULL COMMENT 'Line Item Count',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for all orders';

--
-- Table structure for table `reporting_system_updates`
--

DROP TABLE IF EXISTS `reporting_system_updates`;
CREATE TABLE `reporting_system_updates` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'Update Type',
  `action` varchar(255) DEFAULT NULL COMMENT 'Action Performed',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for system updates';

--
-- Table structure for table `reporting_users`
--

DROP TABLE IF EXISTS `reporting_users`;
CREATE TABLE `reporting_users` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'User Type',
  `action` varchar(255) DEFAULT NULL COMMENT 'Action Performed',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for user actions';

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `review_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Review create date',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity id',
  `entity_pk_value` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `status_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Status code',
  PRIMARY KEY (`review_id`),
  KEY `REVIEW_ENTITY_ID` (`entity_id`),
  KEY `REVIEW_STATUS_ID` (`status_id`),
  KEY `REVIEW_ENTITY_PK_VALUE` (`entity_pk_value`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Review base information';

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,'2016-10-12 08:33:27',1,'2',1),(2,'2016-10-12 08:40:38',1,'2',2),(3,'2016-10-12 08:50:10',1,'3',1),(4,'2016-10-12 08:55:47',1,'4',1),(5,'2016-10-12 09:44:38',1,'19',2),(6,'2016-10-18 01:32:03',1,'18',1),(7,'2016-10-18 01:33:28',1,'17',1),(8,'2016-10-18 01:34:58',1,'5',1),(9,'2016-10-18 01:37:47',1,'9',1),(10,'2016-10-18 01:39:05',1,'10',1),(11,'2016-10-18 01:50:20',1,'14',1),(12,'2016-10-18 01:52:19',1,'11',1),(13,'2016-10-18 01:53:25',1,'16',1),(14,'2016-10-18 01:54:36',1,'15',1);
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_detail`
--

DROP TABLE IF EXISTS `review_detail`;
CREATE TABLE `review_detail` (
  `detail_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review detail id',
  `review_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Review id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store id',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `detail` text NOT NULL COMMENT 'Detail description',
  `nickname` varchar(128) NOT NULL COMMENT 'User nickname',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  PRIMARY KEY (`detail_id`),
  KEY `REVIEW_DETAIL_REVIEW_ID` (`review_id`),
  KEY `REVIEW_DETAIL_STORE_ID` (`store_id`),
  KEY `REVIEW_DETAIL_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Review detail information';

--
-- Dumping data for table `review_detail`
--

LOCK TABLES `review_detail` WRITE;
/*!40000 ALTER TABLE `review_detail` DISABLE KEYS */;
INSERT INTO `review_detail` VALUES (1,1,0,'plaza','test1','plaza',NULL),(2,2,NULL,'plaza1','test2','plaza1',NULL),(3,3,0,'plaza','test3','plaza',NULL),(4,4,0,'plaza','test4','plaza',NULL),(5,5,NULL,'plaza','test6','plaza',NULL),(6,6,0,'plaza','test7','plaza',NULL),(7,7,0,'plaza','test8','plaza',NULL),(8,8,0,'plaza','test9','plaza',NULL),(9,9,0,'plaza','test10','plaza',NULL),(10,10,0,'plaza','test11','plaza',NULL),(11,11,0,'plaza','test12','plaza',NULL),(12,12,0,'plaza','test13','plaza',NULL),(13,13,0,'plaza','test14','plaza',NULL),(14,14,0,'plaza','test15','plaza',NULL);
/*!40000 ALTER TABLE `review_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_entity`
--

DROP TABLE IF EXISTS `review_entity`;
CREATE TABLE `review_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review entity id',
  `entity_code` varchar(32) NOT NULL COMMENT 'Review entity code',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review entities';

--
-- Dumping data for table `review_entity`
--

LOCK TABLES `review_entity` WRITE;
/*!40000 ALTER TABLE `review_entity` DISABLE KEYS */;
INSERT INTO `review_entity` VALUES (1,'product'),(2,'customer'),(3,'category');
/*!40000 ALTER TABLE `review_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_entity_summary`
--

DROP TABLE IF EXISTS `review_entity_summary`;
CREATE TABLE `review_entity_summary` (
  `primary_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Summary review entity id',
  `entity_pk_value` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Product id',
  `entity_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Entity type id',
  `reviews_count` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Qty of reviews',
  `rating_summary` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Summarized rating',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`primary_id`),
  KEY `REVIEW_ENTITY_SUMMARY_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COMMENT='Review aggregates';

--
-- Dumping data for table `review_entity_summary`
--

LOCK TABLES `review_entity_summary` WRITE;
/*!40000 ALTER TABLE `review_entity_summary` DISABLE KEYS */;
INSERT INTO `review_entity_summary` VALUES (2,2,1,1,60,1),(3,2,1,1,60,3),(4,2,1,1,60,4),(5,2,1,1,60,0),(6,3,1,1,60,0),(7,3,1,1,60,1),(9,3,1,1,60,3),(10,3,1,1,60,4),(11,4,1,1,73,0),(12,4,1,1,73,1),(14,4,1,1,73,3),(15,4,1,1,73,4),(17,19,1,0,0,1),(18,19,1,0,0,3),(19,19,1,0,0,4),(20,18,1,1,67,0),(21,18,1,1,67,1),(23,18,1,1,67,3),(24,18,1,1,67,4),(25,17,1,1,87,0),(26,17,1,1,87,1),(28,17,1,1,87,3),(29,17,1,1,87,4),(30,5,1,1,67,4),(31,5,1,1,67,1),(33,5,1,1,67,3),(34,9,1,1,73,0),(35,9,1,1,73,1),(37,9,1,1,73,3),(38,9,1,1,73,4),(39,10,1,1,53,0),(40,10,1,1,53,1),(42,10,1,1,53,3),(43,10,1,1,53,4),(44,14,1,1,67,4),(45,14,1,1,67,1),(47,14,1,1,67,3),(48,14,1,1,67,0),(49,11,1,1,73,0),(50,11,1,1,73,1),(52,11,1,1,73,3),(53,11,1,1,73,4),(54,16,1,1,60,0),(55,16,1,1,60,1),(57,16,1,1,60,3),(58,16,1,1,60,4),(59,15,1,1,80,0),(60,15,1,1,80,1),(62,15,1,1,80,3),(63,15,1,1,80,4),(64,5,1,1,67,0);
/*!40000 ALTER TABLE `review_entity_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_status`
--

DROP TABLE IF EXISTS `review_status`;
CREATE TABLE `review_status` (
  `status_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Status id',
  `status_code` varchar(32) NOT NULL COMMENT 'Status code',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review statuses';

--
-- Dumping data for table `review_status`
--

LOCK TABLES `review_status` WRITE;
/*!40000 ALTER TABLE `review_status` DISABLE KEYS */;
INSERT INTO `review_status` VALUES (1,'Approved'),(2,'Pending'),(3,'Not Approved');
/*!40000 ALTER TABLE `review_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_store`
--

DROP TABLE IF EXISTS `review_store`;
CREATE TABLE `review_store` (
  `review_id` bigint(20) unsigned NOT NULL COMMENT 'Review Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`review_id`,`store_id`),
  KEY `REVIEW_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review Store';

--
-- Dumping data for table `review_store`
--

LOCK TABLES `review_store` WRITE;
/*!40000 ALTER TABLE `review_store` DISABLE KEYS */;
INSERT INTO `review_store` VALUES (1,0),(1,1),(1,3),(1,4),(2,0),(2,1),(2,3),(2,4),(3,0),(3,1),(3,3),(3,4),(4,0),(4,1),(4,3),(4,4),(5,0),(5,1),(5,3),(5,4),(6,0),(6,1),(6,3),(6,4),(7,0),(7,1),(7,3),(7,4),(8,0),(8,1),(8,3),(8,4),(9,0),(9,1),(9,3),(9,4),(10,0),(10,1),(10,3),(10,4),(11,0),(11,1),(11,3),(11,4),(12,0),(12,1),(12,3),(12,4),(13,0),(13,1),(13,3),(13,4),(14,0),(14,1),(14,3),(14,4);
/*!40000 ALTER TABLE `review_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_bestsellers_aggregated_daily`
--

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_daily`;
CREATE TABLE `sales_bestsellers_aggregated_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Daily';

--
-- Table structure for table `sales_bestsellers_aggregated_monthly`
--

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_monthly`;
CREATE TABLE `sales_bestsellers_aggregated_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Monthly';

--
-- Table structure for table `sales_bestsellers_aggregated_yearly`
--

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_yearly`;
CREATE TABLE `sales_bestsellers_aggregated_yearly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Yearly';

--
-- Table structure for table `sales_creditmemo`
--

DROP TABLE IF EXISTS `sales_creditmemo`;
CREATE TABLE `sales_creditmemo` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `creditmemo_status` int(11) DEFAULT NULL COMMENT 'Creditmemo Status',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'Invoice Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_CREDITMEMO_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_CREDITMEMO_STORE_ID` (`store_id`),
  KEY `SALES_CREDITMEMO_ORDER_ID` (`order_id`),
  KEY `SALES_CREDITMEMO_CREDITMEMO_STATUS` (`creditmemo_status`),
  KEY `SALES_CREDITMEMO_STATE` (`state`),
  KEY `SALES_CREDITMEMO_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_UPDATED_AT` (`updated_at`),
  KEY `SALES_CREDITMEMO_SEND_EMAIL` (`send_email`),
  KEY `SALES_CREDITMEMO_EMAIL_SENT` (`email_sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo';

--
-- Table structure for table `sales_creditmemo_comment`
--

DROP TABLE IF EXISTS `sales_creditmemo_comment`;
CREATE TABLE `sales_creditmemo_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_CREDITMEMO_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_COMMENT_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Comment';

--
-- Table structure for table `sales_creditmemo_grid`
--

DROP TABLE IF EXISTS `sales_creditmemo_grid`;
CREATE TABLE `sales_creditmemo_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `state` int(11) DEFAULT NULL COMMENT 'Status',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `order_status` varchar(32) DEFAULT NULL COMMENT 'Order Status',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `order_base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Order Grand Total',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_CREDITMEMO_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_CREDITMEMO_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_CREDITMEMO_GRID_STATE` (`state`),
  KEY `SALES_CREDITMEMO_GRID_BILLING_NAME` (`billing_name`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_STATUS` (`order_status`),
  KEY `SALES_CREDITMEMO_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `SALES_CREDITMEMO_GRID_STORE_ID` (`store_id`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_BASE_GRAND_TOTAL` (`order_base_grand_total`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_ID` (`order_id`),
  FULLTEXT KEY `FTI_32B7BA885941A8254EE84AE650ABDC86` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Grid';

--
-- Table structure for table `sales_creditmemo_item`
--

DROP TABLE IF EXISTS `sales_creditmemo_item`;
CREATE TABLE `sales_creditmemo_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `tax_ratio` text COMMENT 'Ratio of tax in the creditmemo item over tax of the order item',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_CREDITMEMO_ITEM_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Item';

--
-- Table structure for table `sales_invoice`
--

DROP TABLE IF EXISTS `sales_invoice`;
CREATE TABLE `sales_invoice` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `is_used_for_refund` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Used For Refund',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `can_void_flag` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Void Flag',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_INVOICE_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_INVOICE_STORE_ID` (`store_id`),
  KEY `SALES_INVOICE_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_INVOICE_ORDER_ID` (`order_id`),
  KEY `SALES_INVOICE_STATE` (`state`),
  KEY `SALES_INVOICE_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_UPDATED_AT` (`updated_at`),
  KEY `SALES_INVOICE_SEND_EMAIL` (`send_email`),
  KEY `SALES_INVOICE_EMAIL_SENT` (`email_sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice';

--
-- Table structure for table `sales_invoice_comment`
--

DROP TABLE IF EXISTS `sales_invoice_comment`;
CREATE TABLE `sales_invoice_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_INVOICE_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_COMMENT_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Comment';

--
-- Table structure for table `sales_invoice_grid`
--

DROP TABLE IF EXISTS `sales_invoice_grid`;
CREATE TABLE `sales_invoice_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(128) DEFAULT NULL COMMENT 'Payment Method',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_INVOICE_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_INVOICE_GRID_STORE_ID` (`store_id`),
  KEY `SALES_INVOICE_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_INVOICE_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `SALES_INVOICE_GRID_ORDER_ID` (`order_id`),
  KEY `SALES_INVOICE_GRID_STATE` (`state`),
  KEY `SALES_INVOICE_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_INVOICE_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_INVOICE_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_INVOICE_GRID_BILLING_NAME` (`billing_name`),
  FULLTEXT KEY `FTI_95D9C924DD6A8734EB8B5D01D60F90DE` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Grid';

--
-- Table structure for table `sales_invoice_item`
--

DROP TABLE IF EXISTS `sales_invoice_item`;
CREATE TABLE `sales_invoice_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `tax_ratio` text COMMENT 'Ratio of tax invoiced over tax of the order item',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_INVOICE_ITEM_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Item';

--
-- Table structure for table `sales_invoiced_aggregated`
--

DROP TABLE IF EXISTS `sales_invoiced_aggregated`;
CREATE TABLE `sales_invoiced_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_INVOICED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_INVOICED_AGGREGATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated';

--
-- Table structure for table `sales_invoiced_aggregated_order`
--

DROP TABLE IF EXISTS `sales_invoiced_aggregated_order`;
CREATE TABLE `sales_invoiced_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_INVOICED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_INVOICED_AGGREGATED_ORDER_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated Order';

--
-- Table structure for table `sales_order`
--

DROP TABLE IF EXISTS `sales_order`;
CREATE TABLE `sales_order` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `state` varchar(32) DEFAULT NULL COMMENT 'State',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `protect_code` varchar(255) DEFAULT NULL COMMENT 'Protect Code',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Canceled',
  `base_discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Invoiced',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `base_shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Canceled',
  `base_shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Invoiced',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Refunded',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `base_subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Canceled',
  `base_subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Invoiced',
  `base_subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Refunded',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Canceled',
  `base_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Invoiced',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `base_total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Canceled',
  `base_total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced',
  `base_total_invoiced_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced Cost',
  `base_total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Offline Refunded',
  `base_total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Online Refunded',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `base_total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Qty Ordered',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Canceled',
  `discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Invoiced',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Canceled',
  `shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Invoiced',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Refunded',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Canceled',
  `subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Invoiced',
  `subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Refunded',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Tax Invoiced',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Total Canceled',
  `total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Total Invoiced',
  `total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Offline Refunded',
  `total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Online Refunded',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty Ordered',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  `can_ship_partially` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially',
  `can_ship_partially_item` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially Item',
  `customer_is_guest` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Is Guest',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `edit_increment` int(11) DEFAULT NULL COMMENT 'Edit Increment',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `forced_shipment_with_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Forced Do Shipment With Invoice',
  `payment_auth_expiration` int(11) DEFAULT NULL COMMENT 'Payment Authorization Expiration',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `quote_id` int(11) DEFAULT NULL COMMENT 'Quote Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `base_total_due` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Due',
  `payment_authorization_amount` decimal(12,4) DEFAULT NULL COMMENT 'Payment Authorization Amount',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `total_due` decimal(12,4) DEFAULT NULL COMMENT 'Total Due',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `increment_id` varchar(32) DEFAULT NULL COMMENT 'Increment Id',
  `applied_rule_ids` varchar(128) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_firstname` varchar(128) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_lastname` varchar(128) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_middlename` varchar(128) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_prefix` varchar(32) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_suffix` varchar(32) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_taxvat` varchar(32) DEFAULT NULL COMMENT 'Customer Taxvat',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `ext_customer_id` varchar(32) DEFAULT NULL COMMENT 'Ext Customer Id',
  `ext_order_id` varchar(32) DEFAULT NULL COMMENT 'Ext Order Id',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `hold_before_state` varchar(32) DEFAULT NULL COMMENT 'Hold Before State',
  `hold_before_status` varchar(32) DEFAULT NULL COMMENT 'Hold Before Status',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `original_increment_id` varchar(32) DEFAULT NULL COMMENT 'Original Increment Id',
  `relation_child_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Id',
  `relation_child_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Real Id',
  `relation_parent_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Id',
  `relation_parent_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Real Id',
  `remote_ip` varchar(32) DEFAULT NULL COMMENT 'Remote Ip',
  `shipping_method` varchar(32) DEFAULT NULL COMMENT 'Shipping Method',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `store_name` varchar(32) DEFAULT NULL COMMENT 'Store Name',
  `x_forwarded_for` varchar(32) DEFAULT NULL COMMENT 'X Forwarded For',
  `customer_note` text COMMENT 'Customer Note',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `total_item_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Total Item Count',
  `customer_gender` int(11) DEFAULT NULL COMMENT 'Customer Gender',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
  `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
  `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
  `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `coupon_rule_name` varchar(255) DEFAULT NULL COMMENT 'Coupon Sales Rule Name',
  `paypal_ipn_customer_notified` int(11) DEFAULT '0' COMMENT 'Paypal Ipn Customer Notified',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_ORDER_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_ORDER_STATUS` (`status`),
  KEY `SALES_ORDER_STATE` (`state`),
  KEY `SALES_ORDER_STORE_ID` (`store_id`),
  KEY `SALES_ORDER_CREATED_AT` (`created_at`),
  KEY `SALES_ORDER_CUSTOMER_ID` (`customer_id`),
  KEY `SALES_ORDER_EXT_ORDER_ID` (`ext_order_id`),
  KEY `SALES_ORDER_QUOTE_ID` (`quote_id`),
  KEY `SALES_ORDER_UPDATED_AT` (`updated_at`),
  KEY `SALES_ORDER_SEND_EMAIL` (`send_email`),
  KEY `SALES_ORDER_EMAIL_SENT` (`email_sent`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order';

--
-- Dumping data for table `sales_order`
--

LOCK TABLES `sales_order` WRITE;
/*!40000 ALTER TABLE `sales_order` DISABLE KEYS */;
INSERT INTO `sales_order` VALUES ('1','new','pending',NULL,'17955c','Flat Rate - Fixed',0,NULL,NULL,'0.0000',NULL,NULL,NULL,'300.0000','20.0000',NULL,NULL,NULL,'0.0000',NULL,'280.0000',NULL,NULL,NULL,'0.0000',NULL,NULL,NULL,'1.0000','1.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0.0000',NULL,NULL,NULL,'300.0000','20.0000',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','280.0000',NULL,NULL,NULL,'0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4.0000',NULL,NULL,NULL,1,1,'2',0,NULL,1,1,NULL,NULL,NULL,'9','1',NULL,NULL,NULL,NULL,'0.0000','280.0000','300.0000',NULL,'0.0000','280.0000','300.0000','0.0000',NULL,'000000001',NULL,'USD','plaza@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'USD',NULL,NULL,'USD',NULL,NULL,NULL,NULL,NULL,'::1','flatrate_flatrate','USD','Main Website\r\nMain Website Store',NULL,NULL,'2016-10-14 06:43:32','2016-10-14 06:44:03',4,NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,'20.0000','20.0000',NULL,'0',NULL);
/*!40000 ALTER TABLE `sales_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_address`
--

DROP TABLE IF EXISTS `sales_order_address`;
CREATE TABLE `sales_order_address` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `customer_address_id` int(11) DEFAULT NULL COMMENT 'Customer Address Id',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `region_id` int(11) DEFAULT NULL COMMENT 'Region Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `region` varchar(255) DEFAULT NULL COMMENT 'Region',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Postcode',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Lastname',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(255) DEFAULT NULL COMMENT 'City',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `telephone` varchar(255) DEFAULT NULL COMMENT 'Phone Number',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Firstname',
  `address_type` varchar(255) DEFAULT NULL COMMENT 'Address Type',
  `prefix` varchar(255) DEFAULT NULL COMMENT 'Prefix',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middlename',
  `suffix` varchar(255) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_ADDRESS_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Address';

--
-- Dumping data for table `sales_order_address`
--

LOCK TABLES `sales_order_address` WRITE;
/*!40000 ALTER TABLE `sales_order_address` DISABLE KEYS */;
INSERT INTO `sales_order_address` VALUES ('1','1',NULL,NULL,'7',NULL,NULL,'Armed Forces Americas','12345-6789','plazathemes','plaza\r\nplaza','plaza','plaza@gmail.com','0115555662','US','palaza','shipping',NULL,NULL,NULL,'plaza themes',NULL,NULL,NULL,NULL,NULL),('2','1',NULL,NULL,'7',NULL,NULL,'Armed Forces Americas','12345-6789','plazathemes','plaza\r\nplaza','plaza','plaza@gmail.com','0115555662','US','palaza','billing',NULL,NULL,NULL,'plaza themes',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sales_order_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_aggregated_created`
--

DROP TABLE IF EXISTS `sales_order_aggregated_created`;
CREATE TABLE `sales_order_aggregated_created` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_ORDER_AGGREGATED_CREATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Created';

--
-- Table structure for table `sales_order_aggregated_updated`
--

DROP TABLE IF EXISTS `sales_order_aggregated_updated`;
CREATE TABLE `sales_order_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_ORDER_AGGREGATED_UPDATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Updated';

--
-- Table structure for table `sales_order_grid`
--

DROP TABLE IF EXISTS `sales_order_grid`;
CREATE TABLE `sales_order_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `order_currency_code` varchar(255) DEFAULT NULL COMMENT 'Order Currency Code',
  `shipping_name` varchar(255) DEFAULT NULL COMMENT 'Shipping Name',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group` varchar(255) DEFAULT NULL COMMENT 'Customer Group',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
  `payment_method` varchar(255) DEFAULT NULL COMMENT 'Payment Method',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_ORDER_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_ORDER_GRID_STATUS` (`status`),
  KEY `SALES_ORDER_GRID_STORE_ID` (`store_id`),
  KEY `SALES_ORDER_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `SALES_ORDER_GRID_BASE_TOTAL_PAID` (`base_total_paid`),
  KEY `SALES_ORDER_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_ORDER_GRID_TOTAL_PAID` (`total_paid`),
  KEY `SALES_ORDER_GRID_SHIPPING_NAME` (`shipping_name`),
  KEY `SALES_ORDER_GRID_BILLING_NAME` (`billing_name`),
  KEY `SALES_ORDER_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_ORDER_GRID_CUSTOMER_ID` (`customer_id`),
  KEY `SALES_ORDER_GRID_UPDATED_AT` (`updated_at`),
  FULLTEXT KEY `FTI_65B9E9925EC58F0C7C2E2F6379C233E7` (`increment_id`,`billing_name`,`shipping_name`,`shipping_address`,`billing_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Grid';

--
-- Dumping data for table `sales_order_grid`
--

LOCK TABLES `sales_order_grid` WRITE;
/*!40000 ALTER TABLE `sales_order_grid` DISABLE KEYS */;
INSERT INTO `sales_order_grid` VALUES ('1','pending',2,'Main Website\r\nMain Website Store',NULL,'300.0000',NULL,'300.0000',NULL,'000000001','USD','USD','palaza plazathemes','palaza plazathemes','2016-10-14 06:43:32','2016-10-14 06:43:34','plaza\r\nplaza plaza Armed Forces Americas 12345-6789','plaza\r\nplaza plaza Armed Forces Americas 12345-6789','Flat Rate - Fixed','plaza@gmail.com','0','280.0000','20.0000','','checkmo',NULL);
/*!40000 ALTER TABLE `sales_order_grid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_item`
--

DROP TABLE IF EXISTS `sales_order_item`;
CREATE TABLE `sales_order_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Quote Item Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `product_options` text COMMENT 'Product Options',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `is_qty_decimal` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'No Discount',
  `qty_backordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Backordered',
  `qty_canceled` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Canceled',
  `qty_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Invoiced',
  `qty_ordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `qty_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Refunded',
  `qty_shipped` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Shipped',
  `base_cost` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Cost',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `original_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `base_original_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Original Price',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Invoiced',
  `base_tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Invoiced',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Invoiced',
  `base_discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Invoiced',
  `amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Amount Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Amount Refunded',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Invoiced',
  `base_row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Invoiced',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `ext_order_item_id` varchar(255) DEFAULT NULL COMMENT 'Ext Order Item Id',
  `locked_do_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Locked Do Invoice',
  `locked_do_ship` smallint(5) unsigned DEFAULT NULL COMMENT 'Locked Do Ship',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
  `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
  `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
  `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `discount_tax_compensation_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Canceled',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `free_shipping` smallint(6) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `gift_message_available` int(11) DEFAULT NULL COMMENT 'Gift Message Available',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`item_id`),
  KEY `SALES_ORDER_ITEM_ORDER_ID` (`order_id`),
  KEY `SALES_ORDER_ITEM_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Item';

--
-- Dumping data for table `sales_order_item`
--

LOCK TABLES `sales_order_item` WRITE;
/*!40000 ALTER TABLE `sales_order_item` DISABLE KEYS */;
INSERT INTO `sales_order_item` VALUES ('1','1',NULL,'16',NULL,'2016-10-14 06:43:32','2016-10-14 06:43:32','14','simple','a:1:{s:15:\"info_buyRequest\";a:3:{s:4:\"uenc\";s:56:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYS93b21lbi5odG1s\";s:7:\"product\";s:2:\"14\";s:3:\"qty\";d:1;}}',NULL,0,'squa','squa',NULL,NULL,NULL,0,0,NULL,'0.0000','0.0000','1.0000','0.0000','0.0000',NULL,'50.0000','50.0000','70.0000','70.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','50.0000','50.0000','0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,'50.0000','50.0000','50.0000','50.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2','1',NULL,'17',NULL,'2016-10-14 06:43:32','2016-10-14 06:43:32','4','simple','a:1:{s:15:\"info_buyRequest\";a:3:{s:4:\"uenc\";s:56:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYS93b21lbi5odG1s\";s:7:\"product\";s:1:\"4\";s:3:\"qty\";d:1;}}',NULL,0,'Etiam Gravida','Etiam Gravida',NULL,NULL,NULL,0,0,NULL,'0.0000','0.0000','1.0000','0.0000','0.0000',NULL,'60.0000','60.0000','65.0000','65.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','60.0000','60.0000','0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,'60.0000','60.0000','60.0000','60.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3','1',NULL,'18',NULL,'2016-10-14 06:43:32','2016-10-14 06:43:32','17','simple','a:1:{s:15:\"info_buyRequest\";a:3:{s:4:\"uenc\";s:56:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYS93b21lbi5odG1s\";s:7:\"product\";s:2:\"17\";s:3:\"qty\";d:1;}}',NULL,0,'adidas','adidas',NULL,NULL,NULL,0,0,NULL,'0.0000','0.0000','1.0000','0.0000','0.0000',NULL,'100.0000','100.0000','150.0000','150.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','100.0000','100.0000','0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,'100.0000','100.0000','100.0000','100.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4','1',NULL,'19',NULL,'2016-10-14 06:43:33','2016-10-14 06:43:33','10','simple','a:1:{s:15:\"info_buyRequest\";a:3:{s:4:\"uenc\";s:56:\"aHR0cDovL2xvY2FsaG9zdDo4MDgwL21hX3NhaGFyYS93b21lbi5odG1s\";s:7:\"product\";s:2:\"10\";s:3:\"qty\";d:1;}}',NULL,0,'Tempus','Tempus',NULL,NULL,NULL,0,0,NULL,'0.0000','0.0000','1.0000','0.0000','0.0000',NULL,'70.0000','70.0000','80.0000','80.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','70.0000','70.0000','0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,'70.0000','70.0000','70.0000','70.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sales_order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_payment`
--

DROP TABLE IF EXISTS `sales_order_payment`;
CREATE TABLE `sales_order_payment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Captured',
  `shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Captured',
  `amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Amount Refunded',
  `base_amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid',
  `amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Amount Canceled',
  `base_amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Authorized',
  `base_amount_paid_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid Online',
  `base_amount_refunded_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded Online',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Amount Paid',
  `amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Amount Authorized',
  `base_amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Ordered',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded',
  `amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Amount Ordered',
  `base_amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Canceled',
  `quote_payment_id` int(11) DEFAULT NULL COMMENT 'Quote Payment Id',
  `additional_data` text COMMENT 'Additional Data',
  `cc_exp_month` varchar(12) DEFAULT NULL COMMENT 'Cc Exp Month',
  `cc_ss_start_year` varchar(12) DEFAULT NULL COMMENT 'Cc Ss Start Year',
  `echeck_bank_name` varchar(128) DEFAULT NULL COMMENT 'Echeck Bank Name',
  `method` varchar(128) DEFAULT NULL COMMENT 'Method',
  `cc_debug_request_body` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Request Body',
  `cc_secure_verify` varchar(32) DEFAULT NULL COMMENT 'Cc Secure Verify',
  `protection_eligibility` varchar(32) DEFAULT NULL COMMENT 'Protection Eligibility',
  `cc_approval` varchar(32) DEFAULT NULL COMMENT 'Cc Approval',
  `cc_last_4` varchar(100) DEFAULT NULL COMMENT 'Cc Last 4',
  `cc_status_description` varchar(32) DEFAULT NULL COMMENT 'Cc Status Description',
  `echeck_type` varchar(32) DEFAULT NULL COMMENT 'Echeck Type',
  `cc_debug_response_serialized` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Response Serialized',
  `cc_ss_start_month` varchar(128) DEFAULT NULL COMMENT 'Cc Ss Start Month',
  `echeck_account_type` varchar(255) DEFAULT NULL COMMENT 'Echeck Account Type',
  `last_trans_id` varchar(32) DEFAULT NULL COMMENT 'Last Trans Id',
  `cc_cid_status` varchar(32) DEFAULT NULL COMMENT 'Cc Cid Status',
  `cc_owner` varchar(128) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_type` varchar(32) DEFAULT NULL COMMENT 'Cc Type',
  `po_number` varchar(32) DEFAULT NULL COMMENT 'Po Number',
  `cc_exp_year` varchar(4) DEFAULT NULL COMMENT 'Cc Exp Year',
  `cc_status` varchar(4) DEFAULT NULL COMMENT 'Cc Status',
  `echeck_routing_number` varchar(32) DEFAULT NULL COMMENT 'Echeck Routing Number',
  `account_status` varchar(32) DEFAULT NULL COMMENT 'Account Status',
  `anet_trans_method` varchar(32) DEFAULT NULL COMMENT 'Anet Trans Method',
  `cc_debug_response_body` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Response Body',
  `cc_ss_issue` varchar(32) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `echeck_account_name` varchar(32) DEFAULT NULL COMMENT 'Echeck Account Name',
  `cc_avs_status` varchar(32) DEFAULT NULL COMMENT 'Cc Avs Status',
  `cc_number_enc` varchar(32) DEFAULT NULL COMMENT 'Cc Number Enc',
  `cc_trans_id` varchar(32) DEFAULT NULL COMMENT 'Cc Trans Id',
  `address_status` varchar(32) DEFAULT NULL COMMENT 'Address Status',
  `additional_information` text COMMENT 'Additional Information',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_PAYMENT_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Payment';

--
-- Dumping data for table `sales_order_payment`
--

LOCK TABLES `sales_order_payment` WRITE;
/*!40000 ALTER TABLE `sales_order_payment` DISABLE KEYS */;
INSERT INTO `sales_order_payment` VALUES ('1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'20.0000','20.0000',NULL,NULL,'300.0000',NULL,NULL,NULL,'300.0000',NULL,NULL,NULL,NULL,NULL,NULL,'checkmo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a:3:{s:12:\"method_title\";s:19:\"Check / Money order\";s:10:\"payable_to\";N;s:15:\"mailing_address\";N;}');
/*!40000 ALTER TABLE `sales_order_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_status`
--

DROP TABLE IF EXISTS `sales_order_status`;
CREATE TABLE `sales_order_status` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `label` varchar(128) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';

--
-- Dumping data for table `sales_order_status`
--

LOCK TABLES `sales_order_status` WRITE;
/*!40000 ALTER TABLE `sales_order_status` DISABLE KEYS */;
INSERT INTO `sales_order_status` VALUES ('canceled','Canceled'),('closed','Closed'),('complete','Complete'),('fraud','Suspected Fraud'),('holded','On Hold'),('payment_review','Payment Review'),('paypal_canceled_reversal','PayPal Canceled Reversal'),('paypal_reversed','PayPal Reversed'),('pending','Pending'),('pending_payment','Pending Payment'),('pending_paypal','Pending PayPal'),('processing','Processing');
/*!40000 ALTER TABLE `sales_order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_status_history`
--

DROP TABLE IF EXISTS `sales_order_status_history`;
CREATE TABLE `sales_order_status_history` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `entity_name` varchar(32) DEFAULT NULL COMMENT 'Shows what entity history is bind to.',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_STATUS_HISTORY_PARENT_ID` (`parent_id`),
  KEY `SALES_ORDER_STATUS_HISTORY_CREATED_AT` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Status History';

--
-- Table structure for table `sales_order_status_label`
--

DROP TABLE IF EXISTS `sales_order_status_label`;
CREATE TABLE `sales_order_status_label` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(128) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`status`,`store_id`),
  KEY `SALES_ORDER_STATUS_LABEL_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Label Table';

--
-- Table structure for table `sales_order_status_state`
--

DROP TABLE IF EXISTS `sales_order_status_state`;
CREATE TABLE `sales_order_status_state` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `state` varchar(32) NOT NULL COMMENT 'Label',
  `is_default` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Default',
  `visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Visible on front',
  PRIMARY KEY (`status`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';

--
-- Dumping data for table `sales_order_status_state`
--

LOCK TABLES `sales_order_status_state` WRITE;
/*!40000 ALTER TABLE `sales_order_status_state` DISABLE KEYS */;
INSERT INTO `sales_order_status_state` VALUES ('canceled','canceled',1,1),('closed','closed',1,1),('complete','complete',1,1),('fraud','payment_review',0,1),('fraud','processing',0,1),('holded','holded',1,1),('payment_review','payment_review',1,1),('pending','new',1,1),('pending_payment','pending_payment',1,0),('processing','processing',1,1);
/*!40000 ALTER TABLE `sales_order_status_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_tax`
--

DROP TABLE IF EXISTS `sales_order_tax`;
CREATE TABLE `sales_order_tax` (
  `tax_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tax Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `percent` decimal(12,4) DEFAULT NULL COMMENT 'Percent',
  `amount` decimal(12,4) DEFAULT NULL COMMENT 'Amount',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `base_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount',
  `process` smallint(6) NOT NULL COMMENT 'Process',
  `base_real_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Real Amount',
  PRIMARY KEY (`tax_id`),
  KEY `SALES_ORDER_TAX_ORDER_ID_PRIORITY_POSITION` (`order_id`,`priority`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Table';

--
-- Table structure for table `sales_order_tax_item`
--

DROP TABLE IF EXISTS `sales_order_tax_item`;
CREATE TABLE `sales_order_tax_item` (
  `tax_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tax Item Id',
  `tax_id` int(10) unsigned NOT NULL COMMENT 'Tax Id',
  `item_id` int(10) unsigned DEFAULT NULL COMMENT 'Item Id',
  `tax_percent` decimal(12,4) NOT NULL COMMENT 'Real Tax Percent For Item',
  `amount` decimal(12,4) NOT NULL COMMENT 'Tax amount for the item and tax rate',
  `base_amount` decimal(12,4) NOT NULL COMMENT 'Base tax amount for the item and tax rate',
  `real_amount` decimal(12,4) NOT NULL COMMENT 'Real tax amount for the item and tax rate',
  `real_base_amount` decimal(12,4) NOT NULL COMMENT 'Real base tax amount for the item and tax rate',
  `associated_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Id of the associated item',
  `taxable_item_type` varchar(32) NOT NULL COMMENT 'Type of the taxable item',
  PRIMARY KEY (`tax_item_id`),
  UNIQUE KEY `SALES_ORDER_TAX_ITEM_TAX_ID_ITEM_ID` (`tax_id`,`item_id`),
  KEY `SALES_ORDER_TAX_ITEM_ITEM_ID` (`item_id`),
  KEY `SALES_ORDER_TAX_ITEM_ASSOCIATED_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` (`associated_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Item';

--
-- Table structure for table `sales_payment_transaction`
--

DROP TABLE IF EXISTS `sales_payment_transaction`;
CREATE TABLE `sales_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Transaction Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `payment_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Payment Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `parent_txn_id` varchar(100) DEFAULT NULL COMMENT 'Parent Txn Id',
  `txn_type` varchar(15) DEFAULT NULL COMMENT 'Txn Type',
  `is_closed` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Closed',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `SALES_PAYMENT_TRANSACTION_ORDER_ID_PAYMENT_ID_TXN_ID` (`order_id`,`payment_id`,`txn_id`),
  KEY `SALES_PAYMENT_TRANSACTION_PARENT_ID` (`parent_id`),
  KEY `SALES_PAYMENT_TRANSACTION_PAYMENT_ID` (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Payment Transaction';

--
-- Table structure for table `sales_refunded_aggregated`
--

DROP TABLE IF EXISTS `sales_refunded_aggregated`;
CREATE TABLE `sales_refunded_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_REFUNDED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_REFUNDED_AGGREGATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated';

--
-- Table structure for table `sales_refunded_aggregated_order`
--

DROP TABLE IF EXISTS `sales_refunded_aggregated_order`;
CREATE TABLE `sales_refunded_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_REFUNDED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated Order';

--
-- Table structure for table `sales_sequence_meta`
--

DROP TABLE IF EXISTS `sales_sequence_meta`;
CREATE TABLE `sales_sequence_meta` (
  `meta_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `entity_type` varchar(32) NOT NULL COMMENT 'Prefix',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `sequence_table` varchar(32) NOT NULL COMMENT 'table for sequence',
  PRIMARY KEY (`meta_id`),
  UNIQUE KEY `SALES_SEQUENCE_META_ENTITY_TYPE_STORE_ID` (`entity_type`,`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='sales_sequence_meta';

--
-- Dumping data for table `sales_sequence_meta`
--

LOCK TABLES `sales_sequence_meta` WRITE;
/*!40000 ALTER TABLE `sales_sequence_meta` DISABLE KEYS */;
INSERT INTO `sales_sequence_meta` VALUES ('1','order',0,'sequence_order_0'),('2','invoice',0,'sequence_invoice_0'),('3','creditmemo',0,'sequence_creditmemo_0'),('4','shipment',0,'sequence_shipment_0'),('5','order',1,'sequence_order_1'),('6','invoice',1,'sequence_invoice_1'),('7','creditmemo',1,'sequence_creditmemo_1'),('8','shipment',1,'sequence_shipment_1'),('9','order',2,'sequence_order_2'),('10','invoice',2,'sequence_invoice_2'),('11','creditmemo',2,'sequence_creditmemo_2'),('12','shipment',2,'sequence_shipment_2'),('13','order',3,'sequence_order_3'),('14','invoice',3,'sequence_invoice_3'),('15','creditmemo',3,'sequence_creditmemo_3'),('16','shipment',3,'sequence_shipment_3'),('17','order',4,'sequence_order_4'),('18','invoice',4,'sequence_invoice_4'),('19','creditmemo',4,'sequence_creditmemo_4'),('20','shipment',4,'sequence_shipment_4');
/*!40000 ALTER TABLE `sales_sequence_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_sequence_profile`
--

DROP TABLE IF EXISTS `sales_sequence_profile`;
CREATE TABLE `sales_sequence_profile` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `meta_id` int(10) unsigned NOT NULL COMMENT 'Meta_id',
  `prefix` varchar(32) DEFAULT NULL COMMENT 'Prefix',
  `suffix` varchar(32) DEFAULT NULL COMMENT 'Suffix',
  `start_value` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Start value for sequence',
  `step` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Step for sequence',
  `max_value` int(10) unsigned NOT NULL COMMENT 'MaxValue for sequence',
  `warning_value` int(10) unsigned NOT NULL COMMENT 'WarningValue for sequence',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'isActive flag',
  PRIMARY KEY (`profile_id`),
  UNIQUE KEY `SALES_SEQUENCE_PROFILE_META_ID_PREFIX_SUFFIX` (`meta_id`,`prefix`,`suffix`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='sales_sequence_profile';

--
-- Dumping data for table `sales_sequence_profile`
--

LOCK TABLES `sales_sequence_profile` WRITE;
/*!40000 ALTER TABLE `sales_sequence_profile` DISABLE KEYS */;
INSERT INTO `sales_sequence_profile` VALUES ('1','1',NULL,NULL,'1','1','0','0',1),('2','2',NULL,NULL,'1','1','0','0',1),('3','3',NULL,NULL,'1','1','0','0',1),('4','4',NULL,NULL,'1','1','0','0',1),('5','5',NULL,NULL,'1','1','0','0',1),('6','6',NULL,NULL,'1','1','0','0',1),('7','7',NULL,NULL,'1','1','0','0',1),('8','8',NULL,NULL,'1','1','0','0',1),('9','9','2',NULL,'1','1','0','0',1),('10','10','2',NULL,'1','1','0','0',1),('11','11','2',NULL,'1','1','0','0',1),('12','12','2',NULL,'1','1','0','0',1),('13','13','3',NULL,'1','1','0','0',1),('14','14','3',NULL,'1','1','0','0',1),('15','15','3',NULL,'1','1','0','0',1),('16','16','3',NULL,'1','1','0','0',1),('17','17','4',NULL,'1','1','0','0',1),('18','18','4',NULL,'1','1','0','0',1),('19','19','4',NULL,'1','1','0','0',1),('20','20','4',NULL,'1','1','0','0',1);
/*!40000 ALTER TABLE `sales_sequence_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_shipment`
--

DROP TABLE IF EXISTS `sales_shipment`;
CREATE TABLE `sales_shipment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `total_weight` decimal(12,4) DEFAULT NULL COMMENT 'Total Weight',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `packages` text COMMENT 'Packed Products in Packages',
  `shipping_label` mediumblob COMMENT 'Shipping Label Content',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_SHIPMENT_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_SHIPMENT_STORE_ID` (`store_id`),
  KEY `SALES_SHIPMENT_TOTAL_QTY` (`total_qty`),
  KEY `SALES_SHIPMENT_ORDER_ID` (`order_id`),
  KEY `SALES_SHIPMENT_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_UPDATED_AT` (`updated_at`),
  KEY `SALES_SHIPMENT_SEND_EMAIL` (`send_email`),
  KEY `SALES_SHIPMENT_EMAIL_SENT` (`email_sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment';

--
-- Table structure for table `sales_shipment_comment`
--

DROP TABLE IF EXISTS `sales_shipment_comment`;
CREATE TABLE `sales_shipment_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_COMMENT_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Comment';

--
-- Table structure for table `sales_shipment_grid`
--

DROP TABLE IF EXISTS `sales_shipment_grid`;
CREATE TABLE `sales_shipment_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_increment_id` varchar(32) NOT NULL COMMENT 'Order Increment Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Order Increment Id',
  `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `order_status` varchar(32) DEFAULT NULL COMMENT 'Order',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `billing_name` varchar(128) DEFAULT NULL COMMENT 'Billing Name',
  `shipping_name` varchar(128) DEFAULT NULL COMMENT 'Shipping Name',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_SHIPMENT_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_SHIPMENT_GRID_STORE_ID` (`store_id`),
  KEY `SALES_SHIPMENT_GRID_TOTAL_QTY` (`total_qty`),
  KEY `SALES_SHIPMENT_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_SHIPMENT_GRID_SHIPMENT_STATUS` (`shipment_status`),
  KEY `SALES_SHIPMENT_GRID_ORDER_STATUS` (`order_status`),
  KEY `SALES_SHIPMENT_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_SHIPMENT_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_SHIPMENT_GRID_SHIPPING_NAME` (`shipping_name`),
  KEY `SALES_SHIPMENT_GRID_BILLING_NAME` (`billing_name`),
  FULLTEXT KEY `FTI_086B40C8955F167B8EA76653437879B4` (`increment_id`,`order_increment_id`,`shipping_name`,`customer_name`,`customer_email`,`billing_address`,`shipping_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Grid';

--
-- Table structure for table `sales_shipment_item`
--

DROP TABLE IF EXISTS `sales_shipment_item`;
CREATE TABLE `sales_shipment_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_ITEM_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Item';

--
-- Table structure for table `sales_shipment_track`
--

DROP TABLE IF EXISTS `sales_shipment_track`;
CREATE TABLE `sales_shipment_track` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `track_number` text COMMENT 'Number',
  `description` text COMMENT 'Description',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `carrier_code` varchar(32) DEFAULT NULL COMMENT 'Carrier Code',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_TRACK_PARENT_ID` (`parent_id`),
  KEY `SALES_SHIPMENT_TRACK_ORDER_ID` (`order_id`),
  KEY `SALES_SHIPMENT_TRACK_CREATED_AT` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Track';

--
-- Table structure for table `sales_shipping_aggregated`
--

DROP TABLE IF EXISTS `sales_shipping_aggregated`;
CREATE TABLE `sales_shipping_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_SHPP_AGGRED_PERIOD_STORE_ID_ORDER_STS_SHPP_DESCRIPTION` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `SALES_SHIPPING_AGGREGATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated';

--
-- Table structure for table `sales_shipping_aggregated_order`
--

DROP TABLE IF EXISTS `sales_shipping_aggregated_order`;
CREATE TABLE `sales_shipping_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_C05FAE47282EEA68654D0924E946761F` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated Order';

--
-- Table structure for table `salesrule`
--

DROP TABLE IF EXISTS `salesrule`;
CREATE TABLE `salesrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From',
  `to_date` date DEFAULT NULL COMMENT 'To',
  `uses_per_customer` int(11) NOT NULL DEFAULT '0' COMMENT 'Uses Per Customer',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `is_advanced` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Advanced',
  `product_ids` text COMMENT 'Product Ids',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `discount_qty` decimal(12,4) DEFAULT NULL COMMENT 'Discount Qty',
  `discount_step` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Discount Step',
  `apply_to_shipping` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Apply To Shipping',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `is_rss` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Rss',
  `coupon_type` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Coupon Type',
  `use_auto_generation` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Use Auto Generation',
  `uses_per_coupon` int(11) NOT NULL DEFAULT '0' COMMENT 'User Per Coupon',
  `simple_free_shipping` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`rule_id`),
  KEY `SALESRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule';

--
-- Table structure for table `salesrule_coupon`
--

DROP TABLE IF EXISTS `salesrule_coupon`;
CREATE TABLE `salesrule_coupon` (
  `coupon_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Coupon Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `usage_limit` int(10) unsigned DEFAULT NULL COMMENT 'Usage Limit',
  `usage_per_customer` int(10) unsigned DEFAULT NULL COMMENT 'Usage Per Customer',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `expiration_date` timestamp NULL DEFAULT NULL COMMENT 'Expiration Date',
  `is_primary` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Primary',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Coupon Code Creation Date',
  `type` smallint(6) DEFAULT '0' COMMENT 'Coupon Code Type',
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `SALESRULE_COUPON_CODE` (`code`),
  UNIQUE KEY `SALESRULE_COUPON_RULE_ID_IS_PRIMARY` (`rule_id`,`is_primary`),
  KEY `SALESRULE_COUPON_RULE_ID` (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon';

--
-- Table structure for table `salesrule_coupon_aggregated`
--

DROP TABLE IF EXISTS `salesrule_coupon_aggregated`;
CREATE TABLE `salesrule_coupon_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALESRULE_COUPON_AGGRED_PERIOD_STORE_ID_ORDER_STS_COUPON_CODE` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_RULE_NAME` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated';

--
-- Table structure for table `salesrule_coupon_aggregated_order`
--

DROP TABLE IF EXISTS `salesrule_coupon_aggregated_order`;
CREATE TABLE `salesrule_coupon_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_1094D1FBBCBB11704A29DEF3ACC37D2B` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_ORDER_RULE_NAME` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated Order';

--
-- Table structure for table `salesrule_coupon_aggregated_updated`
--

DROP TABLE IF EXISTS `salesrule_coupon_aggregated_updated`;
CREATE TABLE `salesrule_coupon_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_7196FA120A4F0F84E1B66605E87E213E` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_UPDATED_RULE_NAME` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Aggregated Updated';

--
-- Table structure for table `salesrule_coupon_usage`
--

DROP TABLE IF EXISTS `salesrule_coupon_usage`;
CREATE TABLE `salesrule_coupon_usage` (
  `coupon_id` int(10) unsigned NOT NULL COMMENT 'Coupon Id',
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Id',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`coupon_id`,`customer_id`),
  KEY `SALESRULE_COUPON_USAGE_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Usage';

--
-- Table structure for table `salesrule_customer`
--

DROP TABLE IF EXISTS `salesrule_customer`;
CREATE TABLE `salesrule_customer` (
  `rule_customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Customer Id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `times_used` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`rule_customer_id`),
  KEY `SALESRULE_CUSTOMER_RULE_ID_CUSTOMER_ID` (`rule_id`,`customer_id`),
  KEY `SALESRULE_CUSTOMER_CUSTOMER_ID_RULE_ID` (`customer_id`,`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Customer';

--
-- Table structure for table `salesrule_customer_group`
--

DROP TABLE IF EXISTS `salesrule_customer_group`;
CREATE TABLE `salesrule_customer_group` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `SALESRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Customer Groups Relations';

--
-- Table structure for table `salesrule_label`
--

DROP TABLE IF EXISTS `salesrule_label`;
CREATE TABLE `salesrule_label` (
  `label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Label Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  PRIMARY KEY (`label_id`),
  UNIQUE KEY `SALESRULE_LABEL_RULE_ID_STORE_ID` (`rule_id`,`store_id`),
  KEY `SALESRULE_LABEL_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Label';

--
-- Table structure for table `salesrule_product_attribute`
--

DROP TABLE IF EXISTS `salesrule_product_attribute`;
CREATE TABLE `salesrule_product_attribute` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`rule_id`,`website_id`,`customer_group_id`,`attribute_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID` (`website_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Product Attribute';

--
-- Table structure for table `salesrule_website`
--

DROP TABLE IF EXISTS `salesrule_website`;
CREATE TABLE `salesrule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `SALESRULE_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Websites Relations';

--
-- Table structure for table `search_query`
--

DROP TABLE IF EXISTS `search_query`;
CREATE TABLE `search_query` (
  `query_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Query ID',
  `query_text` varchar(255) DEFAULT NULL COMMENT 'Query text',
  `num_results` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Num results',
  `popularity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Popularity',
  `redirect` varchar(255) DEFAULT NULL COMMENT 'Redirect',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `display_in_terms` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Display in terms',
  `is_active` smallint(6) DEFAULT '1' COMMENT 'Active status',
  `is_processed` smallint(6) DEFAULT '0' COMMENT 'Processed status',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated at',
  PRIMARY KEY (`query_id`),
  UNIQUE KEY `SEARCH_QUERY_QUERY_TEXT_STORE_ID` (`query_text`,`store_id`),
  KEY `SEARCH_QUERY_QUERY_TEXT_STORE_ID_POPULARITY` (`query_text`,`store_id`,`popularity`),
  KEY `SEARCH_QUERY_STORE_ID` (`store_id`),
  KEY `SEARCH_QUERY_IS_PROCESSED` (`is_processed`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Search query table';

--
-- Dumping data for table `search_query`
--

LOCK TABLES `search_query` WRITE;
/*!40000 ALTER TABLE `search_query` DISABLE KEYS */;
INSERT INTO `search_query` VALUES ('1','accu','1','1',NULL,1,1,1,0,'2016-11-24 09:25:15'),('3','accumsan','1','3',NULL,1,1,1,0,'2016-11-28 01:53:33'),('4','ACCUMSAN ELIT','2','1',NULL,4,1,1,0,'2016-11-26 04:42:34'),('8','accumsan','1','1',NULL,3,1,1,0,'2016-11-28 02:45:41');
/*!40000 ALTER TABLE `search_query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_synonyms`
--

DROP TABLE IF EXISTS `search_synonyms`;
CREATE TABLE `search_synonyms` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Synonyms Group Id',
  `synonyms` text NOT NULL COMMENT 'list of synonyms making up this group',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id - identifies the store view these synonyms belong to',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id - identifies the website id these synonyms belong to',
  PRIMARY KEY (`group_id`),
  KEY `SEARCH_SYNONYMS_STORE_ID` (`store_id`),
  KEY `SEARCH_SYNONYMS_WEBSITE_ID` (`website_id`),
  FULLTEXT KEY `SEARCH_SYNONYMS_SYNONYMS` (`synonyms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table storing various synonyms groups';

--
-- Table structure for table `sendfriend_log`
--

DROP TABLE IF EXISTS `sendfriend_log`;
CREATE TABLE `sendfriend_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `ip` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer IP address',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Log time',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  PRIMARY KEY (`log_id`),
  KEY `SENDFRIEND_LOG_IP` (`ip`),
  KEY `SENDFRIEND_LOG_TIME` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Send to friend function log storage table';

--
-- Table structure for table `sequence_creditmemo_0`
--

DROP TABLE IF EXISTS `sequence_creditmemo_0`;
CREATE TABLE `sequence_creditmemo_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_creditmemo_1`
--

DROP TABLE IF EXISTS `sequence_creditmemo_1`;
CREATE TABLE `sequence_creditmemo_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_creditmemo_2`
--

DROP TABLE IF EXISTS `sequence_creditmemo_2`;
CREATE TABLE `sequence_creditmemo_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_creditmemo_3`
--

DROP TABLE IF EXISTS `sequence_creditmemo_3`;
CREATE TABLE `sequence_creditmemo_3` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_creditmemo_4`
--

DROP TABLE IF EXISTS `sequence_creditmemo_4`;
CREATE TABLE `sequence_creditmemo_4` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_invoice_0`
--

DROP TABLE IF EXISTS `sequence_invoice_0`;
CREATE TABLE `sequence_invoice_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_invoice_1`
--

DROP TABLE IF EXISTS `sequence_invoice_1`;
CREATE TABLE `sequence_invoice_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_invoice_2`
--

DROP TABLE IF EXISTS `sequence_invoice_2`;
CREATE TABLE `sequence_invoice_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_invoice_3`
--

DROP TABLE IF EXISTS `sequence_invoice_3`;
CREATE TABLE `sequence_invoice_3` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_invoice_4`
--

DROP TABLE IF EXISTS `sequence_invoice_4`;
CREATE TABLE `sequence_invoice_4` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_order_0`
--

DROP TABLE IF EXISTS `sequence_order_0`;
CREATE TABLE `sequence_order_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_order_1`
--

DROP TABLE IF EXISTS `sequence_order_1`;
CREATE TABLE `sequence_order_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sequence_order_1`
--

LOCK TABLES `sequence_order_1` WRITE;
/*!40000 ALTER TABLE `sequence_order_1` DISABLE KEYS */;
INSERT INTO `sequence_order_1` VALUES ('1');
/*!40000 ALTER TABLE `sequence_order_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_order_2`
--

DROP TABLE IF EXISTS `sequence_order_2`;
CREATE TABLE `sequence_order_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_order_3`
--

DROP TABLE IF EXISTS `sequence_order_3`;
CREATE TABLE `sequence_order_3` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_order_4`
--

DROP TABLE IF EXISTS `sequence_order_4`;
CREATE TABLE `sequence_order_4` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_shipment_0`
--

DROP TABLE IF EXISTS `sequence_shipment_0`;
CREATE TABLE `sequence_shipment_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_shipment_1`
--

DROP TABLE IF EXISTS `sequence_shipment_1`;
CREATE TABLE `sequence_shipment_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_shipment_2`
--

DROP TABLE IF EXISTS `sequence_shipment_2`;
CREATE TABLE `sequence_shipment_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_shipment_3`
--

DROP TABLE IF EXISTS `sequence_shipment_3`;
CREATE TABLE `sequence_shipment_3` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_shipment_4`
--

DROP TABLE IF EXISTS `sequence_shipment_4`;
CREATE TABLE `sequence_shipment_4` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `session_id` varchar(255) NOT NULL COMMENT 'Session Id',
  `session_expires` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Date of Session Expiration',
  `session_data` mediumblob NOT NULL COMMENT 'Session Data',
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Database Sessions Storage';

--
-- Table structure for table `setup_module`
--

DROP TABLE IF EXISTS `setup_module`;
CREATE TABLE `setup_module` (
  `module` varchar(50) NOT NULL COMMENT 'Module',
  `schema_version` varchar(50) DEFAULT NULL COMMENT 'Schema Version',
  `data_version` varchar(50) DEFAULT NULL COMMENT 'Data Version',
  PRIMARY KEY (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module versions registry';

--
-- Dumping data for table `setup_module`
--

LOCK TABLES `setup_module` WRITE;
/*!40000 ALTER TABLE `setup_module` DISABLE KEYS */;
INSERT INTO `setup_module` VALUES ('Magento_AdminNotification','2.0.0','2.0.0'),('Magento_AdvancedPricingImportExport','2.0.0','2.0.0'),('Magento_Authorization','2.0.0','2.0.0'),('Magento_Authorizenet','2.0.0','2.0.0'),('Magento_Backend','2.0.0','2.0.0'),('Magento_Backup','2.0.0','2.0.0'),('Magento_Braintree','2.0.0','2.0.0'),('Magento_Bundle','2.0.2','2.0.2'),('Magento_BundleImportExport','2.0.0','2.0.0'),('Magento_CacheInvalidate','2.0.0','2.0.0'),('Magento_Captcha','2.0.0','2.0.0'),('Magento_Catalog','2.0.7','2.0.7'),('Magento_CatalogImportExport','2.0.0','2.0.0'),('Magento_CatalogInventory','2.0.0','2.0.0'),('Magento_CatalogRule','2.0.1','2.0.1'),('Magento_CatalogRuleConfigurable','2.0.0','2.0.0'),('Magento_CatalogSearch','2.0.0','2.0.0'),('Magento_CatalogUrlRewrite','2.0.0','2.0.0'),('Magento_CatalogWidget','2.0.0','2.0.0'),('Magento_Checkout','2.0.0','2.0.0'),('Magento_CheckoutAgreements','2.0.1','2.0.1'),('Magento_Cms','2.0.1','2.0.1'),('Magento_CmsUrlRewrite','2.0.0','2.0.0'),('Magento_Config','2.0.0','2.0.0'),('Magento_ConfigurableImportExport','2.0.0','2.0.0'),('Magento_ConfigurableProduct','2.0.0','2.0.0'),('Magento_Contact','2.0.0','2.0.0'),('Magento_Cookie','2.0.0','2.0.0'),('Magento_Cron','2.0.0','2.0.0'),('Magento_CurrencySymbol','2.0.0','2.0.0'),('Magento_Customer','2.0.7','2.0.7'),('Magento_CustomerImportExport','2.0.0','2.0.0'),('Magento_Deploy','2.0.0','2.0.0'),('Magento_Developer','2.0.0','2.0.0'),('Magento_Dhl','2.0.0','2.0.0'),('Magento_Directory','2.0.0','2.0.0'),('Magento_Downloadable','2.0.1','2.0.1'),('Magento_DownloadableImportExport','2.0.0','2.0.0'),('Magento_Eav','2.0.0','2.0.0'),('Magento_Email','2.0.0','2.0.0'),('Magento_EncryptionKey','2.0.0','2.0.0'),('Magento_Fedex','2.0.0','2.0.0'),('Magento_GiftMessage','2.0.1','2.0.1'),('Magento_GoogleAdwords','2.0.0','2.0.0'),('Magento_GoogleAnalytics','2.0.0','2.0.0'),('Magento_GoogleOptimizer','2.0.0','2.0.0'),('Magento_GroupedImportExport','2.0.0','2.0.0'),('Magento_GroupedProduct','2.0.1','2.0.1'),('Magento_ImportExport','2.0.1','2.0.1'),('Magento_Indexer','2.0.0','2.0.0'),('Magento_Integration','2.0.1','2.0.1'),('Magento_LayeredNavigation','2.0.0','2.0.0'),('Magento_Marketplace','1.0.0','1.0.0'),('Magento_MediaStorage','2.0.0','2.0.0'),('Magento_Msrp','2.0.0','2.0.0'),('Magento_Multishipping','2.0.0','2.0.0'),('Magento_NewRelicReporting','2.0.0','2.0.0'),('Magento_Newsletter','2.0.0','2.0.0'),('Magento_OfflinePayments','2.0.0','2.0.0'),('Magento_OfflineShipping','2.0.0','2.0.0'),('Magento_PageCache','2.0.0','2.0.0'),('Magento_Payment','2.0.0','2.0.0'),('Magento_Paypal','2.0.0','2.0.0'),('Magento_Persistent','2.0.0','2.0.0'),('Magento_ProductAlert','2.0.0','2.0.0'),('Magento_ProductVideo','2.0.0.2','2.0.0.2'),('Magento_Quote','2.0.2','2.0.2'),('Magento_Reports','2.0.0','2.0.0'),('Magento_RequireJs','2.0.0','2.0.0'),('Magento_Review','2.0.0','2.0.0'),('Magento_Rss','2.0.0','2.0.0'),('Magento_Rule','2.0.0','2.0.0'),('Magento_Sales','2.0.2','2.0.2'),('Magento_SalesRule','2.0.1','2.0.1'),('Magento_SalesSequence','2.0.0','2.0.0'),('Magento_SampleData','2.0.0','2.0.0'),('Magento_Search','2.0.4','2.0.4'),('Magento_Security','2.0.1','2.0.1'),('Magento_SendFriend','2.0.0','2.0.0'),('Magento_Shipping','2.0.0','2.0.0'),('Magento_Sitemap','2.0.0','2.0.0'),('Magento_Store','2.0.0','2.0.0'),('Magento_Swagger','2.0.0','2.0.0'),('Magento_Swatches','2.0.1','2.0.1'),('Magento_SwatchesLayeredNavigation','2.0.0','2.0.0'),('Magento_Tax','2.0.1','2.0.1'),('Magento_TaxImportExport','2.0.0','2.0.0'),('Magento_Theme','2.0.1','2.0.1'),('Magento_Translation','2.0.0','2.0.0'),('Magento_Ui','2.0.0','2.0.0'),('Magento_Ups','2.0.0','2.0.0'),('Magento_UrlRewrite','2.0.0','2.0.0'),('Magento_User','2.0.1','2.0.1'),('Magento_Usps','2.0.0','2.0.0'),('Magento_Variable','2.0.0','2.0.0'),('Magento_Vault','2.0.0','2.0.0'),('Magento_Version','2.0.0','2.0.0'),('Magento_Webapi','2.0.0','2.0.0'),('Magento_WebapiSecurity','2.0.0','2.0.0'),('Magento_Weee','2.0.0','2.0.0'),('Magento_Widget','2.0.0','2.0.0'),('Magento_Wishlist','2.0.0','2.0.0'),('Plazathemes_Bannerslider','1.0.0','1.0.0'),('Plazathemes_Bestsellerproduct','2.0.0','2.0.0'),('Plazathemes_Blog','2.0.0','2.0.0'),('Plazathemes_Brandslider','1.0.0','1.0.0'),('Plazathemes_Categorytab','2.0.0','2.0.0'),('Plazathemes_Categorytop','2.0.0','2.0.0'),('Plazathemes_Featureproductslider','2.0.0','2.0.0'),('Plazathemes_Hozmegamenu','2.0.0','2.0.0'),('Plazathemes_Layerednavigation','2.0.0','2.0.0'),('Plazathemes_Layout','2.0.0','2.0.0'),('Plazathemes_Loadjs','2.0.0','2.0.0'),('Plazathemes_Newproductslider','2.0.0','2.0.0'),('Plazathemes_Newsletterpopup','2.0.0','2.0.0'),('Plazathemes_Override','2.0.0','2.0.0'),('Plazathemes_Pricecountdown','2.0.0','2.0.0'),('Plazathemes_Producttab','2.0.0','2.0.0'),('Plazathemes_Quickview','2.0.0','2.0.0'),('Plazathemes_Recentproductslider','1.0.0','1.0.0'),('Plazathemes_Saleproductslider','2.0.0','2.0.0'),('Plazathemes_Template','2.0.0','2.0.0'),('Plazathemes_Themeoptions','2.0.0','2.0.0');
/*!40000 ALTER TABLE `setup_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_tablerate`
--

DROP TABLE IF EXISTS `shipping_tablerate`;
CREATE TABLE `shipping_tablerate` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `website_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `dest_country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Destination coutry ISO/2 or ISO/3 code',
  `dest_region_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Destination Region Id',
  `dest_zip` varchar(10) NOT NULL DEFAULT '*' COMMENT 'Destination Post Code (Zip)',
  `condition_name` varchar(20) NOT NULL COMMENT 'Rate Condition name',
  `condition_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rate condition value',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `cost` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Cost',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `UNQ_D60821CDB2AFACEE1566CFC02D0D4CAA` (`website_id`,`dest_country_id`,`dest_region_id`,`dest_zip`,`condition_name`,`condition_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Shipping Tablerate';

--
-- Table structure for table `sitemap`
--

DROP TABLE IF EXISTS `sitemap`;
CREATE TABLE `sitemap` (
  `sitemap_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Sitemap Id',
  `sitemap_type` varchar(32) DEFAULT NULL COMMENT 'Sitemap Type',
  `sitemap_filename` varchar(32) DEFAULT NULL COMMENT 'Sitemap Filename',
  `sitemap_path` varchar(255) DEFAULT NULL COMMENT 'Sitemap Path',
  `sitemap_time` timestamp NULL DEFAULT NULL COMMENT 'Sitemap Time',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`sitemap_id`),
  KEY `SITEMAP_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='XML Sitemap';

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `store_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Store Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Name',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Sort Order',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Activity',
  PRIMARY KEY (`store_id`),
  UNIQUE KEY `STORE_CODE` (`code`),
  KEY `STORE_WEBSITE_ID` (`website_id`),
  KEY `STORE_IS_ACTIVE_SORT_ORDER` (`is_active`,`sort_order`),
  KEY `STORE_GROUP_ID` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Stores';

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` VALUES (0,'admin',0,0,'Admin',0,1),(1,'english',1,1,'English',0,1),(3,'french',1,1,'French',0,1),(4,'germany',1,1,'Germany',0,1);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_group`
--

DROP TABLE IF EXISTS `store_group`;
CREATE TABLE `store_group` (
  `group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Group Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Group Name',
  `root_category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Root Category Id',
  `default_store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Store Id',
  PRIMARY KEY (`group_id`),
  KEY `STORE_GROUP_WEBSITE_ID` (`website_id`),
  KEY `STORE_GROUP_DEFAULT_STORE_ID` (`default_store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Store Groups';

--
-- Dumping data for table `store_group`
--

LOCK TABLES `store_group` WRITE;
/*!40000 ALTER TABLE `store_group` DISABLE KEYS */;
INSERT INTO `store_group` VALUES (0,0,'Default','0',0),(1,1,'Main Website Store','2',1);
/*!40000 ALTER TABLE `store_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_website`
--

DROP TABLE IF EXISTS `store_website`;
CREATE TABLE `store_website` (
  `website_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Website Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `name` varchar(64) DEFAULT NULL COMMENT 'Website Name',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Group Id',
  `is_default` smallint(5) unsigned DEFAULT '0' COMMENT 'Defines Is Website Default',
  PRIMARY KEY (`website_id`),
  UNIQUE KEY `STORE_WEBSITE_CODE` (`code`),
  KEY `STORE_WEBSITE_SORT_ORDER` (`sort_order`),
  KEY `STORE_WEBSITE_DEFAULT_GROUP_ID` (`default_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Websites';

--
-- Dumping data for table `store_website`
--

LOCK TABLES `store_website` WRITE;
/*!40000 ALTER TABLE `store_website` DISABLE KEYS */;
INSERT INTO `store_website` VALUES (0,'admin','Admin',0,0,0),(1,'base','Main Website',0,1,1);
/*!40000 ALTER TABLE `store_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_calculation`
--

DROP TABLE IF EXISTS `tax_calculation`;
CREATE TABLE `tax_calculation` (
  `tax_calculation_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `tax_calculation_rule_id` int(11) NOT NULL COMMENT 'Tax Calculation Rule Id',
  `customer_tax_class_id` smallint(6) NOT NULL COMMENT 'Customer Tax Class Id',
  `product_tax_class_id` smallint(6) NOT NULL COMMENT 'Product Tax Class Id',
  PRIMARY KEY (`tax_calculation_id`),
  KEY `TAX_CALCULATION_TAX_CALCULATION_RULE_ID` (`tax_calculation_rule_id`),
  KEY `TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID` (`customer_tax_class_id`),
  KEY `TAX_CALCULATION_PRODUCT_TAX_CLASS_ID` (`product_tax_class_id`),
  KEY `TAX_CALC_TAX_CALC_RATE_ID_CSTR_TAX_CLASS_ID_PRD_TAX_CLASS_ID` (`tax_calculation_rate_id`,`customer_tax_class_id`,`product_tax_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation';

--
-- Table structure for table `tax_calculation_rate`
--

DROP TABLE IF EXISTS `tax_calculation_rate`;
CREATE TABLE `tax_calculation_rate` (
  `tax_calculation_rate_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Id',
  `tax_country_id` varchar(2) NOT NULL COMMENT 'Tax Country Id',
  `tax_region_id` int(11) NOT NULL COMMENT 'Tax Region Id',
  `tax_postcode` varchar(21) DEFAULT NULL COMMENT 'Tax Postcode',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `rate` decimal(12,4) NOT NULL COMMENT 'Rate',
  `zip_is_range` smallint(6) DEFAULT NULL COMMENT 'Zip Is Range',
  `zip_from` int(10) unsigned DEFAULT NULL COMMENT 'Zip From',
  `zip_to` int(10) unsigned DEFAULT NULL COMMENT 'Zip To',
  PRIMARY KEY (`tax_calculation_rate_id`),
  KEY `TAX_CALCULATION_RATE_TAX_COUNTRY_ID_TAX_REGION_ID_TAX_POSTCODE` (`tax_country_id`,`tax_region_id`,`tax_postcode`),
  KEY `TAX_CALCULATION_RATE_CODE` (`code`),
  KEY `IDX_CA799F1E2CB843495F601E56C84A626D` (`tax_calculation_rate_id`,`tax_country_id`,`tax_region_id`,`zip_is_range`,`tax_postcode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate';

--
-- Dumping data for table `tax_calculation_rate`
--

LOCK TABLES `tax_calculation_rate` WRITE;
/*!40000 ALTER TABLE `tax_calculation_rate` DISABLE KEYS */;
INSERT INTO `tax_calculation_rate` VALUES ('1','US','12','*','US-CA-*-Rate 1','8.2500',NULL,NULL,NULL),('2','US','43','*','US-NY-*-Rate 1','8.3750',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tax_calculation_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_calculation_rate_title`
--

DROP TABLE IF EXISTS `tax_calculation_rate_title`;
CREATE TABLE `tax_calculation_rate_title` (
  `tax_calculation_rate_title_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Title Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`tax_calculation_rate_title_id`),
  KEY `TAX_CALCULATION_RATE_TITLE_TAX_CALCULATION_RATE_ID_STORE_ID` (`tax_calculation_rate_id`,`store_id`),
  KEY `TAX_CALCULATION_RATE_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate Title';

--
-- Table structure for table `tax_calculation_rule`
--

DROP TABLE IF EXISTS `tax_calculation_rule`;
CREATE TABLE `tax_calculation_rule` (
  `tax_calculation_rule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rule Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `calculate_subtotal` int(11) NOT NULL COMMENT 'Calculate off subtotal option',
  PRIMARY KEY (`tax_calculation_rule_id`),
  KEY `TAX_CALCULATION_RULE_PRIORITY_POSITION` (`priority`,`position`),
  KEY `TAX_CALCULATION_RULE_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rule';

--
-- Table structure for table `tax_class`
--

DROP TABLE IF EXISTS `tax_class`;
CREATE TABLE `tax_class` (
  `class_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Class Id',
  `class_name` varchar(255) NOT NULL COMMENT 'Class Name',
  `class_type` varchar(8) NOT NULL DEFAULT 'CUSTOMER' COMMENT 'Class Type',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Tax Class';

--
-- Dumping data for table `tax_class`
--

LOCK TABLES `tax_class` WRITE;
/*!40000 ALTER TABLE `tax_class` DISABLE KEYS */;
INSERT INTO `tax_class` VALUES (2,'Taxable Goods','PRODUCT'),(3,'Retail Customer','CUSTOMER');
/*!40000 ALTER TABLE `tax_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_order_aggregated_created`
--

DROP TABLE IF EXISTS `tax_order_aggregated_created`;
CREATE TABLE `tax_order_aggregated_created` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum',
  PRIMARY KEY (`id`),
  UNIQUE KEY `TAX_ORDER_AGGRED_CREATED_PERIOD_STORE_ID_CODE_PERCENT_ORDER_STS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `TAX_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregation';

--
-- Table structure for table `tax_order_aggregated_updated`
--

DROP TABLE IF EXISTS `tax_order_aggregated_updated`;
CREATE TABLE `tax_order_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum',
  PRIMARY KEY (`id`),
  UNIQUE KEY `TAX_ORDER_AGGRED_UPDATED_PERIOD_STORE_ID_CODE_PERCENT_ORDER_STS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `TAX_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregated Updated';

--
-- Table structure for table `theme`
--

DROP TABLE IF EXISTS `theme`;
CREATE TABLE `theme` (
  `theme_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Theme identifier',
  `parent_id` int(11) DEFAULT NULL COMMENT 'Parent Id',
  `theme_path` varchar(255) DEFAULT NULL COMMENT 'Theme Path',
  `theme_title` varchar(255) NOT NULL COMMENT 'Theme Title',
  `preview_image` varchar(255) DEFAULT NULL COMMENT 'Preview Image',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Is Theme Featured',
  `area` varchar(255) NOT NULL COMMENT 'Theme Area',
  `type` smallint(6) NOT NULL COMMENT 'Theme type: 0:physical, 1:virtual, 2:staging',
  `code` text COMMENT 'Full theme code, including package',
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Core theme';

--
-- Dumping data for table `theme`
--

LOCK TABLES `theme` WRITE;
/*!40000 ALTER TABLE `theme` DISABLE KEYS */;
INSERT INTO `theme` VALUES ('1',NULL,'Magento/blank','Magento Blank','preview_image_57f5b3bc239cb.jpeg',0,'frontend',0,'Magento/blank'),('2','1','Magento/luma','Magento Luma','preview_image_57f5b3bc83b94.jpeg',0,'frontend',0,'Magento/luma'),('3',NULL,'Magento/backend','Magento 2 backend',NULL,0,'adminhtml',0,'Magento/backend'),('4','1','Sahara/sahara_fashion8','Sahara Fashion8','preview_image_57f5b63ae7db2.jpeg',0,'frontend',0,'Sahara/sahara_fashion8'),('5','1','Sahara/sahara_fashion9','Sahara Fashion9','preview_image_58042a35800fa.jpeg',0,'frontend',0,'Sahara/sahara_fashion9'),('6','1','Sahara/sahara_fashion10','Sahara Fashion10','preview_image_58048a04bcf6b.jpeg',0,'frontend',0,'Sahara/sahara_fashion10');
/*!40000 ALTER TABLE `theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `theme_file`
--

DROP TABLE IF EXISTS `theme_file`;
CREATE TABLE `theme_file` (
  `theme_files_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Theme files identifier',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme Id',
  `file_path` varchar(255) DEFAULT NULL COMMENT 'Relative path to file',
  `file_type` varchar(32) NOT NULL COMMENT 'File Type',
  `content` longtext NOT NULL COMMENT 'File Content',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `is_temporary` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Is Temporary File',
  PRIMARY KEY (`theme_files_id`),
  KEY `THEME_FILE_THEME_ID_THEME_THEME_ID` (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Core theme files';

--
-- Table structure for table `translation`
--

DROP TABLE IF EXISTS `translation`;
CREATE TABLE `translation` (
  `key_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Key Id of Translation',
  `string` varchar(255) NOT NULL DEFAULT 'Translate String' COMMENT 'Translation String',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `translate` varchar(255) DEFAULT NULL COMMENT 'Translate',
  `locale` varchar(20) NOT NULL DEFAULT 'en_US' COMMENT 'Locale',
  `crc_string` bigint(20) NOT NULL DEFAULT '1591228201' COMMENT 'Translation String CRC32 Hash',
  PRIMARY KEY (`key_id`),
  UNIQUE KEY `TRANSLATION_STORE_ID_LOCALE_CRC_STRING_STRING` (`store_id`,`locale`,`crc_string`,`string`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Translations';

--
-- Table structure for table `ui_bookmark`
--

DROP TABLE IF EXISTS `ui_bookmark`;
CREATE TABLE `ui_bookmark` (
  `bookmark_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Bookmark identifier',
  `user_id` int(10) unsigned NOT NULL COMMENT 'User Id',
  `namespace` varchar(255) NOT NULL COMMENT 'Bookmark namespace',
  `identifier` varchar(255) NOT NULL COMMENT 'Bookmark Identifier',
  `current` smallint(6) NOT NULL COMMENT 'Mark current bookmark per user and identifier',
  `title` varchar(255) DEFAULT NULL COMMENT 'Bookmark title',
  `config` longtext COMMENT 'Bookmark config',
  `created_at` datetime NOT NULL COMMENT 'Bookmark created at',
  `updated_at` datetime NOT NULL COMMENT 'Bookmark updated at',
  PRIMARY KEY (`bookmark_id`),
  KEY `UI_BOOKMARK_USER_ID_NAMESPACE_IDENTIFIER` (`user_id`,`namespace`,`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Bookmark';

--
-- Dumping data for table `ui_bookmark`
--

LOCK TABLES `ui_bookmark` WRITE;
/*!40000 ALTER TABLE `ui_bookmark` DISABLE KEYS */;
INSERT INTO `ui_bookmark` VALUES ('1','1','design_config_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"default\":{\"visible\":true,\"sorting\":false},\"store_website_id\":{\"visible\":true,\"sorting\":false},\"store_group_id\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"theme_theme_id\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"default\":0,\"store_website_id\":1,\"store_group_id\":2,\"store_id\":3,\"theme_theme_id\":4,\"actions\":5}},\"value\":\"Default View\"}}}','1970-01-01 00:00:00','1970-01-01 00:00:00'),('2','1','design_config_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"default\":{\"visible\":true,\"sorting\":false},\"store_website_id\":{\"visible\":true,\"sorting\":false},\"store_group_id\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"theme_theme_id\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"default\":0,\"store_website_id\":1,\"store_group_id\":2,\"store_id\":3,\"theme_theme_id\":4,\"actions\":5}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('3','1','cms_page_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"search\":{\"value\":\"\"},\"columns\":{\"page_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keywords\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"custom_theme\":{\"visible\":false,\"sorting\":false},\"custom_root_template\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false},\"custom_theme_from\":{\"visible\":false,\"sorting\":false},\"custom_theme_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"page_id\":1,\"title\":2,\"identifier\":3,\"page_layout\":4,\"store_id\":5,\"is_active\":6,\"creation_time\":7,\"update_time\":8,\"custom_theme_from\":9,\"custom_theme_to\":10,\"custom_theme\":11,\"custom_root_template\":12,\"meta_title\":13,\"meta_keywords\":14,\"meta_description\":15,\"actions\":16}}}','1970-01-01 00:00:00','1970-01-01 00:00:00'),('4','1','cms_page_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"search\":{\"value\":\"\"},\"columns\":{\"page_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keywords\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"custom_theme\":{\"visible\":false,\"sorting\":false},\"custom_root_template\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false},\"custom_theme_from\":{\"visible\":false,\"sorting\":false},\"custom_theme_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"page_id\":1,\"title\":2,\"identifier\":3,\"page_layout\":4,\"store_id\":5,\"is_active\":6,\"creation_time\":7,\"update_time\":8,\"custom_theme_from\":9,\"custom_theme_to\":10,\"custom_theme\":11,\"custom_root_template\":12,\"meta_title\":13,\"meta_keywords\":14,\"meta_description\":15,\"actions\":16}},\"value\":\"Default View\"}}}','1970-01-01 00:00:00','1970-01-01 00:00:00'),('5','1','cms_block_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"search\":{\"value\":\"\"},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8}}}','1970-01-01 00:00:00','1970-01-01 00:00:00'),('6','1','cms_block_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"search\":{\"value\":\"\"},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8}},\"value\":\"Default View\"}}}','1970-01-01 00:00:00','1970-01-01 00:00:00'),('7','1','product_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"color\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"featured\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"compare_products\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"color\":22,\"news_from_date\":23,\"news_to_date\":24,\"custom_design\":25,\"custom_design_from\":26,\"custom_design_to\":27,\"page_layout\":28,\"country_of_manufacture\":29,\"custom_layout\":30,\"url_key\":31,\"msrp\":32,\"tax_class_id\":33,\"gift_message_available\":34,\"featured\":35,\"actions\":36}}}','1970-01-01 00:00:00','1970-01-01 00:00:00'),('8','1','product_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"color\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"featured\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"color\":22,\"news_from_date\":23,\"news_to_date\":24,\"custom_design\":25,\"custom_design_from\":26,\"custom_design_to\":27,\"page_layout\":28,\"country_of_manufacture\":29,\"custom_layout\":30,\"url_key\":31,\"msrp\":32,\"tax_class_id\":33,\"gift_message_available\":34,\"featured\":35,\"actions\":36}},\"value\":\"Default View\"}}}','1970-01-01 00:00:00','1970-01-01 00:00:00');
/*!40000 ALTER TABLE `ui_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `url_rewrite`
--

DROP TABLE IF EXISTS `url_rewrite`;
CREATE TABLE `url_rewrite` (
  `url_rewrite_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rewrite Id',
  `entity_type` varchar(32) NOT NULL COMMENT 'Entity type code',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `request_path` varchar(255) DEFAULT NULL COMMENT 'Request Path',
  `target_path` varchar(255) DEFAULT NULL COMMENT 'Target Path',
  `redirect_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Redirect Type',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `is_autogenerated` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is rewrite generated automatically flag',
  `metadata` varchar(255) DEFAULT NULL COMMENT 'Meta data for url rewrite',
  PRIMARY KEY (`url_rewrite_id`),
  UNIQUE KEY `URL_REWRITE_REQUEST_PATH_STORE_ID` (`request_path`,`store_id`),
  KEY `URL_REWRITE_TARGET_PATH` (`target_path`),
  KEY `URL_REWRITE_STORE_ID_ENTITY_ID` (`store_id`,`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3870 DEFAULT CHARSET=utf8 COMMENT='Url Rewrites';

--
-- Dumping data for table `url_rewrite`
--

LOCK TABLES `url_rewrite` WRITE;
/*!40000 ALTER TABLE `url_rewrite` DISABLE KEYS */;
INSERT INTO `url_rewrite` VALUES ('1','cms-page','1','no-route','cms/page/view/page_id/1',0,1,NULL,1,NULL),('2','cms-page','2','home','cms/page/view/page_id/2',0,1,NULL,1,NULL),('3','cms-page','3','enable-cookies','cms/page/view/page_id/3',0,1,NULL,1,NULL),('4','cms-page','4','privacy-policy-cookie-restriction-mode','cms/page/view/page_id/4',0,1,NULL,1,NULL),('26','category','8','jewelry.html','catalog/category/view/id/8',0,1,NULL,1,NULL),('28','category','8','jewelry.html','catalog/category/view/id/8',0,3,NULL,1,NULL),('29','category','8','jewelry.html','catalog/category/view/id/8',0,4,NULL,1,NULL),('34','cms-page','5','sahara-fashion8','cms/page/view/page_id/5',0,1,NULL,1,NULL),('1820','category','3','men.html','catalog/category/view/id/3',0,1,NULL,1,NULL),('1822','category','3','men.html','catalog/category/view/id/3',0,3,NULL,1,NULL),('1823','category','3','men.html','catalog/category/view/id/3',0,4,NULL,1,NULL),('2124','category','4','women.html','catalog/category/view/id/4',0,1,NULL,1,NULL),('2126','category','4','women.html','catalog/category/view/id/4',0,3,NULL,1,NULL),('2127','category','4','women.html','catalog/category/view/id/4',0,4,NULL,1,NULL),('2524','category','5','fashion.html','catalog/category/view/id/5',0,1,NULL,1,NULL),('2525','category','10','fashion/women.html','catalog/category/view/id/10',0,1,NULL,1,NULL),('2526','category','13','fashion/women/accessories.html','catalog/category/view/id/13',0,1,NULL,1,NULL),('2527','category','14','fashion/women/bags-purses.html','catalog/category/view/id/14',0,1,NULL,1,NULL),('2528','category','15','fashion/women/beauty.html','catalog/category/view/id/15',0,1,NULL,1,NULL),('2529','category','16','fashion/women/blazers.html','catalog/category/view/id/16',0,1,NULL,1,NULL),('2530','category','17','fashion/women/coats-jackets.html','catalog/category/view/id/17',0,1,NULL,1,NULL),('2531','category','18','fashion/women/curve-plus-size.html','catalog/category/view/id/18',0,1,NULL,1,NULL),('2532','category','11','fashion/men.html','catalog/category/view/id/11',0,1,NULL,1,NULL),('2533','category','19','fashion/men/coats-jackets.html','catalog/category/view/id/19',0,1,NULL,1,NULL),('2534','category','20','fashion/men/curve-plus-size.html','catalog/category/view/id/20',0,1,NULL,1,NULL),('2535','category','21','fashion/men/denim.html','catalog/category/view/id/21',0,1,NULL,1,NULL),('2536','category','22','fashion/men/designer.html','catalog/category/view/id/22',0,1,NULL,1,NULL),('2537','category','23','fashion/men/dresses.html','catalog/category/view/id/23',0,1,NULL,1,NULL),('2538','category','24','fashion/men/accessories.html','catalog/category/view/id/24',0,1,NULL,1,NULL),('2539','category','12','fashion/fashion.html','catalog/category/view/id/12',0,1,NULL,1,NULL),('2540','category','25','fashion/fashion/bags-purses.html','catalog/category/view/id/25',0,1,NULL,1,NULL),('2541','category','26','fashion/fashion/beauty.html','catalog/category/view/id/26',0,1,NULL,1,NULL),('2542','category','27','fashion/fashion/blazers.html','catalog/category/view/id/27',0,1,NULL,1,NULL),('2543','category','28','fashion/fashion/coats-jackets.html','catalog/category/view/id/28',0,1,NULL,1,NULL),('2544','category','29','fashion/fashion/curve-plus-size.html','catalog/category/view/id/29',0,1,NULL,1,NULL),('2545','category','30','fashion/fashion/denim.html','catalog/category/view/id/30',0,1,NULL,1,NULL),('2568','category','5','fashion.html','catalog/category/view/id/5',0,3,NULL,1,NULL),('2569','category','10','fashion/women.html','catalog/category/view/id/10',0,3,NULL,1,NULL),('2570','category','13','fashion/women/accessories.html','catalog/category/view/id/13',0,3,NULL,1,NULL),('2571','category','14','fashion/women/bags-purses.html','catalog/category/view/id/14',0,3,NULL,1,NULL),('2572','category','15','fashion/women/beauty.html','catalog/category/view/id/15',0,3,NULL,1,NULL),('2573','category','16','fashion/women/blazers.html','catalog/category/view/id/16',0,3,NULL,1,NULL),('2574','category','17','fashion/women/coats-jackets.html','catalog/category/view/id/17',0,3,NULL,1,NULL),('2575','category','18','fashion/women/curve-plus-size.html','catalog/category/view/id/18',0,3,NULL,1,NULL),('2576','category','11','fashion/men.html','catalog/category/view/id/11',0,3,NULL,1,NULL),('2577','category','19','fashion/men/coats-jackets.html','catalog/category/view/id/19',0,3,NULL,1,NULL),('2578','category','20','fashion/men/curve-plus-size.html','catalog/category/view/id/20',0,3,NULL,1,NULL),('2579','category','21','fashion/men/denim.html','catalog/category/view/id/21',0,3,NULL,1,NULL),('2580','category','22','fashion/men/designer.html','catalog/category/view/id/22',0,3,NULL,1,NULL),('2581','category','23','fashion/men/dresses.html','catalog/category/view/id/23',0,3,NULL,1,NULL),('2582','category','24','fashion/men/accessories.html','catalog/category/view/id/24',0,3,NULL,1,NULL),('2583','category','12','fashion/fashion.html','catalog/category/view/id/12',0,3,NULL,1,NULL),('2584','category','25','fashion/fashion/bags-purses.html','catalog/category/view/id/25',0,3,NULL,1,NULL),('2585','category','26','fashion/fashion/beauty.html','catalog/category/view/id/26',0,3,NULL,1,NULL),('2586','category','27','fashion/fashion/blazers.html','catalog/category/view/id/27',0,3,NULL,1,NULL),('2587','category','28','fashion/fashion/coats-jackets.html','catalog/category/view/id/28',0,3,NULL,1,NULL),('2588','category','29','fashion/fashion/curve-plus-size.html','catalog/category/view/id/29',0,3,NULL,1,NULL),('2589','category','30','fashion/fashion/denim.html','catalog/category/view/id/30',0,3,NULL,1,NULL),('2590','category','5','fashion.html','catalog/category/view/id/5',0,4,NULL,1,NULL),('2591','category','10','fashion/women.html','catalog/category/view/id/10',0,4,NULL,1,NULL),('2592','category','13','fashion/women/accessories.html','catalog/category/view/id/13',0,4,NULL,1,NULL),('2593','category','14','fashion/women/bags-purses.html','catalog/category/view/id/14',0,4,NULL,1,NULL),('2594','category','15','fashion/women/beauty.html','catalog/category/view/id/15',0,4,NULL,1,NULL),('2595','category','16','fashion/women/blazers.html','catalog/category/view/id/16',0,4,NULL,1,NULL),('2596','category','17','fashion/women/coats-jackets.html','catalog/category/view/id/17',0,4,NULL,1,NULL),('2597','category','18','fashion/women/curve-plus-size.html','catalog/category/view/id/18',0,4,NULL,1,NULL),('2598','category','11','fashion/men.html','catalog/category/view/id/11',0,4,NULL,1,NULL),('2599','category','19','fashion/men/coats-jackets.html','catalog/category/view/id/19',0,4,NULL,1,NULL),('2600','category','20','fashion/men/curve-plus-size.html','catalog/category/view/id/20',0,4,NULL,1,NULL),('2601','category','21','fashion/men/denim.html','catalog/category/view/id/21',0,4,NULL,1,NULL),('2602','category','22','fashion/men/designer.html','catalog/category/view/id/22',0,4,NULL,1,NULL),('2603','category','23','fashion/men/dresses.html','catalog/category/view/id/23',0,4,NULL,1,NULL),('2604','category','24','fashion/men/accessories.html','catalog/category/view/id/24',0,4,NULL,1,NULL),('2605','category','12','fashion/fashion.html','catalog/category/view/id/12',0,4,NULL,1,NULL),('2606','category','25','fashion/fashion/bags-purses.html','catalog/category/view/id/25',0,4,NULL,1,NULL),('2607','category','26','fashion/fashion/beauty.html','catalog/category/view/id/26',0,4,NULL,1,NULL),('2608','category','27','fashion/fashion/blazers.html','catalog/category/view/id/27',0,4,NULL,1,NULL),('2609','category','28','fashion/fashion/coats-jackets.html','catalog/category/view/id/28',0,4,NULL,1,NULL),('2610','category','29','fashion/fashion/curve-plus-size.html','catalog/category/view/id/29',0,4,NULL,1,NULL),('2611','category','30','fashion/fashion/denim.html','catalog/category/view/id/30',0,4,NULL,1,NULL),('2804','product','11','aliquam.html','catalog/product/view/id/11',0,1,NULL,1,NULL),('2805','product','11','women/aliquam.html','catalog/product/view/id/11/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('2806','product','11','fashion/aliquam.html','catalog/product/view/id/11/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('2807','product','11','/aliquam.html','catalog/product/view/id/11/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('2812','product','11','aliquam.html','catalog/product/view/id/11',0,3,NULL,1,NULL),('2813','product','11','women/aliquam.html','catalog/product/view/id/11/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('2814','product','11','fashion/aliquam.html','catalog/product/view/id/11/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('2815','product','11','/aliquam.html','catalog/product/view/id/11/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('2816','product','11','aliquam.html','catalog/product/view/id/11',0,4,NULL,1,NULL),('2817','product','11','women/aliquam.html','catalog/product/view/id/11/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('2818','product','11','fashion/aliquam.html','catalog/product/view/id/11/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('2819','product','11','/aliquam.html','catalog/product/view/id/11/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3008','category','6','gift.html','catalog/category/view/id/6',0,1,NULL,1,NULL),('3010','category','6','gift.html','catalog/category/view/id/6',0,3,NULL,1,NULL),('3011','category','6','gift.html','catalog/category/view/id/6',0,4,NULL,1,NULL),('3180','product','10','tempus.html','catalog/product/view/id/10',0,1,NULL,1,NULL),('3181','product','10','women/tempus.html','catalog/product/view/id/10/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3182','product','10','fashion/tempus.html','catalog/product/view/id/10/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3183','product','10','gift/tempus.html','catalog/product/view/id/10/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3184','product','10','jewelry/tempus.html','catalog/product/view/id/10/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3185','product','10','/tempus.html','catalog/product/view/id/10/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3192','product','10','tempus.html','catalog/product/view/id/10',0,3,NULL,1,NULL),('3193','product','10','women/tempus.html','catalog/product/view/id/10/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3194','product','10','fashion/tempus.html','catalog/product/view/id/10/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3195','product','10','gift/tempus.html','catalog/product/view/id/10/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3196','product','10','jewelry/tempus.html','catalog/product/view/id/10/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3197','product','10','/tempus.html','catalog/product/view/id/10/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3198','product','10','tempus.html','catalog/product/view/id/10',0,4,NULL,1,NULL),('3199','product','10','women/tempus.html','catalog/product/view/id/10/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3200','product','10','fashion/tempus.html','catalog/product/view/id/10/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3201','product','10','gift/tempus.html','catalog/product/view/id/10/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3202','product','10','jewelry/tempus.html','catalog/product/view/id/10/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3203','product','10','/tempus.html','catalog/product/view/id/10/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3392','category','7','kids.html','catalog/category/view/id/7',0,1,NULL,1,NULL),('3394','category','7','kids.html','catalog/category/view/id/7',0,3,NULL,1,NULL),('3395','category','7','kids.html','catalog/category/view/id/7',0,4,NULL,1,NULL),('3396','product','2','accumsan-elit.html','catalog/product/view/id/2',0,1,NULL,1,NULL),('3397','product','2','men/accumsan-elit.html','catalog/product/view/id/2/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3398','product','2','women/accumsan-elit.html','catalog/product/view/id/2/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3399','product','2','fashion/accumsan-elit.html','catalog/product/view/id/2/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3400','product','2','gift/accumsan-elit.html','catalog/product/view/id/2/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3401','product','2','kids/accumsan-elit.html','catalog/product/view/id/2/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3402','product','2','jewelry/accumsan-elit.html','catalog/product/view/id/2/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3403','product','2','/accumsan-elit.html','catalog/product/view/id/2/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3412','product','2','accumsan-elit.html','catalog/product/view/id/2',0,3,NULL,1,NULL),('3413','product','2','men/accumsan-elit.html','catalog/product/view/id/2/category/3',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3414','product','2','women/accumsan-elit.html','catalog/product/view/id/2/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3415','product','2','fashion/accumsan-elit.html','catalog/product/view/id/2/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3416','product','2','gift/accumsan-elit.html','catalog/product/view/id/2/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3417','product','2','kids/accumsan-elit.html','catalog/product/view/id/2/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3418','product','2','jewelry/accumsan-elit.html','catalog/product/view/id/2/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3419','product','2','/accumsan-elit.html','catalog/product/view/id/2/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3420','product','2','accumsan-elit.html','catalog/product/view/id/2',0,4,NULL,1,NULL),('3421','product','2','men/accumsan-elit.html','catalog/product/view/id/2/category/3',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3422','product','2','women/accumsan-elit.html','catalog/product/view/id/2/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3423','product','2','fashion/accumsan-elit.html','catalog/product/view/id/2/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3424','product','2','gift/accumsan-elit.html','catalog/product/view/id/2/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3425','product','2','kids/accumsan-elit.html','catalog/product/view/id/2/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3426','product','2','jewelry/accumsan-elit.html','catalog/product/view/id/2/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3427','product','2','/accumsan-elit.html','catalog/product/view/id/2/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3536','product','9','habitant.html','catalog/product/view/id/9',0,1,NULL,1,NULL),('3537','product','9','women/habitant.html','catalog/product/view/id/9/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3538','product','9','fashion/habitant.html','catalog/product/view/id/9/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3539','product','9','gift/habitant.html','catalog/product/view/id/9/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3540','product','9','kids/habitant.html','catalog/product/view/id/9/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3541','product','9','jewelry/habitant.html','catalog/product/view/id/9/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3542','product','9','/habitant.html','catalog/product/view/id/9/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3550','product','9','habitant.html','catalog/product/view/id/9',0,3,NULL,1,NULL),('3551','product','9','women/habitant.html','catalog/product/view/id/9/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3552','product','9','fashion/habitant.html','catalog/product/view/id/9/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3553','product','9','gift/habitant.html','catalog/product/view/id/9/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3554','product','9','kids/habitant.html','catalog/product/view/id/9/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3555','product','9','jewelry/habitant.html','catalog/product/view/id/9/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3556','product','9','/habitant.html','catalog/product/view/id/9/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3557','product','9','habitant.html','catalog/product/view/id/9',0,4,NULL,1,NULL),('3558','product','9','women/habitant.html','catalog/product/view/id/9/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3559','product','9','fashion/habitant.html','catalog/product/view/id/9/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3560','product','9','gift/habitant.html','catalog/product/view/id/9/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3561','product','9','kids/habitant.html','catalog/product/view/id/9/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3562','product','9','jewelry/habitant.html','catalog/product/view/id/9/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3563','product','9','/habitant.html','catalog/product/view/id/9/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3564','product','14','squa.html','catalog/product/view/id/14',0,1,NULL,1,NULL),('3565','product','14','women/squa.html','catalog/product/view/id/14/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3566','product','14','fashion/squa.html','catalog/product/view/id/14/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3567','product','14','men/squa.html','catalog/product/view/id/14/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3568','product','14','gift/squa.html','catalog/product/view/id/14/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3569','product','14','kids/squa.html','catalog/product/view/id/14/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3570','product','14','jewelry/squa.html','catalog/product/view/id/14/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3571','product','14','/squa.html','catalog/product/view/id/14/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3580','product','14','squa.html','catalog/product/view/id/14',0,3,NULL,1,NULL),('3581','product','14','women/squa.html','catalog/product/view/id/14/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3582','product','14','fashion/squa.html','catalog/product/view/id/14/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3583','product','14','men/squa.html','catalog/product/view/id/14/category/3',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3584','product','14','gift/squa.html','catalog/product/view/id/14/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3585','product','14','kids/squa.html','catalog/product/view/id/14/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3586','product','14','jewelry/squa.html','catalog/product/view/id/14/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3587','product','14','/squa.html','catalog/product/view/id/14/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3588','product','14','squa.html','catalog/product/view/id/14',0,4,NULL,1,NULL),('3589','product','14','women/squa.html','catalog/product/view/id/14/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3590','product','14','fashion/squa.html','catalog/product/view/id/14/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3591','product','14','men/squa.html','catalog/product/view/id/14/category/3',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3592','product','14','gift/squa.html','catalog/product/view/id/14/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3593','product','14','kids/squa.html','catalog/product/view/id/14/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3594','product','14','jewelry/squa.html','catalog/product/view/id/14/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3595','product','14','/squa.html','catalog/product/view/id/14/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3596','product','15','levis.html','catalog/product/view/id/15',0,1,NULL,1,NULL),('3597','product','15','men/levis.html','catalog/product/view/id/15/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3598','product','15','women/levis.html','catalog/product/view/id/15/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3599','product','15','fashion/levis.html','catalog/product/view/id/15/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3600','product','15','gift/levis.html','catalog/product/view/id/15/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3601','product','15','kids/levis.html','catalog/product/view/id/15/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3602','product','15','jewelry/levis.html','catalog/product/view/id/15/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3603','product','15','/levis.html','catalog/product/view/id/15/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3612','product','15','levis.html','catalog/product/view/id/15',0,3,NULL,1,NULL),('3613','product','15','men/levis.html','catalog/product/view/id/15/category/3',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3614','product','15','women/levis.html','catalog/product/view/id/15/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3615','product','15','fashion/levis.html','catalog/product/view/id/15/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3616','product','15','gift/levis.html','catalog/product/view/id/15/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3617','product','15','kids/levis.html','catalog/product/view/id/15/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3618','product','15','jewelry/levis.html','catalog/product/view/id/15/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3619','product','15','/levis.html','catalog/product/view/id/15/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3620','product','15','levis.html','catalog/product/view/id/15',0,4,NULL,1,NULL),('3621','product','15','men/levis.html','catalog/product/view/id/15/category/3',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3622','product','15','women/levis.html','catalog/product/view/id/15/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3623','product','15','fashion/levis.html','catalog/product/view/id/15/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3624','product','15','gift/levis.html','catalog/product/view/id/15/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3625','product','15','kids/levis.html','catalog/product/view/id/15/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3626','product','15','jewelry/levis.html','catalog/product/view/id/15/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3627','product','15','/levis.html','catalog/product/view/id/15/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3628','product','16','puma.html','catalog/product/view/id/16',0,1,NULL,1,NULL),('3629','product','16','men/puma.html','catalog/product/view/id/16/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3630','product','16','women/puma.html','catalog/product/view/id/16/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3631','product','16','fashion/puma.html','catalog/product/view/id/16/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3632','product','16','gift/puma.html','catalog/product/view/id/16/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3633','product','16','kids/puma.html','catalog/product/view/id/16/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3634','product','16','jewelry/puma.html','catalog/product/view/id/16/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3635','product','16','/puma.html','catalog/product/view/id/16/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3644','product','16','puma.html','catalog/product/view/id/16',0,3,NULL,1,NULL),('3645','product','16','men/puma.html','catalog/product/view/id/16/category/3',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3646','product','16','women/puma.html','catalog/product/view/id/16/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3647','product','16','fashion/puma.html','catalog/product/view/id/16/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3648','product','16','gift/puma.html','catalog/product/view/id/16/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3649','product','16','kids/puma.html','catalog/product/view/id/16/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3650','product','16','jewelry/puma.html','catalog/product/view/id/16/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3651','product','16','/puma.html','catalog/product/view/id/16/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3652','product','16','puma.html','catalog/product/view/id/16',0,4,NULL,1,NULL),('3653','product','16','men/puma.html','catalog/product/view/id/16/category/3',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3654','product','16','women/puma.html','catalog/product/view/id/16/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3655','product','16','fashion/puma.html','catalog/product/view/id/16/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3656','product','16','gift/puma.html','catalog/product/view/id/16/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3657','product','16','kids/puma.html','catalog/product/view/id/16/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3658','product','16','jewelry/puma.html','catalog/product/view/id/16/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3659','product','16','/puma.html','catalog/product/view/id/16/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3660','product','17','adidas.html','catalog/product/view/id/17',0,1,NULL,1,NULL),('3661','product','17','men/adidas.html','catalog/product/view/id/17/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3662','product','17','women/adidas.html','catalog/product/view/id/17/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3663','product','17','fashion/adidas.html','catalog/product/view/id/17/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3664','product','17','gift/adidas.html','catalog/product/view/id/17/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3665','product','17','kids/adidas.html','catalog/product/view/id/17/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3666','product','17','jewelry/adidas.html','catalog/product/view/id/17/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3667','product','17','/adidas.html','catalog/product/view/id/17/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3676','product','17','adidas.html','catalog/product/view/id/17',0,3,NULL,1,NULL),('3677','product','17','men/adidas.html','catalog/product/view/id/17/category/3',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3678','product','17','women/adidas.html','catalog/product/view/id/17/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3679','product','17','fashion/adidas.html','catalog/product/view/id/17/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3680','product','17','gift/adidas.html','catalog/product/view/id/17/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3681','product','17','kids/adidas.html','catalog/product/view/id/17/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3682','product','17','jewelry/adidas.html','catalog/product/view/id/17/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3683','product','17','/adidas.html','catalog/product/view/id/17/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3684','product','17','adidas.html','catalog/product/view/id/17',0,4,NULL,1,NULL),('3685','product','17','men/adidas.html','catalog/product/view/id/17/category/3',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3686','product','17','women/adidas.html','catalog/product/view/id/17/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3687','product','17','fashion/adidas.html','catalog/product/view/id/17/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3688','product','17','gift/adidas.html','catalog/product/view/id/17/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3689','product','17','kids/adidas.html','catalog/product/view/id/17/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3690','product','17','jewelry/adidas.html','catalog/product/view/id/17/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3691','product','17','/adidas.html','catalog/product/view/id/17/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3692','product','18','nike.html','catalog/product/view/id/18',0,1,NULL,1,NULL),('3693','product','18','men/nike.html','catalog/product/view/id/18/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3694','product','18','women/nike.html','catalog/product/view/id/18/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3695','product','18','fashion/nike.html','catalog/product/view/id/18/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3696','product','18','gift/nike.html','catalog/product/view/id/18/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3697','product','18','kids/nike.html','catalog/product/view/id/18/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3698','product','18','jewelry/nike.html','catalog/product/view/id/18/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3699','product','18','/nike.html','catalog/product/view/id/18/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3708','product','18','nike.html','catalog/product/view/id/18',0,3,NULL,1,NULL),('3709','product','18','men/nike.html','catalog/product/view/id/18/category/3',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3710','product','18','women/nike.html','catalog/product/view/id/18/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3711','product','18','fashion/nike.html','catalog/product/view/id/18/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3712','product','18','gift/nike.html','catalog/product/view/id/18/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3713','product','18','kids/nike.html','catalog/product/view/id/18/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3714','product','18','jewelry/nike.html','catalog/product/view/id/18/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3715','product','18','/nike.html','catalog/product/view/id/18/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3716','product','18','nike.html','catalog/product/view/id/18',0,4,NULL,1,NULL),('3717','product','18','men/nike.html','catalog/product/view/id/18/category/3',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3718','product','18','women/nike.html','catalog/product/view/id/18/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3719','product','18','fashion/nike.html','catalog/product/view/id/18/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3720','product','18','gift/nike.html','catalog/product/view/id/18/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3721','product','18','kids/nike.html','catalog/product/view/id/18/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3722','product','18','jewelry/nike.html','catalog/product/view/id/18/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3723','product','18','/nike.html','catalog/product/view/id/18/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3724','product','19','violet.html','catalog/product/view/id/19',0,1,NULL,1,NULL),('3725','product','19','women/violet.html','catalog/product/view/id/19/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3726','product','19','fashion/violet.html','catalog/product/view/id/19/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3727','product','19','gift/violet.html','catalog/product/view/id/19/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3728','product','19','kids/violet.html','catalog/product/view/id/19/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3729','product','19','jewelry/violet.html','catalog/product/view/id/19/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3730','product','19','/violet.html','catalog/product/view/id/19/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3738','product','19','violet.html','catalog/product/view/id/19',0,3,NULL,1,NULL),('3739','product','19','women/violet.html','catalog/product/view/id/19/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3740','product','19','fashion/violet.html','catalog/product/view/id/19/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3741','product','19','gift/violet.html','catalog/product/view/id/19/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3742','product','19','kids/violet.html','catalog/product/view/id/19/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3743','product','19','jewelry/violet.html','catalog/product/view/id/19/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3744','product','19','/violet.html','catalog/product/view/id/19/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3745','product','19','violet.html','catalog/product/view/id/19',0,4,NULL,1,NULL),('3746','product','19','women/violet.html','catalog/product/view/id/19/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3747','product','19','fashion/violet.html','catalog/product/view/id/19/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3748','product','19','gift/violet.html','catalog/product/view/id/19/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3749','product','19','kids/violet.html','catalog/product/view/id/19/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3750','product','19','jewelry/violet.html','catalog/product/view/id/19/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3751','product','19','/violet.html','catalog/product/view/id/19/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3752','category','9','contact-us.html','catalog/category/view/id/9',0,1,NULL,1,NULL),('3754','category','9','contact-us.html','catalog/category/view/id/9',0,3,NULL,1,NULL),('3755','category','9','contact-us.html','catalog/category/view/id/9',0,4,NULL,1,NULL),('3756','product','3','nunc-facilisis.html','catalog/product/view/id/3',0,1,NULL,1,NULL),('3757','product','3','men/nunc-facilisis.html','catalog/product/view/id/3/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3758','product','3','women/nunc-facilisis.html','catalog/product/view/id/3/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3759','product','3','fashion/nunc-facilisis.html','catalog/product/view/id/3/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3760','product','3','gift/nunc-facilisis.html','catalog/product/view/id/3/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3761','product','3','kids/nunc-facilisis.html','catalog/product/view/id/3/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3762','product','3','jewelry/nunc-facilisis.html','catalog/product/view/id/3/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3763','product','3','contact-us/nunc-facilisis.html','catalog/product/view/id/3/category/9',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"9\";}'),('3764','product','3','/nunc-facilisis.html','catalog/product/view/id/3/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3774','product','3','nunc-facilisis.html','catalog/product/view/id/3',0,3,NULL,1,NULL),('3775','product','3','men/nunc-facilisis.html','catalog/product/view/id/3/category/3',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3776','product','3','women/nunc-facilisis.html','catalog/product/view/id/3/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3777','product','3','fashion/nunc-facilisis.html','catalog/product/view/id/3/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3778','product','3','gift/nunc-facilisis.html','catalog/product/view/id/3/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3779','product','3','kids/nunc-facilisis.html','catalog/product/view/id/3/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3780','product','3','jewelry/nunc-facilisis.html','catalog/product/view/id/3/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3781','product','3','contact-us/nunc-facilisis.html','catalog/product/view/id/3/category/9',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"9\";}'),('3782','product','3','/nunc-facilisis.html','catalog/product/view/id/3/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3783','product','3','nunc-facilisis.html','catalog/product/view/id/3',0,4,NULL,1,NULL),('3784','product','3','men/nunc-facilisis.html','catalog/product/view/id/3/category/3',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3785','product','3','women/nunc-facilisis.html','catalog/product/view/id/3/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3786','product','3','fashion/nunc-facilisis.html','catalog/product/view/id/3/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3787','product','3','gift/nunc-facilisis.html','catalog/product/view/id/3/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3788','product','3','kids/nunc-facilisis.html','catalog/product/view/id/3/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3789','product','3','jewelry/nunc-facilisis.html','catalog/product/view/id/3/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3790','product','3','contact-us/nunc-facilisis.html','catalog/product/view/id/3/category/9',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"9\";}'),('3791','product','3','/nunc-facilisis.html','catalog/product/view/id/3/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3792','product','4','etiam-gravida.html','catalog/product/view/id/4',0,1,NULL,1,NULL),('3793','product','4','men/etiam-gravida.html','catalog/product/view/id/4/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3794','product','4','women/etiam-gravida.html','catalog/product/view/id/4/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3795','product','4','fashion/etiam-gravida.html','catalog/product/view/id/4/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3796','product','4','gift/etiam-gravida.html','catalog/product/view/id/4/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3797','product','4','kids/etiam-gravida.html','catalog/product/view/id/4/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3798','product','4','jewelry/etiam-gravida.html','catalog/product/view/id/4/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3799','product','4','contact-us/etiam-gravida.html','catalog/product/view/id/4/category/9',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"9\";}'),('3800','product','4','/etiam-gravida.html','catalog/product/view/id/4/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3810','product','4','etiam-gravida.html','catalog/product/view/id/4',0,3,NULL,1,NULL),('3811','product','4','men/etiam-gravida.html','catalog/product/view/id/4/category/3',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3812','product','4','women/etiam-gravida.html','catalog/product/view/id/4/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3813','product','4','fashion/etiam-gravida.html','catalog/product/view/id/4/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3814','product','4','gift/etiam-gravida.html','catalog/product/view/id/4/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3815','product','4','kids/etiam-gravida.html','catalog/product/view/id/4/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3816','product','4','jewelry/etiam-gravida.html','catalog/product/view/id/4/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3817','product','4','contact-us/etiam-gravida.html','catalog/product/view/id/4/category/9',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"9\";}'),('3818','product','4','/etiam-gravida.html','catalog/product/view/id/4/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3819','product','4','etiam-gravida.html','catalog/product/view/id/4',0,4,NULL,1,NULL),('3820','product','4','men/etiam-gravida.html','catalog/product/view/id/4/category/3',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3821','product','4','women/etiam-gravida.html','catalog/product/view/id/4/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3822','product','4','fashion/etiam-gravida.html','catalog/product/view/id/4/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3823','product','4','gift/etiam-gravida.html','catalog/product/view/id/4/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3824','product','4','kids/etiam-gravida.html','catalog/product/view/id/4/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3825','product','4','jewelry/etiam-gravida.html','catalog/product/view/id/4/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3826','product','4','contact-us/etiam-gravida.html','catalog/product/view/id/4/category/9',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"9\";}'),('3827','product','4','/etiam-gravida.html','catalog/product/view/id/4/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3828','product','5','donec-non-est.html','catalog/product/view/id/5',0,1,NULL,1,NULL),('3829','product','5','men/donec-non-est.html','catalog/product/view/id/5/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3830','product','5','women/donec-non-est.html','catalog/product/view/id/5/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3831','product','5','fashion/donec-non-est.html','catalog/product/view/id/5/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3832','product','5','gift/donec-non-est.html','catalog/product/view/id/5/category/6',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3833','product','5','kids/donec-non-est.html','catalog/product/view/id/5/category/7',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3834','product','5','jewelry/donec-non-est.html','catalog/product/view/id/5/category/8',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3835','product','5','contact-us/donec-non-est.html','catalog/product/view/id/5/category/9',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"9\";}'),('3836','product','5','/donec-non-est.html','catalog/product/view/id/5/category/2',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3846','product','5','donec-non-est.html','catalog/product/view/id/5',0,3,NULL,1,NULL),('3847','product','5','men/donec-non-est.html','catalog/product/view/id/5/category/3',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3848','product','5','women/donec-non-est.html','catalog/product/view/id/5/category/4',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3849','product','5','fashion/donec-non-est.html','catalog/product/view/id/5/category/5',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3850','product','5','gift/donec-non-est.html','catalog/product/view/id/5/category/6',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3851','product','5','kids/donec-non-est.html','catalog/product/view/id/5/category/7',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3852','product','5','jewelry/donec-non-est.html','catalog/product/view/id/5/category/8',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3853','product','5','contact-us/donec-non-est.html','catalog/product/view/id/5/category/9',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"9\";}'),('3854','product','5','/donec-non-est.html','catalog/product/view/id/5/category/2',0,3,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3855','product','5','donec-non-est.html','catalog/product/view/id/5',0,4,NULL,1,NULL),('3856','product','5','men/donec-non-est.html','catalog/product/view/id/5/category/3',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('3857','product','5','women/donec-non-est.html','catalog/product/view/id/5/category/4',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('3858','product','5','fashion/donec-non-est.html','catalog/product/view/id/5/category/5',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('3859','product','5','gift/donec-non-est.html','catalog/product/view/id/5/category/6',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"6\";}'),('3860','product','5','kids/donec-non-est.html','catalog/product/view/id/5/category/7',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"7\";}'),('3861','product','5','jewelry/donec-non-est.html','catalog/product/view/id/5/category/8',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"8\";}'),('3862','product','5','contact-us/donec-non-est.html','catalog/product/view/id/5/category/9',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"9\";}'),('3863','product','5','/donec-non-est.html','catalog/product/view/id/5/category/2',0,4,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"2\";}'),('3864','cms-page','6','sahara-fashion9','cms/page/view/page_id/6',0,3,NULL,1,NULL),('3869','cms-page','7','sahara-fashion10','cms/page/view/page_id/7',0,4,NULL,1,NULL);
/*!40000 ALTER TABLE `url_rewrite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `variable`
--

DROP TABLE IF EXISTS `variable`;
CREATE TABLE `variable` (
  `variable_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Variable Code',
  `name` varchar(255) DEFAULT NULL COMMENT 'Variable Name',
  PRIMARY KEY (`variable_id`),
  UNIQUE KEY `VARIABLE_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variables';

--
-- Table structure for table `variable_value`
--

DROP TABLE IF EXISTS `variable_value`;
CREATE TABLE `variable_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable Value Id',
  `variable_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Variable Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `plain_value` text COMMENT 'Plain Text Value',
  `html_value` text COMMENT 'Html Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `VARIABLE_VALUE_VARIABLE_ID_STORE_ID` (`variable_id`,`store_id`),
  KEY `VARIABLE_VALUE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variable Value';

--
-- Table structure for table `vault_payment_token`
--

DROP TABLE IF EXISTS `vault_payment_token`;
CREATE TABLE `vault_payment_token` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `public_hash` varchar(128) NOT NULL COMMENT 'Hash code for using on frontend',
  `payment_method_code` varchar(128) NOT NULL COMMENT 'Payment method code',
  `type` varchar(128) NOT NULL COMMENT 'Type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `expires_at` timestamp NULL DEFAULT NULL COMMENT 'Expires At',
  `gateway_token` varchar(255) NOT NULL COMMENT 'Gateway Token',
  `details` text COMMENT 'Details',
  `is_active` tinyint(1) NOT NULL COMMENT 'Is active flag',
  `is_visible` tinyint(1) NOT NULL COMMENT 'Is visible flag',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `VAULT_PAYMENT_TOKEN_HASH_UNIQUE_INDEX_PUBLIC_HASH` (`public_hash`),
  UNIQUE KEY `UNQ_54DCE14AEAEA03B587F9EF723EB10A10` (`payment_method_code`,`customer_id`,`gateway_token`),
  KEY `VAULT_PAYMENT_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Vault tokens of payment';

--
-- Table structure for table `vault_payment_token_order_payment_link`
--

DROP TABLE IF EXISTS `vault_payment_token_order_payment_link`;
CREATE TABLE `vault_payment_token_order_payment_link` (
  `order_payment_id` int(10) unsigned NOT NULL COMMENT 'Order payment Id',
  `payment_token_id` int(10) unsigned NOT NULL COMMENT 'Payment token Id',
  PRIMARY KEY (`order_payment_id`,`payment_token_id`),
  KEY `FK_4ED894655446D385894580BECA993862` (`payment_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Order payments to vault token';

--
-- Table structure for table `weee_tax`
--

DROP TABLE IF EXISTS `weee_tax`;
CREATE TABLE `weee_tax` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `country` varchar(2) DEFAULT NULL COMMENT 'Country',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT 'State',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`value_id`),
  KEY `WEEE_TAX_WEBSITE_ID` (`website_id`),
  KEY `WEEE_TAX_ENTITY_ID` (`entity_id`),
  KEY `WEEE_TAX_COUNTRY` (`country`),
  KEY `WEEE_TAX_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Weee Tax';

--
-- Table structure for table `widget`
--

DROP TABLE IF EXISTS `widget`;
CREATE TABLE `widget` (
  `widget_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Widget Id',
  `widget_code` varchar(255) DEFAULT NULL COMMENT 'Widget code for template directive',
  `widget_type` varchar(255) DEFAULT NULL COMMENT 'Widget Type',
  `parameters` text COMMENT 'Parameters',
  PRIMARY KEY (`widget_id`),
  KEY `WIDGET_WIDGET_CODE` (`widget_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Preconfigured Widgets';

--
-- Table structure for table `widget_instance`
--

DROP TABLE IF EXISTS `widget_instance`;
CREATE TABLE `widget_instance` (
  `instance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Instance Id',
  `instance_type` varchar(255) DEFAULT NULL COMMENT 'Instance Type',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Widget Title',
  `store_ids` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Store ids',
  `widget_parameters` text COMMENT 'Widget parameters',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order',
  PRIMARY KEY (`instance_id`),
  KEY `WIDGET_INSTANCE_THEME_ID_THEME_THEME_ID` (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Instances of Widget for Package Theme';

--
-- Table structure for table `widget_instance_page`
--

DROP TABLE IF EXISTS `widget_instance_page`;
CREATE TABLE `widget_instance_page` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Page Id',
  `instance_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Id',
  `page_group` varchar(25) DEFAULT NULL COMMENT 'Block Group Type',
  `layout_handle` varchar(255) DEFAULT NULL COMMENT 'Layout Handle',
  `block_reference` varchar(255) DEFAULT NULL COMMENT 'Container',
  `page_for` varchar(25) DEFAULT NULL COMMENT 'For instance entities',
  `entities` text COMMENT 'Catalog entities (comma separated)',
  `page_template` varchar(255) DEFAULT NULL COMMENT 'Path to widget template',
  PRIMARY KEY (`page_id`),
  KEY `WIDGET_INSTANCE_PAGE_INSTANCE_ID` (`instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Instance of Widget on Page';

--
-- Table structure for table `widget_instance_page_layout`
--

DROP TABLE IF EXISTS `widget_instance_page_layout`;
CREATE TABLE `widget_instance_page_layout` (
  `page_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Page Id',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Layout Update Id',
  UNIQUE KEY `WIDGET_INSTANCE_PAGE_LAYOUT_LAYOUT_UPDATE_ID_PAGE_ID` (`layout_update_id`,`page_id`),
  KEY `WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout updates';

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE `wishlist` (
  `wishlist_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Wishlist ID',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer ID',
  `shared` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sharing flag (0 or 1)',
  `sharing_code` varchar(32) DEFAULT NULL COMMENT 'Sharing encrypted code',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Last updated date',
  PRIMARY KEY (`wishlist_id`),
  UNIQUE KEY `WISHLIST_CUSTOMER_ID` (`customer_id`),
  KEY `WISHLIST_SHARED` (`shared`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist main Table';

--
-- Table structure for table `wishlist_item`
--

DROP TABLE IF EXISTS `wishlist_item`;
CREATE TABLE `wishlist_item` (
  `wishlist_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Wishlist item ID',
  `wishlist_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Wishlist ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store ID',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Add date and time',
  `description` text COMMENT 'Short description of wish list item',
  `qty` decimal(12,4) NOT NULL COMMENT 'Qty',
  PRIMARY KEY (`wishlist_item_id`),
  KEY `WISHLIST_ITEM_WISHLIST_ID` (`wishlist_id`),
  KEY `WISHLIST_ITEM_PRODUCT_ID` (`product_id`),
  KEY `WISHLIST_ITEM_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist items';

--
-- Table structure for table `wishlist_item_option`
--

DROP TABLE IF EXISTS `wishlist_item_option`;
CREATE TABLE `wishlist_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `wishlist_item_id` int(10) unsigned NOT NULL COMMENT 'Wishlist Item Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`option_id`),
  KEY `FK_A014B30B04B72DD0EAB3EECD779728D6` (`wishlist_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist Item Option Table';

ALTER TABLE `admin_passwords`
  ADD CONSTRAINT `ADMIN_PASSWORDS_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE;

ALTER TABLE `admin_user_session`
  ADD CONSTRAINT `ADMIN_USER_SESSION_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE;

ALTER TABLE `authorization_rule`
  ADD CONSTRAINT `AUTHORIZATION_RULE_ROLE_ID_AUTHORIZATION_ROLE_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `authorization_role` (`role_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_entity_datetime`
  ADD CONSTRAINT `CATALOG_CATEGORY_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_DTIME_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_entity_decimal`
  ADD CONSTRAINT `CATALOG_CATEGORY_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_DEC_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_entity_int`
  ADD CONSTRAINT `CATALOG_CATEGORY_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_INT_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_entity_text`
  ADD CONSTRAINT `CATALOG_CATEGORY_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_TEXT_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_entity_varchar`
  ADD CONSTRAINT `CATALOG_CATEGORY_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_VCHR_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_product`
  ADD CONSTRAINT `CAT_CTGR_PRD_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_PRD_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_compare_item`
  ADD CONSTRAINT `CATALOG_COMPARE_ITEM_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATALOG_COMPARE_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATALOG_COMPARE_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `catalog_eav_attribute`
  ADD CONSTRAINT `CATALOG_EAV_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_bundle_option`
  ADD CONSTRAINT `CAT_PRD_BNDL_OPT_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_bundle_option_value`
  ADD CONSTRAINT `CAT_PRD_BNDL_OPT_VAL_OPT_ID_CAT_PRD_BNDL_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_bundle_price_index`
  ADD CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_bundle_selection`
  ADD CONSTRAINT `CAT_PRD_BNDL_SELECTION_OPT_ID_CAT_PRD_BNDL_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_BNDL_SELECTION_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_bundle_selection_price`
  ADD CONSTRAINT `CAT_PRD_BNDL_SELECTION_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DCF37523AA05D770A70AA4ED7C2616E4` FOREIGN KEY (`selection_id`) REFERENCES `catalog_product_bundle_selection` (`selection_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity`
  ADD CONSTRAINT `CAT_PRD_ENTT_ATTR_SET_ID_EAV_ATTR_SET_ATTR_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_datetime`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_DTIME_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_decimal`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_DEC_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_gallery`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_GALLERY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_GLR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_int`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_INT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_media_gallery`
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_media_gallery_value`
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_VAL_ID_CAT_PRD_ENTT_MDA_GLR_VAL_ID` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_media_gallery_value_to_entity`
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_A6C6C8FAA386736921D3A7C4B50B1185` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_media_gallery_value_video`
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6FDF205946906B0E653E60AA769899F8` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_text`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_TEXT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_tier_price`
  ADD CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_varchar`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_VCHR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_index_price`
  ADD CONSTRAINT `CATALOG_PRODUCT_INDEX_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_IDX_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_IDX_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_index_tier_price`
  ADD CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_index_website`
  ADD CONSTRAINT `CAT_PRD_IDX_WS_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_link`
  ADD CONSTRAINT `CATALOG_PRODUCT_LINK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_LNK_LNKED_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`linked_product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_LNK_LNK_TYPE_ID_CAT_PRD_LNK_TYPE_LNK_TYPE_ID` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_link_attribute`
  ADD CONSTRAINT `CAT_PRD_LNK_ATTR_LNK_TYPE_ID_CAT_PRD_LNK_TYPE_LNK_TYPE_ID` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_link_attribute_decimal`
  ADD CONSTRAINT `CAT_PRD_LNK_ATTR_DEC_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AB2EFA9A14F7BCF1D5400056203D14B6` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_link_attribute_int`
  ADD CONSTRAINT `CAT_PRD_LNK_ATTR_INT_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_D6D878F8BA2A4282F8DDED7E6E3DE35C` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_link_attribute_varchar`
  ADD CONSTRAINT `CAT_PRD_LNK_ATTR_VCHR_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DEE9C4DA61CFCC01DFCF50F0D79CEA51` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option`
  ADD CONSTRAINT `CAT_PRD_OPT_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option_price`
  ADD CONSTRAINT `CATALOG_PRODUCT_OPTION_PRICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_OPT_PRICE_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option_title`
  ADD CONSTRAINT `CATALOG_PRODUCT_OPTION_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_OPT_TTL_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option_type_price`
  ADD CONSTRAINT `CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B523E3378E8602F376CC415825576B7F` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option_type_title`
  ADD CONSTRAINT `CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_C085B9CF2C2A302E8043FDEA1937D6A2` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option_type_value`
  ADD CONSTRAINT `CAT_PRD_OPT_TYPE_VAL_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_relation`
  ADD CONSTRAINT `CAT_PRD_RELATION_CHILD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`child_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_RELATION_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_super_attribute`
  ADD CONSTRAINT `CAT_PRD_SPR_ATTR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_super_attribute_label`
  ADD CONSTRAINT `CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_309442281DF7784210ED82B2CC51E5D5` FOREIGN KEY (`product_super_attribute_id`) REFERENCES `catalog_product_super_attribute` (`product_super_attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_super_link`
  ADD CONSTRAINT `CAT_PRD_SPR_LNK_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_SPR_LNK_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_website`
  ADD CONSTRAINT `CATALOG_PRODUCT_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_WS_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_url_rewrite_product_category`
  ADD CONSTRAINT `CAT_URL_REWRITE_PRD_CTGR_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_URL_REWRITE_PRD_CTGR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_BB79E64705D7F17FE181F23144528FC8` FOREIGN KEY (`url_rewrite_id`) REFERENCES `url_rewrite` (`url_rewrite_id`) ON DELETE CASCADE;

ALTER TABLE `cataloginventory_stock_item`
  ADD CONSTRAINT `CATINV_STOCK_ITEM_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATINV_STOCK_ITEM_STOCK_ID_CATINV_STOCK_STOCK_ID` FOREIGN KEY (`stock_id`) REFERENCES `cataloginventory_stock` (`stock_id`) ON DELETE CASCADE;

ALTER TABLE `catalogrule_customer_group`
  ADD CONSTRAINT `CATALOGRULE_CUSTOMER_GROUP_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE;

ALTER TABLE `catalogrule_group_website`
  ADD CONSTRAINT `CATALOGRULE_GROUP_WEBSITE_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATALOGRULE_GROUP_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATRULE_GROUP_WS_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE;

ALTER TABLE `catalogrule_website`
  ADD CONSTRAINT `CATALOGRULE_WEBSITE_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATALOGRULE_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `checkout_agreement_store`
  ADD CONSTRAINT `CHECKOUT_AGREEMENT_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CHKT_AGRT_STORE_AGRT_ID_CHKT_AGRT_AGRT_ID` FOREIGN KEY (`agreement_id`) REFERENCES `checkout_agreement` (`agreement_id`) ON DELETE CASCADE;

ALTER TABLE `cms_block_store`
  ADD CONSTRAINT `CMS_BLOCK_STORE_BLOCK_ID_CMS_BLOCK_BLOCK_ID` FOREIGN KEY (`block_id`) REFERENCES `cms_block` (`block_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CMS_BLOCK_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `cms_page_store`
  ADD CONSTRAINT `CMS_PAGE_STORE_PAGE_ID_CMS_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`page_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CMS_PAGE_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity`
  ADD CONSTRAINT `CUSTOMER_ADDRESS_ENTITY_PARENT_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity_datetime`
  ADD CONSTRAINT `CSTR_ADDR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_ADDR_ENTT_DTIME_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity_decimal`
  ADD CONSTRAINT `CSTR_ADDR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_ADDR_ENTT_DEC_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity_int`
  ADD CONSTRAINT `CSTR_ADDR_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_ADDR_ENTT_INT_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity_text`
  ADD CONSTRAINT `CSTR_ADDR_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_ADDR_ENTT_TEXT_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity_varchar`
  ADD CONSTRAINT `CSTR_ADDR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_ADDR_ENTT_VCHR_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_eav_attribute`
  ADD CONSTRAINT `CUSTOMER_EAV_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `customer_eav_attribute_website`
  ADD CONSTRAINT `CSTR_EAV_ATTR_WS_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_EAV_ATTR_WS_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `customer_entity`
  ADD CONSTRAINT `CUSTOMER_ENTITY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `CUSTOMER_ENTITY_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE SET NULL;

ALTER TABLE `customer_entity_datetime`
  ADD CONSTRAINT `CUSTOMER_ENTITY_DATETIME_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_entity_decimal`
  ADD CONSTRAINT `CUSTOMER_ENTITY_DECIMAL_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_entity_int`
  ADD CONSTRAINT `CUSTOMER_ENTITY_INT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CUSTOMER_ENTITY_INT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_entity_text`
  ADD CONSTRAINT `CUSTOMER_ENTITY_TEXT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CUSTOMER_ENTITY_TEXT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_entity_varchar`
  ADD CONSTRAINT `CUSTOMER_ENTITY_VARCHAR_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_form_attribute`
  ADD CONSTRAINT `CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `design_change`
  ADD CONSTRAINT `DESIGN_CHANGE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `directory_country_region_name`
  ADD CONSTRAINT `DIR_COUNTRY_REGION_NAME_REGION_ID_DIR_COUNTRY_REGION_REGION_ID` FOREIGN KEY (`region_id`) REFERENCES `directory_country_region` (`region_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_link`
  ADD CONSTRAINT `DOWNLOADABLE_LINK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_link_price`
  ADD CONSTRAINT `DOWNLOADABLE_LINK_PRICE_LINK_ID_DOWNLOADABLE_LINK_LINK_ID` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `DOWNLOADABLE_LINK_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_link_purchased`
  ADD CONSTRAINT `DL_LNK_PURCHASED_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `DOWNLOADABLE_LINK_PURCHASED_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE SET NULL;

ALTER TABLE `downloadable_link_purchased_item`
  ADD CONSTRAINT `DL_LNK_PURCHASED_ITEM_ORDER_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`order_item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `DL_LNK_PURCHASED_ITEM_PURCHASED_ID_DL_LNK_PURCHASED_PURCHASED_ID` FOREIGN KEY (`purchased_id`) REFERENCES `downloadable_link_purchased` (`purchased_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_link_title`
  ADD CONSTRAINT `DOWNLOADABLE_LINK_TITLE_LINK_ID_DOWNLOADABLE_LINK_LINK_ID` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `DOWNLOADABLE_LINK_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_sample`
  ADD CONSTRAINT `DOWNLOADABLE_SAMPLE_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_sample_title`
  ADD CONSTRAINT `DL_SAMPLE_TTL_SAMPLE_ID_DL_SAMPLE_SAMPLE_ID` FOREIGN KEY (`sample_id`) REFERENCES `downloadable_sample` (`sample_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `DOWNLOADABLE_SAMPLE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute`
  ADD CONSTRAINT `EAV_ATTRIBUTE_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_group`
  ADD CONSTRAINT `EAV_ATTR_GROUP_ATTR_SET_ID_EAV_ATTR_SET_ATTR_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_label`
  ADD CONSTRAINT `EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ATTRIBUTE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_option`
  ADD CONSTRAINT `EAV_ATTRIBUTE_OPTION_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_option_swatch`
  ADD CONSTRAINT `EAV_ATTRIBUTE_OPTION_SWATCH_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ATTR_OPT_SWATCH_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_option_value`
  ADD CONSTRAINT `EAV_ATTRIBUTE_OPTION_VALUE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ATTR_OPT_VAL_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_set`
  ADD CONSTRAINT `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity`
  ADD CONSTRAINT `EAV_ENTITY_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_attribute`
  ADD CONSTRAINT `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTT_ATTR_ATTR_GROUP_ID_EAV_ATTR_GROUP_ATTR_GROUP_ID` FOREIGN KEY (`attribute_group_id`) REFERENCES `eav_attribute_group` (`attribute_group_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_datetime`
  ADD CONSTRAINT `EAV_ENTITY_DATETIME_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTT_DTIME_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_decimal`
  ADD CONSTRAINT `EAV_ENTITY_DECIMAL_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_DECIMAL_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_int`
  ADD CONSTRAINT `EAV_ENTITY_INT_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_INT_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_store`
  ADD CONSTRAINT `EAV_ENTITY_STORE_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_text`
  ADD CONSTRAINT `EAV_ENTITY_TEXT_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_TEXT_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_varchar`
  ADD CONSTRAINT `EAV_ENTITY_VARCHAR_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_VARCHAR_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_form_element`
  ADD CONSTRAINT `EAV_FORM_ELEMENT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_FORM_ELEMENT_FIELDSET_ID_EAV_FORM_FIELDSET_FIELDSET_ID` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `EAV_FORM_ELEMENT_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE;

ALTER TABLE `eav_form_fieldset`
  ADD CONSTRAINT `EAV_FORM_FIELDSET_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE;

ALTER TABLE `eav_form_fieldset_label`
  ADD CONSTRAINT `EAV_FORM_FIELDSET_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_FORM_FSET_LBL_FSET_ID_EAV_FORM_FSET_FSET_ID` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE CASCADE;

ALTER TABLE `eav_form_type`
  ADD CONSTRAINT `EAV_FORM_TYPE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_form_type_entity`
  ADD CONSTRAINT `EAV_FORM_TYPE_ENTITY_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_FORM_TYPE_ENTT_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE;

ALTER TABLE `googleoptimizer_code`
  ADD CONSTRAINT `GOOGLEOPTIMIZER_CODE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `integration`
  ADD CONSTRAINT `INTEGRATION_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `layout_link`
  ADD CONSTRAINT `LAYOUT_LINK_LAYOUT_UPDATE_ID_LAYOUT_UPDATE_LAYOUT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `layout_update` (`layout_update_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `LAYOUT_LINK_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `LAYOUT_LINK_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE;

ALTER TABLE `newsletter_problem`
  ADD CONSTRAINT `NEWSLETTER_PROBLEM_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `NLTTR_PROBLEM_SUBSCRIBER_ID_NLTTR_SUBSCRIBER_SUBSCRIBER_ID` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE;

ALTER TABLE `newsletter_queue`
  ADD CONSTRAINT `NEWSLETTER_QUEUE_TEMPLATE_ID_NEWSLETTER_TEMPLATE_TEMPLATE_ID` FOREIGN KEY (`template_id`) REFERENCES `newsletter_template` (`template_id`) ON DELETE CASCADE;

ALTER TABLE `newsletter_queue_link`
  ADD CONSTRAINT `NEWSLETTER_QUEUE_LINK_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `NLTTR_QUEUE_LNK_SUBSCRIBER_ID_NLTTR_SUBSCRIBER_SUBSCRIBER_ID` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE;

ALTER TABLE `newsletter_queue_store_link`
  ADD CONSTRAINT `NEWSLETTER_QUEUE_STORE_LINK_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `NEWSLETTER_QUEUE_STORE_LINK_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `newsletter_subscriber`
  ADD CONSTRAINT `NEWSLETTER_SUBSCRIBER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `oauth_nonce`
  ADD CONSTRAINT `OAUTH_NONCE_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `oauth_token`
  ADD CONSTRAINT `OAUTH_TOKEN_ADMIN_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`admin_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `OAUTH_TOKEN_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `OAUTH_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `paypal_billing_agreement`
  ADD CONSTRAINT `PAYPAL_BILLING_AGREEMENT_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PAYPAL_BILLING_AGREEMENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `paypal_billing_agreement_order`
  ADD CONSTRAINT `PAYPAL_BILLING_AGREEMENT_ORDER_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PAYPAL_BILLING_AGRT_ORDER_AGRT_ID_PAYPAL_BILLING_AGRT_AGRT_ID` FOREIGN KEY (`agreement_id`) REFERENCES `paypal_billing_agreement` (`agreement_id`) ON DELETE CASCADE;

ALTER TABLE `paypal_cert`
  ADD CONSTRAINT `PAYPAL_CERT_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `paypal_settlement_report_row`
  ADD CONSTRAINT `FK_E183E488F593E0DE10C6EBFFEBAC9B55` FOREIGN KEY (`report_id`) REFERENCES `paypal_settlement_report` (`report_id`) ON DELETE CASCADE;

ALTER TABLE `persistent_session`
  ADD CONSTRAINT `PERSISTENT_SESSION_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PERSISTENT_SESSION_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `plazathemes_blog_category_store`
  ADD CONSTRAINT `FK_5CE7EA43DAADC8EAFEDD24CB3765DBCB` FOREIGN KEY (`category_id`) REFERENCES `plazathemes_blog_category` (`category_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PLAZATHEMES_BLOG_CATEGORY_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `plazathemes_blog_post_category`
  ADD CONSTRAINT `PLAZATHEMES_BLOG_POST_CTGR_CTGR_ID_PLAZATHEMES_BLOG_CTGR_CTGR_ID` FOREIGN KEY (`category_id`) REFERENCES `plazathemes_blog_category` (`category_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PLAZATHEMES_BLOG_POST_CTGR_POST_ID_PLAZATHEMES_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `plazathemes_blog_post` (`post_id`) ON DELETE CASCADE;

ALTER TABLE `plazathemes_blog_post_relatedpost`
  ADD CONSTRAINT `FK_9F4A4184A025A73B339BA1263D565D1E` FOREIGN KEY (`post_id`) REFERENCES `plazathemes_blog_post` (`post_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_D1DFCD681F1AE23DD14596C624890A62` FOREIGN KEY (`post_id`) REFERENCES `plazathemes_blog_post` (`post_id`) ON DELETE CASCADE;

ALTER TABLE `plazathemes_blog_post_relatedproduct`
  ADD CONSTRAINT `FK_F087337CF909C30B9C993D16115A82A6` FOREIGN KEY (`post_id`) REFERENCES `plazathemes_blog_post` (`post_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PLAZATHEMES_BLOG_POST_RELATEDPRD_RELATED_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`related_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `plazathemes_blog_post_store`
  ADD CONSTRAINT `FK_A8F0C30220F3C333078301EB9CD221CC` FOREIGN KEY (`post_id`) REFERENCES `plazathemes_blog_post` (`post_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PLAZATHEMES_BLOG_POST_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `product_alert_price`
  ADD CONSTRAINT `PRODUCT_ALERT_PRICE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PRODUCT_ALERT_PRICE_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PRODUCT_ALERT_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `product_alert_stock`
  ADD CONSTRAINT `PRODUCT_ALERT_STOCK_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PRODUCT_ALERT_STOCK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PRODUCT_ALERT_STOCK_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `quote`
  ADD CONSTRAINT `QUOTE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `quote_address`
  ADD CONSTRAINT `QUOTE_ADDRESS_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `quote_address_item`
  ADD CONSTRAINT `QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS_ID_QUOTE_ADDRESS_ADDRESS_ID` FOREIGN KEY (`quote_address_id`) REFERENCES `quote_address` (`address_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `QUOTE_ADDRESS_ITEM_QUOTE_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`quote_item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `QUOTE_ADDR_ITEM_PARENT_ITEM_ID_QUOTE_ADDR_ITEM_ADDR_ITEM_ID` FOREIGN KEY (`parent_item_id`) REFERENCES `quote_address_item` (`address_item_id`) ON DELETE CASCADE;

ALTER TABLE `quote_id_mask`
  ADD CONSTRAINT `QUOTE_ID_MASK_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `quote_item`
  ADD CONSTRAINT `QUOTE_ITEM_PARENT_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`parent_item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `QUOTE_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `QUOTE_ITEM_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `QUOTE_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `quote_item_option`
  ADD CONSTRAINT `QUOTE_ITEM_OPTION_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE;

ALTER TABLE `quote_payment`
  ADD CONSTRAINT `QUOTE_PAYMENT_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `quote_shipping_rate`
  ADD CONSTRAINT `QUOTE_SHIPPING_RATE_ADDRESS_ID_QUOTE_ADDRESS_ADDRESS_ID` FOREIGN KEY (`address_id`) REFERENCES `quote_address` (`address_id`) ON DELETE CASCADE;

ALTER TABLE `rating`
  ADD CONSTRAINT `RATING_ENTITY_ID_RATING_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `rating_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `rating_option`
  ADD CONSTRAINT `RATING_OPTION_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE;

ALTER TABLE `rating_option_vote`
  ADD CONSTRAINT `RATING_OPTION_VOTE_OPTION_ID_RATING_OPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `rating_option` (`option_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `RATING_OPTION_VOTE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE;

ALTER TABLE `rating_option_vote_aggregated`
  ADD CONSTRAINT `RATING_OPTION_VOTE_AGGREGATED_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `RATING_OPTION_VOTE_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `rating_store`
  ADD CONSTRAINT `RATING_STORE_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `RATING_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `rating_title`
  ADD CONSTRAINT `RATING_TITLE_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `RATING_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `report_compared_product_index`
  ADD CONSTRAINT `REPORT_CMPD_PRD_IDX_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_CMPD_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_COMPARED_PRODUCT_INDEX_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `report_event`
  ADD CONSTRAINT `REPORT_EVENT_EVENT_TYPE_ID_REPORT_EVENT_TYPES_EVENT_TYPE_ID` FOREIGN KEY (`event_type_id`) REFERENCES `report_event_types` (`event_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_EVENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `report_viewed_product_aggregated_daily`
  ADD CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_DAILY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `report_viewed_product_aggregated_monthly`
  ADD CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_MONTHLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `report_viewed_product_aggregated_yearly`
  ADD CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_YEARLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `report_viewed_product_index`
  ADD CONSTRAINT `REPORT_VIEWED_PRD_IDX_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_VIEWED_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_VIEWED_PRODUCT_INDEX_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `review`
  ADD CONSTRAINT `REVIEW_ENTITY_ID_REVIEW_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `review_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REVIEW_STATUS_ID_REVIEW_STATUS_STATUS_ID` FOREIGN KEY (`status_id`) REFERENCES `review_status` (`status_id`) ON DELETE NO ACTION;

ALTER TABLE `review_detail`
  ADD CONSTRAINT `REVIEW_DETAIL_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `REVIEW_DETAIL_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REVIEW_DETAIL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `review_entity_summary`
  ADD CONSTRAINT `REVIEW_ENTITY_SUMMARY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `review_store`
  ADD CONSTRAINT `REVIEW_STORE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REVIEW_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `sales_bestsellers_aggregated_daily`
  ADD CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `sales_bestsellers_aggregated_monthly`
  ADD CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `sales_bestsellers_aggregated_yearly`
  ADD CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `sales_creditmemo`
  ADD CONSTRAINT `SALES_CREDITMEMO_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_CREDITMEMO_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_creditmemo_comment`
  ADD CONSTRAINT `SALES_CREDITMEMO_COMMENT_PARENT_ID_SALES_CREDITMEMO_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_creditmemo` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_creditmemo_item`
  ADD CONSTRAINT `SALES_CREDITMEMO_ITEM_PARENT_ID_SALES_CREDITMEMO_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_creditmemo` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_invoice`
  ADD CONSTRAINT `SALES_INVOICE_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_INVOICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_invoice_comment`
  ADD CONSTRAINT `SALES_INVOICE_COMMENT_PARENT_ID_SALES_INVOICE_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_invoice` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_invoice_item`
  ADD CONSTRAINT `SALES_INVOICE_ITEM_PARENT_ID_SALES_INVOICE_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_invoice` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_invoiced_aggregated`
  ADD CONSTRAINT `SALES_INVOICED_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_invoiced_aggregated_order`
  ADD CONSTRAINT `SALES_INVOICED_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_order`
  ADD CONSTRAINT `SALES_ORDER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `SALES_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_order_address`
  ADD CONSTRAINT `SALES_ORDER_ADDRESS_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_order_aggregated_created`
  ADD CONSTRAINT `SALES_ORDER_AGGREGATED_CREATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_order_aggregated_updated`
  ADD CONSTRAINT `SALES_ORDER_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_order_item`
  ADD CONSTRAINT `SALES_ORDER_ITEM_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_order_payment`
  ADD CONSTRAINT `SALES_ORDER_PAYMENT_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_order_status_history`
  ADD CONSTRAINT `SALES_ORDER_STATUS_HISTORY_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_order_status_label`
  ADD CONSTRAINT `SALES_ORDER_STATUS_LABEL_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_STATUS_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `sales_order_status_state`
  ADD CONSTRAINT `SALES_ORDER_STATUS_STATE_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE;

ALTER TABLE `sales_order_tax_item`
  ADD CONSTRAINT `SALES_ORDER_TAX_ITEM_ASSOCIATED_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`associated_item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_TAX_ITEM_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_TAX_ITEM_TAX_ID_SALES_ORDER_TAX_TAX_ID` FOREIGN KEY (`tax_id`) REFERENCES `sales_order_tax` (`tax_id`) ON DELETE CASCADE;

ALTER TABLE `sales_payment_transaction`
  ADD CONSTRAINT `FK_B99FF1A06402D725EBDB0F3A7ECD47A2` FOREIGN KEY (`parent_id`) REFERENCES `sales_payment_transaction` (`transaction_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_PAYMENT_TRANSACTION_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_PAYMENT_TRANSACTION_PAYMENT_ID_SALES_ORDER_PAYMENT_ENTT_ID` FOREIGN KEY (`payment_id`) REFERENCES `sales_order_payment` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_refunded_aggregated`
  ADD CONSTRAINT `SALES_REFUNDED_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_refunded_aggregated_order`
  ADD CONSTRAINT `SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_sequence_profile`
  ADD CONSTRAINT `SALES_SEQUENCE_PROFILE_META_ID_SALES_SEQUENCE_META_META_ID` FOREIGN KEY (`meta_id`) REFERENCES `sales_sequence_meta` (`meta_id`) ON DELETE CASCADE;

ALTER TABLE `sales_shipment`
  ADD CONSTRAINT `SALES_SHIPMENT_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_SHIPMENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_shipment_comment`
  ADD CONSTRAINT `SALES_SHIPMENT_COMMENT_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_shipment_item`
  ADD CONSTRAINT `SALES_SHIPMENT_ITEM_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_shipment_track`
  ADD CONSTRAINT `SALES_SHIPMENT_TRACK_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_shipping_aggregated`
  ADD CONSTRAINT `SALES_SHIPPING_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_shipping_aggregated_order`
  ADD CONSTRAINT `SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `salesrule_coupon`
  ADD CONSTRAINT `SALESRULE_COUPON_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_coupon_aggregated`
  ADD CONSTRAINT `SALESRULE_COUPON_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_coupon_aggregated_order`
  ADD CONSTRAINT `SALESRULE_COUPON_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_coupon_aggregated_updated`
  ADD CONSTRAINT `SALESRULE_COUPON_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_coupon_usage`
  ADD CONSTRAINT `SALESRULE_COUPON_USAGE_COUPON_ID_SALESRULE_COUPON_COUPON_ID` FOREIGN KEY (`coupon_id`) REFERENCES `salesrule_coupon` (`coupon_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_COUPON_USAGE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_customer`
  ADD CONSTRAINT `SALESRULE_CUSTOMER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_CUSTOMER_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_customer_group`
  ADD CONSTRAINT `SALESRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_CUSTOMER_GROUP_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_label`
  ADD CONSTRAINT `SALESRULE_LABEL_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_product_attribute`
  ADD CONSTRAINT `SALESRULE_PRD_ATTR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_PRD_ATTR_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_PRODUCT_ATTRIBUTE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_website`
  ADD CONSTRAINT `SALESRULE_WEBSITE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `search_query`
  ADD CONSTRAINT `SEARCH_QUERY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `search_synonyms`
  ADD CONSTRAINT `SEARCH_SYNONYMS_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SEARCH_SYNONYMS_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `sitemap`
  ADD CONSTRAINT `SITEMAP_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `store`
  ADD CONSTRAINT `STORE_GROUP_ID_STORE_GROUP_GROUP_ID` FOREIGN KEY (`group_id`) REFERENCES `store_group` (`group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `STORE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `store_group`
  ADD CONSTRAINT `STORE_GROUP_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `tax_calculation`
  ADD CONSTRAINT `TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`customer_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALCULATION_PRODUCT_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`product_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALC_TAX_CALC_RATE_ID_TAX_CALC_RATE_TAX_CALC_RATE_ID` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALC_TAX_CALC_RULE_ID_TAX_CALC_RULE_TAX_CALC_RULE_ID` FOREIGN KEY (`tax_calculation_rule_id`) REFERENCES `tax_calculation_rule` (`tax_calculation_rule_id`) ON DELETE CASCADE;

ALTER TABLE `tax_calculation_rate_title`
  ADD CONSTRAINT `FK_37FB965F786AD5897BB3AE90470C42AB` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALCULATION_RATE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `tax_order_aggregated_created`
  ADD CONSTRAINT `TAX_ORDER_AGGREGATED_CREATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `tax_order_aggregated_updated`
  ADD CONSTRAINT `TAX_ORDER_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `theme_file`
  ADD CONSTRAINT `THEME_FILE_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE;

ALTER TABLE `translation`
  ADD CONSTRAINT `TRANSLATION_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `ui_bookmark`
  ADD CONSTRAINT `UI_BOOKMARK_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE;

ALTER TABLE `variable_value`
  ADD CONSTRAINT `VARIABLE_VALUE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `VARIABLE_VALUE_VARIABLE_ID_VARIABLE_VARIABLE_ID` FOREIGN KEY (`variable_id`) REFERENCES `variable` (`variable_id`) ON DELETE CASCADE;

ALTER TABLE `vault_payment_token`
  ADD CONSTRAINT `VAULT_PAYMENT_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `vault_payment_token_order_payment_link`
  ADD CONSTRAINT `FK_4ED894655446D385894580BECA993862` FOREIGN KEY (`payment_token_id`) REFERENCES `vault_payment_token` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_CF37B9D854256534BE23C818F6291CA2` FOREIGN KEY (`order_payment_id`) REFERENCES `sales_order_payment` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `weee_tax`
  ADD CONSTRAINT `WEEE_TAX_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WEEE_TAX_COUNTRY_DIRECTORY_COUNTRY_COUNTRY_ID` FOREIGN KEY (`country`) REFERENCES `directory_country` (`country_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WEEE_TAX_ENTITY_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WEEE_TAX_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `widget_instance`
  ADD CONSTRAINT `WIDGET_INSTANCE_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE;

ALTER TABLE `widget_instance_page`
  ADD CONSTRAINT `WIDGET_INSTANCE_PAGE_INSTANCE_ID_WIDGET_INSTANCE_INSTANCE_ID` FOREIGN KEY (`instance_id`) REFERENCES `widget_instance` (`instance_id`) ON DELETE CASCADE;

ALTER TABLE `widget_instance_page_layout`
  ADD CONSTRAINT `WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID_WIDGET_INSTANCE_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `widget_instance_page` (`page_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WIDGET_INSTANCE_PAGE_LYT_LYT_UPDATE_ID_LYT_UPDATE_LYT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `layout_update` (`layout_update_id`) ON DELETE CASCADE;

ALTER TABLE `wishlist`
  ADD CONSTRAINT `WISHLIST_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `wishlist_item`
  ADD CONSTRAINT `WISHLIST_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WISHLIST_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `WISHLIST_ITEM_WISHLIST_ID_WISHLIST_WISHLIST_ID` FOREIGN KEY (`wishlist_id`) REFERENCES `wishlist` (`wishlist_id`) ON DELETE CASCADE;

ALTER TABLE `wishlist_item_option`
  ADD CONSTRAINT `FK_A014B30B04B72DD0EAB3EECD779728D6` FOREIGN KEY (`wishlist_item_id`) REFERENCES `wishlist_item` (`wishlist_item_id`) ON DELETE CASCADE;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */; 
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-29 01:22:48 GMT